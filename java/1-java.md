1、Java SE

## 1.1、Java基础 !

基础概念/语法：面向对象（继承、封装、多态）基础、包、类、接口、方法、对象、属性、第一个 Java 程序。

数据类型：1）基本数据类型8种：byte、short、int、long、float、double、char、boolean；2）引用数据类型

变量类型：局部变量、实例变量（成员变量）、类变量（静态变量）

修饰符：public、private、不写(default)、protected、static、final、abstract (抽象的)、synchronized (同步的) ，volatile (易变的)

运算符：1）算术运算符：+、-、*、/、%、++、--；2）关系运算符：==、!=、>、<、>=、<=；3）逻辑运算符：&&、||、!；4）赋值运算符：=、+=、-=、*=、/=；5）条件运算符：Object x = (expression) ? value if true : value if false；

循环结构：for 循环、while 循环、do...while 循环

条件语句：if...else、if...else if...else

异常处理：try...catch...finally、throws、throw

字符串：String、StringBuilder、StringBuffer

其他：switch case、数组、日期时间、枚举(enum)、使用 IDE 进行 DEBUG

## 1.2、集合 !

Map：HashMap（最重要）、ConcurrentHashMap、TreeMap、Hashtable

List：ArrayList（最重要）、LinkedList

Set：HashSet（最重要）、TreeSet


## 1.3、多线程 !

线程生命周期

创建线程的三种方式：继承 Thread、实现 Runnable；实现 Callable

ThreadPoolExecutor（线程池）

锁：synchronized 和 Lock


## 1.4、I/O流

## 1.5、网络编程

## 1.6、反射

## 1.7、泛型

## 1.8、注解


















