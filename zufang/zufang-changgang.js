var ZUFANG_2 = {
    "code": "0",
    "message": "OK",
    "data": {
      "isToast": false,
      "activity": {
        "title": ""
      },
      "configs": {
        "banner": [],
        "houseSubject": []
      },
      "tkd": {
        "title": "8号线昌岗整租0_2000租房网，广州8号线昌岗整租0_2000租房信息，8号线昌岗整租0_2000房屋出租价格-广州58安居客",
        "keywords": "8号线昌岗整租0_2000租房，8号线昌岗整租0_2000出租房，8号线昌岗整租0_2000租房子",
        "description": "安居客广州租房网为您提供8号线昌岗整租0_2000租房信息,包括广州二手房出租、新房出租，租房子,找广州出租房,就上58安居客广州租房网。"
      },
      "gongyu": {
        "isShow": true
      },
      "goldOwnerCity": false,
      "list": [
        {
          "infoid": "1876319597142027",
          "cateName": "整租",
          "cateID": "11",
          "title": "昌岗十佳房源  整租  独立阳台厨卫1000元",
          "price": "1000",
          "img": "https://pic1.ajkimg.com/display/anjuke/956aba88371ca5a5aacdc1de087423a1/240x180c.jpg?t=1&srotate=1",
          "unit": "元/月",
          "coll": "0",
          "subwayDistance": "距8号线-昌岗站步行136m",
          "fangwuLiangDian": [
            {
              "text": "整租",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "南北",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "近地铁",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "有电梯",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "精装修",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            }
          ],
          "area": "23㎡",
          "toward": "南北",
          "shangquan": "昌岗",
          "isAnxuan": "0",
          "location": false,
          "source_type": "1",
          "isauction": "6",
          "auctionShowText": "广告",
          "legoAuction": "%7B%22isauction%22%3A%226%22%2C%22lego%22%3A%7B%22lego_tid%22%3A%2277f8e089-f2cb-4027-8627-606b792cd6d7%22%2C%22lego_ad_click_url%22%3A%22https%3A%5C%2F%5C%2Flegoclick.58.com%5C%2Fjump%3Ftarget%3DpZwY0ZnlsztdraOWUvYKuadbmyD3uHcvnBYQP10OsHwBuyNVmymLuBYOmvE3rH7-uhckm1NKnWTkPWTknHczTEDQrj0vn1DOPHbLnHEznjcLTHTKnHTkTHD_nHDKnkDQPWcYnWmdnjmOrjELTHDKOplFOUA7OplBOlXxOCB4OUa5OlBsOUJ5sXi38Syc-SB6JrpCCXX1B8pEMrXYC9DQTHDKTHnKUvPHPLDkHD-ogLIvibPOINIimYIaI1IgXNPdwEDQPjTvP1bYP1bQP19dPWTLnH0kTHmK0A7zmyd1n1NYrWcdP1m_ph-6wvNln7tznjTksZK60h7V01ndn1C1P9DQTyc3PH-hnWwWsHEvnynVPAmOrid6nWRBsH6hPjcQPANQuynYPTDQPjTvP1bYP1bQrHnvPWTznHDzTHDYnjmLrHELrHDLrHnOrHDvrjTKTHD_nHDKTEDKsEDKTEDKPH98nWE3sWmvsWDvnkDkTHTKTHD_nHDKXLYKnHTkPHEKnHTknWndrEDLPvm3uHT3ridhnhPBsHEknW0VrjmzPzYvnjuBP1bzmvEvuj0KTHEKIL9kuHD1mhNYmHN1n1m3PjmvTy7JpdI-mv66ITD1THn_nHmdPzkvn19dTHDKUMR_UT7-Ph7BujTYnjmYPHwbnW91%22%2C%22lego_sid%22%3A%22141278743212895671227905755%22%2C%22platformCompany%22%3A%22ajk%22%2C%22lego_show_track_url_base%22%3A%22https%3A%5C%2F%5C%2Fadtrack.58.com%5C%2F1002359%5C%2F%22%2C%22lego_version%22%3A%221.0.1%22%2C%22legoHuiDu%22%3Atrue%7D%7D",
          "tid": "77f8e089-f2cb-4027-8627-606b792cd6d7",
          "isLiving": false,
          "isVR": false,
          "distanceDesc": "",
          "isGoldOwner": false
        },
        {
          "infoid": "2040573610418184",
          "cateName": "整租",
          "cateID": "11",
          "title": "房 东 急 租   全新家电家私 电梯精装修",
          "price": "1000",
          "img": "https://pic1.ajkimg.com/display/anjuke/d4b49e0b423d63446a2c9352e99aebeb/240x180c.jpg?t=1&srotate=1",
          "unit": "元/月",
          "coll": "0",
          "subwayDistance": "距8号线-昌岗站步行136m",
          "fangwuLiangDian": [
            {
              "text": "整租",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "南北",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "近地铁",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "有电梯",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "精装修",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            }
          ],
          "area": "22㎡",
          "toward": "南北",
          "shangquan": "昌岗",
          "isAnxuan": "0",
          "location": false,
          "source_type": "1",
          "isauction": "6",
          "auctionShowText": "",
          "legoAuction": "%7B%22isauction%22%3A%226%22%2C%22lego%22%3A%7B%22lego_tid%22%3A%2285f5c19d-ea02-4517-a8fc-65b30096eb64%22%2C%22lego_ad_click_url%22%3A%22https%3A%5C%2F%5C%2Flegoclick.58.com%5C%2Fjump%3Ftarget%3DpZwY0ZnlsztdraOWUvYKuadbmyD3uHcvnBYQP10OsHwBuyNVmymLuBYOmvE3rH7-uhckm1NKnWTkPWTknHczTEDznjEkPH01PWDkPjD3nH9YTHTKnHTkTHD_nHDKnkDQPWcYnWmdnjmOrjELTHcKOplFOUA7OplBOlXxOCB4OUa5OlBsOUJ5sXi38Syc-SB6JrpCCXX1B8pEMrXYC9DQTHDKTHnKUvPHPLDkHD-ogLIvibPOINIimYIaI1IgXNPdwEDQPjTvrjEvn1mkPHmYPHnOn1b1THmK0A7zmyd1n1NYrWcdP1m_ph-6wvNln7tznjTksZK60h7V01ndn1C1P9DQTHckPWNQmyu6sHT1uW0VPjPbPid6nANQsyE1uhN1PhcOmHDOrEDQPjTvrjEvn1mkP1DdPHnYn1nvTHDYnjm3Pjm1PWTvn1DvPj9zPHmKTHD_nHDKTEDKsEDKTEDKPH98nWE3sWmvsWDvnkDkTHTKTHD_nHDKXLYKnHTkPHEKnHTknWndrED3Pymdm1DOuad-mHTzsHEdnH0VmH6hmzYvPyc1njTOPhRBPWEKTHEKIL9kuHD1mhNYmHN1n1m3PjmvTy7JpdI-mv66ITD1THn_nHmdPzkvn19dTHDKUMR_UT7WPyDkn19duAcLmWmdPhcO%22%2C%22lego_sid%22%3A%22141278743212895671227905755%22%2C%22platformCompany%22%3A%22ajk%22%2C%22lego_show_track_url_base%22%3A%22https%3A%5C%2F%5C%2Fadtrack.58.com%5C%2F1002359%5C%2F%22%2C%22lego_version%22%3A%221.0.1%22%2C%22legoHuiDu%22%3Atrue%7D%7D",
          "tid": "85f5c19d-ea02-4517-a8fc-65b30096eb64",
          "isLiving": false,
          "isVR": false,
          "distanceDesc": "",
          "isGoldOwner": false
        },
        {
          "infoid": "2009208600353805",
          "cateName": "整租",
          "cateID": "11",
          "title": "地铁二号线 海珠区泰沙路五凤金紫里中大对面 落地窗新房出租",
          "price": "1500",
          "img": "https://pic1.ajkimg.com/display/anjuke/048a2222c4ceb32ef940d9817d42a048/240x180c.jpg?t=1&srotate=1",
          "unit": "元/月",
          "coll": "0",
          "subwayDistance": "距8号线-昌岗站步行1.5km",
          "fangwuLiangDian": [
            {
              "text": "整租",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "朝南",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "近地铁",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "精装修",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            }
          ],
          "area": "33㎡",
          "toward": "朝南",
          "shangquan": "东晓南",
          "isAnxuan": "0",
          "location": false,
          "source_type": "1",
          "isauction": "6",
          "auctionShowText": "",
          "legoAuction": "%7B%22isauction%22%3A%226%22%2C%22lego%22%3A%7B%22lego_tid%22%3A%22aa309649-ce7e-4f95-97c4-169866b48ac7%22%2C%22lego_ad_click_url%22%3A%22https%3A%5C%2F%5C%2Flegoclick.58.com%5C%2Fjump%3Ftarget%3DpZwY0ZnlsztdraOWUvYKuadbmyD3uHcvnBYQP10OsHwBuyNVmymLuBYOmvE3rH7-uhckm1NKnWTkrHTYnjmzTEDznjTOnWT3PWTkn1N1rjTdTHTKnHTkTHD_nHDKnkDQPWcYnWmdnjmOrjE3THnKOplFOUA7OplBOlXxOCB4OUa5OlBsOUJ5sXi38Syc-SB6JrpCCXX1B8pEMrXYC9DQTHDKTHnKUvPHPLDkHD-ogLIvibPOINIimYIaI1IgXNPdwEDQPjTvrj9Qn1n3Pjnknj0Yrj9kTHmK0A7zmyd1n1NYrWcdP1m_ph-6wvNln7tznjTksZK60h7V01ndn1C1P9DQTym1PjK6rycksynzuWmVPjmduiY3uAm1sHN1nhRbPWPWPWcknkDQPjTvrj9Qn1n3PHbLrjELnjEkTHDYnjm3rjD1n19Yn1TkP1TLrjEKTHD_nHDKTEDKsEDKTEDKPH98nWE3sWmvsWDvnkDkTHTKTHD_nHDKXLYKnHTkPHEKnHTknWndrE76mHnkrHmYridWuHI-sHwhrHNVrHIWPaYQPWb3PWuBPj66m10KTHEKIL9kuHD1mhNYmHN1n1m3PjmvTy7JpdI-mv66ITD1THn_nHmdPzkQnWm3PEDQTyOdUAkKnWTYuHNznjDOP10znWb1uE%22%2C%22lego_sid%22%3A%22141278743212895671227905755%22%2C%22platformCompany%22%3A%22ajk%22%2C%22lego_show_track_url_base%22%3A%22https%3A%5C%2F%5C%2Fadtrack.58.com%5C%2F1002359%5C%2F%22%2C%22lego_version%22%3A%221.0.1%22%2C%22legoHuiDu%22%3Atrue%7D%7D",
          "tid": "aa309649-ce7e-4f95-97c4-169866b48ac7",
          "isLiving": false,
          "isVR": false,
          "distanceDesc": "",
          "isGoldOwner": false
        },
        {
          "infoid": "2012129728880643",
          "cateName": "整租",
          "cateID": "11",
          "title": "海珠区泰沙路金紫里泰宁怡居中大布匹对面 新房出租可放电动车",
          "price": "1500",
          "img": "https://pic1.ajkimg.com/display/anjuke/fae7691de996e10a9aa22ea82550f98d/240x180c.jpg?t=1&srotate=1",
          "unit": "元/月",
          "coll": "0",
          "subwayDistance": "距8号线-昌岗站步行1.5km",
          "fangwuLiangDian": [
            {
              "text": "整租",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "朝南",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "近地铁",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "精装修",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            }
          ],
          "area": "33㎡",
          "toward": "朝南",
          "shangquan": "东晓南",
          "isAnxuan": "0",
          "location": false,
          "source_type": "1",
          "isauction": "6",
          "auctionShowText": "",
          "legoAuction": "%7B%22isauction%22%3A%226%22%2C%22lego%22%3A%7B%22lego_tid%22%3A%22f2781821-e0ef-4f65-88ad-69c9af69bd12%22%2C%22lego_ad_click_url%22%3A%22https%3A%5C%2F%5C%2Flegoclick.58.com%5C%2Fjump%3Ftarget%3DpZwY0ZnlsztdraOWUvYKuadbmyD3uHcvnBYQP10OsHwBuyNVmymLuBYOmvE3rH7-uhckm1NKnWTkrHTYnjmzTEDznjDznHcOP1c3rj9kPWE1THTKnHTkTHD_nHDKnkDQPWcYnWmdnjmOrjE3THEKOplFOUA7OplBOlXxOCB4OUa5OlBsOUJ5sXi38Syc-SB6JrpCCXX1B8pEMrXYC9DQTHDKTHnKUvPHPLDkHD-ogLIvibPOINIimYIaI1IgXNPdwEDQPjTvrj9QnWDkP19OnjDdPHNzTHmK0A7zmyd1n1NYrWcdP1m_ph-6wvNln7tznjTksZK60h7V01ndn1C1P9DQTHnOPWmYuAcksyuBP1NVPj9knad6Pj-hsHT1Pj7WmW01rj76mkDQPjTvrj9QnWDkrHnQPWcQrjbQTHDYnjm3rjDznHTLrH0YnjTkPWEKTHD_nHDKTEDKsEDKTEDKPH98nWE3sWmvsWDvnkDkTHTKTHD_nHDKXLYKnHTkPHEKnHTknWndrE7hnW03nH9znid-nARhsHwhPWNVrj66uaYvrynOmymvryFbnHcKTHEKIL9kuHD1mhNYmHN1n1m3PjmvTy7JpdI-mv66ITD1THn_nHmdPzkQnWm3PEDQTyOdUAkKm19vPH0Ymh7WnvcYuhNQnE%22%2C%22lego_sid%22%3A%22141278743212895671227905755%22%2C%22platformCompany%22%3A%22ajk%22%2C%22lego_show_track_url_base%22%3A%22https%3A%5C%2F%5C%2Fadtrack.58.com%5C%2F1002359%5C%2F%22%2C%22lego_version%22%3A%221.0.1%22%2C%22legoHuiDu%22%3Atrue%7D%7D",
          "tid": "f2781821-e0ef-4f65-88ad-69c9af69bd12",
          "isLiving": false,
          "isVR": false,
          "distanceDesc": "",
          "isGoldOwner": false
        },
        {
          "infoid": "2012135352966158",
          "cateName": "整租",
          "cateID": "11",
          "title": "地 铁 房 中大对面布匹市场 可停放电动车 新房出租拎包入住",
          "price": "1500",
          "img": "https://pic1.ajkimg.com/display/anjuke/3fec70b089138f636ed2320c1d469e4e/240x180c.jpg?t=1&srotate=1",
          "unit": "元/月",
          "coll": "0",
          "subwayDistance": "距8号线-昌岗站步行1.2km",
          "fangwuLiangDian": [
            {
              "text": "整租",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "朝南",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "近地铁",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "精装修",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            }
          ],
          "area": "28㎡",
          "toward": "朝南",
          "shangquan": "江南西",
          "isAnxuan": "0",
          "location": false,
          "source_type": "1",
          "isauction": "6",
          "auctionShowText": "",
          "legoAuction": "%7B%22isauction%22%3A%226%22%2C%22lego%22%3A%7B%22lego_tid%22%3A%22132ba9db-6ff7-482a-beba-20e40dd529de%22%2C%22lego_ad_click_url%22%3A%22https%3A%5C%2F%5C%2Flegoclick.58.com%5C%2Fjump%3Ftarget%3DpZwY0ZnlsztdraOWUvYKuadbmyD3uHcvnBYQP10OsHwBuyNVmymLuBYOmvE3rH7-uhckm1NKnWTkrHTYnjmzTEDznjDznHndn1NzrHmvnHN3THTKnHTkTHD_nHDKnkDQPWcYnWmdnjmOrjE3THNKOplFOUA7OplBOlXxOCB4OUa5OlBsOUJ5sXi38Syc-SB6JrpCCXX1B8pEMrXYC9DQTHDKTHnKUvPHPLDkHD-ogLIvibPOINIimYIaI1IgXNPdwEDQPjTvrj9QnHcknWn1rHbznHbzTHmK0A7zmyd1n1NYrWcdP1m_ph-6wvNln7tznjTksZK60h7V01ndn1C1P9DQTHNLn16-njbksHNOuW9VPjEkPBd6rjKbsyEzrjTQuhw6PycYuEDQPjTvrj9QnHckPjTdrHN3PWNLTHDYnjm3rjDQnWTzn1nOrj9krHmKTHD_nHDKTEDKsEDKTEDKPH98nWE3sWmvsWDvnkDkTHTKTHD_nHDKXLYKnHTkPHEKnHTknWndrEDQn1FBmH-bmBYvuhmLsHE3nhDVmhRBmiYznANYnAwbPHcOuANKTHEKIL9kuHD1mhNYmHN1n1m3PjmvTy7JpdI-mv66ITD1THn_nHmdPzkLrH0OTHDKUMR_UT7-PHmQPyRhn17BuhF6m1TY%22%2C%22lego_sid%22%3A%22141278743212895671227905755%22%2C%22platformCompany%22%3A%22ajk%22%2C%22lego_show_track_url_base%22%3A%22https%3A%5C%2F%5C%2Fadtrack.58.com%5C%2F1002359%5C%2F%22%2C%22lego_version%22%3A%221.0.1%22%2C%22legoHuiDu%22%3Atrue%7D%7D",
          "tid": "132ba9db-6ff7-482a-beba-20e40dd529de",
          "isLiving": false,
          "isVR": false,
          "distanceDesc": "",
          "isGoldOwner": false
        },
        {
          "infoid": "2007768393933826",
          "cateName": "整租",
          "cateID": "11",
          "title": "地铁8号线石井站 电梯房一房一厅 配置齐全 有阳台厨卫",
          "price": "1000",
          "img": "https://pic1.ajkimg.com/display/anjuke/0c562383bec723b2dc5930af7f64858b/240x180c.jpg?t=1&srotate=1",
          "unit": "元/月",
          "coll": "0",
          "subwayDistance": "距8号线-昌岗站步行1.1km",
          "fangwuLiangDian": [
            {
              "text": "整租",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "朝南",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "近地铁",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "有电梯",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "精装修",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            }
          ],
          "area": "36㎡",
          "toward": "朝南",
          "shangquan": "江南西",
          "isAnxuan": "0",
          "location": false,
          "source_type": "1",
          "isauction": "6",
          "auctionShowText": "",
          "legoAuction": "%7B%22isauction%22%3A%226%22%2C%22lego%22%3A%7B%22lego_tid%22%3A%2238465893-3e83-4f9d-94f7-096279766913%22%2C%22lego_ad_click_url%22%3A%22https%3A%5C%2F%5C%2Flegoclick.58.com%5C%2Fjump%3Ftarget%3DpZwY0ZnlsztdraOWUvYKuadbmyD3uHcvnBYQP10OsHwBuyNVmymLuBYOmvE3rH7-uhckm1NKnWTkPHb3PWnLTEDznjTLP1m3n1b1rHn1rjcvTHTKnHTkTHD_nHDKnkDQPWcYnWmdnjmOrjE3THmKOplFOUA7OplBOlXxOCB4OUa5OlBsOUJ5sXi38Syc-SB6JrpCCXX1B8pEMrXYC9DQTHDKTHnKUvPHPLDkHD-ogLIvibPOINIimYIaI1IgXNPdwEDQPjTvPHTLPjcOnjN3rjEvP1ckTHmK0A7zmyd1n1NYrWcdP1m_ph-6wvNln7tznjTksZK60h7V01ndn1C1P9DQTHbduHNzmH7hsymYuyDVPjDzuadBryD3symQuhmznhRWmhczPEDQPjTvPHTLPjcOnWTdPWELn1mkTHDYnjmdnj0YnWbQnWNOPHNdrjEKTHD_nHDKTEDKsEDKTEDKPH98nWE3sWmvsWDvnkDkTHTKTHD_nHDKXLYKnHTkPHEKnHTknWndrED1rjEvPH9OnzY1uH91sHwhryEVrHwhPzYkrHmzP1bLPWmOnHnKTHEKIL9kuHD1mhNYmHN1n1m3PjmvTy7JpdI-mv66ITD1THn_nHmdPzkLrH0OTHDKUMR_UT7hrHmznHKBPHDkrHwbmvN3%22%2C%22lego_sid%22%3A%22141278743212895671227905755%22%2C%22platformCompany%22%3A%22ajk%22%2C%22lego_show_track_url_base%22%3A%22https%3A%5C%2F%5C%2Fadtrack.58.com%5C%2F1002359%5C%2F%22%2C%22lego_version%22%3A%221.0.1%22%2C%22legoHuiDu%22%3Atrue%7D%7D",
          "tid": "38465893-3e83-4f9d-94f7-096279766913",
          "isLiving": false,
          "isVR": false,
          "distanceDesc": "",
          "isGoldOwner": false
        },
        {
          "infoid": "1995212605283337",
          "cateName": "整租",
          "cateID": "11",
          "title": "望广州塔 中大 赤岗 客村 酒店公寓 全新装修",
          "price": "1980",
          "img": "https://pic1.ajkimg.com/display/anjuke/0db315d4887f9617ce113857d5d0559e/240x180c.jpg?t=1&srotate=1",
          "unit": "元/月",
          "coll": "0",
          "subwayDistance": "距8号线-昌岗站步行1.9km",
          "fangwuLiangDian": [
            {
              "text": "整租",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "朝南",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "近地铁",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "有电梯",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "精装修",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            }
          ],
          "area": "50㎡",
          "toward": "朝南",
          "shangquan": "新港西",
          "isAnxuan": "0",
          "location": false,
          "source_type": "1",
          "isauction": "6",
          "auctionShowText": "",
          "legoAuction": "%7B%22isauction%22%3A%226%22%2C%22lego%22%3A%7B%22lego_tid%22%3A%224ee7d94c-5a09-40f7-a737-b5379fde4dea%22%2C%22lego_ad_click_url%22%3A%22https%3A%5C%2F%5C%2Flegoclick.58.com%5C%2Fjump%3Ftarget%3DpZwY0ZnlsztdraOWUvYKuadbmyD3uHcvnBYQP10OsHwBuyNVmymLuBYOmvE3rH7-uhckm1NKnHTkPHD3nWNOTEDQrHbdnWDzPWTdnW91n1nLTHTKnHTkTHD_nHDKnkDQPWcYnWmdnjmOrjE3TH0KOplFOUA7OplBOlXxOCB4OUa5OlBsOUJ5sXi38Syc-SB6JrpCCXX1B8pEMrXYC9DQTHDKTHnKUvPHPLDkHD-ogLIvibPOINIimYIaI1IgXNPdwEDQPjTvP1bkn1DQPH0vrj0krHDYTHmK0A7zmyd1n1NYrWcdP1m_ph-6wvNln7tznjTksZK60h7V01ndn1C1P9DQTHDzP1N1uH76sHbdnhnVPjcQmzYOuW6bsHbYrARbnjNOmHwBnEDQPjTvP1bkn1DznWczP1b1P1c3THDYnjmLrHT1nHcznHEYnjbznHmKTHD_nHDKTEDKsEDKTEDKPH98nWE3sWmvsWDvnkDkTHTKTHD_nHDKXLYKnHTkPHEKnHTknWndrEDYuyNLujbYmzYdmHTOsHEkuW0VmH01PzdBPHnLryubuHwbuyDKTHEKIL9kuHD1mhNYmHN1n1m3PjmvTy7JpdI-mv66ITD1THn_nHmdPzkLrH0LTHDKUMR_UTDLmHb3P1bQnWNQP1nvmWmv%22%2C%22lego_sid%22%3A%22141278743212895671227905755%22%2C%22platformCompany%22%3A%22ajk%22%2C%22lego_show_track_url_base%22%3A%22https%3A%5C%2F%5C%2Fadtrack.58.com%5C%2F1002359%5C%2F%22%2C%22lego_version%22%3A%221.0.1%22%2C%22legoHuiDu%22%3Atrue%7D%7D",
          "tid": "4ee7d94c-5a09-40f7-a737-b5379fde4dea",
          "isLiving": false,
          "isVR": false,
          "distanceDesc": "",
          "isGoldOwner": false
        },
        {
          "infoid": "1728844896869390",
          "cateName": "整租",
          "cateID": "11",
          "title": "江泰路 泰沙路 泰宁怡居 家电齐全拎包入住 采光好电梯公寓",
          "price": "1800",
          "img": "https://pic1.ajkimg.com/display/anjuke/7681151bc3c559beceb0b911acd3abf7/240x180c.jpg?t=1&srotate=1",
          "unit": "元/月",
          "coll": "0",
          "subwayDistance": "距8号线-昌岗站步行1.5km",
          "fangwuLiangDian": [
            {
              "text": "整租",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "朝南",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "近地铁",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "有电梯",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "精装修",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            }
          ],
          "area": "33㎡",
          "toward": "朝南",
          "shangquan": "东晓南",
          "isAnxuan": "0",
          "location": false,
          "source_type": "1",
          "isauction": "6",
          "auctionShowText": "",
          "legoAuction": "%7B%22isauction%22%3A%226%22%2C%22lego%22%3A%7B%22lego_tid%22%3A%2264a62c36-cb9e-42be-b39b-414c711775c2%22%2C%22lego_ad_click_url%22%3A%22https%3A%5C%2F%5C%2Flegoclick.58.com%5C%2Fjump%3Ftarget%3DpZwY0ZnlsztdraOWUvYKuadbmyD3uHcvnBYQP10OsHwBuyNVmymLuBYOmvE3rH7-uhckm1NKnWTknjDOrjDOTEDQP1c3rjEYrjbvrjmOn1bkTHTKnHTkTHD_nHDKnkDQPWcYnWmdnjmOrjE3TH9KOplFOUA7OplBOlXxOCB4OUa5OlBsOUJ5sXi38Syc-SB6JrpCCXX1B8pEMrXYC9DQTHDKTHnKUvPHPLDkHD-ogLIvibPOINIimYIaI1IgXNPdwEDQPjTvnj01PjcYrjEOnHEOrHNzTHmK0A7zmyd1n1NYrWcdP1m_ph-6wvNln7tznjTksZK60h7V01ndn1C1P9DQTHFBryRbrjc3sH0kmHEVPj0vuBd6njndsHnOPvw-uHRWPhcdnTDQPjTvnj01Pjcdn1TznHn3rj9kTHDYnjmkP1nYnWNQrH0zP10QrjEKTHD_nHDKTEDKsEDKTEDKPH98nWE3sWmvsWDvnkDkTHTKTHD_nHDKXLYKnHTkPHEKnHTknWndrEDvPADvnhn1PBdWmW--sHEzmhNVmWnOmBYYnHwWP1DQP10dm1cKTHEKIL9kuHD1mhNYmHN1n1m3PjmvTy7JpdI-mv66ITD1THn_nHmdPzkQnWm3PEDQTyOdUAkKrHEzPH0dPWm3mv7-m19QP9%22%2C%22lego_sid%22%3A%22141278743212895671227905755%22%2C%22platformCompany%22%3A%22ajk%22%2C%22lego_show_track_url_base%22%3A%22https%3A%5C%2F%5C%2Fadtrack.58.com%5C%2F1002359%5C%2F%22%2C%22lego_version%22%3A%221.0.1%22%2C%22legoHuiDu%22%3Atrue%7D%7D",
          "tid": "64a62c36-cb9e-42be-b39b-414c711775c2",
          "isLiving": false,
          "isVR": false,
          "distanceDesc": "",
          "isGoldOwner": false
        },
        {
          "infoid": "2016236964651013",
          "cateName": "整租",
          "cateID": "11",
          "title": "8号线地铁站旁 实拍图 落地窗大单间 配置齐全 有阳台厨卫",
          "price": "1000",
          "img": "https://pic1.ajkimg.com/display/anjuke/377ba8c678797060e5d73db51f632701/240x180c.jpg?t=1&srotate=1",
          "unit": "元/月",
          "coll": "0",
          "subwayDistance": "距8号线-昌岗站步行614m",
          "fangwuLiangDian": [
            {
              "text": "整租",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "朝南",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "近地铁",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            }
          ],
          "area": "30㎡",
          "toward": "朝南",
          "shangquan": "江南西",
          "isAnxuan": "0",
          "location": false,
          "source_type": "1",
          "isauction": "6",
          "auctionShowText": "",
          "legoAuction": "%7B%22isauction%22%3A%226%22%2C%22lego%22%3A%7B%22lego_tid%22%3A%22c235d325-3b23-4f56-b17c-1d7bbc38a9c3%22%2C%22lego_ad_click_url%22%3A%22https%3A%5C%2F%5C%2Flegoclick.58.com%5C%2Fjump%3Ftarget%3DpZwY0ZnlsztdraOWUvYKuadbmyD3uHcvnBYQP10OsHwBuyNVmymLuBYOmvE3rH7-uhckm1NKP1DdrjDYPTDKnWTQPWc1PWbvPjmdnHTQnkDkTHDknTDQsjDQTHnKnHmzPjcvPHTvrH9YrTDOTXy8BXyQ6Xy8C8XMMSpcfSykWSXCBSylWzLb8sf-BFxCCpWhJ2ZM_3ohbF1MV2cKnEDQTED1TyqWN1IQnDQFpdqLIbGjXgRZNhPZEM0LRL-jINNKnHEkPWDOnW91nH9knH9vnjTOP9DvTgK60h7V01ndPjCzPH0vsAGGmNI-rWKxnWTknaQkmgF6Ugn1PHnln1mKnED3PWEQP1DLnaYvPhRWsHEOuADVmhNYuid6PWbYmhNdPymLrAnKnHEkPWDOnW91nHbdP1TYrHnYPTDQPjTvnHbzrjnQrHDdnHDkPjTkTEDQsjDQTEDKTiYKTEDKTHN3sWcYra3vPB3QPWnKnTDkTEDQsjDQTgVqTHDknjNYTHDknjc1PHbKm1c1PyE1nWNVnvcznzYYuWNvsycQPvnVnyELmhFWn166ryn1TEDYTgI3nANQnvF-PADdn1nvrjEvP976phVguyPCmgEKnkD1sjDvPH0_P1bLrEDQTyOdUAkKmvEOmHw6mHm1PWTQPjmOmE%22%2C%22lego_sid%22%3A%22141278743212895671227905755%22%2C%22platformCompany%22%3A%22ajk%22%2C%22lego_show_track_url_base%22%3A%22https%3A%5C%2F%5C%2Fadtrack.58.com%5C%2F1002359%5C%2F%22%2C%22lego_version%22%3A%221.0.1%22%2C%22legoHuiDu%22%3Atrue%7D%7D",
          "tid": "c235d325-3b23-4f56-b17c-1d7bbc38a9c3",
          "isLiving": false,
          "isVR": false,
          "distanceDesc": "",
          "isGoldOwner": false
        },
        {
          "infoid": "2024887660338185",
          "cateName": "整租",
          "cateID": "11",
          "title": "温馨一居室  电梯精装修  整租",
          "price": "1000",
          "img": "https://pic1.ajkimg.com/display/anjuke/fbdf6d98052a87cc3ce7384feeac0039/240x180c.jpg?t=1&srotate=1",
          "unit": "元/月",
          "coll": "0",
          "subwayDistance": "距8号线-昌岗站步行136m",
          "fangwuLiangDian": [
            {
              "text": "整租",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "南北",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "近地铁",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "有电梯",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "精装修",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            }
          ],
          "area": "20㎡",
          "toward": "南北",
          "shangquan": "昌岗",
          "isAnxuan": "0",
          "location": false,
          "source_type": "1",
          "isauction": "6",
          "auctionShowText": "",
          "legoAuction": "%7B%22isauction%22%3A%226%22%2C%22lego%22%3A%7B%22lego_tid%22%3A%22e7f3e5b8-2106-4a5b-a508-7959d1dd98ed%22%2C%22lego_ad_click_url%22%3A%22https%3A%5C%2F%5C%2Flegoclick.58.com%5C%2Fjump%3Ftarget%3DpZwY0ZnlsztdraOWUvYKuadbmyD3uHcvnBYQP10OsHwBuyNVmymLuBYOmvE3rH7-uhckm1NKnWTkPWTknHczTEDznjcYrj9LPWmkn1n3nH9dTHTKnHTkTHD_nHDKnkDQPWcYnWmdnjmOrjE3THDkTXy8BXyQ6Xy8C8XMMSpcfSykWSXCBSylWzLb8sf-BFxCCpWhJ2ZM_3ohbF1MV2cKnEDQTED1TyqWN1IQnDQFpdqLIbGjXgRZNhPZEM0LRL-jINNKnHEkPWE1PHcOP1NYrHnvnHDdn9DvTgK60h7V01ndPjCzPH0vsAGGmNI-rWKxnWTknaQkmgF6Ugn1PHnln1mKnE7hmhNzPvFWPiYOnvNksHEOP1TVmW76mzdBPhcvm1wbuHmOujnKnHEkPWE1PHcOP1m3P10Ln1D3PEDQPjTvPjndnWbLPWDznW0dP1DzTEDQsjDQTEDKTiYKTEDKTHN3sWcYra3vPB3QPWnKnTDkTEDQsjDQTgVqTHDknjNYTHDknjc1PHbKuHIhnvNdmW9VnWDkPBYYmHRBsyDdnj9VP1bdryEQuAEOrARbTEDYTgI3nANQnvF-PADdn1nvrjEvP976phVguyPCmgEKnkD1sjDvPH0_PWn3PEDQTyOdUAkKPHRWuHTkujIWryDQPhmQu9%22%2C%22lego_sid%22%3A%22141278743212895671227905755%22%2C%22platformCompany%22%3A%22ajk%22%2C%22lego_show_track_url_base%22%3A%22https%3A%5C%2F%5C%2Fadtrack.58.com%5C%2F1002359%5C%2F%22%2C%22lego_version%22%3A%221.0.1%22%2C%22legoHuiDu%22%3Atrue%7D%7D",
          "tid": "e7f3e5b8-2106-4a5b-a508-7959d1dd98ed",
          "isLiving": false,
          "isVR": false,
          "distanceDesc": "",
          "isGoldOwner": false
        },
        {
          "infoid": "2041585352679424",
          "cateName": "整租",
          "cateID": "11",
          "title": "江南西地铁附一房原1800现1300只交电费其它水网杂费全免",
          "price": "1800",
          "img": "https://pic1.ajkimg.com/display/anjuke/524a31ea44b8eb2b3e005afcb2c4dfa8/240x180c.jpg?t=1&srotate=1",
          "unit": "元/月",
          "coll": "0",
          "subwayDistance": "距8号线-昌岗站步行1.2km",
          "fangwuLiangDian": [
            {
              "text": "整租",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "南北",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "近地铁",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "精装修",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            }
          ],
          "area": "12㎡",
          "toward": "南北",
          "shangquan": "江南西",
          "isAnxuan": "0",
          "location": false,
          "source_type": "1",
          "isauction": "6",
          "auctionShowText": "",
          "legoAuction": "%7B%22isauction%22%3A%226%22%2C%22lego%22%3A%7B%22lego_tid%22%3A%22c2d31a27-442a-47a1-87aa-b3d3c11e1cc2%22%2C%22lego_ad_click_url%22%3A%22https%3A%5C%2F%5C%2Flegoclick.58.com%5C%2Fjump%3Ftarget%3DpZwY0ZnlsztdraOWUvYKuadbmyD3uHcvnBYQP10OsHwBuyNVmymLuBYOmvE3rH7-uhckm1NKPWm1nHNYPEDKnWTYnHN3PHndnWmLrHEzPTDkTHDknTDQsjDQTHnKnHmzPjcvPHTvrH9YrTDQnEZ-oCM-_mg-oJsMGO4hBs4-_c4MJcf-8CtVOsBeOmBgl2ACOJB6Oer2OGa0OeiBTHDKnEDKnk7fmdnL0HKniyVxILu2EL-dwdFWwYFLPdIOELR7THDYnjmYPHNLrH9QP1E3PHNQPW9KP97kmgF6Ugn1PHElnWNLPBQJpy7ZuHCkg1cknjT_0A7zmyd1n1N1rWnvTHDKm1nvmWNOm1DVnjP-riYYmHTzsHb3mvmVnH6WuymLrHT3mWbvTHDYnjmYPHNLrH91nWDvPHDLnHcKnHEkPWEdPH0OrjcYPWDdrjn1P9DKnikQnEDKTEDVTEDKTEDdra3zPj98PWm8nHm1THTKnTDKnikQnE7exEDQnjTdPTDQnjTzn1NOTynzujnQmHcLsHEYnhDVPjI6niY3Pv76syc1ujPWnH7-nyPWn9DKPT7LXjK-nHPBuHw6PHn1PW9YPWmKmyGoRvRWpA7YTHnKnzkQPWNLsj0OP1bKnE78IyQ_TyDkuWu-P1KhuWnYnAFBujc%22%2C%22lego_sid%22%3A%22141278743212895671227905755%22%2C%22platformCompany%22%3A%22ajk%22%2C%22lego_show_track_url_base%22%3A%22https%3A%5C%2F%5C%2Fadtrack.58.com%5C%2F1002359%5C%2F%22%2C%22lego_version%22%3A%221.0.1%22%2C%22legoHuiDu%22%3Atrue%7D%7D",
          "tid": "c2d31a27-442a-47a1-87aa-b3d3c11e1cc2",
          "isLiving": false,
          "isVR": false,
          "distanceDesc": "",
          "isGoldOwner": false
        },
        {
          "infoid": "2006503709187073",
          "cateName": "整租",
          "cateID": "11",
          "title": "晓港十佳房源  独立阳台厨卫 电梯精装修",
          "price": "1000",
          "img": "https://pic1.ajkimg.com/display/anjuke/35da7436aeb08f6ea4e0ce6b4feb6d86/240x180c.jpg?t=1&srotate=1",
          "unit": "元/月",
          "coll": "0",
          "subwayDistance": "距8号线-昌岗站步行830m",
          "fangwuLiangDian": [
            {
              "text": "整租",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "南北",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "近地铁",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "有电梯",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "精装修",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            }
          ],
          "area": "19㎡",
          "toward": "南北",
          "shangquan": "晓港",
          "isAnxuan": "0",
          "location": false,
          "source_type": "1",
          "isauction": "6",
          "auctionShowText": "",
          "legoAuction": "%7B%22isauction%22%3A%226%22%2C%22lego%22%3A%7B%22lego_tid%22%3A%22dc49165a-e21b-4723-9e2a-b12ff93c4098%22%2C%22lego_ad_click_url%22%3A%22https%3A%5C%2F%5C%2Flegoclick.58.com%5C%2Fjump%3Ftarget%3DpZwY0ZnlsztdraOWUvYKuadbmyD3uHcvnBYQP10OsHwBuyNVmymLuBYOmvE3rH7-uhckm1NKnWTkPWTknHczTEDznjTvPHT1P1TOnH9Lnj01THTKnHTkTHD_nHDKnkDQPWcYnWmdnjmOrjE3THDzTXy8BXyQ6Xy8C8XMMSpcfSykWSXCBSylWzLb8sf-BFxCCpWhJ2ZM_3ohbF1MV2cKnEDQTED1TyqWN1IQnDQFpdqLIbGjXgRZNhPZEM0LRL-jINNKnHEkPWTvnjcYP1NQnWTknWNvnEDvTgK60h7V01ndPjCzPH0vsAGGmNI-rWKxnWTknaQkmgF6Ugn1PHnln1mKnEDzPyEOuyEYrad-uAwWsHEOPjEVmym1uaYvm10Quj91ujE1uHbKnHEkPWTvnjcYP1mdPjmkrj9OPkDQPjTvnjmknWELPH0YrHDLnHckTEDQsjDQTEDKTiYKTEDKTHN3sWcYra3vPB3QPWnKnTDkTEDQsjDQTgVqTHDknjNYTHDknjc1PHbKuAnYrHDvPyDVuHcQmBYYP1c1sH--nhDVmWDzuhmOnvnYnjb3TEDYTgI3nANQnvF-PADdn1nvrjEvP976phVguyPCmgEKnkD1sjDvPH0_PWEvnkDQTyOdUAkKrycvmhD3rHT3nWIhnvPBmE%22%2C%22lego_sid%22%3A%22141278743212895671227905755%22%2C%22platformCompany%22%3A%22ajk%22%2C%22lego_show_track_url_base%22%3A%22https%3A%5C%2F%5C%2Fadtrack.58.com%5C%2F1002359%5C%2F%22%2C%22lego_version%22%3A%221.0.1%22%2C%22legoHuiDu%22%3Atrue%7D%7D",
          "tid": "dc49165a-e21b-4723-9e2a-b12ff93c4098",
          "isLiving": false,
          "isVR": false,
          "distanceDesc": "",
          "isGoldOwner": false
        },
        {
          "infoid": "2024968287215617",
          "cateName": "整租",
          "cateID": "11",
          "title": "美院温馨一居室，电梯精装修   智能门锁",
          "price": "1000",
          "img": "https://pic1.ajkimg.com/display/anjuke/7b128c10b274dc04c21d9895a138f1d2/240x180c.jpg?t=1&srotate=1",
          "unit": "元/月",
          "coll": "0",
          "subwayDistance": "距8号线-昌岗站步行540m",
          "fangwuLiangDian": [
            {
              "text": "整租",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "南北",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "近地铁",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "有电梯",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "精装修",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            }
          ],
          "area": "20㎡",
          "toward": "南北",
          "shangquan": "晓港",
          "isAnxuan": "0",
          "location": false,
          "source_type": "1",
          "isauction": "6",
          "auctionShowText": "",
          "legoAuction": "%7B%22isauction%22%3A%226%22%2C%22lego%22%3A%7B%22lego_tid%22%3A%223e980c79-ebe9-4a47-bd2e-a842f0705ad5%22%2C%22lego_ad_click_url%22%3A%22https%3A%5C%2F%5C%2Flegoclick.58.com%5C%2Fjump%3Ftarget%3DpZwY0ZnlsztdraOWUvYKuadbmyD3uHcvnBYQP10OsHwBuyNVmymLuBYOmvE3rH7-uhckm1NKnWTkPWTknHczTEDznjcYrHm3nW9LnWDdPWDLTHTKnHTkTHD_nHDKnkDQPWcYnWmdnjmOrjE3THD1TXy8BXyQ6Xy8C8XMMSpcfSykWSXCBSylWzLb8sf-BFxCCpWhJ2ZM_3ohbF1MV2cKnEDQTED1TyqWN1IQnDQFpdqLIbGjXgRZNhPZEM0LRL-jINNKnHEkPWTvnjD3nWbvPH9dPjckrEDvTgK60h7V01ndPjCzPH0vsAGGmNI-rWKxnWTknaQkmgF6Ugn1PHnln1mKnE7-P1nvnHNkmBYOPHFWsHE3nAcVmyEYPaYYujw6uW66ryDknvDKnHEkPWTvnjD3n1DkPjcvPWcYnTDQPjTvnjmknH91njEOP1EYn19dTEDQsjDQTEDKTiYKTEDKTHN3sWcYra3vPB3QPWnKnTDkTEDQsjDQTgVqTHDknjNYTHDknjc1PHbKnvNOrjKWP1bVuyF-riYYmHELsyFbnhNVmH9YnhmkP1TdmyEdTEDYTgI3nANQnvF-PADdn1nvrjEvP976phVguyPCmgEKnkD1sjDvPH0_PWEvnkDQTyOdUAkKPjNLujmdnjTdmyRBnhmdP9%22%2C%22lego_sid%22%3A%22141278743212895671227905755%22%2C%22platformCompany%22%3A%22ajk%22%2C%22lego_show_track_url_base%22%3A%22https%3A%5C%2F%5C%2Fadtrack.58.com%5C%2F1002359%5C%2F%22%2C%22lego_version%22%3A%221.0.1%22%2C%22legoHuiDu%22%3Atrue%7D%7D",
          "tid": "3e980c79-ebe9-4a47-bd2e-a842f0705ad5",
          "isLiving": false,
          "isVR": false,
          "distanceDesc": "",
          "isGoldOwner": false
        },
        {
          "infoid": "1859138929631244",
          "cateName": "整租",
          "cateID": "11",
          "title": "免租七天，大马路边电梯大单间，直租无中介",
          "price": "1500",
          "img": "https://pic1.ajkimg.com/display/anjuke/41c7ee1a34b34de54cb6fbe769b4cde7/240x180c.jpg?t=1&srotate=1",
          "unit": "元/月",
          "coll": "0",
          "subwayDistance": "距8号线-昌岗站步行1.7km",
          "fangwuLiangDian": [
            {
              "text": "整租",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "东南",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "近地铁",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "有电梯",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "豪华装修",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            }
          ],
          "area": "28㎡",
          "toward": "东南",
          "shangquan": "江燕路",
          "isAnxuan": "0",
          "location": false,
          "source_type": "1",
          "isauction": "6",
          "auctionShowText": "",
          "legoAuction": "%7B%22isauction%22%3A%226%22%2C%22lego%22%3A%7B%22lego_tid%22%3A%22a3b91921-daef-447c-a63c-77577e116ee3%22%2C%22lego_ad_click_url%22%3A%22https%3A%5C%2F%5C%2Flegoclick.58.com%5C%2Fjump%3Ftarget%3DpZwY0ZnlsztdraOWUvYKuadbmyD3uHcvnBYQP10OsHwBuyNVmymLuBYOmvE3rH7-uhckm1NKnHTQPj0Yn19kTEDQrjNOnHn3rHcOPWnQnWEYTHTKnHTkTHD_nHDKnkDQPWcYnWmdnjmOrjE3THDYTXy8BXyQ6Xy8C8XMMSpcfSykWSXCBSylWzLb8sf-BFxCCpWhJ2ZM_3ohbF1MV2cKnEDQTED1TyqWN1IQnDQFpdqLIbGjXgRZNhPZEM0LRL-jINNKnHEkPWD1n19Qn1bkrHmvn10YPTDvTgK60h7V01ndPjCzPH0vsAGGmNI-rWKxnWTknaQkmgF6Ugn1PHnln1mKnEDkPvDzPj9QPaYLPj7BsHEYnhnVrjN3PzYvrHcdn1b3uARhnvEKnHEkPWD1n19QPjTOPjckrHTzPTDQPjTvnHn1rjD1rH0vP10zPWTOTEDQsjDQTEDKTiYKTEDKTHN3sWcYra3vPB3QPWnKnTDkTEDQsjDQTgVqTHDknjNYTHDknjc1PHbKmHPBrHDOnWDVuA7-uBYYPjIWsyDvnvnVP10dP1I-nHDvuyN1TEDYTgI3nANQnvF-PADdn1nvrjEvP976phVguyPCmgEKnkD1sjDvPH0_nHDdPWmKnE78IyQ_TyF6PvN3uH9vn10OuWmYuH0%22%2C%22lego_sid%22%3A%22141278743212895671227905755%22%2C%22platformCompany%22%3A%22ajk%22%2C%22lego_show_track_url_base%22%3A%22https%3A%5C%2F%5C%2Fadtrack.58.com%5C%2F1002359%5C%2F%22%2C%22lego_version%22%3A%221.0.1%22%2C%22legoHuiDu%22%3Atrue%7D%7D",
          "tid": "a3b91921-daef-447c-a63c-77577e116ee3",
          "isLiving": false,
          "isVR": false,
          "distanceDesc": "",
          "isGoldOwner": false
        },
        {
          "infoid": "2026302175011854",
          "cateName": "整租",
          "cateID": "11",
          "title": "整租 昌岗优选房源，近地铁，交通便利舒适的生活社区",
          "price": "1000",
          "img": "https://pic1.ajkimg.com/display/anjuke/c20203ec1bff6c38035ca17b2b92aa34/240x180c.jpg?t=1&srotate=1",
          "unit": "元/月",
          "coll": "0",
          "subwayDistance": "距8号线-昌岗站步行136m",
          "fangwuLiangDian": [
            {
              "text": "整租",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "南北",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "近地铁",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "有电梯",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "精装修",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            }
          ],
          "area": "21㎡",
          "toward": "南北",
          "shangquan": "昌岗",
          "isAnxuan": "0",
          "location": false,
          "source_type": "1",
          "isauction": "6",
          "auctionShowText": "",
          "legoAuction": "%7B%22isauction%22%3A%226%22%2C%22lego%22%3A%7B%22lego_tid%22%3A%22fa7c41b9-77f3-4578-82ea-557477363371%22%2C%22lego_ad_click_url%22%3A%22https%3A%5C%2F%5C%2Flegoclick.58.com%5C%2Fjump%3Ftarget%3DpZwY0ZnlsztdraOWUvYKuadbmyD3uHcvnBYQP10OsHwBuyNVmymLuBYOmvE3rH7-uhckm1NKnWTknHE3nW93TEDznjcvn1TznH0dnjDQrjNYTHTKnHTkTHD_nHDKnkDQPWcYnWmdnjmOrjE3THDdTXy8BXyQ6Xy8C8XMMSpcfSykWSXCBSylWzLb8sf-BFxCCpWhJ2ZM_3ohbF1MV2cKnEDQTED1TyqWN1IQnDQFpdqLIbGjXgRZNhPZEM0LRL-jINNKnHEkPH9Qn1NznHbLnjm1Pj0dn9DvTgK60h7V01ndPjCzPH0vsAGGmNI-rWKxnWTknaQkmgF6Ugn1PHnln1mKnE7BnW76nvuWPaY3nWnvsHEYuWmVmHRWnzY3ujTkrH91nWP-rH9KnHEkPH9Qn1NznWDQn1cYnHT3rTDQPjTdrjD1PHcznjn1PHEOn1DzTEDQsjDQTEDKTiYKTEDKTHN3sWcYra3vPB3QPWnKnTDkTEDQsjDQTgVqTHDknjNYTHDknjc1PHbKuhDLm1EQmWbVP1IhnzYYPH03sH9zuyDVPHNLPj0Ln1m1n10QTEDYTgI3nANQnvF-PADdn1nvrjEvP976phVguyPCmgEKnkD1sjDvPH0_PWn3PEDQTyOdUAkKrADOPvD1nW7hnHnduj93uT%22%2C%22lego_sid%22%3A%22141278743212895671227905755%22%2C%22platformCompany%22%3A%22ajk%22%2C%22lego_show_track_url_base%22%3A%22https%3A%5C%2F%5C%2Fadtrack.58.com%5C%2F1002359%5C%2F%22%2C%22lego_version%22%3A%221.0.1%22%2C%22legoHuiDu%22%3Atrue%7D%7D",
          "tid": "fa7c41b9-77f3-4578-82ea-557477363371",
          "isLiving": false,
          "isVR": false,
          "distanceDesc": "",
          "isGoldOwner": false
        },
        {
          "infoid": "2024624388414477",
          "cateName": "整租",
          "cateID": "11",
          "title": "整租 昌岗房东十佳房源 急租 温馨一居室 舒适的生活社区",
          "price": "1000",
          "img": "https://pic1.ajkimg.com/display/anjuke/699cdfc508f92199ad1f5605230a5fea/240x180c.jpg?t=1&srotate=1",
          "unit": "元/月",
          "coll": "0",
          "subwayDistance": "距8号线-昌岗站步行428m",
          "fangwuLiangDian": [
            {
              "text": "整租",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "南北",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "近地铁",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "有电梯",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "精装修",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            }
          ],
          "area": "19.2㎡",
          "toward": "南北",
          "shangquan": "昌岗",
          "isAnxuan": "0",
          "location": false,
          "source_type": "1",
          "isauction": "6",
          "auctionShowText": "",
          "legoAuction": "%7B%22isauction%22%3A%226%22%2C%22lego%22%3A%7B%22lego_tid%22%3A%22aabd5cbf-dd77-451d-afee-b3b4cda939dc%22%2C%22lego_ad_click_url%22%3A%22https%3A%5C%2F%5C%2Flegoclick.58.com%5C%2Fjump%3Ftarget%3DpZwY0ZnlsztdraOWUvYKuadbmyD3uHcvnBYQP10OsHwBuyNVmymLuBYOmvE3rH7-uhckm1NKnWTknHE3nW93TEDznjcYPWcYn193PjDYPj0LTHTKnHTkTHD_nHDKnkDQPWcYnWmdnjmOrjE3THDvTXy8BXyQ6Xy8C8XMMSpcfSykWSXCBSylWzLb8sf-BFxCCpWhJ2ZM_3ohbF1MV2cKnEDQTED1TyqWN1IQnDQFpdqLIbGjXgRZNhPZEM0LRL-jINNKnHEkPH9Qn1EYn1DdrHmvnWNOn9DvTgK60h7V01ndPjCzPH0vsAGGmNI-rWKxnWTknaQkmgF6Ugn1PHnln1mKnE76rju6ujFbraYvuAu6sHEOPhmVmWEvmzYvuj-6nvn1uHKBnjDKnHEkPH9Qn1EYn1nkPWEvn1c1n9DQPjTdrjD1PjE1nWnkrHmdP1mkTEDQsjDQTEDKTiYKTEDKTHN3sWcYra3vPB3QPWnKnTDkTEDQsjDQTgVqTHDknjNYTHDknjc1PHbKmy7BujRWmhmVuAELPzYYPH7bsy7huyNVmWPBPAPbmHb1rywWTEDYTgI3nANQnvF-PADdn1nvrjEvP976phVguyPCmgEKnkD1sjDvPH0_PWn3PEDQTyOdUAkKPjmvrj03mhnLnynkmHbQmE%22%2C%22lego_sid%22%3A%22141278743212895671227905755%22%2C%22platformCompany%22%3A%22ajk%22%2C%22lego_show_track_url_base%22%3A%22https%3A%5C%2F%5C%2Fadtrack.58.com%5C%2F1002359%5C%2F%22%2C%22lego_version%22%3A%221.0.1%22%2C%22legoHuiDu%22%3Atrue%7D%7D",
          "tid": "aabd5cbf-dd77-451d-afee-b3b4cda939dc",
          "isLiving": false,
          "isVR": false,
          "distanceDesc": "",
          "isGoldOwner": false
        },
        {
          "infoid": "2023451187766284",
          "cateName": "整租",
          "cateID": "11",
          "title": "整租 昌岗房东 急租好房源 温馨一居室 舒适的生活社区",
          "price": "1000",
          "img": "https://pic1.ajkimg.com/display/anjuke/ea045c850a020fb85298d830daf5b213/240x180c.jpg?t=1&srotate=1",
          "unit": "元/月",
          "coll": "0",
          "subwayDistance": "距8号线-昌岗站步行614m",
          "fangwuLiangDian": [
            {
              "text": "整租",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "南北",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "近地铁",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "有电梯",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "精装修",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            }
          ],
          "area": "21.3㎡",
          "toward": "南北",
          "shangquan": "江南西",
          "isAnxuan": "0",
          "location": false,
          "source_type": "1",
          "isauction": "6",
          "auctionShowText": "",
          "legoAuction": "%7B%22isauction%22%3A%226%22%2C%22lego%22%3A%7B%22lego_tid%22%3A%22f0a705c0-0687-49d7-b5b8-5843cb16d821%22%2C%22lego_ad_click_url%22%3A%22https%3A%5C%2F%5C%2Flegoclick.58.com%5C%2Fjump%3Ftarget%3DpZwY0ZnlsztdraOWUvYKuadbmyD3uHcvnBYQP10OsHwBuyNVmymLuBYOmvE3rH7-uhckm1NKnWTknHE3nW93TEDznjc1PjNQnH9LP1mvnW9YTHTKnHTkTHD_nHDKnkDQPWcYnWmdnjmOrjE3THDLTXy8BXyQ6Xy8C8XMMSpcfSykWSXCBSylWzLb8sf-BFxCCpWhJ2ZM_3ohbF1MV2cKnEDQTED1TyqWN1IQnDQFpdqLIbGjXgRZNhPZEM0LRL-jINNKnHEkPH9Qn1N3nHcLn19OrHTkrEDvTgK60h7V01ndPjCzPH0vsAGGmNI-rWKxnWTknaQkmgF6Ugn1PHnln1mKnE7BnhndPHm1nBYOPHubsHwbn1mVmW6hPidBnHn1nHPBP1T3nWEKnHEkPH9Qn1N3nHEQnWnQnHTYnTDQPjTdrjD1PH9QnWmQn1cknHbzTEDQsjDQTEDKTiYKTEDKTHN3sWcYra3vPB3QPWnKnTDkTEDQsjDQTgVqTHDknjNYTHDknjc1PHbKuWK6P1Tdm1TVnjm3PzYYryELsycdmW9VPH9YnvPBnHubrjcQTEDYTgI3nANQnvF-PADdn1nvrjEvP976phVguyPCmgEKnkD1sjDvPH0_P1bLrEDQTyOdUAkKuW9kuhNYmvDzmHFBmW9QPE%22%2C%22lego_sid%22%3A%22141278743212895671227905755%22%2C%22platformCompany%22%3A%22ajk%22%2C%22lego_show_track_url_base%22%3A%22https%3A%5C%2F%5C%2Fadtrack.58.com%5C%2F1002359%5C%2F%22%2C%22lego_version%22%3A%221.0.1%22%2C%22legoHuiDu%22%3Atrue%7D%7D",
          "tid": "f0a705c0-0687-49d7-b5b8-5843cb16d821",
          "isLiving": false,
          "isVR": false,
          "distanceDesc": "",
          "isGoldOwner": false
        },
        {
          "infoid": "2044778608139271",
          "cateName": "整租",
          "cateID": "11",
          "title": "民用水电 市二宫地铁D出口 全新1房",
          "price": "1800",
          "img": "https://pic1.ajkimg.com/display/anjuke/b860697b8df1d0f734f6206b4336143f/240x180c.jpg?t=1&srotate=1",
          "unit": "元/月",
          "coll": "0",
          "subwayDistance": "距8号线-昌岗站步行1.9km",
          "fangwuLiangDian": [
            {
              "text": "安选房源",
              "textColor": "#3EC77B",
              "bgColor": "#3EC77B"
            },
            {
              "text": "整租",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "朝南",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "近地铁",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "新上",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "精装修",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            }
          ],
          "area": "30㎡",
          "toward": "朝南",
          "shangquan": "前进路",
          "isAnxuan": "1",
          "location": false,
          "source_type": "1",
          "isauction": "1",
          "auctionShowText": "",
          "legoAuction": "%7B%22isauction%22%3A%221%22%2C%22lego%22%3A%7B%22lego_tid%22%3A%22%22%2C%22lego_ad_click_url%22%3A%22%22%2C%22lego_sid%22%3A%22141278743212895671227905755%22%2C%22platformCompany%22%3A%22ajk%22%2C%22lego_show_track_url_base%22%3A%22%22%2C%22lego_version%22%3A%221.0.1%22%2C%22legoHuiDu%22%3Atrue%7D%7D",
          "tid": "",
          "isLiving": false,
          "isVR": false,
          "distanceDesc": "",
          "isGoldOwner": false
        },
        {
          "infoid": "1871917793533955",
          "cateName": "整租",
          "cateID": "11",
          "title": "两房一厅 招租了 全新为上班一族提供安静,舒适的家",
          "price": "1800",
          "img": "https://pic1.ajkimg.com/display/anjuke/99dec196efaedba3d09235dff3104863/240x180c.jpg?t=1&srotate=1",
          "unit": "元/月",
          "coll": "0",
          "subwayDistance": "距8号线-昌岗站步行614m",
          "fangwuLiangDian": [
            {
              "text": "整租",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "东南",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "近地铁",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "精装修",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            }
          ],
          "area": "56㎡",
          "toward": "东南",
          "shangquan": "江南西",
          "isAnxuan": "0",
          "location": false,
          "source_type": "1",
          "isauction": "1",
          "auctionShowText": "",
          "legoAuction": "%7B%22isauction%22%3A%221%22%2C%22lego%22%3A%7B%22lego_tid%22%3A%22%22%2C%22lego_ad_click_url%22%3A%22%22%2C%22lego_sid%22%3A%22141278743212895671227905755%22%2C%22platformCompany%22%3A%22ajk%22%2C%22lego_show_track_url_base%22%3A%22%22%2C%22lego_version%22%3A%221.0.1%22%2C%22legoHuiDu%22%3Atrue%7D%7D",
          "tid": "",
          "isLiving": false,
          "isVR": false,
          "distanceDesc": "",
          "isGoldOwner": false
        },
        {
          "infoid": "2033555660987406",
          "cateName": "整租",
          "cateID": "11",
          "title": "泉塘路小区 1室0厅1卫 1600元月 配套齐全 精装修",
          "price": "1600",
          "img": "https://pic1.ajkimg.com/display/anjuke/efa6a005b88a8309f6a3718b80f1c274/240x180c.jpg?t=1&srotate=1",
          "unit": "元/月",
          "coll": "0",
          "subwayDistance": "距8号线-昌岗站步行1.7km",
          "fangwuLiangDian": [
            {
              "text": "整租",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "朝南",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "近地铁",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "精装修",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            }
          ],
          "area": "30㎡",
          "toward": "朝南",
          "shangquan": "江燕路",
          "isAnxuan": "0",
          "location": false,
          "source_type": "1",
          "isauction": "1",
          "auctionShowText": "",
          "legoAuction": "%7B%22isauction%22%3A%221%22%2C%22lego%22%3A%7B%22lego_tid%22%3A%22%22%2C%22lego_ad_click_url%22%3A%22%22%2C%22lego_sid%22%3A%22141278743212895671227905755%22%2C%22platformCompany%22%3A%22ajk%22%2C%22lego_show_track_url_base%22%3A%22%22%2C%22lego_version%22%3A%221.0.1%22%2C%22legoHuiDu%22%3Atrue%7D%7D",
          "tid": "",
          "isLiving": false,
          "isVR": false,
          "distanceDesc": "",
          "isGoldOwner": false
        },
        {
          "infoid": "1966491614258179",
          "cateName": "整租",
          "cateID": "11",
          "title": "建方寓24时管家服务独立卫浴 可短租 年签可免一个月",
          "price": "1900",
          "img": "https://pic1.ajkimg.com/display/anjuke/5b88433ad54a774c7e9d0e6b6d1e785a/240x180c.jpg?t=1&srotate=1",
          "unit": "元/月",
          "coll": "0",
          "subwayDistance": "距8号线-昌岗站步行1.2km",
          "fangwuLiangDian": [
            {
              "text": "整租",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "朝南",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "近地铁",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "有电梯",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "精装修",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            }
          ],
          "area": "23㎡",
          "toward": "朝南",
          "shangquan": "江燕路",
          "isAnxuan": "0",
          "location": false,
          "source_type": "1",
          "isauction": "1",
          "auctionShowText": "",
          "legoAuction": "%7B%22isauction%22%3A%221%22%2C%22lego%22%3A%7B%22lego_tid%22%3A%22%22%2C%22lego_ad_click_url%22%3A%22%22%2C%22lego_sid%22%3A%22141278743212895671227905755%22%2C%22platformCompany%22%3A%22ajk%22%2C%22lego_show_track_url_base%22%3A%22%22%2C%22lego_version%22%3A%221.0.1%22%2C%22legoHuiDu%22%3Atrue%7D%7D",
          "tid": "",
          "isLiving": false,
          "isVR": false,
          "distanceDesc": "",
          "isGoldOwner": false
        },
        {
          "infoid": "2010983993826310",
          "cateName": "整租",
          "cateID": "11",
          "title": "2号线火爆单间  可押一 拎包入住全新公寓",
          "price": "1980",
          "img": "https://pic1.ajkimg.com/display/anjuke/243105d66a8918d8af990b815f01476a/240x180c.jpg?t=1&srotate=1",
          "unit": "元/月",
          "coll": "0",
          "subwayDistance": "距8号线-昌岗站步行1.4km",
          "fangwuLiangDian": [
            {
              "text": "整租",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "朝南",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "近地铁",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "有电梯",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            }
          ],
          "area": "30㎡",
          "toward": "朝南",
          "shangquan": "东晓南",
          "isAnxuan": "0",
          "location": false,
          "source_type": "1",
          "isauction": "1",
          "auctionShowText": "",
          "legoAuction": "%7B%22isauction%22%3A%221%22%2C%22lego%22%3A%7B%22lego_tid%22%3A%22%22%2C%22lego_ad_click_url%22%3A%22%22%2C%22lego_sid%22%3A%22141278743212895671227905755%22%2C%22platformCompany%22%3A%22ajk%22%2C%22lego_show_track_url_base%22%3A%22%22%2C%22lego_version%22%3A%221.0.1%22%2C%22legoHuiDu%22%3Atrue%7D%7D",
          "tid": "",
          "isLiving": false,
          "isVR": false,
          "distanceDesc": "",
          "isGoldOwner": false
        },
        {
          "infoid": "1999345162462221",
          "cateName": "整租",
          "cateID": "11",
          "title": "精装一房仅租1800 随时看房",
          "price": "1800",
          "img": "https://pic1.ajkimg.com/display/anjuke/6f9cc1140f9fbad7c0becab31f325c7b/240x180c.jpg?t=1&srotate=1",
          "unit": "元/月",
          "coll": "0",
          "subwayDistance": "距8号线-昌岗站步行895m",
          "fangwuLiangDian": [
            {
              "text": "整租",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "东南",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "近地铁",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "精装修",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            }
          ],
          "area": "35㎡",
          "toward": "东南",
          "shangquan": "江燕路",
          "isAnxuan": "0",
          "location": false,
          "source_type": "1",
          "isauction": "1",
          "auctionShowText": "",
          "legoAuction": "%7B%22isauction%22%3A%221%22%2C%22lego%22%3A%7B%22lego_tid%22%3A%22%22%2C%22lego_ad_click_url%22%3A%22%22%2C%22lego_sid%22%3A%22141278743212895671227905755%22%2C%22platformCompany%22%3A%22ajk%22%2C%22lego_show_track_url_base%22%3A%22%22%2C%22lego_version%22%3A%221.0.1%22%2C%22legoHuiDu%22%3Atrue%7D%7D",
          "tid": "",
          "isLiving": false,
          "isVR": false,
          "distanceDesc": "",
          "isGoldOwner": false
        },
        {
          "infoid": "2044387267830791",
          "cateName": "整租",
          "cateID": "11",
          "title": "昌岗地铁  精装一房  一楼带大花园  仅租1400",
          "price": "1600",
          "img": "https://pic1.ajkimg.com/display/anjuke/80d5ac760ff7a76b12bf13d9940c147e/240x180c.jpg?t=1&srotate=1",
          "unit": "元/月",
          "coll": "0",
          "subwayDistance": "距8号线-昌岗站步行450m",
          "fangwuLiangDian": [
            {
              "text": "整租",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "朝南",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "近地铁",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "精装修",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            }
          ],
          "area": "45㎡",
          "toward": "朝南",
          "shangquan": "昌岗",
          "isAnxuan": "0",
          "location": false,
          "source_type": "1",
          "isauction": "1",
          "auctionShowText": "",
          "legoAuction": "%7B%22isauction%22%3A%221%22%2C%22lego%22%3A%7B%22lego_tid%22%3A%22%22%2C%22lego_ad_click_url%22%3A%22%22%2C%22lego_sid%22%3A%22141278743212895671227905755%22%2C%22platformCompany%22%3A%22ajk%22%2C%22lego_show_track_url_base%22%3A%22%22%2C%22lego_version%22%3A%221.0.1%22%2C%22legoHuiDu%22%3Atrue%7D%7D",
          "tid": "",
          "isLiving": false,
          "isVR": false,
          "distanceDesc": "",
          "isGoldOwner": false
        },
        {
          "infoid": "2001975902331917",
          "cateName": "整租",
          "cateID": "11",
          "title": "冠寓已消毒可短租 冠寓单间整租 精装修家电齐全押一付一",
          "price": "1990",
          "img": "https://pic1.ajkimg.com/display/anjuke/da0128055d540077de1e647adea1c839/240x180c.jpg?t=1&srotate=1",
          "unit": "元/月",
          "coll": "0",
          "subwayDistance": "距8号线-昌岗站步行1.9km",
          "fangwuLiangDian": [
            {
              "text": "整租",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "朝南",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "近地铁",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "有电梯",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "精装修",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            }
          ],
          "area": "17㎡",
          "toward": "朝南",
          "shangquan": "前进路",
          "isAnxuan": "0",
          "location": false,
          "source_type": "1",
          "isauction": "1",
          "auctionShowText": "",
          "legoAuction": "%7B%22isauction%22%3A%221%22%2C%22lego%22%3A%7B%22lego_tid%22%3A%22%22%2C%22lego_ad_click_url%22%3A%22%22%2C%22lego_sid%22%3A%22141278743212895671227905755%22%2C%22platformCompany%22%3A%22ajk%22%2C%22lego_show_track_url_base%22%3A%22%22%2C%22lego_version%22%3A%221.0.1%22%2C%22legoHuiDu%22%3Atrue%7D%7D",
          "tid": "",
          "isLiving": false,
          "isVR": false,
          "distanceDesc": "",
          "isGoldOwner": false
        },
        {
          "infoid": "2006388124064782",
          "cateName": "整租",
          "cateID": "11",
          "title": "市二宫小区 1室0厅1卫 1600元月 35平 南北通透",
          "price": "1600",
          "img": "https://pic1.ajkimg.com/display/anjuke/2c38736dd0f12ed21ece7a98bbb7851b/240x180c.jpg?t=1&srotate=1",
          "unit": "元/月",
          "coll": "0",
          "subwayDistance": "距8号线-昌岗站步行1.9km",
          "fangwuLiangDian": [
            {
              "text": "整租",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "朝南",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "近地铁",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "有电梯",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "豪华装修",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            }
          ],
          "area": "35㎡",
          "toward": "朝南",
          "shangquan": "前进路",
          "isAnxuan": "0",
          "location": false,
          "source_type": "1",
          "isauction": "1",
          "auctionShowText": "",
          "legoAuction": "%7B%22isauction%22%3A%221%22%2C%22lego%22%3A%7B%22lego_tid%22%3A%22%22%2C%22lego_ad_click_url%22%3A%22%22%2C%22lego_sid%22%3A%22141278743212895671227905755%22%2C%22platformCompany%22%3A%22ajk%22%2C%22lego_show_track_url_base%22%3A%22%22%2C%22lego_version%22%3A%221.0.1%22%2C%22legoHuiDu%22%3Atrue%7D%7D",
          "tid": "",
          "isLiving": false,
          "isVR": false,
          "distanceDesc": "",
          "isGoldOwner": false
        },
        {
          "infoid": "2044437896290314",
          "cateName": "整租",
          "cateID": "11",
          "title": "整租 昌岗房东十佳房源 急租 温馨一居室 舒适的生活社区",
          "price": "1000",
          "img": "https://pic1.ajkimg.com/display/anjuke/e160779b7e681cfa0a8bf61feb27bab5/240x180c.jpg?t=1&srotate=1",
          "unit": "元/月",
          "coll": "0",
          "subwayDistance": "距8号线-昌岗站步行894m",
          "fangwuLiangDian": [
            {
              "text": "整租",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "南北",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "近地铁",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "有电梯",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "精装修",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            }
          ],
          "area": "19.2㎡",
          "toward": "南北",
          "shangquan": "昌岗",
          "isAnxuan": "0",
          "location": false,
          "source_type": "1",
          "isauction": "1",
          "auctionShowText": "",
          "legoAuction": "%7B%22isauction%22%3A%221%22%2C%22lego%22%3A%7B%22lego_tid%22%3A%22%22%2C%22lego_ad_click_url%22%3A%22%22%2C%22lego_sid%22%3A%22141278743212895671227905755%22%2C%22platformCompany%22%3A%22ajk%22%2C%22lego_show_track_url_base%22%3A%22%22%2C%22lego_version%22%3A%221.0.1%22%2C%22legoHuiDu%22%3Atrue%7D%7D",
          "tid": "",
          "isLiving": false,
          "isVR": false,
          "distanceDesc": "",
          "isGoldOwner": false
        },
        {
          "infoid": "2024313407904783",
          "cateName": "整租",
          "cateID": "11",
          "title": "实图直租晓岗地铁站旁大小单间 一房一厅 家私家电齐全独立厨卫",
          "price": "900",
          "img": "https://pic1.ajkimg.com/display/anjuke/50bf5a53c339e4a16b00196c399d06a7/240x180c.jpg?t=1&srotate=1",
          "unit": "元/月",
          "coll": "0",
          "subwayDistance": "距8号线-昌岗站步行614m",
          "fangwuLiangDian": [
            {
              "text": "整租",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "南北",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "近地铁",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "精装修",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            }
          ],
          "area": "20㎡",
          "toward": "南北",
          "shangquan": "江南西",
          "isAnxuan": "0",
          "location": false,
          "source_type": "1",
          "isauction": "1",
          "auctionShowText": "",
          "legoAuction": "%7B%22isauction%22%3A%221%22%2C%22lego%22%3A%7B%22lego_tid%22%3A%22%22%2C%22lego_ad_click_url%22%3A%22%22%2C%22lego_sid%22%3A%22141278743212895671227905755%22%2C%22platformCompany%22%3A%22ajk%22%2C%22lego_show_track_url_base%22%3A%22%22%2C%22lego_version%22%3A%221.0.1%22%2C%22legoHuiDu%22%3Atrue%7D%7D",
          "tid": "",
          "isLiving": false,
          "isVR": false,
          "distanceDesc": "",
          "isGoldOwner": false
        },
        {
          "infoid": "2040880448069646",
          "cateName": "整租",
          "cateID": "11",
          "title": "江泰路 地铁2号线 D出口 电梯单间 直租 步行500米",
          "price": "1850",
          "img": "https://pic1.ajkimg.com/display/anjuke/14e07de5bfa2151b7211c95b635b524d/240x180c.jpg?t=1&srotate=1",
          "unit": "元/月",
          "coll": "0",
          "subwayDistance": "距8号线-昌岗站步行1.7km",
          "fangwuLiangDian": [
            {
              "text": "整租",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "朝南",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "近地铁",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "有电梯",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "精装修",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            }
          ],
          "area": "33㎡",
          "toward": "朝南",
          "shangquan": "江南西",
          "isAnxuan": "0",
          "location": false,
          "source_type": "1",
          "isauction": "1",
          "auctionShowText": "",
          "legoAuction": "%7B%22isauction%22%3A%221%22%2C%22lego%22%3A%7B%22lego_tid%22%3A%22%22%2C%22lego_ad_click_url%22%3A%22%22%2C%22lego_sid%22%3A%22141278743212895671227905755%22%2C%22platformCompany%22%3A%22ajk%22%2C%22lego_show_track_url_base%22%3A%22%22%2C%22lego_version%22%3A%221.0.1%22%2C%22legoHuiDu%22%3Atrue%7D%7D",
          "tid": "",
          "isLiving": false,
          "isVR": false,
          "distanceDesc": "",
          "isGoldOwner": false
        },
        {
          "infoid": "2041548463348745",
          "cateName": "整租",
          "cateID": "11",
          "title": "采光好，房间干净宽敞，交通便利，拎包入住",
          "price": "600",
          "img": "https://pic1.ajkimg.com/display/anjuke/25b80dbfb4b9dab0380a21dd166dc60b/240x180c.jpg?t=1&srotate=1",
          "unit": "元/月",
          "coll": "0",
          "subwayDistance": "距8号线-昌岗站步行614m",
          "fangwuLiangDian": [
            {
              "text": "整租",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "朝南",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "近地铁",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            }
          ],
          "area": "23㎡",
          "toward": "朝南",
          "shangquan": "江南西",
          "isAnxuan": "0",
          "location": false,
          "source_type": "1",
          "isauction": "1",
          "auctionShowText": "",
          "legoAuction": "%7B%22isauction%22%3A%221%22%2C%22lego%22%3A%7B%22lego_tid%22%3A%22%22%2C%22lego_ad_click_url%22%3A%22%22%2C%22lego_sid%22%3A%22141278743212895671227905755%22%2C%22platformCompany%22%3A%22ajk%22%2C%22lego_show_track_url_base%22%3A%22%22%2C%22lego_version%22%3A%221.0.1%22%2C%22legoHuiDu%22%3Atrue%7D%7D",
          "tid": "",
          "isLiving": false,
          "isVR": false,
          "distanceDesc": "",
          "isGoldOwner": false
        },
        {
          "infoid": "1723274406763524",
          "cateName": "整租",
          "cateID": "11",
          "title": "WowWay品牌公寓地铁8号线整租大阳台独立卫浴",
          "price": "1500",
          "img": "https://pic1.ajkimg.com/display/anjuke/6bb0a52b4d4909ca52fd530e5e275b6b/240x180c.jpg?t=1&srotate=1",
          "unit": "元/月",
          "coll": "0",
          "subwayDistance": "距8号线-昌岗站步行1.7km",
          "fangwuLiangDian": [
            {
              "text": "整租",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "朝南",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "近地铁",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "有电梯",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "豪华装修",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            }
          ],
          "area": "20㎡",
          "toward": "朝南",
          "shangquan": "新港西",
          "isAnxuan": "0",
          "location": false,
          "source_type": "1",
          "isauction": "1",
          "auctionShowText": "",
          "legoAuction": "%7B%22isauction%22%3A%221%22%2C%22lego%22%3A%7B%22lego_tid%22%3A%22%22%2C%22lego_ad_click_url%22%3A%22%22%2C%22lego_sid%22%3A%22141278743212895671227905755%22%2C%22platformCompany%22%3A%22ajk%22%2C%22lego_show_track_url_base%22%3A%22%22%2C%22lego_version%22%3A%221.0.1%22%2C%22legoHuiDu%22%3Atrue%7D%7D",
          "tid": "",
          "isLiving": false,
          "isVR": false,
          "distanceDesc": "",
          "isGoldOwner": false
        },
        {
          "infoid": "2023068902628367",
          "cateName": "整租",
          "cateID": "11",
          "title": "8号线大单间 近地铁（房东）直租 有阳台 拎包入住",
          "price": "600",
          "img": "https://pic1.ajkimg.com/display/anjuke/d79a4680ae109ad94e256f0a06e41afe/240x180c.jpg?t=1&srotate=1",
          "unit": "元/月",
          "coll": "0",
          "subwayDistance": "距8号线-昌岗站步行614m",
          "fangwuLiangDian": [
            {
              "text": "整租",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "南北",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "近地铁",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "精装修",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            }
          ],
          "area": "27㎡",
          "toward": "南北",
          "shangquan": "江南西",
          "isAnxuan": "0",
          "location": false,
          "source_type": "1",
          "isauction": "1",
          "auctionShowText": "",
          "legoAuction": "%7B%22isauction%22%3A%221%22%2C%22lego%22%3A%7B%22lego_tid%22%3A%22%22%2C%22lego_ad_click_url%22%3A%22%22%2C%22lego_sid%22%3A%22141278743212895671227905755%22%2C%22platformCompany%22%3A%22ajk%22%2C%22lego_show_track_url_base%22%3A%22%22%2C%22lego_version%22%3A%221.0.1%22%2C%22legoHuiDu%22%3Atrue%7D%7D",
          "tid": "",
          "isLiving": false,
          "isVR": false,
          "distanceDesc": "",
          "isGoldOwner": false
        },
        {
          "infoid": "2009561381248010",
          "cateName": "整租",
          "cateID": "11",
          "title": "中大地铁站 品牌公寓直租 租期押付灵活 拎包入住 自建房",
          "price": "1500",
          "img": "https://pic1.ajkimg.com/display/anjuke/680154a38262699c45ab50c257f6cf8f/240x180c.jpg?t=1&srotate=1",
          "unit": "元/月",
          "coll": "0",
          "subwayDistance": "距8号线-昌岗站步行1.9km",
          "fangwuLiangDian": [
            {
              "text": "整租",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "南北",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "近地铁",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "有电梯",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "精装修",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            }
          ],
          "area": "23㎡",
          "toward": "南北",
          "shangquan": "中大",
          "isAnxuan": "0",
          "location": false,
          "source_type": "1",
          "isauction": "1",
          "auctionShowText": "",
          "legoAuction": "%7B%22isauction%22%3A%221%22%2C%22lego%22%3A%7B%22lego_tid%22%3A%22%22%2C%22lego_ad_click_url%22%3A%22%22%2C%22lego_sid%22%3A%22141278743212895671227905755%22%2C%22platformCompany%22%3A%22ajk%22%2C%22lego_show_track_url_base%22%3A%22%22%2C%22lego_version%22%3A%221.0.1%22%2C%22legoHuiDu%22%3Atrue%7D%7D",
          "tid": "",
          "isLiving": false,
          "isVR": false,
          "distanceDesc": "",
          "isGoldOwner": false
        },
        {
          "infoid": "1741466149004293",
          "cateName": "整租",
          "cateID": "11",
          "title": "WowWay品牌公寓地铁鸺号线整租大阳台独立卫浴",
          "price": "1500",
          "img": "https://pic1.ajkimg.com/display/anjuke/7ef5e198b55402a1c744b584bcc1b0a4/240x180c.jpg?t=1&srotate=1",
          "unit": "元/月",
          "coll": "0",
          "subwayDistance": "距8号线-昌岗站步行2.0km",
          "fangwuLiangDian": [
            {
              "text": "整租",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "朝南",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "近地铁",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "有电梯",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "豪华装修",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            }
          ],
          "area": "20㎡",
          "toward": "朝南",
          "shangquan": "东晓南",
          "isAnxuan": "0",
          "location": false,
          "source_type": "1",
          "isauction": "1",
          "auctionShowText": "",
          "legoAuction": "%7B%22isauction%22%3A%221%22%2C%22lego%22%3A%7B%22lego_tid%22%3A%22%22%2C%22lego_ad_click_url%22%3A%22%22%2C%22lego_sid%22%3A%22141278743212895671227905755%22%2C%22platformCompany%22%3A%22ajk%22%2C%22lego_show_track_url_base%22%3A%22%22%2C%22lego_version%22%3A%221.0.1%22%2C%22legoHuiDu%22%3Atrue%7D%7D",
          "tid": "",
          "isLiving": false,
          "isVR": false,
          "distanceDesc": "",
          "isGoldOwner": false
        },
        {
          "infoid": "2030480779615237",
          "cateName": "整租",
          "cateID": "11",
          "title": "毕业生  8号线一房一厅 房东租 配置新 有阳台 厨卫",
          "price": "1000",
          "img": "https://pic1.ajkimg.com/display/anjuke/52f96624f87996ce621a0d8d20df7567/240x180c.jpg?t=1&srotate=1",
          "unit": "元/月",
          "coll": "0",
          "subwayDistance": "距8号线-昌岗站步行614m",
          "fangwuLiangDian": [
            {
              "text": "整租",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "朝南",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "近地铁",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "精装修",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            }
          ],
          "area": "45㎡",
          "toward": "朝南",
          "shangquan": "江南西",
          "isAnxuan": "0",
          "location": false,
          "source_type": "1",
          "isauction": "1",
          "auctionShowText": "",
          "legoAuction": "%7B%22isauction%22%3A%221%22%2C%22lego%22%3A%7B%22lego_tid%22%3A%22%22%2C%22lego_ad_click_url%22%3A%22%22%2C%22lego_sid%22%3A%22141278743212895671227905755%22%2C%22platformCompany%22%3A%22ajk%22%2C%22lego_show_track_url_base%22%3A%22%22%2C%22lego_version%22%3A%221.0.1%22%2C%22legoHuiDu%22%3Atrue%7D%7D",
          "tid": "",
          "isLiving": false,
          "isVR": false,
          "distanceDesc": "",
          "isGoldOwner": false
        },
        {
          "infoid": "2045758129757194",
          "cateName": "整租",
          "cateID": "11",
          "title": "沙涌全新一房一厅 （房东）直租 配置齐 交通方便",
          "price": "1100",
          "img": "https://pic1.ajkimg.com/display/anjuke/c5630c906aa12872d62ab14f2f9585e0/240x180c.jpg?t=1&srotate=1",
          "unit": "元/月",
          "coll": "0",
          "subwayDistance": "距8号线-昌岗站步行1.5km",
          "fangwuLiangDian": [
            {
              "text": "整租",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "东南",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "近地铁",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "新上",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "精装修",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            }
          ],
          "area": "38㎡",
          "toward": "东南",
          "shangquan": "江南西",
          "isAnxuan": "0",
          "location": false,
          "source_type": "1",
          "isauction": "1",
          "auctionShowText": "",
          "legoAuction": "%7B%22isauction%22%3A%221%22%2C%22lego%22%3A%7B%22lego_tid%22%3A%22%22%2C%22lego_ad_click_url%22%3A%22%22%2C%22lego_sid%22%3A%22141278743212895671227905755%22%2C%22platformCompany%22%3A%22ajk%22%2C%22lego_show_track_url_base%22%3A%22%22%2C%22lego_version%22%3A%221.0.1%22%2C%22legoHuiDu%22%3Atrue%7D%7D",
          "tid": "",
          "isLiving": false,
          "isVR": false,
          "distanceDesc": "",
          "isGoldOwner": false
        },
        {
          "infoid": "2033358110172162",
          "cateName": "整租",
          "cateID": "11",
          "title": "昌岗十佳房源.个人房东.近地铁口.温馨一居室，精装修，可短租",
          "price": "1000",
          "img": "https://pic1.ajkimg.com/display/anjuke/d207c22b3b398d484fd430da52f4c2bd/240x180c.jpg?t=1&srotate=1",
          "unit": "元/月",
          "coll": "0",
          "subwayDistance": "距8号线-昌岗站步行614m",
          "fangwuLiangDian": [
            {
              "text": "整租",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "南北",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "近地铁",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "有电梯",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "精装修",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            }
          ],
          "area": "30㎡",
          "toward": "南北",
          "shangquan": "江南西",
          "isAnxuan": "0",
          "location": false,
          "source_type": "1",
          "isauction": "1",
          "auctionShowText": "",
          "legoAuction": "%7B%22isauction%22%3A%221%22%2C%22lego%22%3A%7B%22lego_tid%22%3A%22%22%2C%22lego_ad_click_url%22%3A%22%22%2C%22lego_sid%22%3A%22141278743212895671227905755%22%2C%22platformCompany%22%3A%22ajk%22%2C%22lego_show_track_url_base%22%3A%22%22%2C%22lego_version%22%3A%221.0.1%22%2C%22legoHuiDu%22%3Atrue%7D%7D",
          "tid": "",
          "isLiving": false,
          "isVR": false,
          "distanceDesc": "",
          "isGoldOwner": false
        },
        {
          "infoid": "2031583192719368",
          "cateName": "整租",
          "cateID": "11",
          "title": "燕子岗小区 2室1厅1卫 2000元月 60平 精装修",
          "price": "2000",
          "img": "https://pic1.ajkimg.com/display/anjuke/e1aa212258bb4f55db4f090d5727df05/240x180c.jpg?t=1&srotate=1",
          "unit": "元/月",
          "coll": "0",
          "subwayDistance": "距8号线-昌岗站步行1.7km",
          "fangwuLiangDian": [
            {
              "text": "整租",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "朝南",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "近地铁",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "精装修",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            }
          ],
          "area": "60㎡",
          "toward": "朝南",
          "shangquan": "工业大道北",
          "isAnxuan": "0",
          "location": false,
          "source_type": "1",
          "isauction": "1",
          "auctionShowText": "",
          "legoAuction": "%7B%22isauction%22%3A%221%22%2C%22lego%22%3A%7B%22lego_tid%22%3A%22%22%2C%22lego_ad_click_url%22%3A%22%22%2C%22lego_sid%22%3A%22141278743212895671227905755%22%2C%22platformCompany%22%3A%22ajk%22%2C%22lego_show_track_url_base%22%3A%22%22%2C%22lego_version%22%3A%221.0.1%22%2C%22legoHuiDu%22%3Atrue%7D%7D",
          "tid": "",
          "isLiving": false,
          "isVR": false,
          "distanceDesc": "",
          "isGoldOwner": false
        },
        {
          "infoid": "1999121864514566",
          "cateName": "整租",
          "cateID": "11",
          "title": "精装一房仅租1900",
          "price": "1900",
          "img": "https://pic1.ajkimg.com/display/anjuke/2ed149d89286a7568bdd669d2419feb1/240x180c.jpg?t=1&srotate=1",
          "unit": "元/月",
          "coll": "0",
          "subwayDistance": "距8号线-昌岗站步行1.3km",
          "fangwuLiangDian": [
            {
              "text": "整租",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "南北",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "近地铁",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "精装修",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            }
          ],
          "area": "35㎡",
          "toward": "南北",
          "shangquan": "沙园",
          "isAnxuan": "0",
          "location": false,
          "source_type": "1",
          "isauction": "1",
          "auctionShowText": "",
          "legoAuction": "%7B%22isauction%22%3A%221%22%2C%22lego%22%3A%7B%22lego_tid%22%3A%22%22%2C%22lego_ad_click_url%22%3A%22%22%2C%22lego_sid%22%3A%22141278743212895671227905755%22%2C%22platformCompany%22%3A%22ajk%22%2C%22lego_show_track_url_base%22%3A%22%22%2C%22lego_version%22%3A%221.0.1%22%2C%22legoHuiDu%22%3Atrue%7D%7D",
          "tid": "",
          "isLiving": false,
          "isVR": false,
          "distanceDesc": "",
          "isGoldOwner": false
        },
        {
          "infoid": "2041484785786888",
          "cateName": "整租",
          "cateID": "11",
          "title": "精装修单间环境好 光线好 格局好 家具齐全 交通方便",
          "price": "700",
          "img": "https://pic1.ajkimg.com/display/anjuke/ba1d0f28e3314f68a33fe5fbfcffb425/240x180c.jpg?t=1&srotate=1",
          "unit": "元/月",
          "coll": "0",
          "subwayDistance": "距8号线-昌岗站步行1.1km",
          "fangwuLiangDian": [
            {
              "text": "整租",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "南北",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "近地铁",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "精装修",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            }
          ],
          "area": "23㎡",
          "toward": "南北",
          "shangquan": "江南西",
          "isAnxuan": "0",
          "location": false,
          "source_type": "1",
          "isauction": "1",
          "auctionShowText": "",
          "legoAuction": "%7B%22isauction%22%3A%221%22%2C%22lego%22%3A%7B%22lego_tid%22%3A%22%22%2C%22lego_ad_click_url%22%3A%22%22%2C%22lego_sid%22%3A%22141278743212895671227905755%22%2C%22platformCompany%22%3A%22ajk%22%2C%22lego_show_track_url_base%22%3A%22%22%2C%22lego_version%22%3A%221.0.1%22%2C%22legoHuiDu%22%3Atrue%7D%7D",
          "tid": "",
          "isLiving": false,
          "isVR": false,
          "distanceDesc": "",
          "isGoldOwner": false
        },
        {
          "infoid": "1966559486747654",
          "cateName": "整租",
          "cateID": "11",
          "title": "建方寓 高楼层房间1650起 出门在外不孤独 签一年优惠多多",
          "price": "1650",
          "img": "https://pic1.ajkimg.com/display/anjuke/9a9bb567d4858595999306aedf0c0ae7/240x180c.jpg?t=1&srotate=1",
          "unit": "元/月",
          "coll": "0",
          "subwayDistance": "距8号线-昌岗站步行1.1km",
          "fangwuLiangDian": [
            {
              "text": "整租",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "朝南",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "近地铁",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "有电梯",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "精装修",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            }
          ],
          "area": "22㎡",
          "toward": "朝南",
          "shangquan": "江燕路",
          "isAnxuan": "0",
          "location": false,
          "source_type": "1",
          "isauction": "1",
          "auctionShowText": "",
          "legoAuction": "%7B%22isauction%22%3A%221%22%2C%22lego%22%3A%7B%22lego_tid%22%3A%22%22%2C%22lego_ad_click_url%22%3A%22%22%2C%22lego_sid%22%3A%22141278743212895671227905755%22%2C%22platformCompany%22%3A%22ajk%22%2C%22lego_show_track_url_base%22%3A%22%22%2C%22lego_version%22%3A%221.0.1%22%2C%22legoHuiDu%22%3Atrue%7D%7D",
          "tid": "",
          "isLiving": false,
          "isVR": false,
          "distanceDesc": "",
          "isGoldOwner": false
        },
        {
          "infoid": "2024519182457860",
          "cateName": "整租",
          "cateID": "11",
          "title": "6月大优惠实图直租晓岗地铁站旁单间一房 家私家电齐全独立厨卫",
          "price": "1500",
          "img": "https://pic1.ajkimg.com/display/anjuke/4f6a78f1a6d159cc2ff83f100d744cac/240x180c.jpg?t=1&srotate=1",
          "unit": "元/月",
          "coll": "0",
          "subwayDistance": "距8号线-昌岗站步行614m",
          "fangwuLiangDian": [
            {
              "text": "整租",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "南北",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "近地铁",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "精装修",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            }
          ],
          "area": "30㎡",
          "toward": "南北",
          "shangquan": "江南西",
          "isAnxuan": "0",
          "location": false,
          "source_type": "1",
          "isauction": "1",
          "auctionShowText": "",
          "legoAuction": "%7B%22isauction%22%3A%221%22%2C%22lego%22%3A%7B%22lego_tid%22%3A%22%22%2C%22lego_ad_click_url%22%3A%22%22%2C%22lego_sid%22%3A%22141278743212895671227905755%22%2C%22platformCompany%22%3A%22ajk%22%2C%22lego_show_track_url_base%22%3A%22%22%2C%22lego_version%22%3A%221.0.1%22%2C%22legoHuiDu%22%3Atrue%7D%7D",
          "tid": "",
          "isLiving": false,
          "isVR": false,
          "distanceDesc": "",
          "isGoldOwner": false
        },
        {
          "infoid": "2024507217632263",
          "cateName": "整租",
          "cateID": "11",
          "title": "6月大优惠实图直租晓岗地铁站旁单间一房 家私家电齐全独立厨卫",
          "price": "900",
          "img": "https://pic1.ajkimg.com/display/anjuke/61d61c5086c9d75fd0fc32e51d9c1931/240x180c.jpg?t=1&srotate=1",
          "unit": "元/月",
          "coll": "0",
          "subwayDistance": "距8号线-昌岗站步行830m",
          "fangwuLiangDian": [
            {
              "text": "整租",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "南北",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "近地铁",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "精装修",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            }
          ],
          "area": "20㎡",
          "toward": "南北",
          "shangquan": "晓港",
          "isAnxuan": "0",
          "location": false,
          "source_type": "1",
          "isauction": "1",
          "auctionShowText": "",
          "legoAuction": "%7B%22isauction%22%3A%221%22%2C%22lego%22%3A%7B%22lego_tid%22%3A%22%22%2C%22lego_ad_click_url%22%3A%22%22%2C%22lego_sid%22%3A%22141278743212895671227905755%22%2C%22platformCompany%22%3A%22ajk%22%2C%22lego_show_track_url_base%22%3A%22%22%2C%22lego_version%22%3A%221.0.1%22%2C%22legoHuiDu%22%3Atrue%7D%7D",
          "tid": "",
          "isLiving": false,
          "isVR": false,
          "distanceDesc": "",
          "isGoldOwner": false
        },
        {
          "infoid": "2023284054907910",
          "cateName": "整租",
          "cateID": "11",
          "title": "整租 昌岗房东急租 温馨一居室 舒适的生活社区",
          "price": "1000",
          "img": "https://pic1.ajkimg.com/display/anjuke/4b130f1e95778a6cb1759810d9145c77/240x180c.jpg?t=1&srotate=1",
          "unit": "元/月",
          "coll": "0",
          "subwayDistance": "距8号线-昌岗站步行136m",
          "fangwuLiangDian": [
            {
              "text": "整租",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "南北",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "近地铁",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "有电梯",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "精装修",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            }
          ],
          "area": "23㎡",
          "toward": "南北",
          "shangquan": "昌岗",
          "isAnxuan": "0",
          "location": false,
          "source_type": "1",
          "isauction": "1",
          "auctionShowText": "",
          "legoAuction": "%7B%22isauction%22%3A%221%22%2C%22lego%22%3A%7B%22lego_tid%22%3A%22%22%2C%22lego_ad_click_url%22%3A%22%22%2C%22lego_sid%22%3A%22141278743212895671227905755%22%2C%22platformCompany%22%3A%22ajk%22%2C%22lego_show_track_url_base%22%3A%22%22%2C%22lego_version%22%3A%221.0.1%22%2C%22legoHuiDu%22%3Atrue%7D%7D",
          "tid": "",
          "isLiving": false,
          "isVR": false,
          "distanceDesc": "",
          "isGoldOwner": false
        },
        {
          "infoid": "2009561373664269",
          "cateName": "整租",
          "cateID": "11",
          "title": "昌岗地铁站 海珠区 新年特价 精装修 房间温馨舒适整洁",
          "price": "800",
          "img": "https://pic1.ajkimg.com/display/anjuke/414926d833aa59c2f029d87f292cd625/240x180c.jpg?t=1&srotate=1",
          "unit": "元/月",
          "coll": "0",
          "subwayDistance": "距8号线-昌岗站步行614m",
          "fangwuLiangDian": [
            {
              "text": "整租",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "南北",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "近地铁",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "有电梯",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "精装修",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            }
          ],
          "area": "28.9㎡",
          "toward": "南北",
          "shangquan": "江南西",
          "isAnxuan": "0",
          "location": false,
          "source_type": "1",
          "isauction": "1",
          "auctionShowText": "",
          "legoAuction": "%7B%22isauction%22%3A%221%22%2C%22lego%22%3A%7B%22lego_tid%22%3A%22%22%2C%22lego_ad_click_url%22%3A%22%22%2C%22lego_sid%22%3A%22141278743212895671227905755%22%2C%22platformCompany%22%3A%22ajk%22%2C%22lego_show_track_url_base%22%3A%22%22%2C%22lego_version%22%3A%221.0.1%22%2C%22legoHuiDu%22%3Atrue%7D%7D",
          "tid": "",
          "isLiving": false,
          "isVR": false,
          "distanceDesc": "",
          "isGoldOwner": false
        },
        {
          "infoid": "2017726790737935",
          "cateName": "整租",
          "cateID": "11",
          "title": "2号线市二宫地铁站 无仲介费 精装单间 实价实拍 拎包即入住",
          "price": "1600",
          "img": "https://pic1.ajkimg.com/display/anjuke/c3c424a373ac8d84cc1fb26bf4961bf6/240x180c.jpg?t=1&srotate=1",
          "unit": "元/月",
          "coll": "0",
          "subwayDistance": "距8号线-昌岗站步行1.9km",
          "fangwuLiangDian": [
            {
              "text": "整租",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "朝南",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "近地铁",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "有电梯",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "精装修",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            }
          ],
          "area": "20㎡",
          "toward": "朝南",
          "shangquan": "前进路",
          "isAnxuan": "0",
          "location": false,
          "source_type": "1",
          "isauction": "1",
          "auctionShowText": "",
          "legoAuction": "%7B%22isauction%22%3A%221%22%2C%22lego%22%3A%7B%22lego_tid%22%3A%22%22%2C%22lego_ad_click_url%22%3A%22%22%2C%22lego_sid%22%3A%22141278743212895671227905755%22%2C%22platformCompany%22%3A%22ajk%22%2C%22lego_show_track_url_base%22%3A%22%22%2C%22lego_version%22%3A%221.0.1%22%2C%22legoHuiDu%22%3Atrue%7D%7D",
          "tid": "",
          "isLiving": false,
          "isVR": false,
          "distanceDesc": "",
          "isGoldOwner": false
        },
        {
          "infoid": "2037366711704586",
          "cateName": "整租",
          "cateID": "11",
          "title": "[房东直。租]2号地铁线东晓南旁精装单间出租 交通便利通风好",
          "price": "1500",
          "img": "https://pic1.ajkimg.com/display/anjuke/8e2dda223166292b8020e94b6272e744/240x180c.jpg?t=1&srotate=1",
          "unit": "元/月",
          "coll": "0",
          "subwayDistance": "距8号线-昌岗站步行1.4km",
          "fangwuLiangDian": [
            {
              "text": "整租",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "南北",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "近地铁",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "有电梯",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "精装修",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            }
          ],
          "area": "24㎡",
          "toward": "南北",
          "shangquan": "东晓南",
          "isAnxuan": "0",
          "location": false,
          "source_type": "1",
          "isauction": "1",
          "auctionShowText": "",
          "legoAuction": "%7B%22isauction%22%3A%221%22%2C%22lego%22%3A%7B%22lego_tid%22%3A%22%22%2C%22lego_ad_click_url%22%3A%22%22%2C%22lego_sid%22%3A%22141278743212895671227905755%22%2C%22platformCompany%22%3A%22ajk%22%2C%22lego_show_track_url_base%22%3A%22%22%2C%22lego_version%22%3A%221.0.1%22%2C%22legoHuiDu%22%3Atrue%7D%7D",
          "tid": "",
          "isLiving": false,
          "isVR": false,
          "distanceDesc": "",
          "isGoldOwner": false
        },
        {
          "infoid": "2011851970666498",
          "cateName": "整租",
          "cateID": "11",
          "title": "昌岗东一巷小区 1室0厅1卫 900元月 南北通透",
          "price": "900",
          "img": "https://pic1.ajkimg.com/display/anjuke/4c753c03f4e7ed813c2a010d796f24d0/240x180c.jpg?t=1&srotate=1",
          "unit": "元/月",
          "coll": "0",
          "subwayDistance": "距8号线-昌岗站步行614m",
          "fangwuLiangDian": [
            {
              "text": "整租",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "南北",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "近地铁",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "有电梯",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "精装修",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            }
          ],
          "area": "30㎡",
          "toward": "南北",
          "shangquan": "江南西",
          "isAnxuan": "0",
          "location": false,
          "source_type": "1",
          "isauction": "1",
          "auctionShowText": "",
          "legoAuction": "%7B%22isauction%22%3A%221%22%2C%22lego%22%3A%7B%22lego_tid%22%3A%22%22%2C%22lego_ad_click_url%22%3A%22%22%2C%22lego_sid%22%3A%22141278743212895671227905755%22%2C%22platformCompany%22%3A%22ajk%22%2C%22lego_show_track_url_base%22%3A%22%22%2C%22lego_version%22%3A%221.0.1%22%2C%22legoHuiDu%22%3Atrue%7D%7D",
          "tid": "",
          "isLiving": false,
          "isVR": false,
          "distanceDesc": "",
          "isGoldOwner": false
        },
        {
          "infoid": "2040870246030338",
          "cateName": "整租",
          "cateID": "11",
          "title": "海珠区 地铁步行500米 公交步行30米  精装修 配套齐全",
          "price": "1800",
          "img": "https://pic1.ajkimg.com/display/anjuke/42c9c57ff99c53d521d2cb51c27005a3/240x180c.jpg?t=1&srotate=1",
          "unit": "元/月",
          "coll": "0",
          "subwayDistance": "距8号线-昌岗站步行1.7km",
          "fangwuLiangDian": [
            {
              "text": "整租",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "朝南",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "近地铁",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "有电梯",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "精装修",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            }
          ],
          "area": "33㎡",
          "toward": "朝南",
          "shangquan": "江燕路",
          "isAnxuan": "0",
          "location": false,
          "source_type": "1",
          "isauction": "1",
          "auctionShowText": "",
          "legoAuction": "%7B%22isauction%22%3A%221%22%2C%22lego%22%3A%7B%22lego_tid%22%3A%22%22%2C%22lego_ad_click_url%22%3A%22%22%2C%22lego_sid%22%3A%22141278743212895671227905755%22%2C%22platformCompany%22%3A%22ajk%22%2C%22lego_show_track_url_base%22%3A%22%22%2C%22lego_version%22%3A%221.0.1%22%2C%22legoHuiDu%22%3Atrue%7D%7D",
          "tid": "",
          "isLiving": false,
          "isVR": false,
          "distanceDesc": "",
          "isGoldOwner": false
        },
        {
          "infoid": "2004709330625550",
          "cateName": "整租",
          "cateID": "11",
          "title": "怡乐路  一房一厅  近中大地铁口  家私齐全 租1900",
          "price": "1900",
          "img": "https://pic1.ajkimg.com/display/anjuke/e87f53e0c56d3598bcbb929654ef25d4/240x180c.jpg?t=1&srotate=1",
          "unit": "元/月",
          "coll": "0",
          "subwayDistance": "距8号线-昌岗站步行1.5km",
          "fangwuLiangDian": [
            {
              "text": "整租",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "朝南",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "近地铁",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "精装修",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            }
          ],
          "area": "35㎡",
          "toward": "朝南",
          "shangquan": "江南西",
          "isAnxuan": "0",
          "location": false,
          "source_type": "1",
          "isauction": "1",
          "auctionShowText": "",
          "legoAuction": "%7B%22isauction%22%3A%221%22%2C%22lego%22%3A%7B%22lego_tid%22%3A%22%22%2C%22lego_ad_click_url%22%3A%22%22%2C%22lego_sid%22%3A%22141278743212895671227905755%22%2C%22platformCompany%22%3A%22ajk%22%2C%22lego_show_track_url_base%22%3A%22%22%2C%22lego_version%22%3A%221.0.1%22%2C%22legoHuiDu%22%3Atrue%7D%7D",
          "tid": "",
          "isLiving": false,
          "isVR": false,
          "distanceDesc": "",
          "isGoldOwner": false
        },
        {
          "infoid": "2045938728936448",
          "cateName": "整租",
          "cateID": "11",
          "title": "前进路36号 1室1厅1卫 2000元月 30平 精装修",
          "price": "2000",
          "img": "https://pic1.ajkimg.com/display/anjuke/66542e5b60df7d5e71bcfee1e21902f8/240x180c.jpg?t=1&srotate=1",
          "unit": "元/月",
          "coll": "0",
          "subwayDistance": "距8号线-昌岗站步行1.6km",
          "fangwuLiangDian": [
            {
              "text": "整租",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "南北",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "近地铁",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "新上",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "精装修",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            }
          ],
          "area": "30㎡",
          "toward": "南北",
          "shangquan": "前进路",
          "isAnxuan": "0",
          "location": false,
          "source_type": "1",
          "isauction": "1",
          "auctionShowText": "",
          "legoAuction": "%7B%22isauction%22%3A%221%22%2C%22lego%22%3A%7B%22lego_tid%22%3A%22%22%2C%22lego_ad_click_url%22%3A%22%22%2C%22lego_sid%22%3A%22141278743212895671227905755%22%2C%22platformCompany%22%3A%22ajk%22%2C%22lego_show_track_url_base%22%3A%22%22%2C%22lego_version%22%3A%221.0.1%22%2C%22legoHuiDu%22%3Atrue%7D%7D",
          "tid": "",
          "isLiving": false,
          "isVR": false,
          "distanceDesc": "",
          "isGoldOwner": false
        },
        {
          "infoid": "2041683942137870",
          "cateName": "整租",
          "cateID": "11",
          "title": "江南西八楼超平单间 看中优惠100 拎包入住 发家致富",
          "price": "1500",
          "img": "https://pic1.ajkimg.com/display/anjuke/399bd3220295841effaeb77cd2deeb1c/240x180c.jpg?t=1&srotate=1",
          "unit": "元/月",
          "coll": "0",
          "subwayDistance": "距8号线-昌岗站步行1.2km",
          "fangwuLiangDian": [
            {
              "text": "整租",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "朝南",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "近地铁",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "精装修",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            }
          ],
          "area": "13㎡",
          "toward": "朝南",
          "shangquan": "江南西",
          "isAnxuan": "0",
          "location": false,
          "source_type": "1",
          "isauction": "1",
          "auctionShowText": "",
          "legoAuction": "%7B%22isauction%22%3A%221%22%2C%22lego%22%3A%7B%22lego_tid%22%3A%22%22%2C%22lego_ad_click_url%22%3A%22%22%2C%22lego_sid%22%3A%22141278743212895671227905755%22%2C%22platformCompany%22%3A%22ajk%22%2C%22lego_show_track_url_base%22%3A%22%22%2C%22lego_version%22%3A%221.0.1%22%2C%22legoHuiDu%22%3Atrue%7D%7D",
          "tid": "",
          "isLiving": false,
          "isVR": false,
          "distanceDesc": "",
          "isGoldOwner": false
        },
        {
          "infoid": "1972434861006859",
          "cateName": "整租",
          "cateID": "11",
          "title": "精装修1室出现了 不看后悔",
          "price": "1850",
          "img": "https://pic1.ajkimg.com/display/anjuke/452cef75be65139f8753739eae2cffe7/240x180c.jpg?t=1&srotate=1",
          "unit": "元/月",
          "coll": "0",
          "subwayDistance": "距8号线-昌岗站步行932m",
          "fangwuLiangDian": [
            {
              "text": "整租",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "朝南",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "近地铁",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "精装修",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            }
          ],
          "area": "37.4㎡",
          "toward": "朝南",
          "shangquan": "江燕路",
          "isAnxuan": "0",
          "location": false,
          "source_type": "1",
          "isauction": "1",
          "auctionShowText": "",
          "legoAuction": "%7B%22isauction%22%3A%221%22%2C%22lego%22%3A%7B%22lego_tid%22%3A%22%22%2C%22lego_ad_click_url%22%3A%22%22%2C%22lego_sid%22%3A%22141278743212895671227905755%22%2C%22platformCompany%22%3A%22ajk%22%2C%22lego_show_track_url_base%22%3A%22%22%2C%22lego_version%22%3A%221.0.1%22%2C%22legoHuiDu%22%3Atrue%7D%7D",
          "tid": "",
          "isLiving": false,
          "isVR": false,
          "distanceDesc": "",
          "isGoldOwner": false
        },
        {
          "infoid": "2018825623149577",
          "cateName": "整租",
          "cateID": "11",
          "title": "昌岗地铁站 海珠区 夏季特价 精装修 房间温馨舒适整洁",
          "price": "1000",
          "img": "https://pic1.ajkimg.com/display/anjuke/38fff8414ac9ee5c9ae7860feb10eba3/240x180c.jpg?t=1&srotate=1",
          "unit": "元/月",
          "coll": "0",
          "subwayDistance": "距8号线-昌岗站步行136m",
          "fangwuLiangDian": [
            {
              "text": "整租",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "南北",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "近地铁",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "有电梯",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "精装修",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            }
          ],
          "area": "36㎡",
          "toward": "南北",
          "shangquan": "昌岗",
          "isAnxuan": "0",
          "location": false,
          "source_type": "1",
          "isauction": "1",
          "auctionShowText": "",
          "legoAuction": "%7B%22isauction%22%3A%221%22%2C%22lego%22%3A%7B%22lego_tid%22%3A%22%22%2C%22lego_ad_click_url%22%3A%22%22%2C%22lego_sid%22%3A%22141278743212895671227905755%22%2C%22platformCompany%22%3A%22ajk%22%2C%22lego_show_track_url_base%22%3A%22%22%2C%22lego_version%22%3A%221.0.1%22%2C%22legoHuiDu%22%3Atrue%7D%7D",
          "tid": "",
          "isLiving": false,
          "isVR": false,
          "distanceDesc": "",
          "isGoldOwner": false
        },
        {
          "infoid": "2043464489051143",
          "cateName": "整租",
          "cateID": "11",
          "title": "基立下道小区 中层一房一厅 东南向 随时可看房 拎包入住",
          "price": "1800",
          "img": "https://pic1.ajkimg.com/display/anjuke/ece685c3fdbd337c78f794653c3fc0e4/240x180c.jpg?t=1&srotate=1",
          "unit": "元/月",
          "coll": "0",
          "subwayDistance": "距8号线-昌岗站步行1.6km",
          "fangwuLiangDian": [
            {
              "text": "整租",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "东南",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "近地铁",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            }
          ],
          "area": "20㎡",
          "toward": "东南",
          "shangquan": "江南西",
          "isAnxuan": "0",
          "location": false,
          "source_type": "1",
          "isauction": "1",
          "auctionShowText": "",
          "legoAuction": "%7B%22isauction%22%3A%221%22%2C%22lego%22%3A%7B%22lego_tid%22%3A%22%22%2C%22lego_ad_click_url%22%3A%22%22%2C%22lego_sid%22%3A%22141278743212895671227905755%22%2C%22platformCompany%22%3A%22ajk%22%2C%22lego_show_track_url_base%22%3A%22%22%2C%22lego_version%22%3A%221.0.1%22%2C%22legoHuiDu%22%3Atrue%7D%7D",
          "tid": "",
          "isLiving": false,
          "isVR": false,
          "distanceDesc": "",
          "isGoldOwner": false
        },
        {
          "infoid": "2014611162913802",
          "cateName": "整租",
          "cateID": "11",
          "title": "近地铁8号线 房东租 一房一厅 精装修 家具齐全 光线好",
          "price": "1000",
          "img": "https://pic1.ajkimg.com/display/anjuke/a387cbe5cf515afb2f422036f7622020/240x180c.jpg?t=1&srotate=1",
          "unit": "元/月",
          "coll": "0",
          "subwayDistance": "距8号线-昌岗站步行614m",
          "fangwuLiangDian": [
            {
              "text": "整租",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "朝南",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "近地铁",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "精装修",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            }
          ],
          "area": "36㎡",
          "toward": "朝南",
          "shangquan": "江南西",
          "isAnxuan": "0",
          "location": false,
          "source_type": "1",
          "isauction": "1",
          "auctionShowText": "",
          "legoAuction": "%7B%22isauction%22%3A%221%22%2C%22lego%22%3A%7B%22lego_tid%22%3A%22%22%2C%22lego_ad_click_url%22%3A%22%22%2C%22lego_sid%22%3A%22141278743212895671227905755%22%2C%22platformCompany%22%3A%22ajk%22%2C%22lego_show_track_url_base%22%3A%22%22%2C%22lego_version%22%3A%221.0.1%22%2C%22legoHuiDu%22%3Atrue%7D%7D",
          "tid": "",
          "isLiving": false,
          "isVR": false,
          "distanceDesc": "",
          "isGoldOwner": false
        },
        {
          "infoid": "2033319430872072",
          "cateName": "整租",
          "cateID": "11",
          "title": "整租十佳房源.近地铁.交通便利.整租的生活社区.欢迎前来入住",
          "price": "1000",
          "img": "https://pic1.ajkimg.com/display/anjuke/3971ef63cf353662d8645d95be52c70c/240x180c.jpg?t=1&srotate=1",
          "unit": "元/月",
          "coll": "0",
          "subwayDistance": "距8号线-昌岗站步行136m",
          "fangwuLiangDian": [
            {
              "text": "整租",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "南北",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "近地铁",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "有电梯",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "精装修",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            }
          ],
          "area": "25.5㎡",
          "toward": "南北",
          "shangquan": "昌岗",
          "isAnxuan": "0",
          "location": false,
          "source_type": "1",
          "isauction": "1",
          "auctionShowText": "",
          "legoAuction": "%7B%22isauction%22%3A%221%22%2C%22lego%22%3A%7B%22lego_tid%22%3A%22%22%2C%22lego_ad_click_url%22%3A%22%22%2C%22lego_sid%22%3A%22141278743212895671227905755%22%2C%22platformCompany%22%3A%22ajk%22%2C%22lego_show_track_url_base%22%3A%22%22%2C%22lego_version%22%3A%221.0.1%22%2C%22legoHuiDu%22%3Atrue%7D%7D",
          "tid": "",
          "isLiving": false,
          "isVR": false,
          "distanceDesc": "",
          "isGoldOwner": false
        },
        {
          "infoid": "2042850687654918",
          "cateName": "整租",
          "cateID": "11",
          "title": "宝岗大道地铁，宝泉大厦，舒适大单房，低于市场价，带独立卫生间",
          "price": "1600",
          "img": "https://pic1.ajkimg.com/display/anjuke/22559d42b300665453d4f5ce0b68a804/240x180c.jpg?t=1&srotate=1",
          "unit": "元/月",
          "coll": "0",
          "subwayDistance": "距8号线-昌岗站步行1.1km",
          "fangwuLiangDian": [
            {
              "text": "整租",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "朝东",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "近地铁",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "有电梯",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "精装修",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            }
          ],
          "area": "25㎡",
          "toward": "朝东",
          "shangquan": "宝岗",
          "isAnxuan": "0",
          "location": false,
          "source_type": "1",
          "isauction": "1",
          "auctionShowText": "",
          "legoAuction": "%7B%22isauction%22%3A%221%22%2C%22lego%22%3A%7B%22lego_tid%22%3A%22%22%2C%22lego_ad_click_url%22%3A%22%22%2C%22lego_sid%22%3A%22141278743212895671227905755%22%2C%22platformCompany%22%3A%22ajk%22%2C%22lego_show_track_url_base%22%3A%22%22%2C%22lego_version%22%3A%221.0.1%22%2C%22legoHuiDu%22%3Atrue%7D%7D",
          "tid": "",
          "isLiving": false,
          "isVR": false,
          "distanceDesc": "",
          "isGoldOwner": false
        },
        {
          "infoid": "1734846629929998",
          "cateName": "整租",
          "cateID": "11",
          "title": "小蜗居，大梦想，找房子而烦恼打电话联系我",
          "price": "900",
          "img": "https://pic1.ajkimg.com/display/anjuke/186506ef57577197ba539e79cc6fdb95/240x180c.jpg?t=1&srotate=1",
          "unit": "元/月",
          "coll": "0",
          "subwayDistance": "距8号线-昌岗站步行614m",
          "fangwuLiangDian": [
            {
              "text": "整租",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "南北",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "近地铁",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "精装修",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            }
          ],
          "area": "48㎡",
          "toward": "南北",
          "shangquan": "江南西",
          "isAnxuan": "0",
          "location": false,
          "source_type": "1",
          "isauction": "1",
          "auctionShowText": "",
          "legoAuction": "%7B%22isauction%22%3A%221%22%2C%22lego%22%3A%7B%22lego_tid%22%3A%22%22%2C%22lego_ad_click_url%22%3A%22%22%2C%22lego_sid%22%3A%22141278743212895671227905755%22%2C%22platformCompany%22%3A%22ajk%22%2C%22lego_show_track_url_base%22%3A%22%22%2C%22lego_version%22%3A%221.0.1%22%2C%22legoHuiDu%22%3Atrue%7D%7D",
          "tid": "",
          "isLiving": false,
          "isVR": false,
          "distanceDesc": "",
          "isGoldOwner": false
        },
        {
          "infoid": "1979840270277635",
          "cateName": "整租",
          "cateID": "11",
          "title": "江南西 单间 有家私电器 独立卫生间和厨房 租1500",
          "price": "1500",
          "img": "https://pic1.ajkimg.com/display/anjuke/f13b695b5f187637ba302a8ac9f9ccb7/240x180c.jpg?t=1&srotate=1",
          "unit": "元/月",
          "coll": "0",
          "subwayDistance": "距8号线-昌岗站步行1.2km",
          "fangwuLiangDian": [
            {
              "text": "整租",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "朝南",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "近地铁",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            }
          ],
          "area": "30㎡",
          "toward": "朝南",
          "shangquan": "江南西",
          "isAnxuan": "0",
          "location": false,
          "source_type": "1",
          "isauction": "1",
          "auctionShowText": "",
          "legoAuction": "%7B%22isauction%22%3A%221%22%2C%22lego%22%3A%7B%22lego_tid%22%3A%22%22%2C%22lego_ad_click_url%22%3A%22%22%2C%22lego_sid%22%3A%22141278743212895671227905755%22%2C%22platformCompany%22%3A%22ajk%22%2C%22lego_show_track_url_base%22%3A%22%22%2C%22lego_version%22%3A%221.0.1%22%2C%22legoHuiDu%22%3Atrue%7D%7D",
          "tid": "",
          "isLiving": false,
          "isVR": false,
          "distanceDesc": "",
          "isGoldOwner": false
        },
        {
          "infoid": "2042931313057802",
          "cateName": "整租",
          "cateID": "11",
          "title": "遇见公寓，遇见美好，打造精致舒适生活从住开始，可短租",
          "price": "700",
          "img": "https://pic1.ajkimg.com/display/anjuke/1ac61fb3b1efc4ed860775df9dfad0fc/240x180c.jpg?t=1&srotate=1",
          "unit": "元/月",
          "coll": "0",
          "subwayDistance": "距8号线-昌岗站步行136m",
          "fangwuLiangDian": [
            {
              "text": "整租",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "南北",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "近地铁",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "精装修",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            }
          ],
          "area": "23㎡",
          "toward": "南北",
          "shangquan": "昌岗",
          "isAnxuan": "0",
          "location": false,
          "source_type": "1",
          "isauction": "1",
          "auctionShowText": "",
          "legoAuction": "%7B%22isauction%22%3A%221%22%2C%22lego%22%3A%7B%22lego_tid%22%3A%22%22%2C%22lego_ad_click_url%22%3A%22%22%2C%22lego_sid%22%3A%22141278743212895671227905755%22%2C%22platformCompany%22%3A%22ajk%22%2C%22lego_show_track_url_base%22%3A%22%22%2C%22lego_version%22%3A%221.0.1%22%2C%22legoHuiDu%22%3Atrue%7D%7D",
          "tid": "",
          "isLiving": false,
          "isVR": false,
          "distanceDesc": "",
          "isGoldOwner": false
        },
        {
          "infoid": "1999391114862592",
          "cateName": "整租",
          "cateID": "11",
          "title": "毕业季实习生特价两房一厅，非中介",
          "price": "1280",
          "img": "https://pic1.ajkimg.com/display/anjuke/19e8b01ec5a942da8a933f9ca7398759/240x180c.jpg?t=1&srotate=1",
          "unit": "元/月",
          "coll": "0",
          "subwayDistance": "距8号线-昌岗站步行136m",
          "fangwuLiangDian": [
            {
              "text": "整租",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "朝东",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "近地铁",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "有电梯",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "精装修",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            }
          ],
          "area": "42㎡",
          "toward": "朝东",
          "shangquan": "昌岗",
          "isAnxuan": "0",
          "location": false,
          "source_type": "1",
          "isauction": "1",
          "auctionShowText": "",
          "legoAuction": "%7B%22isauction%22%3A%221%22%2C%22lego%22%3A%7B%22lego_tid%22%3A%22%22%2C%22lego_ad_click_url%22%3A%22%22%2C%22lego_sid%22%3A%22141278743212895671227905755%22%2C%22platformCompany%22%3A%22ajk%22%2C%22lego_show_track_url_base%22%3A%22%22%2C%22lego_version%22%3A%221.0.1%22%2C%22legoHuiDu%22%3Atrue%7D%7D",
          "tid": "",
          "isLiving": false,
          "isVR": false,
          "distanceDesc": "",
          "isGoldOwner": false
        },
        {
          "infoid": "1972480078879751",
          "cateName": "整租",
          "cateID": "11",
          "title": "全新出租 精装一房一厅 家电齐全 交通方便",
          "price": "600",
          "img": "https://pic1.ajkimg.com/display/anjuke/e62312c1c0efcabf725f874b17c49051/240x180c.jpg?t=1&srotate=1",
          "unit": "元/月",
          "coll": "0",
          "subwayDistance": "距8号线-昌岗站步行136m",
          "fangwuLiangDian": [
            {
              "text": "整租",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "朝南",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "近地铁",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "有电梯",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "精装修",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            }
          ],
          "area": "39㎡",
          "toward": "朝南",
          "shangquan": "昌岗",
          "isAnxuan": "0",
          "location": false,
          "source_type": "1",
          "isauction": "1",
          "auctionShowText": "",
          "legoAuction": "%7B%22isauction%22%3A%221%22%2C%22lego%22%3A%7B%22lego_tid%22%3A%22%22%2C%22lego_ad_click_url%22%3A%22%22%2C%22lego_sid%22%3A%22141278743212895671227905755%22%2C%22platformCompany%22%3A%22ajk%22%2C%22lego_show_track_url_base%22%3A%22%22%2C%22lego_version%22%3A%221.0.1%22%2C%22legoHuiDu%22%3Atrue%7D%7D",
          "tid": "",
          "isLiving": false,
          "isVR": false,
          "distanceDesc": "",
          "isGoldOwner": false
        },
        {
          "infoid": "1992529035811840",
          "cateName": "整租",
          "cateID": "11",
          "title": "沙园地铁口月付可短租)无中介(家电全)",
          "price": "500",
          "img": "https://pic1.ajkimg.com/display/anjuke/ef8fb4493184436d11f2458a50be3a85/240x180c.jpg?t=1&srotate=1",
          "unit": "元/月",
          "coll": "0",
          "subwayDistance": "距8号线-昌岗站步行1.8km",
          "fangwuLiangDian": [
            {
              "text": "整租",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "朝南",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "近地铁",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "有电梯",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            }
          ],
          "area": "12㎡",
          "toward": "朝南",
          "shangquan": "江南西",
          "isAnxuan": "0",
          "location": false,
          "source_type": "1",
          "isauction": "1",
          "auctionShowText": "",
          "legoAuction": "%7B%22isauction%22%3A%221%22%2C%22lego%22%3A%7B%22lego_tid%22%3A%22%22%2C%22lego_ad_click_url%22%3A%22%22%2C%22lego_sid%22%3A%22141278743212895671227905755%22%2C%22platformCompany%22%3A%22ajk%22%2C%22lego_show_track_url_base%22%3A%22%22%2C%22lego_version%22%3A%221.0.1%22%2C%22legoHuiDu%22%3Atrue%7D%7D",
          "tid": "",
          "isLiving": false,
          "isVR": false,
          "distanceDesc": "",
          "isGoldOwner": false
        },
        {
          "infoid": "1978195817178113",
          "cateName": "整租",
          "cateID": "11",
          "title": "可短租 近地铁 江泰路 精装修单间 电梯房 拎包入住 直租",
          "price": "1500",
          "img": "https://pic1.ajkimg.com/display/anjuke/f9e9ad9abeaf114909b44deeb66984a4/240x180c.jpg?t=1&srotate=1",
          "unit": "元/月",
          "coll": "0",
          "subwayDistance": "距8号线-昌岗站步行1.6km",
          "fangwuLiangDian": [
            {
              "text": "整租",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "南北",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "近地铁",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "有电梯",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "精装修",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            }
          ],
          "area": "30㎡",
          "toward": "南北",
          "shangquan": "江南西",
          "isAnxuan": "0",
          "location": false,
          "source_type": "1",
          "isauction": "1",
          "auctionShowText": "",
          "legoAuction": "%7B%22isauction%22%3A%221%22%2C%22lego%22%3A%7B%22lego_tid%22%3A%22%22%2C%22lego_ad_click_url%22%3A%22%22%2C%22lego_sid%22%3A%22141278743212895671227905755%22%2C%22platformCompany%22%3A%22ajk%22%2C%22lego_show_track_url_base%22%3A%22%22%2C%22lego_version%22%3A%221.0.1%22%2C%22legoHuiDu%22%3Atrue%7D%7D",
          "tid": "",
          "isLiving": false,
          "isVR": false,
          "distanceDesc": "",
          "isGoldOwner": false
        },
        {
          "infoid": "1734738198127630",
          "cateName": "整租",
          "cateID": "11",
          "title": "价钱实惠！！配置齐全，户型舒适，南北全采光",
          "price": "850",
          "img": "https://pic1.ajkimg.com/display/anjuke/25d0010a39e96c254267667a34e5bcf1/240x180c.jpg?t=1&srotate=1",
          "unit": "元/月",
          "coll": "0",
          "subwayDistance": "距8号线-昌岗站步行614m",
          "fangwuLiangDian": [
            {
              "text": "整租",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "西北",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "近地铁",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            }
          ],
          "area": "30㎡",
          "toward": "西北",
          "shangquan": "江南西",
          "isAnxuan": "0",
          "location": false,
          "source_type": "1",
          "isauction": "1",
          "auctionShowText": "",
          "legoAuction": "%7B%22isauction%22%3A%221%22%2C%22lego%22%3A%7B%22lego_tid%22%3A%22%22%2C%22lego_ad_click_url%22%3A%22%22%2C%22lego_sid%22%3A%22141278743212895671227905755%22%2C%22platformCompany%22%3A%22ajk%22%2C%22lego_show_track_url_base%22%3A%22%22%2C%22lego_version%22%3A%221.0.1%22%2C%22legoHuiDu%22%3Atrue%7D%7D",
          "tid": "",
          "isLiving": false,
          "isVR": false,
          "distanceDesc": "",
          "isGoldOwner": false
        },
        {
          "infoid": "2037297309443078",
          "cateName": "整租",
          "cateID": "11",
          "title": "沙园地铁口(月付)(可短租)拎包住(家电全)",
          "price": "500",
          "img": "https://pic1.ajkimg.com/display/anjuke/097f61e76ed629f8b0dcad000de614a1/240x180c.jpg?t=1&srotate=1",
          "unit": "元/月",
          "coll": "0",
          "subwayDistance": "距8号线-昌岗站步行1.8km",
          "fangwuLiangDian": [
            {
              "text": "整租",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "朝南",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "近地铁",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            }
          ],
          "area": "12㎡",
          "toward": "朝南",
          "shangquan": "江南西",
          "isAnxuan": "0",
          "location": false,
          "source_type": "1",
          "isauction": "1",
          "auctionShowText": "",
          "legoAuction": "%7B%22isauction%22%3A%221%22%2C%22lego%22%3A%7B%22lego_tid%22%3A%22%22%2C%22lego_ad_click_url%22%3A%22%22%2C%22lego_sid%22%3A%22141278743212895671227905755%22%2C%22platformCompany%22%3A%22ajk%22%2C%22lego_show_track_url_base%22%3A%22%22%2C%22lego_version%22%3A%221.0.1%22%2C%22legoHuiDu%22%3Atrue%7D%7D",
          "tid": "",
          "isLiving": false,
          "isVR": false,
          "distanceDesc": "",
          "isGoldOwner": false
        },
        {
          "infoid": "2030534727901186",
          "cateName": "整租",
          "cateID": "11",
          "title": "小1居高使用率，南北格局，中间楼层，紧邻地铁！！",
          "price": "1800",
          "img": "https://pic1.ajkimg.com/display/anjuke/626b505dfd4adc23a3830879903fcb4a/240x180c.jpg?t=1&srotate=1",
          "unit": "元/月",
          "coll": "0",
          "subwayDistance": "距8号线-昌岗站步行1.9km",
          "fangwuLiangDian": [
            {
              "text": "整租",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "南北",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "近地铁",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "精装修",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            }
          ],
          "area": "25㎡",
          "toward": "南北",
          "shangquan": "前进路",
          "isAnxuan": "0",
          "location": false,
          "source_type": "1",
          "isauction": "1",
          "auctionShowText": "",
          "legoAuction": "%7B%22isauction%22%3A%221%22%2C%22lego%22%3A%7B%22lego_tid%22%3A%22%22%2C%22lego_ad_click_url%22%3A%22%22%2C%22lego_sid%22%3A%22141278743212895671227905755%22%2C%22platformCompany%22%3A%22ajk%22%2C%22lego_show_track_url_base%22%3A%22%22%2C%22lego_version%22%3A%221.0.1%22%2C%22legoHuiDu%22%3Atrue%7D%7D",
          "tid": "",
          "isLiving": false,
          "isVR": false,
          "distanceDesc": "",
          "isGoldOwner": false
        },
        {
          "infoid": "2022296510833669",
          "cateName": "整租",
          "cateID": "11",
          "title": "距离2号线江泰路地铁站步行600米电梯楼，可以短租和长租",
          "price": "1500",
          "img": "https://pic1.ajkimg.com/display/anjuke/10cc05f8bd29f1269fa3b47cbc2ec29b/240x180c.jpg?t=1&srotate=1",
          "unit": "元/月",
          "coll": "0",
          "subwayDistance": "距8号线-昌岗站步行1.2km",
          "fangwuLiangDian": [
            {
              "text": "整租",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "东南",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "近地铁",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "精装修",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            }
          ],
          "area": "25㎡",
          "toward": "东南",
          "shangquan": "江燕路",
          "isAnxuan": "0",
          "location": false,
          "source_type": "1",
          "isauction": "1",
          "auctionShowText": "",
          "legoAuction": "%7B%22isauction%22%3A%221%22%2C%22lego%22%3A%7B%22lego_tid%22%3A%22%22%2C%22lego_ad_click_url%22%3A%22%22%2C%22lego_sid%22%3A%22141278743212895671227905755%22%2C%22platformCompany%22%3A%22ajk%22%2C%22lego_show_track_url_base%22%3A%22%22%2C%22lego_version%22%3A%221.0.1%22%2C%22legoHuiDu%22%3Atrue%7D%7D",
          "tid": "",
          "isLiving": false,
          "isVR": false,
          "distanceDesc": "",
          "isGoldOwner": false
        },
        {
          "infoid": "2034609369392140",
          "cateName": "整租",
          "cateID": "11",
          "title": "珠江医院宿舍 电梯房 1898元月 精装修 配套齐全",
          "price": "1898",
          "img": "https://pic1.ajkimg.com/display/anjuke/2d9bd0425003f6b05a7ec7dfd3475a51/240x180c.jpg?t=1&srotate=1",
          "unit": "元/月",
          "coll": "0",
          "subwayDistance": "距8号线-昌岗站步行1.5km",
          "fangwuLiangDian": [
            {
              "text": "整租",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "朝东",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "近地铁",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "有电梯",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "精装修",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            }
          ],
          "area": "11㎡",
          "toward": "朝东",
          "shangquan": "江燕路",
          "isAnxuan": "0",
          "location": false,
          "source_type": "1",
          "isauction": "1",
          "auctionShowText": "",
          "legoAuction": "%7B%22isauction%22%3A%221%22%2C%22lego%22%3A%7B%22lego_tid%22%3A%22%22%2C%22lego_ad_click_url%22%3A%22%22%2C%22lego_sid%22%3A%22141278743212895671227905755%22%2C%22platformCompany%22%3A%22ajk%22%2C%22lego_show_track_url_base%22%3A%22%22%2C%22lego_version%22%3A%221.0.1%22%2C%22legoHuiDu%22%3Atrue%7D%7D",
          "tid": "",
          "isLiving": false,
          "isVR": false,
          "distanceDesc": "",
          "isGoldOwner": false
        },
        {
          "infoid": "2039222681328653",
          "cateName": "整租",
          "cateID": "11",
          "title": "市二宫E出口  2楼大单间1800",
          "price": "1800",
          "img": "https://pic1.ajkimg.com/display/anjuke/2e2526a34a20772c77a30e70c9942323/240x180c.jpg?t=1&srotate=1",
          "unit": "元/月",
          "coll": "0",
          "subwayDistance": "距8号线-昌岗站步行1.9km",
          "fangwuLiangDian": [
            {
              "text": "整租",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "南北",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "近地铁",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "有电梯",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "精装修",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            }
          ],
          "area": "35㎡",
          "toward": "南北",
          "shangquan": "前进路",
          "isAnxuan": "0",
          "location": false,
          "source_type": "1",
          "isauction": "1",
          "auctionShowText": "",
          "legoAuction": "%7B%22isauction%22%3A%221%22%2C%22lego%22%3A%7B%22lego_tid%22%3A%22%22%2C%22lego_ad_click_url%22%3A%22%22%2C%22lego_sid%22%3A%22141278743212895671227905755%22%2C%22platformCompany%22%3A%22ajk%22%2C%22lego_show_track_url_base%22%3A%22%22%2C%22lego_version%22%3A%221.0.1%22%2C%22legoHuiDu%22%3Atrue%7D%7D",
          "tid": "",
          "isLiving": false,
          "isVR": false,
          "distanceDesc": "",
          "isGoldOwner": false
        },
        {
          "infoid": "2027322842586114",
          "cateName": "整租",
          "cateID": "11",
          "title": "昌岗2号线精装修公寓 可拎包入住单间",
          "price": "1880",
          "img": "https://pic1.ajkimg.com/display/anjuke/b011775cd767ee528e0257f2220074fc/240x180c.jpg?t=1&srotate=1",
          "unit": "元/月",
          "coll": "0",
          "subwayDistance": "距8号线-昌岗站步行312m",
          "fangwuLiangDian": [
            {
              "text": "整租",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "朝南",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "近地铁",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "精装修",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            }
          ],
          "area": "30㎡",
          "toward": "朝南",
          "shangquan": "昌岗",
          "isAnxuan": "0",
          "location": false,
          "source_type": "1",
          "isauction": "1",
          "auctionShowText": "",
          "legoAuction": "%7B%22isauction%22%3A%221%22%2C%22lego%22%3A%7B%22lego_tid%22%3A%22%22%2C%22lego_ad_click_url%22%3A%22%22%2C%22lego_sid%22%3A%22141278743212895671227905755%22%2C%22platformCompany%22%3A%22ajk%22%2C%22lego_show_track_url_base%22%3A%22%22%2C%22lego_version%22%3A%221.0.1%22%2C%22legoHuiDu%22%3Atrue%7D%7D",
          "tid": "",
          "isLiving": false,
          "isVR": false,
          "distanceDesc": "",
          "isGoldOwner": false
        },
        {
          "infoid": "1999326697743371",
          "cateName": "整租",
          "cateID": "11",
          "title": "珠江医院附近 精装修电梯房 2000元月 配套齐全拎包入住",
          "price": "2000",
          "img": "https://pic1.ajkimg.com/display/anjuke/b7be69d1094a61d5e3fce6a37af9fa65/240x180c.jpg?t=1&srotate=1",
          "unit": "元/月",
          "coll": "0",
          "subwayDistance": "距8号线-昌岗站步行1.3km",
          "fangwuLiangDian": [
            {
              "text": "整租",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "朝东",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "近地铁",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "有电梯",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "精装修",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            }
          ],
          "area": "17㎡",
          "toward": "朝东",
          "shangquan": "沙园",
          "isAnxuan": "0",
          "location": false,
          "source_type": "1",
          "isauction": "1",
          "auctionShowText": "",
          "legoAuction": "%7B%22isauction%22%3A%221%22%2C%22lego%22%3A%7B%22lego_tid%22%3A%22%22%2C%22lego_ad_click_url%22%3A%22%22%2C%22lego_sid%22%3A%22141278743212895671227905755%22%2C%22platformCompany%22%3A%22ajk%22%2C%22lego_show_track_url_base%22%3A%22%22%2C%22lego_version%22%3A%221.0.1%22%2C%22legoHuiDu%22%3Atrue%7D%7D",
          "tid": "",
          "isLiving": false,
          "isVR": false,
          "distanceDesc": "",
          "isGoldOwner": false
        },
        {
          "infoid": "2007956353916935",
          "cateName": "整租",
          "cateID": "11",
          "title": "市二宫小区 1室1厅1卫 1900元月 35平 南北通透",
          "price": "1900",
          "img": "https://pic1.ajkimg.com/display/anjuke/1fc65c6a7bc5e055893097049e6d4ad2/240x180c.jpg?t=1&srotate=1",
          "unit": "元/月",
          "coll": "0",
          "subwayDistance": "距8号线-昌岗站步行1.9km",
          "fangwuLiangDian": [
            {
              "text": "整租",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "东南",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "近地铁",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "精装修",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            }
          ],
          "area": "35㎡",
          "toward": "东南",
          "shangquan": "前进路",
          "isAnxuan": "0",
          "location": false,
          "source_type": "1",
          "isauction": "1",
          "auctionShowText": "",
          "legoAuction": "%7B%22isauction%22%3A%221%22%2C%22lego%22%3A%7B%22lego_tid%22%3A%22%22%2C%22lego_ad_click_url%22%3A%22%22%2C%22lego_sid%22%3A%22141278743212895671227905755%22%2C%22platformCompany%22%3A%22ajk%22%2C%22lego_show_track_url_base%22%3A%22%22%2C%22lego_version%22%3A%221.0.1%22%2C%22legoHuiDu%22%3Atrue%7D%7D",
          "tid": "",
          "isLiving": false,
          "isVR": false,
          "distanceDesc": "",
          "isGoldOwner": false
        },
        {
          "infoid": "1990605506159616",
          "cateName": "整租",
          "cateID": "11",
          "title": "精装电梯房 一房一厅780 采光好 家私家电齐全随时看房",
          "price": "780",
          "img": "https://pic1.ajkimg.com/display/anjuke/e0ad931ff370a5feffa5a3db506ee5a9/240x180c.jpg?t=1&srotate=1",
          "unit": "元/月",
          "coll": "0",
          "subwayDistance": "距8号线-昌岗站步行614m",
          "fangwuLiangDian": [
            {
              "text": "整租",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "朝东",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "近地铁",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "有电梯",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "精装修",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            }
          ],
          "area": "38㎡",
          "toward": "朝东",
          "shangquan": "江南西",
          "isAnxuan": "0",
          "location": false,
          "source_type": "1",
          "isauction": "1",
          "auctionShowText": "",
          "legoAuction": "%7B%22isauction%22%3A%221%22%2C%22lego%22%3A%7B%22lego_tid%22%3A%22%22%2C%22lego_ad_click_url%22%3A%22%22%2C%22lego_sid%22%3A%22141278743212895671227905755%22%2C%22platformCompany%22%3A%22ajk%22%2C%22lego_show_track_url_base%22%3A%22%22%2C%22lego_version%22%3A%221.0.1%22%2C%22legoHuiDu%22%3Atrue%7D%7D",
          "tid": "",
          "isLiving": false,
          "isVR": false,
          "distanceDesc": "",
          "isGoldOwner": false
        },
        {
          "infoid": "2000511609347083",
          "cateName": "整租",
          "cateID": "11",
          "title": "珠江医院附近  基建新村 1房1厅 家私电器齐全",
          "price": "1900",
          "img": "https://pic1.ajkimg.com/display/anjuke/d2fd53b88e5a85b4723b0990806596cd/240x180c.jpg?t=1&srotate=1",
          "unit": "元/月",
          "coll": "0",
          "subwayDistance": "距8号线-昌岗站步行1.7km",
          "fangwuLiangDian": [
            {
              "text": "整租",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "东南",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "近地铁",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            }
          ],
          "area": "45㎡",
          "toward": "东南",
          "shangquan": "昌岗",
          "isAnxuan": "0",
          "location": false,
          "source_type": "1",
          "isauction": "1",
          "auctionShowText": "",
          "legoAuction": "%7B%22isauction%22%3A%221%22%2C%22lego%22%3A%7B%22lego_tid%22%3A%22%22%2C%22lego_ad_click_url%22%3A%22%22%2C%22lego_sid%22%3A%22141278743212895671227905755%22%2C%22platformCompany%22%3A%22ajk%22%2C%22lego_show_track_url_base%22%3A%22%22%2C%22lego_version%22%3A%221.0.1%22%2C%22legoHuiDu%22%3Atrue%7D%7D",
          "tid": "",
          "isLiving": false,
          "isVR": false,
          "distanceDesc": "",
          "isGoldOwner": false
        },
        {
          "infoid": "1980624245496846",
          "cateName": "整租",
          "cateID": "11",
          "title": "怡乐路小区 1室1厅1卫 1900元月 精装修 30平",
          "price": "1900",
          "img": "https://pic1.ajkimg.com/display/anjuke/ac2b6b303e2013865c858d744c394917/240x180c.jpg?t=1&srotate=1",
          "unit": "元/月",
          "coll": "0",
          "subwayDistance": "距8号线-昌岗站步行1.5km",
          "fangwuLiangDian": [
            {
              "text": "整租",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "朝南",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "近地铁",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "精装修",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            }
          ],
          "area": "30㎡",
          "toward": "朝南",
          "shangquan": "江南西",
          "isAnxuan": "0",
          "location": false,
          "source_type": "1",
          "isauction": "1",
          "auctionShowText": "",
          "legoAuction": "%7B%22isauction%22%3A%221%22%2C%22lego%22%3A%7B%22lego_tid%22%3A%22%22%2C%22lego_ad_click_url%22%3A%22%22%2C%22lego_sid%22%3A%22141278743212895671227905755%22%2C%22platformCompany%22%3A%22ajk%22%2C%22lego_show_track_url_base%22%3A%22%22%2C%22lego_version%22%3A%221.0.1%22%2C%22legoHuiDu%22%3Atrue%7D%7D",
          "tid": "",
          "isLiving": false,
          "isVR": false,
          "distanceDesc": "",
          "isGoldOwner": false
        },
        {
          "infoid": "2025061322120204",
          "cateName": "整租",
          "cateID": "11",
          "title": "3号线 地铁口 毕业季特惠 海珠区 绿码地区 直租 拎包入住",
          "price": "1820",
          "img": "https://pic1.ajkimg.com/display/anjuke/5e11899351172614491fad7f2aeb3db5/240x180c.jpg?t=1&srotate=1",
          "unit": "元/月",
          "coll": "0",
          "subwayDistance": "距8号线-昌岗站步行1.7km",
          "fangwuLiangDian": [
            {
              "text": "整租",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "朝北",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "近地铁",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "有电梯",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "精装修",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            }
          ],
          "area": "28㎡",
          "toward": "朝北",
          "shangquan": "东晓南",
          "isAnxuan": "0",
          "location": false,
          "source_type": "1",
          "isauction": "1",
          "auctionShowText": "",
          "legoAuction": "%7B%22isauction%22%3A%221%22%2C%22lego%22%3A%7B%22lego_tid%22%3A%22%22%2C%22lego_ad_click_url%22%3A%22%22%2C%22lego_sid%22%3A%22141278743212895671227905755%22%2C%22platformCompany%22%3A%22ajk%22%2C%22lego_show_track_url_base%22%3A%22%22%2C%22lego_version%22%3A%221.0.1%22%2C%22legoHuiDu%22%3Atrue%7D%7D",
          "tid": "",
          "isLiving": false,
          "isVR": false,
          "distanceDesc": "",
          "isGoldOwner": false
        },
        {
          "infoid": "2017688323234822",
          "cateName": "整租",
          "cateID": "11",
          "title": "嘉丽苑，东南向，小两房，有钥匙",
          "price": "1900",
          "img": "https://pic1.ajkimg.com/display/anjuke/1e3f21fc615b14d40cae5cc690ca7482/240x180c.jpg?t=1&srotate=1",
          "unit": "元/月",
          "coll": "0",
          "subwayDistance": "距8号线-昌岗站步行1.3km",
          "fangwuLiangDian": [
            {
              "text": "整租",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "东南",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "近地铁",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            }
          ],
          "area": "50㎡",
          "toward": "东南",
          "shangquan": "江南西",
          "isAnxuan": "0",
          "location": false,
          "source_type": "1",
          "isauction": "1",
          "auctionShowText": "",
          "legoAuction": "%7B%22isauction%22%3A%221%22%2C%22lego%22%3A%7B%22lego_tid%22%3A%22%22%2C%22lego_ad_click_url%22%3A%22%22%2C%22lego_sid%22%3A%22141278743212895671227905755%22%2C%22platformCompany%22%3A%22ajk%22%2C%22lego_show_track_url_base%22%3A%22%22%2C%22lego_version%22%3A%221.0.1%22%2C%22legoHuiDu%22%3Atrue%7D%7D",
          "tid": "",
          "isLiving": false,
          "isVR": false,
          "distanceDesc": "",
          "isGoldOwner": false
        },
        {
          "infoid": "2023751548264457",
          "cateName": "整租",
          "cateID": "11",
          "title": "一个月起租 全新公寓房欧式简约风格 家私家电齐全赠送wifi",
          "price": "1500",
          "img": "https://pic1.ajkimg.com/display/anjuke/79dbc8fe294eb6c4cade3c5a2142ab93/240x180c.jpg?t=1&srotate=1",
          "unit": "元/月",
          "coll": "0",
          "subwayDistance": "距8号线-昌岗站步行1.9km",
          "fangwuLiangDian": [
            {
              "text": "整租",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "南北",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "近地铁",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "有电梯",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "精装修",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            }
          ],
          "area": "24㎡",
          "toward": "南北",
          "shangquan": "中大",
          "isAnxuan": "0",
          "location": false,
          "source_type": "1",
          "isauction": "1",
          "auctionShowText": "",
          "legoAuction": "%7B%22isauction%22%3A%221%22%2C%22lego%22%3A%7B%22lego_tid%22%3A%22%22%2C%22lego_ad_click_url%22%3A%22%22%2C%22lego_sid%22%3A%22141278743212895671227905755%22%2C%22platformCompany%22%3A%22ajk%22%2C%22lego_show_track_url_base%22%3A%22%22%2C%22lego_version%22%3A%221.0.1%22%2C%22legoHuiDu%22%3Atrue%7D%7D",
          "tid": "",
          "isLiving": false,
          "isVR": false,
          "distanceDesc": "",
          "isGoldOwner": false
        },
        {
          "infoid": "2042923389771788",
          "cateName": "整租",
          "cateID": "11",
          "title": "昌岗东一巷小区 1室0厅1卫 600元月 23平",
          "price": "600",
          "img": "https://pic1.ajkimg.com/display/anjuke/f477c17f3f02c7b799b80bc7c07bbbbe/240x180c.jpg?t=1&srotate=1",
          "unit": "元/月",
          "coll": "0",
          "subwayDistance": "距8号线-昌岗站步行614m",
          "fangwuLiangDian": [
            {
              "text": "整租",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "朝南",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "近地铁",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            }
          ],
          "area": "23㎡",
          "toward": "朝南",
          "shangquan": "江南西",
          "isAnxuan": "0",
          "location": false,
          "source_type": "1",
          "isauction": "1",
          "auctionShowText": "",
          "legoAuction": "%7B%22isauction%22%3A%221%22%2C%22lego%22%3A%7B%22lego_tid%22%3A%22%22%2C%22lego_ad_click_url%22%3A%22%22%2C%22lego_sid%22%3A%22141278743212895671227905755%22%2C%22platformCompany%22%3A%22ajk%22%2C%22lego_show_track_url_base%22%3A%22%22%2C%22lego_version%22%3A%221.0.1%22%2C%22legoHuiDu%22%3Atrue%7D%7D",
          "tid": "",
          "isLiving": false,
          "isVR": false,
          "distanceDesc": "",
          "isGoldOwner": false
        },
        {
          "infoid": "1985358310712330",
          "cateName": "整租",
          "cateID": "11",
          "title": "珠江医院宿舍附近 配套齐全 1950元月 南北通透",
          "price": "1950",
          "img": "https://pic1.ajkimg.com/display/anjuke/03a1eaa65edf7c0e8148cad069298059/240x180c.jpg?t=1&srotate=1",
          "unit": "元/月",
          "coll": "0",
          "subwayDistance": "距8号线-昌岗站步行1.5km",
          "fangwuLiangDian": [
            {
              "text": "整租",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "朝东",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "近地铁",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "有电梯",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "精装修",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            }
          ],
          "area": "12㎡",
          "toward": "朝东",
          "shangquan": "江燕路",
          "isAnxuan": "0",
          "location": false,
          "source_type": "1",
          "isauction": "1",
          "auctionShowText": "",
          "legoAuction": "%7B%22isauction%22%3A%221%22%2C%22lego%22%3A%7B%22lego_tid%22%3A%22%22%2C%22lego_ad_click_url%22%3A%22%22%2C%22lego_sid%22%3A%22141278743212895671227905755%22%2C%22platformCompany%22%3A%22ajk%22%2C%22lego_show_track_url_base%22%3A%22%22%2C%22lego_version%22%3A%221.0.1%22%2C%22legoHuiDu%22%3Atrue%7D%7D",
          "tid": "",
          "isLiving": false,
          "isVR": false,
          "distanceDesc": "",
          "isGoldOwner": false
        },
        {
          "infoid": "2030132283581454",
          "cateName": "整租",
          "cateID": "11",
          "title": "江南西小区 1室0厅1卫 1850元月 精装修 南北通透",
          "price": "1850",
          "img": "https://pic1.ajkimg.com/display/anjuke/4fd591ba447ea30f054934e0ce1efe28/240x180c.jpg?t=1&srotate=1",
          "unit": "元/月",
          "coll": "0",
          "subwayDistance": "距8号线-昌岗站步行1.3km",
          "fangwuLiangDian": [
            {
              "text": "整租",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "东南",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "近地铁",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "精装修",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            }
          ],
          "area": "35㎡",
          "toward": "东南",
          "shangquan": "江南西",
          "isAnxuan": "0",
          "location": false,
          "source_type": "1",
          "isauction": "1",
          "auctionShowText": "",
          "legoAuction": "%7B%22isauction%22%3A%221%22%2C%22lego%22%3A%7B%22lego_tid%22%3A%22%22%2C%22lego_ad_click_url%22%3A%22%22%2C%22lego_sid%22%3A%22141278743212895671227905755%22%2C%22platformCompany%22%3A%22ajk%22%2C%22lego_show_track_url_base%22%3A%22%22%2C%22lego_version%22%3A%221.0.1%22%2C%22legoHuiDu%22%3Atrue%7D%7D",
          "tid": "",
          "isLiving": false,
          "isVR": false,
          "distanceDesc": "",
          "isGoldOwner": false
        },
        {
          "infoid": "2044350440087554",
          "cateName": "整租",
          "cateID": "11",
          "title": "晓怡居 全新精装修公寓，性价比高，采光好拎包入住",
          "price": "560",
          "img": "https://pic1.ajkimg.com/display/anjuke/bc356e9d64b1ee66ecbb6c488aab52c6/240x180c.jpg?t=1&srotate=1",
          "unit": "元/月",
          "coll": "0",
          "subwayDistance": "距8号线-昌岗站步行830m",
          "fangwuLiangDian": [
            {
              "text": "整租",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "东南",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "近地铁",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            }
          ],
          "area": "20㎡",
          "toward": "东南",
          "shangquan": "晓港",
          "isAnxuan": "0",
          "location": false,
          "source_type": "1",
          "isauction": "1",
          "auctionShowText": "",
          "legoAuction": "%7B%22isauction%22%3A%221%22%2C%22lego%22%3A%7B%22lego_tid%22%3A%22%22%2C%22lego_ad_click_url%22%3A%22%22%2C%22lego_sid%22%3A%22141278743212895671227905755%22%2C%22platformCompany%22%3A%22ajk%22%2C%22lego_show_track_url_base%22%3A%22%22%2C%22lego_version%22%3A%221.0.1%22%2C%22legoHuiDu%22%3Atrue%7D%7D",
          "tid": "",
          "isLiving": false,
          "isVR": false,
          "distanceDesc": "",
          "isGoldOwner": false
        },
        {
          "infoid": "2041838783963151",
          "cateName": "整租",
          "cateID": "11",
          "title": "远洋宿舍，晓港，中大交通方便，精装大单间，装修好，价格便宜",
          "price": "1700",
          "img": "https://pic1.ajkimg.com/display/anjuke/a251d9b809588812135a5f93952fbecf/240x180c.jpg?t=1&srotate=1",
          "unit": "元/月",
          "coll": "0",
          "subwayDistance": "距8号线-昌岗站步行1.2km",
          "fangwuLiangDian": [
            {
              "text": "整租",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "南北",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "近地铁",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "精装修",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            }
          ],
          "area": "30㎡",
          "toward": "南北",
          "shangquan": "新港西",
          "isAnxuan": "0",
          "location": false,
          "source_type": "1",
          "isauction": "1",
          "auctionShowText": "",
          "legoAuction": "%7B%22isauction%22%3A%221%22%2C%22lego%22%3A%7B%22lego_tid%22%3A%22%22%2C%22lego_ad_click_url%22%3A%22%22%2C%22lego_sid%22%3A%22141278743212895671227905755%22%2C%22platformCompany%22%3A%22ajk%22%2C%22lego_show_track_url_base%22%3A%22%22%2C%22lego_version%22%3A%221.0.1%22%2C%22legoHuiDu%22%3Atrue%7D%7D",
          "tid": "",
          "isLiving": false,
          "isVR": false,
          "distanceDesc": "",
          "isGoldOwner": false
        },
        {
          "infoid": "2006578396111886",
          "cateName": "整租",
          "cateID": "11",
          "title": "市二宫商圈大单间，交通便利，随时看房",
          "price": "1500",
          "img": "https://pic1.ajkimg.com/display/anjuke/5fcd34e3efc7dbc09091152b14083ada/240x180c.jpg?t=1&srotate=1",
          "unit": "元/月",
          "coll": "0",
          "subwayDistance": "距8号线-昌岗站步行1.9km",
          "fangwuLiangDian": [
            {
              "text": "整租",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "东南",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "近地铁",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "精装修",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            }
          ],
          "area": "15㎡",
          "toward": "东南",
          "shangquan": "前进路",
          "isAnxuan": "0",
          "location": false,
          "source_type": "1",
          "isauction": "1",
          "auctionShowText": "",
          "legoAuction": "%7B%22isauction%22%3A%221%22%2C%22lego%22%3A%7B%22lego_tid%22%3A%22%22%2C%22lego_ad_click_url%22%3A%22%22%2C%22lego_sid%22%3A%22141278743212895671227905755%22%2C%22platformCompany%22%3A%22ajk%22%2C%22lego_show_track_url_base%22%3A%22%22%2C%22lego_version%22%3A%221.0.1%22%2C%22legoHuiDu%22%3Atrue%7D%7D",
          "tid": "",
          "isLiving": false,
          "isVR": false,
          "distanceDesc": "",
          "isGoldOwner": false
        },
        {
          "infoid": "1955226193552385",
          "cateName": "整租",
          "cateID": "11",
          "title": "宝业路电梯楼 1室1厅1卫 2000元月 30平 电梯房",
          "price": "2000",
          "img": "https://pic1.ajkimg.com/display/anjuke/0d67e44163879c2cc0ee71a339e0f184/240x180c.jpg?t=1&srotate=1",
          "unit": "元/月",
          "coll": "0",
          "subwayDistance": "距8号线-昌岗站步行1.6km",
          "fangwuLiangDian": [
            {
              "text": "整租",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "东南",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "近地铁",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "有电梯",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "精装修",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            }
          ],
          "area": "30㎡",
          "toward": "东南",
          "shangquan": "沙园",
          "isAnxuan": "0",
          "location": false,
          "source_type": "1",
          "isauction": "1",
          "auctionShowText": "",
          "legoAuction": "%7B%22isauction%22%3A%221%22%2C%22lego%22%3A%7B%22lego_tid%22%3A%22%22%2C%22lego_ad_click_url%22%3A%22%22%2C%22lego_sid%22%3A%22141278743212895671227905755%22%2C%22platformCompany%22%3A%22ajk%22%2C%22lego_show_track_url_base%22%3A%22%22%2C%22lego_version%22%3A%221.0.1%22%2C%22legoHuiDu%22%3Atrue%7D%7D",
          "tid": "",
          "isLiving": false,
          "isVR": false,
          "distanceDesc": "",
          "isGoldOwner": false
        },
        {
          "infoid": "2030193217313803",
          "cateName": "整租",
          "cateID": "11",
          "title": "2号线直达江南西海珠广场公园前 豪华一室 可做饭 可押一",
          "price": "1680",
          "img": "https://pic1.ajkimg.com/display/anjuke/8d898458ed5f6982f0cf654bc1dd2ceb/240x180c.jpg?t=1&srotate=1",
          "unit": "元/月",
          "coll": "0",
          "subwayDistance": "距8号线-昌岗站步行1.9km",
          "fangwuLiangDian": [
            {
              "text": "整租",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "朝南",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "近地铁",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "有电梯",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "豪华装修",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            }
          ],
          "area": "30㎡",
          "toward": "朝南",
          "shangquan": "前进路",
          "isAnxuan": "0",
          "location": false,
          "source_type": "1",
          "isauction": "1",
          "auctionShowText": "",
          "legoAuction": "%7B%22isauction%22%3A%221%22%2C%22lego%22%3A%7B%22lego_tid%22%3A%22%22%2C%22lego_ad_click_url%22%3A%22%22%2C%22lego_sid%22%3A%22141278743212895671227905755%22%2C%22platformCompany%22%3A%22ajk%22%2C%22lego_show_track_url_base%22%3A%22%22%2C%22lego_version%22%3A%221.0.1%22%2C%22legoHuiDu%22%3Atrue%7D%7D",
          "tid": "",
          "isLiving": false,
          "isVR": false,
          "distanceDesc": "",
          "isGoldOwner": false
        },
        {
          "infoid": "1827210178371593",
          "cateName": "整租",
          "cateID": "11",
          "title": "直租海珠区泰沙路大马路边电梯房，无中介，家私家电齐全",
          "price": "1500",
          "img": "https://pic1.ajkimg.com/display/anjuke/f327c7676426c874200cabadf0c78346/240x180c.jpg?t=1&srotate=1",
          "unit": "元/月",
          "coll": "0",
          "subwayDistance": "距8号线-昌岗站步行1.7km",
          "fangwuLiangDian": [
            {
              "text": "整租",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "朝西",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "近地铁",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "有电梯",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "精装修",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            }
          ],
          "area": "35㎡",
          "toward": "朝西",
          "shangquan": "江燕路",
          "isAnxuan": "0",
          "location": false,
          "source_type": "1",
          "isauction": "1",
          "auctionShowText": "",
          "legoAuction": "%7B%22isauction%22%3A%221%22%2C%22lego%22%3A%7B%22lego_tid%22%3A%22%22%2C%22lego_ad_click_url%22%3A%22%22%2C%22lego_sid%22%3A%22141278743212895671227905755%22%2C%22platformCompany%22%3A%22ajk%22%2C%22lego_show_track_url_base%22%3A%22%22%2C%22lego_version%22%3A%221.0.1%22%2C%22legoHuiDu%22%3Atrue%7D%7D",
          "tid": "",
          "isLiving": false,
          "isVR": false,
          "distanceDesc": "",
          "isGoldOwner": false
        },
        {
          "infoid": "1939726259348480",
          "cateName": "整租",
          "cateID": "11",
          "title": "江南西 聚龙新街 两房一厅有阳台 家电齐租价低",
          "price": "2000",
          "img": "https://pic1.ajkimg.com/display/anjuke/ce6e40116c78999eb5900be3b80f4dc0/240x180c.jpg?t=1&srotate=1",
          "unit": "元/月",
          "coll": "0",
          "subwayDistance": "距8号线-昌岗站步行1.2km",
          "fangwuLiangDian": [
            {
              "text": "整租",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "东南",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "近地铁",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            }
          ],
          "area": "65㎡",
          "toward": "东南",
          "shangquan": "江南西",
          "isAnxuan": "0",
          "location": false,
          "source_type": "1",
          "isauction": "1",
          "auctionShowText": "",
          "legoAuction": "%7B%22isauction%22%3A%221%22%2C%22lego%22%3A%7B%22lego_tid%22%3A%22%22%2C%22lego_ad_click_url%22%3A%22%22%2C%22lego_sid%22%3A%22141278743212895671227905755%22%2C%22platformCompany%22%3A%22ajk%22%2C%22lego_show_track_url_base%22%3A%22%22%2C%22lego_version%22%3A%221.0.1%22%2C%22legoHuiDu%22%3Atrue%7D%7D",
          "tid": "",
          "isLiving": false,
          "isVR": false,
          "distanceDesc": "",
          "isGoldOwner": false
        },
        {
          "infoid": "1984788939563009",
          "cateName": "整租",
          "cateID": "11",
          "title": "[整租]与之8号线 凤凰新村沙园 配套齐全通勤方便 独立厨卫",
          "price": "1888",
          "img": "https://pic1.ajkimg.com/display/anjuke/8f8dda3f1a262929c7ce55f745f22f1d/240x180c.jpg?t=1&srotate=1",
          "unit": "元/月",
          "coll": "0",
          "subwayDistance": "距8号线-昌岗站步行1.5km",
          "fangwuLiangDian": [
            {
              "text": "整租",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "朝南",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "近地铁",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "有电梯",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "精装修",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            }
          ],
          "area": "30㎡",
          "toward": "朝南",
          "shangquan": "沙园",
          "isAnxuan": "0",
          "location": false,
          "source_type": "1",
          "isauction": "1",
          "auctionShowText": "",
          "legoAuction": "%7B%22isauction%22%3A%221%22%2C%22lego%22%3A%7B%22lego_tid%22%3A%22%22%2C%22lego_ad_click_url%22%3A%22%22%2C%22lego_sid%22%3A%22141278743212895671227905755%22%2C%22platformCompany%22%3A%22ajk%22%2C%22lego_show_track_url_base%22%3A%22%22%2C%22lego_version%22%3A%221.0.1%22%2C%22legoHuiDu%22%3Atrue%7D%7D",
          "tid": "",
          "isLiving": false,
          "isVR": false,
          "distanceDesc": "",
          "isGoldOwner": false
        },
        {
          "infoid": "1975453740704783",
          "cateName": "整租",
          "cateID": "11",
          "title": "海珠区泰沙路金紫里电梯房超靓电梯单间，家电齐全拎包入住",
          "price": "1700",
          "img": "https://pic1.ajkimg.com/display/anjuke/6e72822e526bea1451337d9cc54cd7ec/240x180c.jpg?t=1&srotate=1",
          "unit": "元/月",
          "coll": "0",
          "subwayDistance": "距8号线-昌岗站步行1.7km",
          "fangwuLiangDian": [
            {
              "text": "整租",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "东南",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "近地铁",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "有电梯",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "精装修",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            }
          ],
          "area": "28㎡",
          "toward": "东南",
          "shangquan": "江燕路",
          "isAnxuan": "0",
          "location": false,
          "source_type": "1",
          "isauction": "1",
          "auctionShowText": "",
          "legoAuction": "%7B%22isauction%22%3A%221%22%2C%22lego%22%3A%7B%22lego_tid%22%3A%22%22%2C%22lego_ad_click_url%22%3A%22%22%2C%22lego_sid%22%3A%22141278743212895671227905755%22%2C%22platformCompany%22%3A%22ajk%22%2C%22lego_show_track_url_base%22%3A%22%22%2C%22lego_version%22%3A%221.0.1%22%2C%22legoHuiDu%22%3Atrue%7D%7D",
          "tid": "",
          "isLiving": false,
          "isVR": false,
          "distanceDesc": "",
          "isGoldOwner": false
        }
      ]
    }
  }