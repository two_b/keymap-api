var ZUFANG_4 = {
    "code": "0",
    "message": "OK",
    "data": {
        "isToast": false,
        "activity": {
            "title": ""
        },
        "configs": {
            "banner": [],
            "houseSubject": []
        },
        "tkd": {
            "title": "8号线中大整租0_2000租房网，广州8号线中大整租0_2000租房信息，8号线中大整租0_2000房屋出租价格-广州58安居客",
            "keywords": "8号线中大整租0_2000租房，8号线中大整租0_2000出租房，8号线中大整租0_2000租房子",
            "description": "安居客广州租房网为您提供8号线中大整租0_2000租房信息,包括广州二手房出租、新房出租，租房子,找广州出租房,就上58安居客广州租房网。"
        },
        "gongyu": {
            "isShow": true
        },
        "goldOwnerCity": false,
        "list": [
            {
                "infoid": "2029547079557135",
                "cateName": "整租",
                "cateID": "11",
                "title": "中大地铁 周边环境保证安全从不接住闲杂人等,租得放心住得舒心",
                "price": "800",
                "img": "https://pic1.ajkimg.com/display/anjuke/ece58a1c6d9ea706b48f0488b4e73fb6/240x180c.jpg?t=1&srotate=1",
                "unit": "元/月",
                "coll": "0",
                "subwayDistance": "距8号线-中大站步行1.8km",
                "fangwuLiangDian": [
                    {
                        "text": "整租",
                        "textColor": "#7397C1",
                        "bgColor": "#7397C1"
                    },
                    {
                        "text": "南北",
                        "textColor": "#7397C1",
                        "bgColor": "#7397C1"
                    },
                    {
                        "text": "近地铁",
                        "textColor": "#7397C1",
                        "bgColor": "#7397C1"
                    },
                    {
                        "text": "有电梯",
                        "textColor": "#7397C1",
                        "bgColor": "#7397C1"
                    },
                    {
                        "text": "精装修",
                        "textColor": "#7397C1",
                        "bgColor": "#7397C1"
                    }
                ],
                "area": "35㎡",
                "toward": "南北",
                "shangquan": "中大",
                "isAnxuan": "0",
                "location": false,
                "source_type": "1",
                "isauction": "6",
                "auctionShowText": "广告",
                "legoAuction": "%7B%22isauction%22%3A%226%22%2C%22lego%22%3A%7B%22lego_tid%22%3A%226f598044-1c2a-49f6-9310-6de6c22e7001%22%2C%22lego_ad_click_url%22%3A%22https%3A%5C%2F%5C%2Flegoclick.58.com%5C%2Fjump%3Ftarget%3DpZwY0ZnlsztdraOWUvYKuaYLnhPhnHmQPBYLuW9QsHw-mhDVrHbYnzdhuycQuHn3nvcOnvcKnWTknWmLnWm1TEDznjcOPHELnj0OPHNLnHndTHTKnHTkTHD_nHDKnkDQPWcYnWnOPHc3P1bYTHDKOplFOUA7OplBOlXxOCB4OUa5OlBsOUJ5sXi38Syc-SB6JrpCCXX1B8pEMrXYC9DQTHDKTHnKUvPHPLDkHD-ogLIvibPOINIimYIaI1IgXNPdwEDQPjTdrHcQrHEOP10kPHbznWNvTHmK0A7zmyd1n1NYrWcdP19_ph-6wvNln7tznjTksZK60h7V01ndn1C1P9DQTHnzrAPhm1FBsHcdnANVPjcYPzd6n166sywhrjmvn1m1PH0kmEDQPjTdrHcQrHNknjD3njNznjbvTHDYnjNOnWDOPjbOPjcdPH9LnWTKTHD_nHDKTEDKsEDKTEDKPH98nWE3sWmvsWDvnkDkTHTKTHD_nHDKXLYKnHTkPHEKnHTknWndrEDvuWNOrjTYPaYQm1F6sHEOuWmVrHnQnaYvuANvm1czuH0knjDKTHEKIL9kuHD1mhNYmHN1n1m3PjmvTy7JpdI-mv66ITD1THn_nHmdPzkvn193THDKUMR_UTDkP1D1uAD3uhPBnhn1nj6B%22%2C%22lego_sid%22%3A%22110967499212892323512397686%22%2C%22platformCompany%22%3A%22ajk%22%2C%22lego_show_track_url_base%22%3A%22https%3A%5C%2F%5C%2Fadtrack.58.com%5C%2F1002359%5C%2F%22%2C%22lego_version%22%3A%221.0.1%22%2C%22legoHuiDu%22%3Atrue%7D%7D",
                "tid": "6f598044-1c2a-49f6-9310-6de6c22e7001",
                "isLiving": false,
                "isVR": false,
                "distanceDesc": "",
                "isGoldOwner": false
            },
            {
                "infoid": "2040870305766400",
                "cateName": "整租",
                "cateID": "11",
                "title": "海珠区江泰路泰沙路地铁2号线 中大对面新房大阳台出租拎包入住",
                "price": "1500",
                "img": "https://pic1.ajkimg.com/display/anjuke/a23dff7d03d33709b2141415ec9d793a/240x180c.jpg?t=1&srotate=1",
                "unit": "元/月",
                "coll": "0",
                "subwayDistance": "距8号线-中大站步行1.9km",
                "fangwuLiangDian": [
                    {
                        "text": "整租",
                        "textColor": "#7397C1",
                        "bgColor": "#7397C1"
                    },
                    {
                        "text": "朝南",
                        "textColor": "#7397C1",
                        "bgColor": "#7397C1"
                    },
                    {
                        "text": "近地铁",
                        "textColor": "#7397C1",
                        "bgColor": "#7397C1"
                    },
                    {
                        "text": "精装修",
                        "textColor": "#7397C1",
                        "bgColor": "#7397C1"
                    }
                ],
                "area": "33㎡",
                "toward": "朝南",
                "shangquan": "宝岗",
                "isAnxuan": "0",
                "location": false,
                "source_type": "1",
                "isauction": "6",
                "auctionShowText": "",
                "legoAuction": "%7B%22isauction%22%3A%226%22%2C%22lego%22%3A%7B%22lego_tid%22%3A%22530ce167-381f-4570-94c5-8b5e4d14a441%22%2C%22lego_ad_click_url%22%3A%22https%3A%5C%2F%5C%2Flegoclick.58.com%5C%2Fjump%3Ftarget%3DpZwY0ZnlsztdraOWUvYKuaYLnhPhnHmQPBYLuW9QsHw-mhDVrHbYnzdhuycQuHn3nvcOnvcKnWTkrHTYnjmzTEDznjEkrj0kn1TdP1mvPjTkTHTKnHTkTHD_nHDKnkDQPWcYnWnOPHc3P1bYTHcKOplFOUA7OplBOlXxOCB4OUa5OlBsOUJ5sXi38Syc-SB6JrpCCXX1B8pEMrXYC9DQTHDKTHnKUvPHPLDkHD-ogLIvibPOINIimYIaI1IgXNPdwEDQPjTvPjcLPjbknHn1Pj91PHcQTHmK0A7zmyd1n1NYrWcdP19_ph-6wvNln7tznjTksZK60h7V01ndn1C1P9DQTyNQrjnYmWubsyNduj0VPAFBPzdBP1N1sycLPW-BryNYPHD1nEDQPjTvPjcLPjbknW9YPj9zPHmkTHDYnjmYnW0YrHTznjTdrHc1rjEKTHD_nHDKTEDKsEDKTEDKPH98nWE3sWmvsWDvnkDkTHTKTHD_nHDKXLYKnHTkPHEKnHTknWndrEDdn1KWuHDvPzY1rj7hsHEdP1TVrHwWPiY3mWR-PAEQPADYPjDKTHEKIL9kuHD1mhNYmHN1n1m3PjmvTy7JpdI-mv66ITD1THn_nHmdPzkzP1TLTHDKUMR_UT7-uWmYrywBuH9QnhmLuAc1%22%2C%22lego_sid%22%3A%22110967499212892323512397686%22%2C%22platformCompany%22%3A%22ajk%22%2C%22lego_show_track_url_base%22%3A%22https%3A%5C%2F%5C%2Fadtrack.58.com%5C%2F1002359%5C%2F%22%2C%22lego_version%22%3A%221.0.1%22%2C%22legoHuiDu%22%3Atrue%7D%7D",
                "tid": "530ce167-381f-4570-94c5-8b5e4d14a441",
                "isLiving": false,
                "isVR": false,
                "distanceDesc": "",
                "isGoldOwner": false
            },
            {
                "infoid": "2012135352966158",
                "cateName": "整租",
                "cateID": "11",
                "title": "地 铁 房 中大对面布匹市场 可停放电动车 新房出租拎包入住",
                "price": "1500",
                "img": "https://pic1.ajkimg.com/display/anjuke/3fec70b089138f636ed2320c1d469e4e/240x180c.jpg?t=1&srotate=1",
                "unit": "元/月",
                "coll": "0",
                "subwayDistance": "距8号线-中大站步行1.7km",
                "fangwuLiangDian": [
                    {
                        "text": "整租",
                        "textColor": "#7397C1",
                        "bgColor": "#7397C1"
                    },
                    {
                        "text": "朝南",
                        "textColor": "#7397C1",
                        "bgColor": "#7397C1"
                    },
                    {
                        "text": "近地铁",
                        "textColor": "#7397C1",
                        "bgColor": "#7397C1"
                    },
                    {
                        "text": "精装修",
                        "textColor": "#7397C1",
                        "bgColor": "#7397C1"
                    }
                ],
                "area": "28㎡",
                "toward": "朝南",
                "shangquan": "江南西",
                "isAnxuan": "0",
                "location": false,
                "source_type": "1",
                "isauction": "6",
                "auctionShowText": "",
                "legoAuction": "%7B%22isauction%22%3A%226%22%2C%22lego%22%3A%7B%22lego_tid%22%3A%225d9d57c3-8d22-4a39-9423-6fc895a296ac%22%2C%22lego_ad_click_url%22%3A%22https%3A%5C%2F%5C%2Flegoclick.58.com%5C%2Fjump%3Ftarget%3DpZwY0ZnlsztdraOWUvYKuaYLnhPhnHmQPBYLuW9QsHw-mhDVrHbYnzdhuycQuHn3nvcOnvcKnWTkrHTYnjmzTEDznjDznHndn1NzrHmvnHN3THTKnHTkTHD_nHDKnkDQPWcYnWnOPHc3P1bYTHnKOplFOUA7OplBOlXxOCB4OUa5OlBsOUJ5sXi38Syc-SB6JrpCCXX1B8pEMrXYC9DQTHDKTHnKUvPHPLDkHD-ogLIvibPOINIimYIaI1IgXNPdwEDQPjTvPjcLn10LnW01nHNdPH9YTHmK0A7zmyd1n1NYrWcdP19_ph-6wvNln7tznjTksZK60h7V01ndn1C1P9DQTHFBrjNkuj9dsywBmHNVPjK-raYOnjcOsHPBPADzrjDkmHwhnEDQPjTvPjcLn10LPjEkrHcLP1EdTHDYnjmYnW01P101PjEYPHEvPHmKTHD_nHDKTEDKsEDKTEDKPH98nWE3sWmvsWDvnkDkTHTKTHD_nHDKXLYKnHTkPHEKnHTknWndrEDduj-bPHIWnzY3ujczsHw6n1bVrHEznzYvuhn3rHR6nWbvmynKTHEKIL9kuHD1mhNYmHN1n1m3PjmvTy7JpdI-mv66ITD1THn_nHmdPzkLrH0OTHDKUMR_UT7bmyR-m1nYrH9zPju-PH0Q%22%2C%22lego_sid%22%3A%22110967499212892323512397686%22%2C%22platformCompany%22%3A%22ajk%22%2C%22lego_show_track_url_base%22%3A%22https%3A%5C%2F%5C%2Fadtrack.58.com%5C%2F1002359%5C%2F%22%2C%22lego_version%22%3A%221.0.1%22%2C%22legoHuiDu%22%3Atrue%7D%7D",
                "tid": "5d9d57c3-8d22-4a39-9423-6fc895a296ac",
                "isLiving": false,
                "isVR": false,
                "distanceDesc": "",
                "isGoldOwner": false
            },
            {
                "infoid": "2012129728880643",
                "cateName": "整租",
                "cateID": "11",
                "title": "海珠区泰沙路金紫里泰宁怡居中大布匹对面 新房出租可放电动车",
                "price": "1500",
                "img": "https://pic1.ajkimg.com/display/anjuke/fae7691de996e10a9aa22ea82550f98d/240x180c.jpg?t=1&srotate=1",
                "unit": "元/月",
                "coll": "0",
                "subwayDistance": "距8号线-中大站步行1.3km",
                "fangwuLiangDian": [
                    {
                        "text": "整租",
                        "textColor": "#7397C1",
                        "bgColor": "#7397C1"
                    },
                    {
                        "text": "朝南",
                        "textColor": "#7397C1",
                        "bgColor": "#7397C1"
                    },
                    {
                        "text": "近地铁",
                        "textColor": "#7397C1",
                        "bgColor": "#7397C1"
                    },
                    {
                        "text": "精装修",
                        "textColor": "#7397C1",
                        "bgColor": "#7397C1"
                    }
                ],
                "area": "33㎡",
                "toward": "朝南",
                "shangquan": "东晓南",
                "isAnxuan": "0",
                "location": false,
                "source_type": "1",
                "isauction": "6",
                "auctionShowText": "",
                "legoAuction": "%7B%22isauction%22%3A%226%22%2C%22lego%22%3A%7B%22lego_tid%22%3A%22678a1a60-ee55-46ac-b8ef-f441472ee1be%22%2C%22lego_ad_click_url%22%3A%22https%3A%5C%2F%5C%2Flegoclick.58.com%5C%2Fjump%3Ftarget%3DpZwY0ZnlsztdraOWUvYKuaYLnhPhnHmQPBYLuW9QsHw-mhDVrHbYnzdhuycQuHn3nvcOnvcKnWTkrHTYnjmzTEDznjDznHcOP1c3rj9kPWE1THTKnHTkTHD_nHDKnkDQPWcYnWnOPHc3P1bdTHEKOplFOUA7OplBOlXxOCB4OUa5OlBsOUJ5sXi38Syc-SB6JrpCCXX1B8pEMrXYC9DQTHDKTHnKUvPHPLDkHD-ogLIvibPOINIimYIaI1IgXNPdwEDQPjTvPj0OnjT3nW03P19vnjE3THmK0A7zmyd1n1NYrWcdP19_ph-6wvNln7tznjTksZK60h7V01ndn1C1P9DQTHTzuym1uW0vsHN3nhnVPAckPzYOP1-bsHIBPynLnHEvPjmQmkDQPjTvPj0OnjT3PjDzrHbOPW9kTHDYnjmYP1bknj91PHTkrjbznHmKTHD_nHDKTEDKsEDKTEDKPH98nWE3sWmvsWDvnkDkTHTKTHD_nHDKXLYKnHTkPHEKnHTknWndrEDvP166nyDvnad-uHNdsHEvmynVmW6-uBdhPjEQPj0zuyNQmhNKTHEKIL9kuHD1mhNYmHN1n1m3PjmvTy7JpdI-mv66ITD1THn_nHmdPzkQnWm3PEDQTyOdUAkKuW01PyDkP1DzP1Rbmv7brT%22%2C%22lego_sid%22%3A%22110967499212892323512397686%22%2C%22platformCompany%22%3A%22ajk%22%2C%22lego_show_track_url_base%22%3A%22https%3A%5C%2F%5C%2Fadtrack.58.com%5C%2F1002359%5C%2F%22%2C%22lego_version%22%3A%221.0.1%22%2C%22legoHuiDu%22%3Atrue%7D%7D",
                "tid": "678a1a60-ee55-46ac-b8ef-f441472ee1be",
                "isLiving": false,
                "isVR": false,
                "distanceDesc": "",
                "isGoldOwner": false
            },
            {
                "infoid": "2009208600353805",
                "cateName": "整租",
                "cateID": "11",
                "title": "地铁二号线 海珠区泰沙路五凤金紫里中大对面 落地窗新房出租",
                "price": "1500",
                "img": "https://pic1.ajkimg.com/display/anjuke/048a2222c4ceb32ef940d9817d42a048/240x180c.jpg?t=1&srotate=1",
                "unit": "元/月",
                "coll": "0",
                "subwayDistance": "距8号线-中大站步行1.3km",
                "fangwuLiangDian": [
                    {
                        "text": "整租",
                        "textColor": "#7397C1",
                        "bgColor": "#7397C1"
                    },
                    {
                        "text": "朝南",
                        "textColor": "#7397C1",
                        "bgColor": "#7397C1"
                    },
                    {
                        "text": "近地铁",
                        "textColor": "#7397C1",
                        "bgColor": "#7397C1"
                    },
                    {
                        "text": "精装修",
                        "textColor": "#7397C1",
                        "bgColor": "#7397C1"
                    }
                ],
                "area": "33㎡",
                "toward": "朝南",
                "shangquan": "东晓南",
                "isAnxuan": "0",
                "location": false,
                "source_type": "1",
                "isauction": "6",
                "auctionShowText": "",
                "legoAuction": "%7B%22isauction%22%3A%226%22%2C%22lego%22%3A%7B%22lego_tid%22%3A%224c7362ec-c7e7-432d-8819-1914393bcdf5%22%2C%22lego_ad_click_url%22%3A%22https%3A%5C%2F%5C%2Flegoclick.58.com%5C%2Fjump%3Ftarget%3DpZwY0ZnlsztdraOWUvYKuaYLnhPhnHmQPBYLuW9QsHw-mhDVrHbYnzdhuycQuHn3nvcOnvcKnWTkrHTYnjmzTEDznjTOnWT3PWTkn1N1rjTdTHTKnHTkTHD_nHDKnkDQPWcYnWnOPHc3P1bdTHNKOplFOUA7OplBOlXxOCB4OUa5OlBsOUJ5sXi38Syc-SB6JrpCCXX1B8pEMrXYC9DQTHDKTHnKUvPHPLDkHD-ogLIvibPOINIimYIaI1IgXNPdwEDQPjTvPj0Onj0LrHbvPHTLnHnvTHmK0A7zmyd1n1NYrWcdP19_ph-6wvNln7tznjTksZK60h7V01ndn1C1P9DQTymQmv7-m1cksHEYPjEVPjK6rid6PjT3syEdrynvnhu-nAndmEDQPjTvPj0Onj03nHELPHTznj9kTHDYnjmYP1bkP19kP1mQrH9OnHcKTHD_nHDKTEDKsEDKTEDKPH98nWE3sWmvsWDvnkDkTHTKTHD_nHDKXLYKnHTkPHEKnHTknWndrEDYm101PWF-mzdWPvNLsHE1nhEVrj9QriYQrHDYn1b1mhPbuWNKTHEKIL9kuHD1mhNYmHN1n1m3PjmvTy7JpdI-mv66ITD1THn_nHmdPzkQnWm3PEDQTyOdUAkKuhF-PHTdmy7-n1mLnHmYPE%22%2C%22lego_sid%22%3A%22110967499212892323512397686%22%2C%22platformCompany%22%3A%22ajk%22%2C%22lego_show_track_url_base%22%3A%22https%3A%5C%2F%5C%2Fadtrack.58.com%5C%2F1002359%5C%2F%22%2C%22lego_version%22%3A%221.0.1%22%2C%22legoHuiDu%22%3Atrue%7D%7D",
                "tid": "4c7362ec-c7e7-432d-8819-1914393bcdf5",
                "isLiving": false,
                "isVR": false,
                "distanceDesc": "",
                "isGoldOwner": false
            },
            {
                "infoid": "1997818067672076",
                "cateName": "整租",
                "cateID": "11",
                "title": "8号线石井站电梯房 一房一厅 实拍图 无需中介 独立厨卫",
                "price": "1000",
                "img": "https://pic1.ajkimg.com/display/anjuke/68d80d4d14f14da172765636e84590fa/240x180c.jpg?t=1&srotate=1",
                "unit": "元/月",
                "coll": "0",
                "subwayDistance": "距8号线-中大站步行1.8km",
                "fangwuLiangDian": [
                    {
                        "text": "整租",
                        "textColor": "#7397C1",
                        "bgColor": "#7397C1"
                    },
                    {
                        "text": "朝南",
                        "textColor": "#7397C1",
                        "bgColor": "#7397C1"
                    },
                    {
                        "text": "近地铁",
                        "textColor": "#7397C1",
                        "bgColor": "#7397C1"
                    },
                    {
                        "text": "有电梯",
                        "textColor": "#7397C1",
                        "bgColor": "#7397C1"
                    },
                    {
                        "text": "精装修",
                        "textColor": "#7397C1",
                        "bgColor": "#7397C1"
                    }
                ],
                "area": "40㎡",
                "toward": "朝南",
                "shangquan": "中大",
                "isAnxuan": "0",
                "location": false,
                "source_type": "1",
                "isauction": "6",
                "auctionShowText": "",
                "legoAuction": "%7B%22isauction%22%3A%226%22%2C%22lego%22%3A%7B%22lego_tid%22%3A%2263986f5d-c7a5-4a1c-9f27-8e212878854c%22%2C%22lego_ad_click_url%22%3A%22https%3A%5C%2F%5C%2Flegoclick.58.com%5C%2Fjump%3Ftarget%3DpZwY0ZnlsztdraOWUvYKuaYLnhPhnHmQPBYLuW9QsHw-mhDVrHbYnzdhuycQuHn3nvcOnvcKnWTkPHb3PWnLTEDQrHbLrjD3njmLPW0znj0vTHTKnHTkTHD_nHDKnkDQPWcYnWnOPHc3P1bdTHmKOplFOUA7OplBOlXxOCB4OUa5OlBsOUJ5sXi38Syc-SB6JrpCCXX1B8pEMrXYC9DQTHDKTHnKUvPHPLDkHD-ogLIvibPOINIimYIaI1IgXNPdwEDQPjTvPHTLrH0kPHmknW0kn1nvTHmK0A7zmyd1n1NYrWcdP19_ph-6wvNln7tznjTksZK60h7V01ndn1C1P9DQTHuWnAEQrjT3sHTLrjEVPjN3PiYOPy7hsH0duhmzP1wbPyD3m9DQPjTvPHTLrH0kP1TLnj0krH0vTHDYnjmdnj0OP1Tvn1bOPWmznj9KTHD_nHDKTEDKsEDKTEDKPH98nWE3sWmvsWDvnkDkTHTKTHD_nHDKXLYKnHTkPHEKnHTknWndrEDvn1b3PhmduadWPvDdsHw6nynVrymzPzY3uHcQnW9Lrj9dPAnKTHEKIL9kuHD1mhNYmHN1n1m3PjmvTy7JpdI-mv66ITD1THn_nHmdPzkvn193THDKUMR_UTDdPW0kPjDdn1-BmycOmHKB%22%2C%22lego_sid%22%3A%22110967499212892323512397686%22%2C%22platformCompany%22%3A%22ajk%22%2C%22lego_show_track_url_base%22%3A%22https%3A%5C%2F%5C%2Fadtrack.58.com%5C%2F1002359%5C%2F%22%2C%22lego_version%22%3A%221.0.1%22%2C%22legoHuiDu%22%3Atrue%7D%7D",
                "tid": "63986f5d-c7a5-4a1c-9f27-8e212878854c",
                "isLiving": false,
                "isVR": false,
                "distanceDesc": "",
                "isGoldOwner": false
            },
            {
                "infoid": "2007764387897345",
                "cateName": "整租",
                "cateID": "11",
                "title": "电梯房 一房一厅 500M到地铁 配置齐全 有阳台厨卫",
                "price": "1000",
                "img": "https://pic1.ajkimg.com/display/anjuke/8c4f31ba7badb48d9f9a9b7db32c57ef/240x180c.jpg?t=1&srotate=1",
                "unit": "元/月",
                "coll": "0",
                "subwayDistance": "距8号线-中大站步行1.3km",
                "fangwuLiangDian": [
                    {
                        "text": "整租",
                        "textColor": "#7397C1",
                        "bgColor": "#7397C1"
                    },
                    {
                        "text": "朝南",
                        "textColor": "#7397C1",
                        "bgColor": "#7397C1"
                    },
                    {
                        "text": "近地铁",
                        "textColor": "#7397C1",
                        "bgColor": "#7397C1"
                    },
                    {
                        "text": "精装修",
                        "textColor": "#7397C1",
                        "bgColor": "#7397C1"
                    }
                ],
                "area": "36㎡",
                "toward": "朝南",
                "shangquan": "江南西",
                "isAnxuan": "0",
                "location": false,
                "source_type": "1",
                "isauction": "6",
                "auctionShowText": "",
                "legoAuction": "%7B%22isauction%22%3A%226%22%2C%22lego%22%3A%7B%22lego_tid%22%3A%2234dc6e7f-8700-4308-81f7-18ffb825b004%22%2C%22lego_ad_click_url%22%3A%22https%3A%5C%2F%5C%2Flegoclick.58.com%5C%2Fjump%3Ftarget%3DpZwY0ZnlsztdraOWUvYKuaYLnhPhnHmQPBYLuW9QsHw-mhDVrHbYnzdhuycQuHn3nvcOnvcKnWTkPHb3PWnLTEDznjTLP1mYn19LrjbLn1EdTHTKnHTkTHD_nHDKnkDQPWcYnWnOPHc3P1bdTH0KOplFOUA7OplBOlXxOCB4OUa5OlBsOUJ5sXi38Syc-SB6JrpCCXX1B8pEMrXYC9DQTHDKTHnKUvPHPLDkHD-ogLIvibPOINIimYIaI1IgXNPdwEDQPjTdP10drHbzP1m3P1TkPjDvTHmK0A7zmyd1n1NYrWcdP19_ph-6wvNln7tznjTksZK60h7V01ndn1C1P9DQTym1uADYPAEOsyDOnjDVPjmOradBPHbvsHndrAw-PjT1uWbdnkDQPjTdP10drHbzrHEYrjmdnW9QTHDYnjNLP1NOrHc3PjTknjndrjNKTHD_nHDKTEDKsEDKTEDKPH98nWE3sWmvsWDvnkDkTHTKTHD_nHDKXLYKnHTkPHEKnHTknWndrED1PAwWPhNLuBY3P1TksHE1nj9Vrj7hPzYQrAuhmW9zPycknjEKTHEKIL9kuHD1mhNYmHN1n1m3PjmvTy7JpdI-mv66ITD1THn_nHmdPzkLrH0OTHDKUMR_UT7hPWP-uhEOP1I6rynYrH0v%22%2C%22lego_sid%22%3A%22110967499212892323512397686%22%2C%22platformCompany%22%3A%22ajk%22%2C%22lego_show_track_url_base%22%3A%22https%3A%5C%2F%5C%2Fadtrack.58.com%5C%2F1002359%5C%2F%22%2C%22lego_version%22%3A%221.0.1%22%2C%22legoHuiDu%22%3Atrue%7D%7D",
                "tid": "34dc6e7f-8700-4308-81f7-18ffb825b004",
                "isLiving": false,
                "isVR": false,
                "distanceDesc": "",
                "isGoldOwner": false
            },
            {
                "infoid": "2024887660338185",
                "cateName": "整租",
                "cateID": "11",
                "title": "温馨一居室  电梯精装修  整租",
                "price": "1000",
                "img": "https://pic1.ajkimg.com/display/anjuke/fbdf6d98052a87cc3ce7384feeac0039/240x180c.jpg?t=1&srotate=1",
                "unit": "元/月",
                "coll": "0",
                "subwayDistance": "距8号线-中大站步行1.9km",
                "fangwuLiangDian": [
                    {
                        "text": "整租",
                        "textColor": "#7397C1",
                        "bgColor": "#7397C1"
                    },
                    {
                        "text": "南北",
                        "textColor": "#7397C1",
                        "bgColor": "#7397C1"
                    },
                    {
                        "text": "近地铁",
                        "textColor": "#7397C1",
                        "bgColor": "#7397C1"
                    },
                    {
                        "text": "有电梯",
                        "textColor": "#7397C1",
                        "bgColor": "#7397C1"
                    },
                    {
                        "text": "精装修",
                        "textColor": "#7397C1",
                        "bgColor": "#7397C1"
                    }
                ],
                "area": "20㎡",
                "toward": "南北",
                "shangquan": "昌岗",
                "isAnxuan": "0",
                "location": false,
                "source_type": "1",
                "isauction": "6",
                "auctionShowText": "",
                "legoAuction": "%7B%22isauction%22%3A%226%22%2C%22lego%22%3A%7B%22lego_tid%22%3A%22a6742a40-467e-4870-95ae-d2263245389c%22%2C%22lego_ad_click_url%22%3A%22https%3A%5C%2F%5C%2Flegoclick.58.com%5C%2Fjump%3Ftarget%3DpZwY0ZnlsztdraOWUvYKuaYLnhPhnHmQPBYLuW9QsHw-mhDVrHbYnzdhuycQuHn3nvcOnvcKnWTkPWTknHczTEDznjcYrj9LPWmkn1n3nH9dTHTKnHTkTHD_nHDKnkDQPWcYnWnOPHc3P1bdTH9KOplFOUA7OplBOlXxOCB4OUa5OlBsOUJ5sXi38Syc-SB6JrpCCXX1B8pEMrXYC9DQTHDKTHnKUvPHPLDkHD-ogLIvibPOINIimYIaI1IgXNPdwEDQPjTvPjndnWbLPHEOn1mQnHNzTHmK0A7zmyd1n1NYrWcdP19_ph-6wvNln7tznjTksZK60h7V01ndn1C1P9DQTHKbmy76PWw6sHnknhNVPjnvPzdBn1K6sHEvmHDQPjn3PHIWm9DQPjTvPjndnWbLPW9LP101nH9dTHDYnjmYn1NzrH0vnHczP1NLnHcKTHD_nHDKTEDKsEDKTEDKPH98nWE3sWmvsWDvnkDkTHTKTHD_nHDKXLYKnHTkPHEKnHTknWndrE76PW0YnhDYnaYYPWI-sHE3P1TVrHR6uidbnWcvn1cYPHn3rynKTHEKIL9kuHD1mhNYmHN1n1m3PjmvTy7JpdI-mv66ITD1THn_nHmdPzkvn19dTHDKUMR_UT76rAcOPHn1mHFbmyEdm1-6%22%2C%22lego_sid%22%3A%22110967499212892323512397686%22%2C%22platformCompany%22%3A%22ajk%22%2C%22lego_show_track_url_base%22%3A%22https%3A%5C%2F%5C%2Fadtrack.58.com%5C%2F1002359%5C%2F%22%2C%22lego_version%22%3A%221.0.1%22%2C%22legoHuiDu%22%3Atrue%7D%7D",
                "tid": "a6742a40-467e-4870-95ae-d2263245389c",
                "isLiving": false,
                "isVR": false,
                "distanceDesc": "",
                "isGoldOwner": false
            },
            {
                "infoid": "2040874474381326",
                "cateName": "整租",
                "cateID": "11",
                "title": "地 铁 房 中大对面布匹市场 可停电动车 新房出租拎包入住",
                "price": "1500",
                "img": "https://pic1.ajkimg.com/display/anjuke/97176e913a82955abc183c6cbfccc122/240x180c.jpg?t=1&srotate=1",
                "unit": "元/月",
                "coll": "0",
                "subwayDistance": "距8号线-中大站步行1.7km",
                "fangwuLiangDian": [
                    {
                        "text": "整租",
                        "textColor": "#7397C1",
                        "bgColor": "#7397C1"
                    },
                    {
                        "text": "朝南",
                        "textColor": "#7397C1",
                        "bgColor": "#7397C1"
                    },
                    {
                        "text": "近地铁",
                        "textColor": "#7397C1",
                        "bgColor": "#7397C1"
                    },
                    {
                        "text": "精装修",
                        "textColor": "#7397C1",
                        "bgColor": "#7397C1"
                    }
                ],
                "area": "28㎡",
                "toward": "朝南",
                "shangquan": "江南西",
                "isAnxuan": "0",
                "location": false,
                "source_type": "1",
                "isauction": "6",
                "auctionShowText": "",
                "legoAuction": "%7B%22isauction%22%3A%226%22%2C%22lego%22%3A%7B%22lego_tid%22%3A%2271b11322-fd62-4dac-9611-4c2d56dc7e7b%22%2C%22lego_ad_click_url%22%3A%22https%3A%5C%2F%5C%2Flegoclick.58.com%5C%2Fjump%3Ftarget%3DpZwY0ZnlsztdraOWUvYKuaYLnhPhnHmQPBYLuW9QsHw-mhDVrHbYnzdhuycQuHn3nvcOnvcKnWTknjDOrjDOTEDznjEkrj0YPj0Yn19Qn1cvTHTKnHTkTHD_nHDKnkDQPWcYnWnOPHc3P1bdTHbKOplFOUA7OplBOlXxOCB4OUa5OlBsOUJ5sXi38Syc-SB6JrpCCXX1B8pEMrXYC9DQTHDKTHnKUvPHPLDkHD-ogLIvibPOINIimYIaI1IgXNPdwEDQPjTvPjmLnjT1PWNvPjEdrHNzTHmK0A7zmyd1n1NYrWcdP19_ph-6wvNln7tznjTksZK60h7V01ndn1C1P9DQTHFhP1Kbm16-sHndmHTVPjmkriYOnHnLsyuWnADznWF6Py7BrTDQPjTvPjmLnjTYnjm1nWb1PjEkTHDYnjmYPW0knjEknj9LP1DdrjEKTHD_nHDKTEDKsEDKTEDKPH98nWE3sWmvsWDvnkDkTHTKTHD_nHDKXLYKnHTkPHEKnHTknWndrEDLnycQnHnznBdhujmzsHwbmynVrHmQniYYm1FbPHubm1I-PvcKTHEKIL9kuHD1mhNYmHN1n1m3PjmvTy7JpdI-mv66ITD1THn_nHmdPzkLrH0OTHDKUMR_UTDQm1wBuA7Bm1cLPW7bmH7b%22%2C%22lego_sid%22%3A%22110967499212892323512397686%22%2C%22platformCompany%22%3A%22ajk%22%2C%22lego_show_track_url_base%22%3A%22https%3A%5C%2F%5C%2Fadtrack.58.com%5C%2F1002359%5C%2F%22%2C%22lego_version%22%3A%221.0.1%22%2C%22legoHuiDu%22%3Atrue%7D%7D",
                "tid": "71b11322-fd62-4dac-9611-4c2d56dc7e7b",
                "isLiving": false,
                "isVR": false,
                "distanceDesc": "",
                "isGoldOwner": false
            },
            {
                "infoid": "2016236964651013",
                "cateName": "整租",
                "cateID": "11",
                "title": "8号线地铁站旁 实拍图 落地窗大单间 配置齐全 有阳台厨卫",
                "price": "1000",
                "img": "https://pic1.ajkimg.com/display/anjuke/377ba8c678797060e5d73db51f632701/240x180c.jpg?t=1&srotate=1",
                "unit": "元/月",
                "coll": "0",
                "subwayDistance": "距8号线-中大站步行1.3km",
                "fangwuLiangDian": [
                    {
                        "text": "整租",
                        "textColor": "#7397C1",
                        "bgColor": "#7397C1"
                    },
                    {
                        "text": "朝南",
                        "textColor": "#7397C1",
                        "bgColor": "#7397C1"
                    },
                    {
                        "text": "近地铁",
                        "textColor": "#7397C1",
                        "bgColor": "#7397C1"
                    }
                ],
                "area": "30㎡",
                "toward": "朝南",
                "shangquan": "江南西",
                "isAnxuan": "0",
                "location": false,
                "source_type": "1",
                "isauction": "6",
                "auctionShowText": "",
                "legoAuction": "%7B%22isauction%22%3A%226%22%2C%22lego%22%3A%7B%22lego_tid%22%3A%22a1514c51-564c-4d62-b698-452e527db4d4%22%2C%22lego_ad_click_url%22%3A%22https%3A%5C%2F%5C%2Flegoclick.58.com%5C%2Fjump%3Ftarget%3DpZwY0ZnlsztdraOWUvYKuaYLnhPhnHmQPBYLuW9QsHw-mhDVrHbYnzdhuycQuHn3nvcOnvcKP1DdrjDYPTDKnWTQPWc1PWbvPjmdnHTQnkDkTHDknTDQsjDQTHnKnHmzPjc1rHNzrj0OPEDQnTZ-oCM-_mg-oJsMGO4hBs4-_c4MJcf-8CtVOsBeOmBgl2ACOJB6Oer2OGa0OeiBTHDKnEDKnk7fmdnL0HKniyVxILu2EL-dwdFWwYFLPdIOELR7THDYnjmQrHc3n1D3njD3PWTkrHmKP97kmgF6Ugn1PHElnWNLraQJpy7ZuHCkg1cknjT_0A7zmyd1n1N1rWnvTHDKPym3PWT1PhnVuycknBYYPHwbsycQm1mVnhnkPvDzPj--P1DvTHDYnjmQrHc3n1DOPH0kPjb1PjEKnHEkPWDOnW91nHbQPHDQnjEknTDKnikQnEDKTEDVTEDKTEDdra3zPj98PWm8nHm1THTKnTDKnikQnE7exEDQnjTdPTDQnjTzn1NOTyDQPHDYm1NQsHNvPAnVPAEvnBdBPWb3sHEdnhNdnWIbmWwbPTDKPT7LXjK-nHPBuHw6PHn1PW9YPWmKmyGoRvRWpA7YTHnKnzkQPWNLsj0OP1bKnE78IyQ_TyEOuWmkuhmvuWRbPHDkuWm%22%2C%22lego_sid%22%3A%22110967499212892323512397686%22%2C%22platformCompany%22%3A%22ajk%22%2C%22lego_show_track_url_base%22%3A%22https%3A%5C%2F%5C%2Fadtrack.58.com%5C%2F1002359%5C%2F%22%2C%22lego_version%22%3A%221.0.1%22%2C%22legoHuiDu%22%3Atrue%7D%7D",
                "tid": "a1514c51-564c-4d62-b698-452e527db4d4",
                "isLiving": false,
                "isVR": false,
                "distanceDesc": "",
                "isGoldOwner": false
            },
            {
                "infoid": "1792367586993167",
                "cateName": "整租",
                "cateID": "11",
                "title": "近地铁 诚心出租 房源性价比高 通风采光拎包入住 随时看房",
                "price": "1080",
                "img": "https://pic1.ajkimg.com/display/anjuke/5033dd4f01b37afeb8422c0bb08f443c/240x180c.jpg?t=1&srotate=1",
                "unit": "元/月",
                "coll": "0",
                "subwayDistance": "距8号线-中大站步行1.9km",
                "fangwuLiangDian": [
                    {
                        "text": "整租",
                        "textColor": "#7397C1",
                        "bgColor": "#7397C1"
                    },
                    {
                        "text": "南北",
                        "textColor": "#7397C1",
                        "bgColor": "#7397C1"
                    },
                    {
                        "text": "近地铁",
                        "textColor": "#7397C1",
                        "bgColor": "#7397C1"
                    },
                    {
                        "text": "有电梯",
                        "textColor": "#7397C1",
                        "bgColor": "#7397C1"
                    },
                    {
                        "text": "精装修",
                        "textColor": "#7397C1",
                        "bgColor": "#7397C1"
                    }
                ],
                "area": "22㎡",
                "toward": "南北",
                "shangquan": "昌岗",
                "isAnxuan": "0",
                "location": false,
                "source_type": "1",
                "isauction": "6",
                "auctionShowText": "",
                "legoAuction": "%7B%22isauction%22%3A%226%22%2C%22lego%22%3A%7B%22lego_tid%22%3A%2263294b05-cdae-40a2-9478-4ba06d8cfbca%22%2C%22lego_ad_click_url%22%3A%22https%3A%5C%2F%5C%2Flegoclick.58.com%5C%2Fjump%3Ftarget%3DpZwY0ZnlsztdraOWUvYKuaYLnhPhnHmQPBYLuW9QsHw-mhDVrHbYnzdhuycQuHn3nvcOnvcKnHTQnHnOnWTQTEDQP1bzn1mLPH9vrHb1nHmLTHTKnHTkTHD_nHDKnkDQPWcYnWnOPHc3P1bdTHDQTXy8BXyQ6Xy8C8XMMSpcfSykWSXCBSylWzLb8sf-BFxCCpWhJ2ZM_3ohbF1MV2cKnEDQTED1TyqWN1IQnDQFpdqLIbGjXgRZNhPZEM0LRL-jINNKnHEkPH0Qrj0drjTvn19LrjDYPEDvTgK60h7V01ndPjCzPH03sAGGmNI-rWKxnWTknaQkmgF6Ugn1PHnln1mKnEDOmHczPANYniYkn1NksHwhnhnVmyc3mzYOrHDvPW-BmvNOm1mKnHEkPH0Qrj0drjckPWE3njn3PTDQPjTdP1D3P1N3nHnkrH9zrHDzTEDQsjDQTEDKTiYKTEDKTHN3sWcYra3vPB3QPWnKnTDkTEDQsjDQTgVqTHDknjNYTHDknjc1PHbKPWnzrHwBnjNVmvw6uiYYnADzsHbYP19VPAF6njubrAPhmhP6TEDYTgI3nANQnvF-PADdn1nvrjEvP976phVguyPCmgEKnkD1sjDvPH0_PWn3PEDQTyOdUAkKm1DLnAFWryNYPWn3mvDOuT%22%2C%22lego_sid%22%3A%22110967499212892323512397686%22%2C%22platformCompany%22%3A%22ajk%22%2C%22lego_show_track_url_base%22%3A%22https%3A%5C%2F%5C%2Fadtrack.58.com%5C%2F1002359%5C%2F%22%2C%22lego_version%22%3A%221.0.1%22%2C%22legoHuiDu%22%3Atrue%7D%7D",
                "tid": "63294b05-cdae-40a2-9478-4ba06d8cfbca",
                "isLiving": false,
                "isVR": false,
                "distanceDesc": "",
                "isGoldOwner": false
            },
            {
                "infoid": "1883654829841416",
                "cateName": "整租",
                "cateID": "11",
                "title": "周边环境保证安全从不接住闲杂人等,租得放心住得舒心  无中介",
                "price": "1380",
                "img": "https://pic1.ajkimg.com/display/anjuke/fe426e5a3e6bb19f9c96d37a367a17aa/240x180c.jpg?t=1&srotate=1",
                "unit": "元/月",
                "coll": "0",
                "subwayDistance": "距8号线-中大站步行1.8km",
                "fangwuLiangDian": [
                    {
                        "text": "整租",
                        "textColor": "#7397C1",
                        "bgColor": "#7397C1"
                    },
                    {
                        "text": "南北",
                        "textColor": "#7397C1",
                        "bgColor": "#7397C1"
                    },
                    {
                        "text": "近地铁",
                        "textColor": "#7397C1",
                        "bgColor": "#7397C1"
                    },
                    {
                        "text": "有电梯",
                        "textColor": "#7397C1",
                        "bgColor": "#7397C1"
                    },
                    {
                        "text": "精装修",
                        "textColor": "#7397C1",
                        "bgColor": "#7397C1"
                    }
                ],
                "area": "25㎡",
                "toward": "南北",
                "shangquan": "中大",
                "isAnxuan": "0",
                "location": false,
                "source_type": "1",
                "isauction": "6",
                "auctionShowText": "",
                "legoAuction": "%7B%22isauction%22%3A%226%22%2C%22lego%22%3A%7B%22lego_tid%22%3A%2271b12c9c-1a49-4a56-91b5-e0e6e0b89d05%22%2C%22lego_ad_click_url%22%3A%22https%3A%5C%2F%5C%2Flegoclick.58.com%5C%2Fjump%3Ftarget%3DpZwY0ZnlsztdraOWUvYKuaYLnhPhnHmQPBYLuW9QsHw-mhDVrHbYnzdhuycQuHn3nvcOnvcKnHTQnHnOnWTQTEDQrj91PWNYrjcOrjEQPjDvTHTKnHTkTHD_nHDKnkDQPWcYnWnOPHc3P1bdTHDzTXy8BXyQ6Xy8C8XMMSpcfSykWSXCBSylWzLb8sf-BFxCCpWhJ2ZM_3ohbF1MV2cKnEDQTED1TyqWN1IQnDQFpdqLIbGjXgRZNhPZEM0LRL-jINNKnHEkPH0Qrj9dn1DvnHn1nW01P9DvTgK60h7V01ndPjCzPH03sAGGmNI-rWKxnWTknaQkmgF6Ugn1PHnln1mKnE7WnhRWn1nvmBd6PjmksHE1PjEVrAN3nzdhmHPBPjN1PHb1njbKnHEkPH0Qrj9dn1cOrH0YnjmLn9DQPjTdP1D3rjN1nWcYnWE1nWTQTEDQsjDQTEDKTiYKTEDKTHN3sWcYra3vPB3QPWnKnTDkTEDQsjDQTgVqTHDknjNYTHDknjc1PHbKP17BnHFWrynVnyDYriYYmHNvsHbQmWNVuHK-PhNkmW9OujTdTEDYTgI3nANQnvF-PADdn1nvrjEvP976phVguyPCmgEKnkD1sjDvPH0_PWn3rTDQTyOdUAkKrjTYP17hrjmkmvwbP1mLm9%22%2C%22lego_sid%22%3A%22110967499212892323512397686%22%2C%22platformCompany%22%3A%22ajk%22%2C%22lego_show_track_url_base%22%3A%22https%3A%5C%2F%5C%2Fadtrack.58.com%5C%2F1002359%5C%2F%22%2C%22lego_version%22%3A%221.0.1%22%2C%22legoHuiDu%22%3Atrue%7D%7D",
                "tid": "71b12c9c-1a49-4a56-91b5-e0e6e0b89d05",
                "isLiving": false,
                "isVR": false,
                "distanceDesc": "",
                "isGoldOwner": false
            },
            {
                "infoid": "2006503709187073",
                "cateName": "整租",
                "cateID": "11",
                "title": "晓港十佳房源  独立阳台厨卫 电梯精装修",
                "price": "1000",
                "img": "https://pic1.ajkimg.com/display/anjuke/35da7436aeb08f6ea4e0ce6b4feb6d86/240x180c.jpg?t=1&srotate=1",
                "unit": "元/月",
                "coll": "0",
                "subwayDistance": "距8号线-中大站步行1.2km",
                "fangwuLiangDian": [
                    {
                        "text": "整租",
                        "textColor": "#7397C1",
                        "bgColor": "#7397C1"
                    },
                    {
                        "text": "南北",
                        "textColor": "#7397C1",
                        "bgColor": "#7397C1"
                    },
                    {
                        "text": "近地铁",
                        "textColor": "#7397C1",
                        "bgColor": "#7397C1"
                    },
                    {
                        "text": "有电梯",
                        "textColor": "#7397C1",
                        "bgColor": "#7397C1"
                    },
                    {
                        "text": "精装修",
                        "textColor": "#7397C1",
                        "bgColor": "#7397C1"
                    }
                ],
                "area": "19㎡",
                "toward": "南北",
                "shangquan": "晓港",
                "isAnxuan": "0",
                "location": false,
                "source_type": "1",
                "isauction": "6",
                "auctionShowText": "",
                "legoAuction": "%7B%22isauction%22%3A%226%22%2C%22lego%22%3A%7B%22lego_tid%22%3A%22eae03c29-74b7-4417-b9af-479b763b7ee1%22%2C%22lego_ad_click_url%22%3A%22https%3A%5C%2F%5C%2Flegoclick.58.com%5C%2Fjump%3Ftarget%3DpZwY0ZnlsztdraOWUvYKuaYLnhPhnHmQPBYLuW9QsHw-mhDVrHbYnzdhuycQuHn3nvcOnvcKnWTkPWTknHczTEDznjTvPHT1P1TOnH9Lnj01THTKnHTkTHD_nHDKnkDQPWcYnWnOPHc3P1bdTHD1TXy8BXyQ6Xy8C8XMMSpcfSykWSXCBSylWzLb8sf-BFxCCpWhJ2ZM_3ohbF1MV2cKnEDQTED1TyqWN1IQnDQFpdqLIbGjXgRZNhPZEM0LRL-jINNKnHEkPWTvnjcYP1NQnWTknWNvnEDvTgK60h7V01ndPjCzPH03sAGGmNI-rWKxnWTknaQkmgF6Ugn1PHnln1mKnEDOP1RbujckmidhmhnvsHEYrHnVmyckmiYvmhDLPyEdmyFWuWbKnHEkPWTvnjcYP1mdPjmkrj9OPkDQPjTvnjmknWELPH0YrHDLnHckTEDQsjDQTEDKTiYKTEDKTHN3sWcYra3vPB3QPWnKnTDkTEDQsjDQTgVqTHDknjNYTHDknjc1PHbKuy7-njPWnWbVP1wBPzYYPjDLsycOmymVPj0OmW0vnvcLuyNQTEDYTgI3nANQnvF-PADdn1nvrjEvP976phVguyPCmgEKnkD1sjDvPH0_PWEvnkDQTyOdUAkKPjRBuj9QuyNvmHcQuANLn9%22%2C%22lego_sid%22%3A%22110967499212892323512397686%22%2C%22platformCompany%22%3A%22ajk%22%2C%22lego_show_track_url_base%22%3A%22https%3A%5C%2F%5C%2Fadtrack.58.com%5C%2F1002359%5C%2F%22%2C%22lego_version%22%3A%221.0.1%22%2C%22legoHuiDu%22%3Atrue%7D%7D",
                "tid": "eae03c29-74b7-4417-b9af-479b763b7ee1",
                "isLiving": false,
                "isVR": false,
                "distanceDesc": "",
                "isGoldOwner": false
            },
            {
                "infoid": "2024968287215617",
                "cateName": "整租",
                "cateID": "11",
                "title": "美院温馨一居室，电梯精装修   智能门锁",
                "price": "1000",
                "img": "https://pic1.ajkimg.com/display/anjuke/7b128c10b274dc04c21d9895a138f1d2/240x180c.jpg?t=1&srotate=1",
                "unit": "元/月",
                "coll": "0",
                "subwayDistance": "距8号线-中大站步行1.9km",
                "fangwuLiangDian": [
                    {
                        "text": "整租",
                        "textColor": "#7397C1",
                        "bgColor": "#7397C1"
                    },
                    {
                        "text": "南北",
                        "textColor": "#7397C1",
                        "bgColor": "#7397C1"
                    },
                    {
                        "text": "近地铁",
                        "textColor": "#7397C1",
                        "bgColor": "#7397C1"
                    },
                    {
                        "text": "有电梯",
                        "textColor": "#7397C1",
                        "bgColor": "#7397C1"
                    },
                    {
                        "text": "精装修",
                        "textColor": "#7397C1",
                        "bgColor": "#7397C1"
                    }
                ],
                "area": "20㎡",
                "toward": "南北",
                "shangquan": "晓港",
                "isAnxuan": "0",
                "location": false,
                "source_type": "1",
                "isauction": "6",
                "auctionShowText": "",
                "legoAuction": "%7B%22isauction%22%3A%226%22%2C%22lego%22%3A%7B%22lego_tid%22%3A%2293c84c60-be57-48ce-9049-67794c62f918%22%2C%22lego_ad_click_url%22%3A%22https%3A%5C%2F%5C%2Flegoclick.58.com%5C%2Fjump%3Ftarget%3DpZwY0ZnlsztdraOWUvYKuaYLnhPhnHmQPBYLuW9QsHw-mhDVrHbYnzdhuycQuHn3nvcOnvcKnWTkPWTknHczTEDznjcYrHm3nW9LnWDdPWDLTHTKnHTkTHD_nHDKnkDQPWcYnWnOPHc3P1bdTHDYTXy8BXyQ6Xy8C8XMMSpcfSykWSXCBSylWzLb8sf-BFxCCpWhJ2ZM_3ohbF1MV2cKnEDQTED1TyqWN1IQnDQFpdqLIbGjXgRZNhPZEM0LRL-jINNKnHEkPWTvnjD3nWbvPH9dPjckrEDvTgK60h7V01ndPjCzPH03sAGGmNI-rWKxnWTknaQkmgF6Ugn1PHnln1mKnED1uyubujbYmiYOmyF-sHEQm1nVmWckmiYLuycvm1cOPhuBnyDKnHEkPWTvnjD3n1DkPjcvPWcYnTDQPjTvnjmknH91njEOP1EYn19dTEDQsjDQTEDKTiYKTEDKTHN3sWcYra3vPB3QPWnKnTDkTEDQsjDQTgVqTHDknjNYTHDknjc1PHbKrHPWrjwWPWTVmhNdPzYYrAP-sHbkPjbVPW0LrHwWPWFhrHD3TEDYTgI3nANQnvF-PADdn1nvrjEvP976phVguyPCmgEKnkD1sjDvPH0_PWEvnkDQTyOdUAkKPH9kmHn3rynYnhDkuWE1m9%22%2C%22lego_sid%22%3A%22110967499212892323512397686%22%2C%22platformCompany%22%3A%22ajk%22%2C%22lego_show_track_url_base%22%3A%22https%3A%5C%2F%5C%2Fadtrack.58.com%5C%2F1002359%5C%2F%22%2C%22lego_version%22%3A%221.0.1%22%2C%22legoHuiDu%22%3Atrue%7D%7D",
                "tid": "93c84c60-be57-48ce-9049-67794c62f918",
                "isLiving": false,
                "isVR": false,
                "distanceDesc": "",
                "isGoldOwner": false
            },
            {
                "infoid": "1730376683740174",
                "cateName": "整租",
                "cateID": "11",
                "title": "海珠区 地铁步行600米 公交步行30米  精装修 配套齐全",
                "price": "1880",
                "img": "https://pic1.ajkimg.com/display/anjuke/b67d65ed5fa30a19e333e099b1b8ecce/240x180c.jpg?t=1&srotate=1",
                "unit": "元/月",
                "coll": "0",
                "subwayDistance": "距8号线-中大站步行1.8km",
                "fangwuLiangDian": [
                    {
                        "text": "整租",
                        "textColor": "#7397C1",
                        "bgColor": "#7397C1"
                    },
                    {
                        "text": "朝南",
                        "textColor": "#7397C1",
                        "bgColor": "#7397C1"
                    },
                    {
                        "text": "近地铁",
                        "textColor": "#7397C1",
                        "bgColor": "#7397C1"
                    },
                    {
                        "text": "有电梯",
                        "textColor": "#7397C1",
                        "bgColor": "#7397C1"
                    },
                    {
                        "text": "精装修",
                        "textColor": "#7397C1",
                        "bgColor": "#7397C1"
                    }
                ],
                "area": "33㎡",
                "toward": "朝南",
                "shangquan": "江燕路",
                "isAnxuan": "0",
                "location": false,
                "source_type": "1",
                "isauction": "6",
                "auctionShowText": "",
                "legoAuction": "%7B%22isauction%22%3A%226%22%2C%22lego%22%3A%7B%22lego_tid%22%3A%22ca191023-d808-446b-abaa-d693ef579bb3%22%2C%22lego_ad_click_url%22%3A%22https%3A%5C%2F%5C%2Flegoclick.58.com%5C%2Fjump%3Ftarget%3DpZwY0ZnlsztdraOWUvYKuaYLnhPhnHmQPBYLuW9QsHw-mhDVrHbYnzdhuycQuHn3nvcOnvcKnWTknjDOrjDOTEDQP1nkn10vPW91P1EknH0YTHTKnHTkTHD_nHDKnkDQPWcYnWnOPHc3P1bdTHDdTXy8BXyQ6Xy8C8XMMSpcfSykWSXCBSylWzLb8sf-BFxCCpWhJ2ZM_3ohbF1MV2cKnEDQTED1TyqWN1IQnDQFpdqLIbGjXgRZNhPZEM0LRL-jINNKnHEkPH0drjnkPjDQn1mvPjTknTDvTgK60h7V01ndPjCzPH03sAGGmNI-rWKxnWTknaQkmgF6Ugn1PHnln1mKnE7WmW9kPAFbraYdPjmdsHwbmWEVrj0LnzYdmWc1m1TvuWwWn10KnHEkPH0drjnkPjNzPj0krH93rTDQPjTdP1N3n1TYPjnzPjnQnHTYTEDQsjDQTEDKTiYKTEDKTHN3sWcYra3vPB3QPWnKnTDkTEDQsjDQTgVqTHDknjNYTHDknjc1PHbKmvDQrHDknWnVuj9kraYYPjuBsy7BmyDVujmOnvRhPH0Omhc1TEDYTgI3nANQnvF-PADdn1nvrjEvP976phVguyPCmgEKnkD1sjDvPH0_nHDdPWmKnE78IyQ_THDdnjDzrAPbPhwhn1NzujN%22%2C%22lego_sid%22%3A%22110967499212892323512397686%22%2C%22platformCompany%22%3A%22ajk%22%2C%22lego_show_track_url_base%22%3A%22https%3A%5C%2F%5C%2Fadtrack.58.com%5C%2F1002359%5C%2F%22%2C%22lego_version%22%3A%221.0.1%22%2C%22legoHuiDu%22%3Atrue%7D%7D",
                "tid": "ca191023-d808-446b-abaa-d693ef579bb3",
                "isLiving": false,
                "isVR": false,
                "distanceDesc": "",
                "isGoldOwner": false
            },
            {
                "infoid": "1728844896869390",
                "cateName": "整租",
                "cateID": "11",
                "title": "江泰路 泰沙路 泰宁怡居 家电齐全拎包入住 采光好电梯公寓",
                "price": "1800",
                "img": "https://pic1.ajkimg.com/display/anjuke/7681151bc3c559beceb0b911acd3abf7/240x180c.jpg?t=1&srotate=1",
                "unit": "元/月",
                "coll": "0",
                "subwayDistance": "距8号线-中大站步行1.3km",
                "fangwuLiangDian": [
                    {
                        "text": "整租",
                        "textColor": "#7397C1",
                        "bgColor": "#7397C1"
                    },
                    {
                        "text": "朝南",
                        "textColor": "#7397C1",
                        "bgColor": "#7397C1"
                    },
                    {
                        "text": "近地铁",
                        "textColor": "#7397C1",
                        "bgColor": "#7397C1"
                    },
                    {
                        "text": "有电梯",
                        "textColor": "#7397C1",
                        "bgColor": "#7397C1"
                    },
                    {
                        "text": "精装修",
                        "textColor": "#7397C1",
                        "bgColor": "#7397C1"
                    }
                ],
                "area": "33㎡",
                "toward": "朝南",
                "shangquan": "东晓南",
                "isAnxuan": "0",
                "location": false,
                "source_type": "1",
                "isauction": "6",
                "auctionShowText": "",
                "legoAuction": "%7B%22isauction%22%3A%226%22%2C%22lego%22%3A%7B%22lego_tid%22%3A%22322af7de-3bf3-4b16-b83a-554bcafbd59d%22%2C%22lego_ad_click_url%22%3A%22https%3A%5C%2F%5C%2Flegoclick.58.com%5C%2Fjump%3Ftarget%3DpZwY0ZnlsztdraOWUvYKuaYLnhPhnHmQPBYLuW9QsHw-mhDVrHbYnzdhuycQuHn3nvcOnvcKnWTknjDOrjDOTEDQP1c3rjEYrjbvrjmOn1bkTHTKnHTkTHD_nHDKnkDQPWcYnWnOPHc3P1bdTHDvTXy8BXyQ6Xy8C8XMMSpcfSykWSXCBSylWzLb8sf-BFxCCpWhJ2ZM_3ohbF1MV2cKnEDQTED1TyqWN1IQnDQFpdqLIbGjXgRZNhPZEM0LRL-jINNKnHEkPWTLn1EzPj9YrHDYrHbdn9DvTgK60h7V01ndPjCzPH03sAGGmNI-rWKxnWTknaQkmgF6Ugn1PHnln1mKnEDYujbzm10QuBYQrH0OsHE1uymVrH-6mBYYrj-hP1NYnj0knjTKnHEkPWTLn1EzPHnknWD1rj93nTDQPjTvnj01PjcdnHbLnW0LnH9YTEDQsjDQTEDKTiYKTEDKTHN3sWcYra3vPB3QPWnKnTDkTEDQsjDQTgVqTHDknjNYTHDknjc1PHbKn1czmymLuANVnvFhnzYYmWDvsyc3nvDVPHNYmhP6uhFbPH-bTEDYTgI3nANQnvF-PADdn1nvrjEvP976phVguyPCmgEKnkD1sjDvPH0_nHcvrjNKnE78IyQ_THuhuWEzmvFBuH9QPyDLuAn%22%2C%22lego_sid%22%3A%22110967499212892323512397686%22%2C%22platformCompany%22%3A%22ajk%22%2C%22lego_show_track_url_base%22%3A%22https%3A%5C%2F%5C%2Fadtrack.58.com%5C%2F1002359%5C%2F%22%2C%22lego_version%22%3A%221.0.1%22%2C%22legoHuiDu%22%3Atrue%7D%7D",
                "tid": "322af7de-3bf3-4b16-b83a-554bcafbd59d",
                "isLiving": false,
                "isVR": false,
                "distanceDesc": "",
                "isGoldOwner": false
            },
            {
                "infoid": "1859138929631244",
                "cateName": "整租",
                "cateID": "11",
                "title": "免租七天，大马路边电梯大单间，直租无中介",
                "price": "1500",
                "img": "https://pic1.ajkimg.com/display/anjuke/41c7ee1a34b34de54cb6fbe769b4cde7/240x180c.jpg?t=1&srotate=1",
                "unit": "元/月",
                "coll": "0",
                "subwayDistance": "距8号线-中大站步行1.7km",
                "fangwuLiangDian": [
                    {
                        "text": "整租",
                        "textColor": "#7397C1",
                        "bgColor": "#7397C1"
                    },
                    {
                        "text": "东南",
                        "textColor": "#7397C1",
                        "bgColor": "#7397C1"
                    },
                    {
                        "text": "近地铁",
                        "textColor": "#7397C1",
                        "bgColor": "#7397C1"
                    },
                    {
                        "text": "有电梯",
                        "textColor": "#7397C1",
                        "bgColor": "#7397C1"
                    },
                    {
                        "text": "豪华装修",
                        "textColor": "#7397C1",
                        "bgColor": "#7397C1"
                    }
                ],
                "area": "28㎡",
                "toward": "东南",
                "shangquan": "江燕路",
                "isAnxuan": "0",
                "location": false,
                "source_type": "1",
                "isauction": "6",
                "auctionShowText": "",
                "legoAuction": "%7B%22isauction%22%3A%226%22%2C%22lego%22%3A%7B%22lego_tid%22%3A%22a3509fde-e46e-4e8f-b13b-3f1f3af2af7b%22%2C%22lego_ad_click_url%22%3A%22https%3A%5C%2F%5C%2Flegoclick.58.com%5C%2Fjump%3Ftarget%3DpZwY0ZnlsztdraOWUvYKuaYLnhPhnHmQPBYLuW9QsHw-mhDVrHbYnzdhuycQuHn3nvcOnvcKnHTQPj0Yn19kTEDQrjNOnHn3rHcOPWnQnWEYTHTKnHTkTHD_nHDKnkDQPWcYnWnOPHc3P1bvTHDLTXy8BXyQ6Xy8C8XMMSpcfSykWSXCBSylWzLb8sf-BFxCCpWhJ2ZM_3ohbF1MV2cKnEDQTED1TyqWN1IQnDQFpdqLIbGjXgRZNhPZEM0LRL-jINNKnHEkPWD1n19Qn1bkrHmvn10YPTDvTgK60h7V01ndPjCzPH03sAGGmNI-rWKxnWTknaQkmgF6Ugn1PHnln1mKnE7-nHTYuhn1nzdhPHnzsHwBuhmVmH91PadBmyDOnWEvuhDYuH9KnHEkPWD1n19QPjTOPjckrHTzPTDQPjTvnHn1rjD1rH0vP10zPWTOTEDQsjDQTEDKTiYKTEDKTHN3sWcYra3vPB3QPWnKnTDkTEDQsjDQTgVqTHDknjNYTHDknjc1PHbKmHndnj-huANVuHEvuiYYuH6hsycQnvcVnvmQuWP6uWF6uWIBTEDYTgI3nANQnvF-PADdn1nvrjEvP976phVguyPCmgEKnkD1sjDvPH0_nHDdPWmKnE78IyQ_TyPbP1R-mhmkrj0LmWFbmHc%22%2C%22lego_sid%22%3A%22110967499212892323512397686%22%2C%22platformCompany%22%3A%22ajk%22%2C%22lego_show_track_url_base%22%3A%22https%3A%5C%2F%5C%2Fadtrack.58.com%5C%2F1002359%5C%2F%22%2C%22lego_version%22%3A%221.0.1%22%2C%22legoHuiDu%22%3Atrue%7D%7D",
                "tid": "a3509fde-e46e-4e8f-b13b-3f1f3af2af7b",
                "isLiving": false,
                "isVR": false,
                "distanceDesc": "",
                "isGoldOwner": false
            },
            {
                "infoid": "2026302175011854",
                "cateName": "整租",
                "cateID": "11",
                "title": "整租 昌岗优选房源，近地铁，交通便利舒适的生活社区",
                "price": "1000",
                "img": "https://pic1.ajkimg.com/display/anjuke/c20203ec1bff6c38035ca17b2b92aa34/240x180c.jpg?t=1&srotate=1",
                "unit": "元/月",
                "coll": "0",
                "subwayDistance": "距8号线-中大站步行1.9km",
                "fangwuLiangDian": [
                    {
                        "text": "整租",
                        "textColor": "#7397C1",
                        "bgColor": "#7397C1"
                    },
                    {
                        "text": "南北",
                        "textColor": "#7397C1",
                        "bgColor": "#7397C1"
                    },
                    {
                        "text": "近地铁",
                        "textColor": "#7397C1",
                        "bgColor": "#7397C1"
                    },
                    {
                        "text": "有电梯",
                        "textColor": "#7397C1",
                        "bgColor": "#7397C1"
                    },
                    {
                        "text": "精装修",
                        "textColor": "#7397C1",
                        "bgColor": "#7397C1"
                    }
                ],
                "area": "21㎡",
                "toward": "南北",
                "shangquan": "昌岗",
                "isAnxuan": "0",
                "location": false,
                "source_type": "1",
                "isauction": "6",
                "auctionShowText": "",
                "legoAuction": "%7B%22isauction%22%3A%226%22%2C%22lego%22%3A%7B%22lego_tid%22%3A%226583ea8a-34a5-4e3d-96d6-4249c074d3fe%22%2C%22lego_ad_click_url%22%3A%22https%3A%5C%2F%5C%2Flegoclick.58.com%5C%2Fjump%3Ftarget%3DpZwY0ZnlsztdraOWUvYKuaYLnhPhnHmQPBYLuW9QsHw-mhDVrHbYnzdhuycQuHn3nvcOnvcKnWTknHE3nW93TEDznjcvn1TznH0dnjDQrjNYTHTKnHTkTHD_nHDKnkDQPWcYnWnOPHc3P1bvTHD3TXy8BXyQ6Xy8C8XMMSpcfSykWSXCBSylWzLb8sf-BFxCCpWhJ2ZM_3ohbF1MV2cKnEDQTED1TyqWN1IQnDQFpdqLIbGjXgRZNhPZEM0LRL-jINNKnHEkPH9Qn1NznHbLnjm1Pj0dn9DvTgK60h7V01ndPjCzPH03sAGGmNI-rWKxnWTknaQkmgF6Ugn1PHnln1mKnEDYPvnYmyEvmzdWPAuWsHEQPj9VmHIhniYYuW91nWcQujELujnKnHEkPH9Qn1NznWDQn1cYnHT3rTDQPjTdrjD1PHcznjn1PHEOn1DzTEDQsjDQTEDKTiYKTEDKTHN3sWcYra3vPB3QPWnKnTDkTEDQsjDQTgVqTHDknjNYTHDknjc1PHbKPWN3nvR6rADVn1w6PiYYuHPbsHbvujmVPjcYrynkP1wbnvu-TEDYTgI3nANQnvF-PADdn1nvrjEvP976phVguyPCmgEKnkD1sjDvPH0_PWn3PEDQTyOdUAkKuhmdrjP-nvcdrjR6uj9kPE%22%2C%22lego_sid%22%3A%22110967499212892323512397686%22%2C%22platformCompany%22%3A%22ajk%22%2C%22lego_show_track_url_base%22%3A%22https%3A%5C%2F%5C%2Fadtrack.58.com%5C%2F1002359%5C%2F%22%2C%22lego_version%22%3A%221.0.1%22%2C%22legoHuiDu%22%3Atrue%7D%7D",
                "tid": "6583ea8a-34a5-4e3d-96d6-4249c074d3fe",
                "isLiving": false,
                "isVR": false,
                "distanceDesc": "",
                "isGoldOwner": false
            },
            {
                "infoid": "2023451187766284",
                "cateName": "整租",
                "cateID": "11",
                "title": "整租 昌岗房东 急租好房源 温馨一居室 舒适的生活社区",
                "price": "1000",
                "img": "https://pic1.ajkimg.com/display/anjuke/ea045c850a020fb85298d830daf5b213/240x180c.jpg?t=1&srotate=1",
                "unit": "元/月",
                "coll": "0",
                "subwayDistance": "距8号线-中大站步行1.3km",
                "fangwuLiangDian": [
                    {
                        "text": "整租",
                        "textColor": "#7397C1",
                        "bgColor": "#7397C1"
                    },
                    {
                        "text": "南北",
                        "textColor": "#7397C1",
                        "bgColor": "#7397C1"
                    },
                    {
                        "text": "近地铁",
                        "textColor": "#7397C1",
                        "bgColor": "#7397C1"
                    },
                    {
                        "text": "有电梯",
                        "textColor": "#7397C1",
                        "bgColor": "#7397C1"
                    },
                    {
                        "text": "精装修",
                        "textColor": "#7397C1",
                        "bgColor": "#7397C1"
                    }
                ],
                "area": "21.3㎡",
                "toward": "南北",
                "shangquan": "江南西",
                "isAnxuan": "0",
                "location": false,
                "source_type": "1",
                "isauction": "6",
                "auctionShowText": "",
                "legoAuction": "%7B%22isauction%22%3A%226%22%2C%22lego%22%3A%7B%22lego_tid%22%3A%2298189b98-5d3c-486b-9055-971da829c64d%22%2C%22lego_ad_click_url%22%3A%22https%3A%5C%2F%5C%2Flegoclick.58.com%5C%2Fjump%3Ftarget%3DpZwY0ZnlsztdraOWUvYKuaYLnhPhnHmQPBYLuW9QsHw-mhDVrHbYnzdhuycQuHn3nvcOnvcKnWTknHE3nW93TEDznjc1PjNQnH9LP1mvnW9YTHTKnHTkTHD_nHDKnkDQPWcYnWnOPHc3P1bvTHDOTXy8BXyQ6Xy8C8XMMSpcfSykWSXCBSylWzLb8sf-BFxCCpWhJ2ZM_3ohbF1MV2cKnEDQTED1TyqWN1IQnDQFpdqLIbGjXgRZNhPZEM0LRL-jINNKnHEkPH9Qn1N3nHcLn19OrHTkrEDvTgK60h7V01ndPjCzPH03sAGGmNI-rWKxnWTknaQkmgF6Ugn1PHnln1mKnEDOuAF6PHEOriYknADYsHEOPWmVmHEYuBdhmHFBPvEzm1NYrjcKnHEkPH9Qn1N3nHEQnWnQnHTYnTDQPjTdrjD1PH9QnWmQn1cknHbzTEDQsjDQTEDKTiYKTEDKTHN3sWcYra3vPB3QPWnKnTDkTEDQsjDQTgVqTHDknjNYTHDknjc1PHbKrH9Qrj-BrH9VPyE1mzYYrjuBsHbkPHNVrH0QuAD3nW-WPWwbTEDYTgI3nANQnvF-PADdn1nvrjEvP976phVguyPCmgEKnkD1sjDvPH0_P1bLrEDQTyOdUAkKPvF-ryRBPW-hPW01uWnQuE%22%2C%22lego_sid%22%3A%22110967499212892323512397686%22%2C%22platformCompany%22%3A%22ajk%22%2C%22lego_show_track_url_base%22%3A%22https%3A%5C%2F%5C%2Fadtrack.58.com%5C%2F1002359%5C%2F%22%2C%22lego_version%22%3A%221.0.1%22%2C%22legoHuiDu%22%3Atrue%7D%7D",
                "tid": "98189b98-5d3c-486b-9055-971da829c64d",
                "isLiving": false,
                "isVR": false,
                "distanceDesc": "",
                "isGoldOwner": false
            },
            {
                "infoid": "2024624388414477",
                "cateName": "整租",
                "cateID": "11",
                "title": "整租 昌岗房东十佳房源 急租 温馨一居室 舒适的生活社区",
                "price": "1000",
                "img": "https://pic1.ajkimg.com/display/anjuke/699cdfc508f92199ad1f5605230a5fea/240x180c.jpg?t=1&srotate=1",
                "unit": "元/月",
                "coll": "0",
                "subwayDistance": "",
                "fangwuLiangDian": [
                    {
                        "text": "整租",
                        "textColor": "#7397C1",
                        "bgColor": "#7397C1"
                    },
                    {
                        "text": "南北",
                        "textColor": "#7397C1",
                        "bgColor": "#7397C1"
                    },
                    {
                        "text": "近地铁",
                        "textColor": "#7397C1",
                        "bgColor": "#7397C1"
                    },
                    {
                        "text": "有电梯",
                        "textColor": "#7397C1",
                        "bgColor": "#7397C1"
                    },
                    {
                        "text": "精装修",
                        "textColor": "#7397C1",
                        "bgColor": "#7397C1"
                    }
                ],
                "area": "19.2㎡",
                "toward": "南北",
                "shangquan": "昌岗",
                "isAnxuan": "0",
                "location": false,
                "source_type": "1",
                "isauction": "6",
                "auctionShowText": "",
                "legoAuction": "%7B%22isauction%22%3A%226%22%2C%22lego%22%3A%7B%22lego_tid%22%3A%22c435bae5-5f7e-4704-ae2c-8119ba5be148%22%2C%22lego_ad_click_url%22%3A%22https%3A%5C%2F%5C%2Flegoclick.58.com%5C%2Fjump%3Ftarget%3DpZwY0ZnlsztdraOWUvYKuaYLnhPhnHmQPBYLuW9QsHw-mhDVrHbYnzdhuycQuHn3nvcOnvcKnWTknHE3nW93TEDznjcYPWcYn193PjDYPj0LTHTKnHTkTHD_nHDKnkDQPWcYnWnOPHc3P1bvTHckTXy8BXyQ6Xy8C8XMMSpcfSykWSXCBSylWzLb8sf-BFxCCpWhJ2ZM_3ohbF1MV2cKnEDQTED1TyqWN1IQnDQFpdqLIbGjXgRZNhPZEM0LRL-jINNKnHEkPH9Qn1EYn1DdrHmvnWNOn9DvTgK60h7V01ndPjCzPH03sAGGmNI-rWKxnWTknaQkmgF6Ugn1PHnln1mKnED3n10LP1NYnidhPyRWsHwWP1cVmyF6PzdBnjbLnAEdnjFWnWmKnHEkPH9Qn1EYn1nkPWEvn1c1n9DQPjTdrjD1PjE1nWnkrHmdP1mkTEDQsjDQTEDKTiYKTEDKTHN3sWcYra3vPB3QPWnKnTDkTEDQsjDQTgVqTHDknjNYTHDknjc1PHbKm1E1PyF6uHNVPymLuiYYP1TYsy7-nhnVrjDQryF6PyF-nHE3TEDYTgI3nANQnvF-PADdn1nvrjEvP976phVguyPCmgEKnkD1sjDvPH0_PWn3PEDQTyOdUAkKn1bLmWPWmhE3rAmkryEdmE%22%2C%22lego_sid%22%3A%22110967499212892323512397686%22%2C%22platformCompany%22%3A%22ajk%22%2C%22lego_show_track_url_base%22%3A%22https%3A%5C%2F%5C%2Fadtrack.58.com%5C%2F1002359%5C%2F%22%2C%22lego_version%22%3A%221.0.1%22%2C%22legoHuiDu%22%3Atrue%7D%7D",
                "tid": "c435bae5-5f7e-4704-ae2c-8119ba5be148",
                "isLiving": false,
                "isVR": false,
                "distanceDesc": "",
                "isGoldOwner": false
            },
            {
                "infoid": "1995212605283337",
                "cateName": "整租",
                "cateID": "11",
                "title": "望广州塔 中大 赤岗 客村 酒店公寓 全新装修",
                "price": "1980",
                "img": "https://pic1.ajkimg.com/display/anjuke/0db315d4887f9617ce113857d5d0559e/240x180c.jpg?t=1&srotate=1",
                "unit": "元/月",
                "coll": "0",
                "subwayDistance": "距8号线-中大站步行171m",
                "fangwuLiangDian": [
                    {
                        "text": "整租",
                        "textColor": "#7397C1",
                        "bgColor": "#7397C1"
                    },
                    {
                        "text": "朝南",
                        "textColor": "#7397C1",
                        "bgColor": "#7397C1"
                    },
                    {
                        "text": "近地铁",
                        "textColor": "#7397C1",
                        "bgColor": "#7397C1"
                    },
                    {
                        "text": "有电梯",
                        "textColor": "#7397C1",
                        "bgColor": "#7397C1"
                    },
                    {
                        "text": "精装修",
                        "textColor": "#7397C1",
                        "bgColor": "#7397C1"
                    }
                ],
                "area": "50㎡",
                "toward": "朝南",
                "shangquan": "新港西",
                "isAnxuan": "0",
                "location": false,
                "source_type": "1",
                "isauction": "1",
                "auctionShowText": "",
                "legoAuction": "%7B%22isauction%22%3A%221%22%2C%22lego%22%3A%7B%22lego_tid%22%3A%22%22%2C%22lego_ad_click_url%22%3A%22%22%2C%22lego_sid%22%3A%22110967499212892323512397686%22%2C%22platformCompany%22%3A%22ajk%22%2C%22lego_show_track_url_base%22%3A%22%22%2C%22lego_version%22%3A%221.0.1%22%2C%22legoHuiDu%22%3Atrue%7D%7D",
                "tid": "",
                "isLiving": false,
                "isVR": false,
                "distanceDesc": "",
                "isGoldOwner": false
            },
            {
                "infoid": "2009561405381633",
                "cateName": "整租",
                "cateID": "11",
                "title": "中大地铁 周边环境保证安全从不接住闲杂人等,租得放心住得舒心",
                "price": "800",
                "img": "https://pic1.ajkimg.com/display/anjuke/e33e503583552094aa52bb782b52f15a/240x180c.jpg?t=1&srotate=1",
                "unit": "元/月",
                "coll": "0",
                "subwayDistance": "距8号线-中大站步行1.8km",
                "fangwuLiangDian": [
                    {
                        "text": "整租",
                        "textColor": "#7397C1",
                        "bgColor": "#7397C1"
                    },
                    {
                        "text": "南北",
                        "textColor": "#7397C1",
                        "bgColor": "#7397C1"
                    },
                    {
                        "text": "近地铁",
                        "textColor": "#7397C1",
                        "bgColor": "#7397C1"
                    },
                    {
                        "text": "有电梯",
                        "textColor": "#7397C1",
                        "bgColor": "#7397C1"
                    },
                    {
                        "text": "豪华装修",
                        "textColor": "#7397C1",
                        "bgColor": "#7397C1"
                    }
                ],
                "area": "35㎡",
                "toward": "南北",
                "shangquan": "中大",
                "isAnxuan": "0",
                "location": false,
                "source_type": "1",
                "isauction": "1",
                "auctionShowText": "",
                "legoAuction": "%7B%22isauction%22%3A%221%22%2C%22lego%22%3A%7B%22lego_tid%22%3A%22%22%2C%22lego_ad_click_url%22%3A%22%22%2C%22lego_sid%22%3A%22110967499212892323512397686%22%2C%22platformCompany%22%3A%22ajk%22%2C%22lego_show_track_url_base%22%3A%22%22%2C%22lego_version%22%3A%221.0.1%22%2C%22legoHuiDu%22%3Atrue%7D%7D",
                "tid": "",
                "isLiving": false,
                "isVR": false,
                "distanceDesc": "",
                "isGoldOwner": false
            },
            {
                "infoid": "1871917793533955",
                "cateName": "整租",
                "cateID": "11",
                "title": "两房一厅 招租了 全新为上班一族提供安静,舒适的家",
                "price": "1800",
                "img": "https://pic1.ajkimg.com/display/anjuke/99dec196efaedba3d09235dff3104863/240x180c.jpg?t=1&srotate=1",
                "unit": "元/月",
                "coll": "0",
                "subwayDistance": "距8号线-中大站步行1.3km",
                "fangwuLiangDian": [
                    {
                        "text": "整租",
                        "textColor": "#7397C1",
                        "bgColor": "#7397C1"
                    },
                    {
                        "text": "东南",
                        "textColor": "#7397C1",
                        "bgColor": "#7397C1"
                    },
                    {
                        "text": "近地铁",
                        "textColor": "#7397C1",
                        "bgColor": "#7397C1"
                    },
                    {
                        "text": "精装修",
                        "textColor": "#7397C1",
                        "bgColor": "#7397C1"
                    }
                ],
                "area": "56㎡",
                "toward": "东南",
                "shangquan": "江南西",
                "isAnxuan": "0",
                "location": false,
                "source_type": "1",
                "isauction": "1",
                "auctionShowText": "",
                "legoAuction": "%7B%22isauction%22%3A%221%22%2C%22lego%22%3A%7B%22lego_tid%22%3A%22%22%2C%22lego_ad_click_url%22%3A%22%22%2C%22lego_sid%22%3A%22110967499212892323512397686%22%2C%22platformCompany%22%3A%22ajk%22%2C%22lego_show_track_url_base%22%3A%22%22%2C%22lego_version%22%3A%221.0.1%22%2C%22legoHuiDu%22%3Atrue%7D%7D",
                "tid": "",
                "isLiving": false,
                "isVR": false,
                "distanceDesc": "",
                "isGoldOwner": false
            },
            {
                "infoid": "2010983993826310",
                "cateName": "整租",
                "cateID": "11",
                "title": "2号线火爆单间  可押一 拎包入住全新公寓",
                "price": "1980",
                "img": "https://pic1.ajkimg.com/display/anjuke/243105d66a8918d8af990b815f01476a/240x180c.jpg?t=1&srotate=1",
                "unit": "元/月",
                "coll": "0",
                "subwayDistance": "距8号线-中大站步行1.9km",
                "fangwuLiangDian": [
                    {
                        "text": "整租",
                        "textColor": "#7397C1",
                        "bgColor": "#7397C1"
                    },
                    {
                        "text": "朝南",
                        "textColor": "#7397C1",
                        "bgColor": "#7397C1"
                    },
                    {
                        "text": "近地铁",
                        "textColor": "#7397C1",
                        "bgColor": "#7397C1"
                    },
                    {
                        "text": "有电梯",
                        "textColor": "#7397C1",
                        "bgColor": "#7397C1"
                    }
                ],
                "area": "30㎡",
                "toward": "朝南",
                "shangquan": "东晓南",
                "isAnxuan": "0",
                "location": false,
                "source_type": "1",
                "isauction": "1",
                "auctionShowText": "",
                "legoAuction": "%7B%22isauction%22%3A%221%22%2C%22lego%22%3A%7B%22lego_tid%22%3A%22%22%2C%22lego_ad_click_url%22%3A%22%22%2C%22lego_sid%22%3A%22110967499212892323512397686%22%2C%22platformCompany%22%3A%22ajk%22%2C%22lego_show_track_url_base%22%3A%22%22%2C%22lego_version%22%3A%221.0.1%22%2C%22legoHuiDu%22%3Atrue%7D%7D",
                "tid": "",
                "isLiving": false,
                "isVR": false,
                "distanceDesc": "",
                "isGoldOwner": false
            },
            {
                "infoid": "1880925878934538",
                "cateName": "整租",
                "cateID": "11",
                "title": "海珠区江泰路泰宁新街泰沙路力能加油站电梯公寓 采光好新房出租",
                "price": "1900",
                "img": "https://pic1.ajkimg.com/display/anjuke/c2d38c5e41fd05c9a60e7bfef225e6c7/240x180c.jpg?t=1&srotate=1",
                "unit": "元/月",
                "coll": "0",
                "subwayDistance": "距8号线-中大站步行1.4km",
                "fangwuLiangDian": [
                    {
                        "text": "整租",
                        "textColor": "#7397C1",
                        "bgColor": "#7397C1"
                    },
                    {
                        "text": "朝南",
                        "textColor": "#7397C1",
                        "bgColor": "#7397C1"
                    },
                    {
                        "text": "近地铁",
                        "textColor": "#7397C1",
                        "bgColor": "#7397C1"
                    },
                    {
                        "text": "有电梯",
                        "textColor": "#7397C1",
                        "bgColor": "#7397C1"
                    },
                    {
                        "text": "精装修",
                        "textColor": "#7397C1",
                        "bgColor": "#7397C1"
                    }
                ],
                "area": "33㎡",
                "toward": "朝南",
                "shangquan": "江南西",
                "isAnxuan": "0",
                "location": false,
                "source_type": "1",
                "isauction": "1",
                "auctionShowText": "",
                "legoAuction": "%7B%22isauction%22%3A%221%22%2C%22lego%22%3A%7B%22lego_tid%22%3A%22%22%2C%22lego_ad_click_url%22%3A%22%22%2C%22lego_sid%22%3A%22110967499212892323512397686%22%2C%22platformCompany%22%3A%22ajk%22%2C%22lego_show_track_url_base%22%3A%22%22%2C%22lego_version%22%3A%221.0.1%22%2C%22legoHuiDu%22%3Atrue%7D%7D",
                "tid": "",
                "isLiving": false,
                "isVR": false,
                "distanceDesc": "",
                "isGoldOwner": false
            },
            {
                "infoid": "2033548164127749",
                "cateName": "整租",
                "cateID": "11",
                "title": "美术学院宿舍 1室0厅1卫 2000元月 30平",
                "price": "2000",
                "img": "https://pic1.ajkimg.com/display/anjuke/968233b7ad0de0cc29b3c4fab1f0203b/240x180c.jpg?t=1&srotate=1",
                "unit": "元/月",
                "coll": "0",
                "subwayDistance": "距8号线-中大站步行1.9km",
                "fangwuLiangDian": [
                    {
                        "text": "整租",
                        "textColor": "#7397C1",
                        "bgColor": "#7397C1"
                    },
                    {
                        "text": "朝南",
                        "textColor": "#7397C1",
                        "bgColor": "#7397C1"
                    },
                    {
                        "text": "近地铁",
                        "textColor": "#7397C1",
                        "bgColor": "#7397C1"
                    },
                    {
                        "text": "精装修",
                        "textColor": "#7397C1",
                        "bgColor": "#7397C1"
                    }
                ],
                "area": "30㎡",
                "toward": "朝南",
                "shangquan": "晓港",
                "isAnxuan": "0",
                "location": false,
                "source_type": "1",
                "isauction": "1",
                "auctionShowText": "",
                "legoAuction": "%7B%22isauction%22%3A%221%22%2C%22lego%22%3A%7B%22lego_tid%22%3A%22%22%2C%22lego_ad_click_url%22%3A%22%22%2C%22lego_sid%22%3A%22110967499212892323512397686%22%2C%22platformCompany%22%3A%22ajk%22%2C%22lego_show_track_url_base%22%3A%22%22%2C%22lego_version%22%3A%221.0.1%22%2C%22legoHuiDu%22%3Atrue%7D%7D",
                "tid": "",
                "isLiving": false,
                "isVR": false,
                "distanceDesc": "",
                "isGoldOwner": false
            },
            {
                "infoid": "1986370389386249",
                "cateName": "整租",
                "cateID": "11",
                "title": "嘉仕花园东区 博雅公寓 精装修电梯房 大单间带阳台  直租",
                "price": "1900",
                "img": "https://pic1.ajkimg.com/display/anjuke/592793eb98077ab6b618144b1d081923/240x180c.jpg?t=1&srotate=1",
                "unit": "元/月",
                "coll": "0",
                "subwayDistance": "距8号线-中大站步行1.6km",
                "fangwuLiangDian": [
                    {
                        "text": "整租",
                        "textColor": "#7397C1",
                        "bgColor": "#7397C1"
                    },
                    {
                        "text": "朝南",
                        "textColor": "#7397C1",
                        "bgColor": "#7397C1"
                    },
                    {
                        "text": "近地铁",
                        "textColor": "#7397C1",
                        "bgColor": "#7397C1"
                    },
                    {
                        "text": "有电梯",
                        "textColor": "#7397C1",
                        "bgColor": "#7397C1"
                    },
                    {
                        "text": "精装修",
                        "textColor": "#7397C1",
                        "bgColor": "#7397C1"
                    }
                ],
                "area": "20㎡",
                "toward": "朝南",
                "shangquan": "滨江东",
                "isAnxuan": "0",
                "location": false,
                "source_type": "1",
                "isauction": "1",
                "auctionShowText": "",
                "legoAuction": "%7B%22isauction%22%3A%221%22%2C%22lego%22%3A%7B%22lego_tid%22%3A%22%22%2C%22lego_ad_click_url%22%3A%22%22%2C%22lego_sid%22%3A%22110967499212892323512397686%22%2C%22platformCompany%22%3A%22ajk%22%2C%22lego_show_track_url_base%22%3A%22%22%2C%22lego_version%22%3A%221.0.1%22%2C%22legoHuiDu%22%3Atrue%7D%7D",
                "tid": "",
                "isLiving": false,
                "isVR": false,
                "distanceDesc": "",
                "isGoldOwner": false
            },
            {
                "infoid": "2024313407904783",
                "cateName": "整租",
                "cateID": "11",
                "title": "实图直租晓岗地铁站旁大小单间 一房一厅 家私家电齐全独立厨卫",
                "price": "900",
                "img": "https://pic1.ajkimg.com/display/anjuke/50bf5a53c339e4a16b00196c399d06a7/240x180c.jpg?t=1&srotate=1",
                "unit": "元/月",
                "coll": "0",
                "subwayDistance": "距8号线-中大站步行1.3km",
                "fangwuLiangDian": [
                    {
                        "text": "整租",
                        "textColor": "#7397C1",
                        "bgColor": "#7397C1"
                    },
                    {
                        "text": "南北",
                        "textColor": "#7397C1",
                        "bgColor": "#7397C1"
                    },
                    {
                        "text": "近地铁",
                        "textColor": "#7397C1",
                        "bgColor": "#7397C1"
                    },
                    {
                        "text": "精装修",
                        "textColor": "#7397C1",
                        "bgColor": "#7397C1"
                    }
                ],
                "area": "20㎡",
                "toward": "南北",
                "shangquan": "江南西",
                "isAnxuan": "0",
                "location": false,
                "source_type": "1",
                "isauction": "1",
                "auctionShowText": "",
                "legoAuction": "%7B%22isauction%22%3A%221%22%2C%22lego%22%3A%7B%22lego_tid%22%3A%22%22%2C%22lego_ad_click_url%22%3A%22%22%2C%22lego_sid%22%3A%22110967499212892323512397686%22%2C%22platformCompany%22%3A%22ajk%22%2C%22lego_show_track_url_base%22%3A%22%22%2C%22lego_version%22%3A%221.0.1%22%2C%22legoHuiDu%22%3Atrue%7D%7D",
                "tid": "",
                "isLiving": false,
                "isVR": false,
                "distanceDesc": "",
                "isGoldOwner": false
            },
            {
                "infoid": "1876319597142027",
                "cateName": "整租",
                "cateID": "11",
                "title": "昌岗十佳房源  整租  独立阳台厨卫1000元",
                "price": "1000",
                "img": "https://pic1.ajkimg.com/display/anjuke/956aba88371ca5a5aacdc1de087423a1/240x180c.jpg?t=1&srotate=1",
                "unit": "元/月",
                "coll": "0",
                "subwayDistance": "距8号线-中大站步行1.9km",
                "fangwuLiangDian": [
                    {
                        "text": "整租",
                        "textColor": "#7397C1",
                        "bgColor": "#7397C1"
                    },
                    {
                        "text": "南北",
                        "textColor": "#7397C1",
                        "bgColor": "#7397C1"
                    },
                    {
                        "text": "近地铁",
                        "textColor": "#7397C1",
                        "bgColor": "#7397C1"
                    },
                    {
                        "text": "有电梯",
                        "textColor": "#7397C1",
                        "bgColor": "#7397C1"
                    },
                    {
                        "text": "精装修",
                        "textColor": "#7397C1",
                        "bgColor": "#7397C1"
                    }
                ],
                "area": "23㎡",
                "toward": "南北",
                "shangquan": "昌岗",
                "isAnxuan": "0",
                "location": false,
                "source_type": "1",
                "isauction": "1",
                "auctionShowText": "",
                "legoAuction": "%7B%22isauction%22%3A%221%22%2C%22lego%22%3A%7B%22lego_tid%22%3A%22%22%2C%22lego_ad_click_url%22%3A%22%22%2C%22lego_sid%22%3A%22110967499212892323512397686%22%2C%22platformCompany%22%3A%22ajk%22%2C%22lego_show_track_url_base%22%3A%22%22%2C%22lego_version%22%3A%221.0.1%22%2C%22legoHuiDu%22%3Atrue%7D%7D",
                "tid": "",
                "isLiving": false,
                "isVR": false,
                "distanceDesc": "",
                "isGoldOwner": false
            },
            {
                "infoid": "2041548463348745",
                "cateName": "整租",
                "cateID": "11",
                "title": "采光好，房间干净宽敞，交通便利，拎包入住",
                "price": "600",
                "img": "https://pic1.ajkimg.com/display/anjuke/25b80dbfb4b9dab0380a21dd166dc60b/240x180c.jpg?t=1&srotate=1",
                "unit": "元/月",
                "coll": "0",
                "subwayDistance": "距8号线-中大站步行1.3km",
                "fangwuLiangDian": [
                    {
                        "text": "整租",
                        "textColor": "#7397C1",
                        "bgColor": "#7397C1"
                    },
                    {
                        "text": "朝南",
                        "textColor": "#7397C1",
                        "bgColor": "#7397C1"
                    },
                    {
                        "text": "近地铁",
                        "textColor": "#7397C1",
                        "bgColor": "#7397C1"
                    }
                ],
                "area": "23㎡",
                "toward": "朝南",
                "shangquan": "江南西",
                "isAnxuan": "0",
                "location": false,
                "source_type": "1",
                "isauction": "1",
                "auctionShowText": "",
                "legoAuction": "%7B%22isauction%22%3A%221%22%2C%22lego%22%3A%7B%22lego_tid%22%3A%22%22%2C%22lego_ad_click_url%22%3A%22%22%2C%22lego_sid%22%3A%22110967499212892323512397686%22%2C%22platformCompany%22%3A%22ajk%22%2C%22lego_show_track_url_base%22%3A%22%22%2C%22lego_version%22%3A%221.0.1%22%2C%22legoHuiDu%22%3Atrue%7D%7D",
                "tid": "",
                "isLiving": false,
                "isVR": false,
                "distanceDesc": "",
                "isGoldOwner": false
            },
            {
                "infoid": "2023068902628367",
                "cateName": "整租",
                "cateID": "11",
                "title": "8号线大单间 近地铁（房东）直租 有阳台 拎包入住",
                "price": "600",
                "img": "https://pic1.ajkimg.com/display/anjuke/d79a4680ae109ad94e256f0a06e41afe/240x180c.jpg?t=1&srotate=1",
                "unit": "元/月",
                "coll": "0",
                "subwayDistance": "距8号线-中大站步行1.3km",
                "fangwuLiangDian": [
                    {
                        "text": "整租",
                        "textColor": "#7397C1",
                        "bgColor": "#7397C1"
                    },
                    {
                        "text": "南北",
                        "textColor": "#7397C1",
                        "bgColor": "#7397C1"
                    },
                    {
                        "text": "近地铁",
                        "textColor": "#7397C1",
                        "bgColor": "#7397C1"
                    },
                    {
                        "text": "精装修",
                        "textColor": "#7397C1",
                        "bgColor": "#7397C1"
                    }
                ],
                "area": "27㎡",
                "toward": "南北",
                "shangquan": "江南西",
                "isAnxuan": "0",
                "location": false,
                "source_type": "1",
                "isauction": "1",
                "auctionShowText": "",
                "legoAuction": "%7B%22isauction%22%3A%221%22%2C%22lego%22%3A%7B%22lego_tid%22%3A%22%22%2C%22lego_ad_click_url%22%3A%22%22%2C%22lego_sid%22%3A%22110967499212892323512397686%22%2C%22platformCompany%22%3A%22ajk%22%2C%22lego_show_track_url_base%22%3A%22%22%2C%22lego_version%22%3A%221.0.1%22%2C%22legoHuiDu%22%3Atrue%7D%7D",
                "tid": "",
                "isLiving": false,
                "isVR": false,
                "distanceDesc": "",
                "isGoldOwner": false
            },
            {
                "infoid": "1723274406763524",
                "cateName": "整租",
                "cateID": "11",
                "title": "WowWay品牌公寓地铁8号线整租大阳台独立卫浴",
                "price": "1500",
                "img": "https://pic1.ajkimg.com/display/anjuke/6bb0a52b4d4909ca52fd530e5e275b6b/240x180c.jpg?t=1&srotate=1",
                "unit": "元/月",
                "coll": "0",
                "subwayDistance": "距8号线-中大站步行896m",
                "fangwuLiangDian": [
                    {
                        "text": "整租",
                        "textColor": "#7397C1",
                        "bgColor": "#7397C1"
                    },
                    {
                        "text": "朝南",
                        "textColor": "#7397C1",
                        "bgColor": "#7397C1"
                    },
                    {
                        "text": "近地铁",
                        "textColor": "#7397C1",
                        "bgColor": "#7397C1"
                    },
                    {
                        "text": "有电梯",
                        "textColor": "#7397C1",
                        "bgColor": "#7397C1"
                    },
                    {
                        "text": "豪华装修",
                        "textColor": "#7397C1",
                        "bgColor": "#7397C1"
                    }
                ],
                "area": "20㎡",
                "toward": "朝南",
                "shangquan": "新港西",
                "isAnxuan": "0",
                "location": false,
                "source_type": "1",
                "isauction": "1",
                "auctionShowText": "",
                "legoAuction": "%7B%22isauction%22%3A%221%22%2C%22lego%22%3A%7B%22lego_tid%22%3A%22%22%2C%22lego_ad_click_url%22%3A%22%22%2C%22lego_sid%22%3A%22110967499212892323512397686%22%2C%22platformCompany%22%3A%22ajk%22%2C%22lego_show_track_url_base%22%3A%22%22%2C%22lego_version%22%3A%221.0.1%22%2C%22legoHuiDu%22%3Atrue%7D%7D",
                "tid": "",
                "isLiving": false,
                "isVR": false,
                "distanceDesc": "",
                "isGoldOwner": false
            },
            {
                "infoid": "2033358110172162",
                "cateName": "整租",
                "cateID": "11",
                "title": "昌岗十佳房源.个人房东.近地铁口.温馨一居室，精装修，可短租",
                "price": "1000",
                "img": "https://pic1.ajkimg.com/display/anjuke/d207c22b3b398d484fd430da52f4c2bd/240x180c.jpg?t=1&srotate=1",
                "unit": "元/月",
                "coll": "0",
                "subwayDistance": "距8号线-中大站步行1.3km",
                "fangwuLiangDian": [
                    {
                        "text": "整租",
                        "textColor": "#7397C1",
                        "bgColor": "#7397C1"
                    },
                    {
                        "text": "南北",
                        "textColor": "#7397C1",
                        "bgColor": "#7397C1"
                    },
                    {
                        "text": "近地铁",
                        "textColor": "#7397C1",
                        "bgColor": "#7397C1"
                    },
                    {
                        "text": "有电梯",
                        "textColor": "#7397C1",
                        "bgColor": "#7397C1"
                    },
                    {
                        "text": "精装修",
                        "textColor": "#7397C1",
                        "bgColor": "#7397C1"
                    }
                ],
                "area": "30㎡",
                "toward": "南北",
                "shangquan": "江南西",
                "isAnxuan": "0",
                "location": false,
                "source_type": "1",
                "isauction": "1",
                "auctionShowText": "",
                "legoAuction": "%7B%22isauction%22%3A%221%22%2C%22lego%22%3A%7B%22lego_tid%22%3A%22%22%2C%22lego_ad_click_url%22%3A%22%22%2C%22lego_sid%22%3A%22110967499212892323512397686%22%2C%22platformCompany%22%3A%22ajk%22%2C%22lego_show_track_url_base%22%3A%22%22%2C%22lego_version%22%3A%221.0.1%22%2C%22legoHuiDu%22%3Atrue%7D%7D",
                "tid": "",
                "isLiving": false,
                "isVR": false,
                "distanceDesc": "",
                "isGoldOwner": false
            },
            {
                "infoid": "2030480779615237",
                "cateName": "整租",
                "cateID": "11",
                "title": "毕业生  8号线一房一厅 房东租 配置新 有阳台 厨卫",
                "price": "1000",
                "img": "https://pic1.ajkimg.com/display/anjuke/52f96624f87996ce621a0d8d20df7567/240x180c.jpg?t=1&srotate=1",
                "unit": "元/月",
                "coll": "0",
                "subwayDistance": "距8号线-中大站步行1.3km",
                "fangwuLiangDian": [
                    {
                        "text": "整租",
                        "textColor": "#7397C1",
                        "bgColor": "#7397C1"
                    },
                    {
                        "text": "朝南",
                        "textColor": "#7397C1",
                        "bgColor": "#7397C1"
                    },
                    {
                        "text": "近地铁",
                        "textColor": "#7397C1",
                        "bgColor": "#7397C1"
                    },
                    {
                        "text": "精装修",
                        "textColor": "#7397C1",
                        "bgColor": "#7397C1"
                    }
                ],
                "area": "45㎡",
                "toward": "朝南",
                "shangquan": "江南西",
                "isAnxuan": "0",
                "location": false,
                "source_type": "1",
                "isauction": "1",
                "auctionShowText": "",
                "legoAuction": "%7B%22isauction%22%3A%221%22%2C%22lego%22%3A%7B%22lego_tid%22%3A%22%22%2C%22lego_ad_click_url%22%3A%22%22%2C%22lego_sid%22%3A%22110967499212892323512397686%22%2C%22platformCompany%22%3A%22ajk%22%2C%22lego_show_track_url_base%22%3A%22%22%2C%22lego_version%22%3A%221.0.1%22%2C%22legoHuiDu%22%3Atrue%7D%7D",
                "tid": "",
                "isLiving": false,
                "isVR": false,
                "distanceDesc": "",
                "isGoldOwner": false
            },
            {
                "infoid": "2024519182457860",
                "cateName": "整租",
                "cateID": "11",
                "title": "6月大优惠实图直租晓岗地铁站旁单间一房 家私家电齐全独立厨卫",
                "price": "1500",
                "img": "https://pic1.ajkimg.com/display/anjuke/4f6a78f1a6d159cc2ff83f100d744cac/240x180c.jpg?t=1&srotate=1",
                "unit": "元/月",
                "coll": "0",
                "subwayDistance": "距8号线-中大站步行1.3km",
                "fangwuLiangDian": [
                    {
                        "text": "整租",
                        "textColor": "#7397C1",
                        "bgColor": "#7397C1"
                    },
                    {
                        "text": "南北",
                        "textColor": "#7397C1",
                        "bgColor": "#7397C1"
                    },
                    {
                        "text": "近地铁",
                        "textColor": "#7397C1",
                        "bgColor": "#7397C1"
                    },
                    {
                        "text": "精装修",
                        "textColor": "#7397C1",
                        "bgColor": "#7397C1"
                    }
                ],
                "area": "30㎡",
                "toward": "南北",
                "shangquan": "江南西",
                "isAnxuan": "0",
                "location": false,
                "source_type": "1",
                "isauction": "1",
                "auctionShowText": "",
                "legoAuction": "%7B%22isauction%22%3A%221%22%2C%22lego%22%3A%7B%22lego_tid%22%3A%22%22%2C%22lego_ad_click_url%22%3A%22%22%2C%22lego_sid%22%3A%22110967499212892323512397686%22%2C%22platformCompany%22%3A%22ajk%22%2C%22lego_show_track_url_base%22%3A%22%22%2C%22lego_version%22%3A%221.0.1%22%2C%22legoHuiDu%22%3Atrue%7D%7D",
                "tid": "",
                "isLiving": false,
                "isVR": false,
                "distanceDesc": "",
                "isGoldOwner": false
            },
            {
                "infoid": "2024507217632263",
                "cateName": "整租",
                "cateID": "11",
                "title": "6月大优惠实图直租晓岗地铁站旁单间一房 家私家电齐全独立厨卫",
                "price": "900",
                "img": "https://pic1.ajkimg.com/display/anjuke/61d61c5086c9d75fd0fc32e51d9c1931/240x180c.jpg?t=1&srotate=1",
                "unit": "元/月",
                "coll": "0",
                "subwayDistance": "距8号线-中大站步行1.2km",
                "fangwuLiangDian": [
                    {
                        "text": "整租",
                        "textColor": "#7397C1",
                        "bgColor": "#7397C1"
                    },
                    {
                        "text": "南北",
                        "textColor": "#7397C1",
                        "bgColor": "#7397C1"
                    },
                    {
                        "text": "近地铁",
                        "textColor": "#7397C1",
                        "bgColor": "#7397C1"
                    },
                    {
                        "text": "精装修",
                        "textColor": "#7397C1",
                        "bgColor": "#7397C1"
                    }
                ],
                "area": "20㎡",
                "toward": "南北",
                "shangquan": "晓港",
                "isAnxuan": "0",
                "location": false,
                "source_type": "1",
                "isauction": "1",
                "auctionShowText": "",
                "legoAuction": "%7B%22isauction%22%3A%221%22%2C%22lego%22%3A%7B%22lego_tid%22%3A%22%22%2C%22lego_ad_click_url%22%3A%22%22%2C%22lego_sid%22%3A%22110967499212892323512397686%22%2C%22platformCompany%22%3A%22ajk%22%2C%22lego_show_track_url_base%22%3A%22%22%2C%22lego_version%22%3A%221.0.1%22%2C%22legoHuiDu%22%3Atrue%7D%7D",
                "tid": "",
                "isLiving": false,
                "isVR": false,
                "distanceDesc": "",
                "isGoldOwner": false
            },
            {
                "infoid": "2023284054907910",
                "cateName": "整租",
                "cateID": "11",
                "title": "整租 昌岗房东急租 温馨一居室 舒适的生活社区",
                "price": "1000",
                "img": "https://pic1.ajkimg.com/display/anjuke/4b130f1e95778a6cb1759810d9145c77/240x180c.jpg?t=1&srotate=1",
                "unit": "元/月",
                "coll": "0",
                "subwayDistance": "距8号线-中大站步行1.9km",
                "fangwuLiangDian": [
                    {
                        "text": "整租",
                        "textColor": "#7397C1",
                        "bgColor": "#7397C1"
                    },
                    {
                        "text": "南北",
                        "textColor": "#7397C1",
                        "bgColor": "#7397C1"
                    },
                    {
                        "text": "近地铁",
                        "textColor": "#7397C1",
                        "bgColor": "#7397C1"
                    },
                    {
                        "text": "有电梯",
                        "textColor": "#7397C1",
                        "bgColor": "#7397C1"
                    },
                    {
                        "text": "精装修",
                        "textColor": "#7397C1",
                        "bgColor": "#7397C1"
                    }
                ],
                "area": "23㎡",
                "toward": "南北",
                "shangquan": "昌岗",
                "isAnxuan": "0",
                "location": false,
                "source_type": "1",
                "isauction": "1",
                "auctionShowText": "",
                "legoAuction": "%7B%22isauction%22%3A%221%22%2C%22lego%22%3A%7B%22lego_tid%22%3A%22%22%2C%22lego_ad_click_url%22%3A%22%22%2C%22lego_sid%22%3A%22110967499212892323512397686%22%2C%22platformCompany%22%3A%22ajk%22%2C%22lego_show_track_url_base%22%3A%22%22%2C%22lego_version%22%3A%221.0.1%22%2C%22legoHuiDu%22%3Atrue%7D%7D",
                "tid": "",
                "isLiving": false,
                "isVR": false,
                "distanceDesc": "",
                "isGoldOwner": false
            },
            {
                "infoid": "1892908682386432",
                "cateName": "整租",
                "cateID": "11",
                "title": "建方寓东晓南路店 年签可免一个月 全新公寓 租房礼包管家服务",
                "price": "2000",
                "img": "https://pic1.ajkimg.com/display/anjuke/f00ff00513e37c436e707a5e99f6cb59/240x180c.jpg?t=1&srotate=1",
                "unit": "元/月",
                "coll": "0",
                "subwayDistance": "距8号线-中大站步行1.5km",
                "fangwuLiangDian": [
                    {
                        "text": "整租",
                        "textColor": "#7397C1",
                        "bgColor": "#7397C1"
                    },
                    {
                        "text": "朝南",
                        "textColor": "#7397C1",
                        "bgColor": "#7397C1"
                    },
                    {
                        "text": "近地铁",
                        "textColor": "#7397C1",
                        "bgColor": "#7397C1"
                    },
                    {
                        "text": "有电梯",
                        "textColor": "#7397C1",
                        "bgColor": "#7397C1"
                    },
                    {
                        "text": "精装修",
                        "textColor": "#7397C1",
                        "bgColor": "#7397C1"
                    }
                ],
                "area": "30㎡",
                "toward": "朝南",
                "shangquan": "东晓南",
                "isAnxuan": "0",
                "location": false,
                "source_type": "1",
                "isauction": "1",
                "auctionShowText": "",
                "legoAuction": "%7B%22isauction%22%3A%221%22%2C%22lego%22%3A%7B%22lego_tid%22%3A%22%22%2C%22lego_ad_click_url%22%3A%22%22%2C%22lego_sid%22%3A%22110967499212892323512397686%22%2C%22platformCompany%22%3A%22ajk%22%2C%22lego_show_track_url_base%22%3A%22%22%2C%22lego_version%22%3A%221.0.1%22%2C%22legoHuiDu%22%3Atrue%7D%7D",
                "tid": "",
                "isLiving": false,
                "isVR": false,
                "distanceDesc": "",
                "isGoldOwner": false
            },
            {
                "infoid": "2009561373664269",
                "cateName": "整租",
                "cateID": "11",
                "title": "昌岗地铁站 海珠区 新年特价 精装修 房间温馨舒适整洁",
                "price": "800",
                "img": "https://pic1.ajkimg.com/display/anjuke/414926d833aa59c2f029d87f292cd625/240x180c.jpg?t=1&srotate=1",
                "unit": "元/月",
                "coll": "0",
                "subwayDistance": "距8号线-中大站步行1.3km",
                "fangwuLiangDian": [
                    {
                        "text": "整租",
                        "textColor": "#7397C1",
                        "bgColor": "#7397C1"
                    },
                    {
                        "text": "南北",
                        "textColor": "#7397C1",
                        "bgColor": "#7397C1"
                    },
                    {
                        "text": "近地铁",
                        "textColor": "#7397C1",
                        "bgColor": "#7397C1"
                    },
                    {
                        "text": "有电梯",
                        "textColor": "#7397C1",
                        "bgColor": "#7397C1"
                    },
                    {
                        "text": "精装修",
                        "textColor": "#7397C1",
                        "bgColor": "#7397C1"
                    }
                ],
                "area": "28.9㎡",
                "toward": "南北",
                "shangquan": "江南西",
                "isAnxuan": "0",
                "location": false,
                "source_type": "1",
                "isauction": "1",
                "auctionShowText": "",
                "legoAuction": "%7B%22isauction%22%3A%221%22%2C%22lego%22%3A%7B%22lego_tid%22%3A%22%22%2C%22lego_ad_click_url%22%3A%22%22%2C%22lego_sid%22%3A%22110967499212892323512397686%22%2C%22platformCompany%22%3A%22ajk%22%2C%22lego_show_track_url_base%22%3A%22%22%2C%22lego_version%22%3A%221.0.1%22%2C%22legoHuiDu%22%3Atrue%7D%7D",
                "tid": "",
                "isLiving": false,
                "isVR": false,
                "distanceDesc": "",
                "isGoldOwner": false
            },
            {
                "infoid": "2037366711704586",
                "cateName": "整租",
                "cateID": "11",
                "title": "[房东直。租]2号地铁线东晓南旁精装单间出租 交通便利通风好",
                "price": "1500",
                "img": "https://pic1.ajkimg.com/display/anjuke/8e2dda223166292b8020e94b6272e744/240x180c.jpg?t=1&srotate=1",
                "unit": "元/月",
                "coll": "0",
                "subwayDistance": "距8号线-中大站步行1.5km",
                "fangwuLiangDian": [
                    {
                        "text": "整租",
                        "textColor": "#7397C1",
                        "bgColor": "#7397C1"
                    },
                    {
                        "text": "南北",
                        "textColor": "#7397C1",
                        "bgColor": "#7397C1"
                    },
                    {
                        "text": "近地铁",
                        "textColor": "#7397C1",
                        "bgColor": "#7397C1"
                    },
                    {
                        "text": "有电梯",
                        "textColor": "#7397C1",
                        "bgColor": "#7397C1"
                    },
                    {
                        "text": "精装修",
                        "textColor": "#7397C1",
                        "bgColor": "#7397C1"
                    }
                ],
                "area": "24㎡",
                "toward": "南北",
                "shangquan": "东晓南",
                "isAnxuan": "0",
                "location": false,
                "source_type": "1",
                "isauction": "1",
                "auctionShowText": "",
                "legoAuction": "%7B%22isauction%22%3A%221%22%2C%22lego%22%3A%7B%22lego_tid%22%3A%22%22%2C%22lego_ad_click_url%22%3A%22%22%2C%22lego_sid%22%3A%22110967499212892323512397686%22%2C%22platformCompany%22%3A%22ajk%22%2C%22lego_show_track_url_base%22%3A%22%22%2C%22lego_version%22%3A%221.0.1%22%2C%22legoHuiDu%22%3Atrue%7D%7D",
                "tid": "",
                "isLiving": false,
                "isVR": false,
                "distanceDesc": "",
                "isGoldOwner": false
            },
            {
                "infoid": "2011851970666498",
                "cateName": "整租",
                "cateID": "11",
                "title": "昌岗东一巷小区 1室0厅1卫 900元月 南北通透",
                "price": "900",
                "img": "https://pic1.ajkimg.com/display/anjuke/4c753c03f4e7ed813c2a010d796f24d0/240x180c.jpg?t=1&srotate=1",
                "unit": "元/月",
                "coll": "0",
                "subwayDistance": "距8号线-中大站步行1.3km",
                "fangwuLiangDian": [
                    {
                        "text": "整租",
                        "textColor": "#7397C1",
                        "bgColor": "#7397C1"
                    },
                    {
                        "text": "南北",
                        "textColor": "#7397C1",
                        "bgColor": "#7397C1"
                    },
                    {
                        "text": "近地铁",
                        "textColor": "#7397C1",
                        "bgColor": "#7397C1"
                    },
                    {
                        "text": "有电梯",
                        "textColor": "#7397C1",
                        "bgColor": "#7397C1"
                    },
                    {
                        "text": "精装修",
                        "textColor": "#7397C1",
                        "bgColor": "#7397C1"
                    }
                ],
                "area": "30㎡",
                "toward": "南北",
                "shangquan": "江南西",
                "isAnxuan": "0",
                "location": false,
                "source_type": "1",
                "isauction": "1",
                "auctionShowText": "",
                "legoAuction": "%7B%22isauction%22%3A%221%22%2C%22lego%22%3A%7B%22lego_tid%22%3A%22%22%2C%22lego_ad_click_url%22%3A%22%22%2C%22lego_sid%22%3A%22110967499212892323512397686%22%2C%22platformCompany%22%3A%22ajk%22%2C%22lego_show_track_url_base%22%3A%22%22%2C%22lego_version%22%3A%221.0.1%22%2C%22legoHuiDu%22%3Atrue%7D%7D",
                "tid": "",
                "isLiving": false,
                "isVR": false,
                "distanceDesc": "",
                "isGoldOwner": false
            },
            {
                "infoid": "2033319430872072",
                "cateName": "整租",
                "cateID": "11",
                "title": "整租十佳房源.近地铁.交通便利.整租的生活社区.欢迎前来入住",
                "price": "1000",
                "img": "https://pic1.ajkimg.com/display/anjuke/3971ef63cf353662d8645d95be52c70c/240x180c.jpg?t=1&srotate=1",
                "unit": "元/月",
                "coll": "0",
                "subwayDistance": "距8号线-中大站步行1.9km",
                "fangwuLiangDian": [
                    {
                        "text": "整租",
                        "textColor": "#7397C1",
                        "bgColor": "#7397C1"
                    },
                    {
                        "text": "南北",
                        "textColor": "#7397C1",
                        "bgColor": "#7397C1"
                    },
                    {
                        "text": "近地铁",
                        "textColor": "#7397C1",
                        "bgColor": "#7397C1"
                    },
                    {
                        "text": "有电梯",
                        "textColor": "#7397C1",
                        "bgColor": "#7397C1"
                    },
                    {
                        "text": "精装修",
                        "textColor": "#7397C1",
                        "bgColor": "#7397C1"
                    }
                ],
                "area": "25.5㎡",
                "toward": "南北",
                "shangquan": "昌岗",
                "isAnxuan": "0",
                "location": false,
                "source_type": "1",
                "isauction": "1",
                "auctionShowText": "",
                "legoAuction": "%7B%22isauction%22%3A%221%22%2C%22lego%22%3A%7B%22lego_tid%22%3A%22%22%2C%22lego_ad_click_url%22%3A%22%22%2C%22lego_sid%22%3A%22110967499212892323512397686%22%2C%22platformCompany%22%3A%22ajk%22%2C%22lego_show_track_url_base%22%3A%22%22%2C%22lego_version%22%3A%221.0.1%22%2C%22legoHuiDu%22%3Atrue%7D%7D",
                "tid": "",
                "isLiving": false,
                "isVR": false,
                "distanceDesc": "",
                "isGoldOwner": false
            },
            {
                "infoid": "1734846629929998",
                "cateName": "整租",
                "cateID": "11",
                "title": "小蜗居，大梦想，找房子而烦恼打电话联系我",
                "price": "900",
                "img": "https://pic1.ajkimg.com/display/anjuke/186506ef57577197ba539e79cc6fdb95/240x180c.jpg?t=1&srotate=1",
                "unit": "元/月",
                "coll": "0",
                "subwayDistance": "距8号线-中大站步行1.3km",
                "fangwuLiangDian": [
                    {
                        "text": "整租",
                        "textColor": "#7397C1",
                        "bgColor": "#7397C1"
                    },
                    {
                        "text": "南北",
                        "textColor": "#7397C1",
                        "bgColor": "#7397C1"
                    },
                    {
                        "text": "近地铁",
                        "textColor": "#7397C1",
                        "bgColor": "#7397C1"
                    },
                    {
                        "text": "精装修",
                        "textColor": "#7397C1",
                        "bgColor": "#7397C1"
                    }
                ],
                "area": "48㎡",
                "toward": "南北",
                "shangquan": "江南西",
                "isAnxuan": "0",
                "location": false,
                "source_type": "1",
                "isauction": "1",
                "auctionShowText": "",
                "legoAuction": "%7B%22isauction%22%3A%221%22%2C%22lego%22%3A%7B%22lego_tid%22%3A%22%22%2C%22lego_ad_click_url%22%3A%22%22%2C%22lego_sid%22%3A%22110967499212892323512397686%22%2C%22platformCompany%22%3A%22ajk%22%2C%22lego_show_track_url_base%22%3A%22%22%2C%22lego_version%22%3A%221.0.1%22%2C%22legoHuiDu%22%3Atrue%7D%7D",
                "tid": "",
                "isLiving": false,
                "isVR": false,
                "distanceDesc": "",
                "isGoldOwner": false
            },
            {
                "infoid": "2033041538161667",
                "cateName": "整租",
                "cateID": "11",
                "title": ".全新出租 一房一厅精装修 采光好 南北通风 配套齐全 手慢",
                "price": "611",
                "img": "https://pic1.ajkimg.com/display/anjuke/999dafecf8c6656e384d9b7d0a10be96/240x180c.jpg?t=1&srotate=1",
                "unit": "元/月",
                "coll": "0",
                "subwayDistance": "距8号线-中大站步行1.8km",
                "fangwuLiangDian": [
                    {
                        "text": "整租",
                        "textColor": "#7397C1",
                        "bgColor": "#7397C1"
                    },
                    {
                        "text": "朝西",
                        "textColor": "#7397C1",
                        "bgColor": "#7397C1"
                    },
                    {
                        "text": "近地铁",
                        "textColor": "#7397C1",
                        "bgColor": "#7397C1"
                    },
                    {
                        "text": "精装修",
                        "textColor": "#7397C1",
                        "bgColor": "#7397C1"
                    }
                ],
                "area": "41㎡",
                "toward": "朝西",
                "shangquan": "中大",
                "isAnxuan": "0",
                "location": false,
                "source_type": "1",
                "isauction": "1",
                "auctionShowText": "",
                "legoAuction": "%7B%22isauction%22%3A%221%22%2C%22lego%22%3A%7B%22lego_tid%22%3A%22%22%2C%22lego_ad_click_url%22%3A%22%22%2C%22lego_sid%22%3A%22110967499212892323512397686%22%2C%22platformCompany%22%3A%22ajk%22%2C%22lego_show_track_url_base%22%3A%22%22%2C%22lego_version%22%3A%221.0.1%22%2C%22legoHuiDu%22%3Atrue%7D%7D",
                "tid": "",
                "isLiving": false,
                "isVR": false,
                "distanceDesc": "",
                "isGoldOwner": false
            },
            {
                "infoid": "2042931313057802",
                "cateName": "整租",
                "cateID": "11",
                "title": "遇见公寓，遇见美好，打造精致舒适生活从住开始，可短租",
                "price": "700",
                "img": "https://pic1.ajkimg.com/display/anjuke/1ac61fb3b1efc4ed860775df9dfad0fc/240x180c.jpg?t=1&srotate=1",
                "unit": "元/月",
                "coll": "0",
                "subwayDistance": "距8号线-中大站步行1.9km",
                "fangwuLiangDian": [
                    {
                        "text": "整租",
                        "textColor": "#7397C1",
                        "bgColor": "#7397C1"
                    },
                    {
                        "text": "南北",
                        "textColor": "#7397C1",
                        "bgColor": "#7397C1"
                    },
                    {
                        "text": "近地铁",
                        "textColor": "#7397C1",
                        "bgColor": "#7397C1"
                    },
                    {
                        "text": "精装修",
                        "textColor": "#7397C1",
                        "bgColor": "#7397C1"
                    }
                ],
                "area": "23㎡",
                "toward": "南北",
                "shangquan": "昌岗",
                "isAnxuan": "0",
                "location": false,
                "source_type": "1",
                "isauction": "1",
                "auctionShowText": "",
                "legoAuction": "%7B%22isauction%22%3A%221%22%2C%22lego%22%3A%7B%22lego_tid%22%3A%22%22%2C%22lego_ad_click_url%22%3A%22%22%2C%22lego_sid%22%3A%22110967499212892323512397686%22%2C%22platformCompany%22%3A%22ajk%22%2C%22lego_show_track_url_base%22%3A%22%22%2C%22lego_version%22%3A%221.0.1%22%2C%22legoHuiDu%22%3Atrue%7D%7D",
                "tid": "",
                "isLiving": false,
                "isVR": false,
                "distanceDesc": "",
                "isGoldOwner": false
            },
            {
                "infoid": "1999391114862592",
                "cateName": "整租",
                "cateID": "11",
                "title": "毕业季实习生特价两房一厅，非中介",
                "price": "1280",
                "img": "https://pic1.ajkimg.com/display/anjuke/19e8b01ec5a942da8a933f9ca7398759/240x180c.jpg?t=1&srotate=1",
                "unit": "元/月",
                "coll": "0",
                "subwayDistance": "距8号线-中大站步行1.9km",
                "fangwuLiangDian": [
                    {
                        "text": "整租",
                        "textColor": "#7397C1",
                        "bgColor": "#7397C1"
                    },
                    {
                        "text": "朝东",
                        "textColor": "#7397C1",
                        "bgColor": "#7397C1"
                    },
                    {
                        "text": "近地铁",
                        "textColor": "#7397C1",
                        "bgColor": "#7397C1"
                    },
                    {
                        "text": "有电梯",
                        "textColor": "#7397C1",
                        "bgColor": "#7397C1"
                    },
                    {
                        "text": "精装修",
                        "textColor": "#7397C1",
                        "bgColor": "#7397C1"
                    }
                ],
                "area": "42㎡",
                "toward": "朝东",
                "shangquan": "昌岗",
                "isAnxuan": "0",
                "location": false,
                "source_type": "1",
                "isauction": "1",
                "auctionShowText": "",
                "legoAuction": "%7B%22isauction%22%3A%221%22%2C%22lego%22%3A%7B%22lego_tid%22%3A%22%22%2C%22lego_ad_click_url%22%3A%22%22%2C%22lego_sid%22%3A%22110967499212892323512397686%22%2C%22platformCompany%22%3A%22ajk%22%2C%22lego_show_track_url_base%22%3A%22%22%2C%22lego_version%22%3A%221.0.1%22%2C%22legoHuiDu%22%3Atrue%7D%7D",
                "tid": "",
                "isLiving": false,
                "isVR": false,
                "distanceDesc": "",
                "isGoldOwner": false
            },
            {
                "infoid": "1972480078879751",
                "cateName": "整租",
                "cateID": "11",
                "title": "全新出租 精装一房一厅 家电齐全 交通方便",
                "price": "600",
                "img": "https://pic1.ajkimg.com/display/anjuke/e62312c1c0efcabf725f874b17c49051/240x180c.jpg?t=1&srotate=1",
                "unit": "元/月",
                "coll": "0",
                "subwayDistance": "距8号线-中大站步行1.9km",
                "fangwuLiangDian": [
                    {
                        "text": "整租",
                        "textColor": "#7397C1",
                        "bgColor": "#7397C1"
                    },
                    {
                        "text": "朝南",
                        "textColor": "#7397C1",
                        "bgColor": "#7397C1"
                    },
                    {
                        "text": "近地铁",
                        "textColor": "#7397C1",
                        "bgColor": "#7397C1"
                    },
                    {
                        "text": "有电梯",
                        "textColor": "#7397C1",
                        "bgColor": "#7397C1"
                    },
                    {
                        "text": "精装修",
                        "textColor": "#7397C1",
                        "bgColor": "#7397C1"
                    }
                ],
                "area": "39㎡",
                "toward": "朝南",
                "shangquan": "昌岗",
                "isAnxuan": "0",
                "location": false,
                "source_type": "1",
                "isauction": "1",
                "auctionShowText": "",
                "legoAuction": "%7B%22isauction%22%3A%221%22%2C%22lego%22%3A%7B%22lego_tid%22%3A%22%22%2C%22lego_ad_click_url%22%3A%22%22%2C%22lego_sid%22%3A%22110967499212892323512397686%22%2C%22platformCompany%22%3A%22ajk%22%2C%22lego_show_track_url_base%22%3A%22%22%2C%22lego_version%22%3A%221.0.1%22%2C%22legoHuiDu%22%3Atrue%7D%7D",
                "tid": "",
                "isLiving": false,
                "isVR": false,
                "distanceDesc": "",
                "isGoldOwner": false
            },
            {
                "infoid": "2014611162913802",
                "cateName": "整租",
                "cateID": "11",
                "title": "近地铁8号线 房东租 一房一厅 精装修 家具齐全 光线好",
                "price": "1000",
                "img": "https://pic1.ajkimg.com/display/anjuke/a387cbe5cf515afb2f422036f7622020/240x180c.jpg?t=1&srotate=1",
                "unit": "元/月",
                "coll": "0",
                "subwayDistance": "距8号线-中大站步行1.3km",
                "fangwuLiangDian": [
                    {
                        "text": "整租",
                        "textColor": "#7397C1",
                        "bgColor": "#7397C1"
                    },
                    {
                        "text": "朝南",
                        "textColor": "#7397C1",
                        "bgColor": "#7397C1"
                    },
                    {
                        "text": "近地铁",
                        "textColor": "#7397C1",
                        "bgColor": "#7397C1"
                    },
                    {
                        "text": "精装修",
                        "textColor": "#7397C1",
                        "bgColor": "#7397C1"
                    }
                ],
                "area": "36㎡",
                "toward": "朝南",
                "shangquan": "江南西",
                "isAnxuan": "0",
                "location": false,
                "source_type": "1",
                "isauction": "1",
                "auctionShowText": "",
                "legoAuction": "%7B%22isauction%22%3A%221%22%2C%22lego%22%3A%7B%22lego_tid%22%3A%22%22%2C%22lego_ad_click_url%22%3A%22%22%2C%22lego_sid%22%3A%22110967499212892323512397686%22%2C%22platformCompany%22%3A%22ajk%22%2C%22lego_show_track_url_base%22%3A%22%22%2C%22lego_version%22%3A%221.0.1%22%2C%22legoHuiDu%22%3Atrue%7D%7D",
                "tid": "",
                "isLiving": false,
                "isVR": false,
                "distanceDesc": "",
                "isGoldOwner": false
            },
            {
                "infoid": "2038697079020558",
                "cateName": "整租",
                "cateID": "11",
                "title": "滨江东路  新凤凰一房一厅出租  偏欧式风格 家私齐全  低",
                "price": "2000",
                "img": "https://pic1.ajkimg.com/display/anjuke/cd32bd0f7eef4b81d3f21a83d9bde4f7/240x180c.jpg?t=1&srotate=1",
                "unit": "元/月",
                "coll": "0",
                "subwayDistance": "距8号线-中大站步行1.2km",
                "fangwuLiangDian": [
                    {
                        "text": "整租",
                        "textColor": "#7397C1",
                        "bgColor": "#7397C1"
                    },
                    {
                        "text": "朝南",
                        "textColor": "#7397C1",
                        "bgColor": "#7397C1"
                    },
                    {
                        "text": "近地铁",
                        "textColor": "#7397C1",
                        "bgColor": "#7397C1"
                    },
                    {
                        "text": "豪华装修",
                        "textColor": "#7397C1",
                        "bgColor": "#7397C1"
                    }
                ],
                "area": "35㎡",
                "toward": "朝南",
                "shangquan": "滨江东",
                "isAnxuan": "0",
                "location": false,
                "source_type": "1",
                "isauction": "1",
                "auctionShowText": "",
                "legoAuction": "%7B%22isauction%22%3A%221%22%2C%22lego%22%3A%7B%22lego_tid%22%3A%22%22%2C%22lego_ad_click_url%22%3A%22%22%2C%22lego_sid%22%3A%22110967499212892323512397686%22%2C%22platformCompany%22%3A%22ajk%22%2C%22lego_show_track_url_base%22%3A%22%22%2C%22lego_version%22%3A%221.0.1%22%2C%22legoHuiDu%22%3Atrue%7D%7D",
                "tid": "",
                "isLiving": false,
                "isVR": false,
                "distanceDesc": "",
                "isGoldOwner": false
            },
            {
                "infoid": "1734738198127630",
                "cateName": "整租",
                "cateID": "11",
                "title": "价钱实惠！！配置齐全，户型舒适，南北全采光",
                "price": "850",
                "img": "https://pic1.ajkimg.com/display/anjuke/25d0010a39e96c254267667a34e5bcf1/240x180c.jpg?t=1&srotate=1",
                "unit": "元/月",
                "coll": "0",
                "subwayDistance": "距8号线-中大站步行1.3km",
                "fangwuLiangDian": [
                    {
                        "text": "整租",
                        "textColor": "#7397C1",
                        "bgColor": "#7397C1"
                    },
                    {
                        "text": "西北",
                        "textColor": "#7397C1",
                        "bgColor": "#7397C1"
                    },
                    {
                        "text": "近地铁",
                        "textColor": "#7397C1",
                        "bgColor": "#7397C1"
                    }
                ],
                "area": "30㎡",
                "toward": "西北",
                "shangquan": "江南西",
                "isAnxuan": "0",
                "location": false,
                "source_type": "1",
                "isauction": "1",
                "auctionShowText": "",
                "legoAuction": "%7B%22isauction%22%3A%221%22%2C%22lego%22%3A%7B%22lego_tid%22%3A%22%22%2C%22lego_ad_click_url%22%3A%22%22%2C%22lego_sid%22%3A%22110967499212892323512397686%22%2C%22platformCompany%22%3A%22ajk%22%2C%22lego_show_track_url_base%22%3A%22%22%2C%22lego_version%22%3A%221.0.1%22%2C%22legoHuiDu%22%3Atrue%7D%7D",
                "tid": "",
                "isLiving": false,
                "isVR": false,
                "distanceDesc": "",
                "isGoldOwner": false
            },
            {
                "infoid": "2037552400720896",
                "cateName": "整租",
                "cateID": "11",
                "title": "3号线海珠 地铁站全新高端电梯公寓招租采光通风极好拎包入住",
                "price": "1280",
                "img": "https://pic1.ajkimg.com/display/anjuke/5a84c3cead8cb2f1cd4b127d3016d1cf/240x180c.jpg?t=1&srotate=1",
                "unit": "元/月",
                "coll": "0",
                "subwayDistance": "距8号线-中大站步行1.3km",
                "fangwuLiangDian": [
                    {
                        "text": "整租",
                        "textColor": "#7397C1",
                        "bgColor": "#7397C1"
                    },
                    {
                        "text": "南北",
                        "textColor": "#7397C1",
                        "bgColor": "#7397C1"
                    },
                    {
                        "text": "近地铁",
                        "textColor": "#7397C1",
                        "bgColor": "#7397C1"
                    },
                    {
                        "text": "有电梯",
                        "textColor": "#7397C1",
                        "bgColor": "#7397C1"
                    },
                    {
                        "text": "精装修",
                        "textColor": "#7397C1",
                        "bgColor": "#7397C1"
                    }
                ],
                "area": "29㎡",
                "toward": "南北",
                "shangquan": "江南西",
                "isAnxuan": "0",
                "location": false,
                "source_type": "1",
                "isauction": "1",
                "auctionShowText": "",
                "legoAuction": "%7B%22isauction%22%3A%221%22%2C%22lego%22%3A%7B%22lego_tid%22%3A%22%22%2C%22lego_ad_click_url%22%3A%22%22%2C%22lego_sid%22%3A%22110967499212892323512397686%22%2C%22platformCompany%22%3A%22ajk%22%2C%22lego_show_track_url_base%22%3A%22%22%2C%22lego_version%22%3A%221.0.1%22%2C%22legoHuiDu%22%3Atrue%7D%7D",
                "tid": "",
                "isLiving": false,
                "isVR": false,
                "distanceDesc": "",
                "isGoldOwner": false
            },
            {
                "infoid": "2018825623149577",
                "cateName": "整租",
                "cateID": "11",
                "title": "昌岗地铁站 海珠区 夏季特价 精装修 房间温馨舒适整洁",
                "price": "1000",
                "img": "https://pic1.ajkimg.com/display/anjuke/38fff8414ac9ee5c9ae7860feb10eba3/240x180c.jpg?t=1&srotate=1",
                "unit": "元/月",
                "coll": "0",
                "subwayDistance": "距8号线-中大站步行1.9km",
                "fangwuLiangDian": [
                    {
                        "text": "整租",
                        "textColor": "#7397C1",
                        "bgColor": "#7397C1"
                    },
                    {
                        "text": "南北",
                        "textColor": "#7397C1",
                        "bgColor": "#7397C1"
                    },
                    {
                        "text": "近地铁",
                        "textColor": "#7397C1",
                        "bgColor": "#7397C1"
                    },
                    {
                        "text": "有电梯",
                        "textColor": "#7397C1",
                        "bgColor": "#7397C1"
                    },
                    {
                        "text": "精装修",
                        "textColor": "#7397C1",
                        "bgColor": "#7397C1"
                    }
                ],
                "area": "36㎡",
                "toward": "南北",
                "shangquan": "昌岗",
                "isAnxuan": "0",
                "location": false,
                "source_type": "1",
                "isauction": "1",
                "auctionShowText": "",
                "legoAuction": "%7B%22isauction%22%3A%221%22%2C%22lego%22%3A%7B%22lego_tid%22%3A%22%22%2C%22lego_ad_click_url%22%3A%22%22%2C%22lego_sid%22%3A%22110967499212892323512397686%22%2C%22platformCompany%22%3A%22ajk%22%2C%22lego_show_track_url_base%22%3A%22%22%2C%22lego_version%22%3A%221.0.1%22%2C%22legoHuiDu%22%3Atrue%7D%7D",
                "tid": "",
                "isLiving": false,
                "isVR": false,
                "distanceDesc": "",
                "isGoldOwner": false
            },
            {
                "infoid": "2016030602861569",
                "cateName": "整租",
                "cateID": "11",
                "title": "广佛线 单间一房一厅 优惠招租啦 交通方便采光格局好",
                "price": "880",
                "img": "https://pic1.ajkimg.com/display/anjuke/94fe4350de3180ceb560b318d2e94cfa/240x180c.jpg?t=1&srotate=1",
                "unit": "元/月",
                "coll": "0",
                "subwayDistance": "距8号线-中大站步行1.0km",
                "fangwuLiangDian": [
                    {
                        "text": "整租",
                        "textColor": "#7397C1",
                        "bgColor": "#7397C1"
                    },
                    {
                        "text": "东南",
                        "textColor": "#7397C1",
                        "bgColor": "#7397C1"
                    },
                    {
                        "text": "近地铁",
                        "textColor": "#7397C1",
                        "bgColor": "#7397C1"
                    },
                    {
                        "text": "精装修",
                        "textColor": "#7397C1",
                        "bgColor": "#7397C1"
                    }
                ],
                "area": "40㎡",
                "toward": "东南",
                "shangquan": "新港西",
                "isAnxuan": "0",
                "location": false,
                "source_type": "1",
                "isauction": "1",
                "auctionShowText": "",
                "legoAuction": "%7B%22isauction%22%3A%221%22%2C%22lego%22%3A%7B%22lego_tid%22%3A%22%22%2C%22lego_ad_click_url%22%3A%22%22%2C%22lego_sid%22%3A%22110967499212892323512397686%22%2C%22platformCompany%22%3A%22ajk%22%2C%22lego_show_track_url_base%22%3A%22%22%2C%22lego_version%22%3A%221.0.1%22%2C%22legoHuiDu%22%3Atrue%7D%7D",
                "tid": "",
                "isLiving": false,
                "isVR": false,
                "distanceDesc": "",
                "isGoldOwner": false
            },
            {
                "infoid": "1990605506159616",
                "cateName": "整租",
                "cateID": "11",
                "title": "精装电梯房 一房一厅780 采光好 家私家电齐全随时看房",
                "price": "780",
                "img": "https://pic1.ajkimg.com/display/anjuke/e0ad931ff370a5feffa5a3db506ee5a9/240x180c.jpg?t=1&srotate=1",
                "unit": "元/月",
                "coll": "0",
                "subwayDistance": "距8号线-中大站步行1.3km",
                "fangwuLiangDian": [
                    {
                        "text": "整租",
                        "textColor": "#7397C1",
                        "bgColor": "#7397C1"
                    },
                    {
                        "text": "朝东",
                        "textColor": "#7397C1",
                        "bgColor": "#7397C1"
                    },
                    {
                        "text": "近地铁",
                        "textColor": "#7397C1",
                        "bgColor": "#7397C1"
                    },
                    {
                        "text": "有电梯",
                        "textColor": "#7397C1",
                        "bgColor": "#7397C1"
                    },
                    {
                        "text": "精装修",
                        "textColor": "#7397C1",
                        "bgColor": "#7397C1"
                    }
                ],
                "area": "38㎡",
                "toward": "朝东",
                "shangquan": "江南西",
                "isAnxuan": "0",
                "location": false,
                "source_type": "1",
                "isauction": "1",
                "auctionShowText": "",
                "legoAuction": "%7B%22isauction%22%3A%221%22%2C%22lego%22%3A%7B%22lego_tid%22%3A%22%22%2C%22lego_ad_click_url%22%3A%22%22%2C%22lego_sid%22%3A%22110967499212892323512397686%22%2C%22platformCompany%22%3A%22ajk%22%2C%22lego_show_track_url_base%22%3A%22%22%2C%22lego_version%22%3A%221.0.1%22%2C%22legoHuiDu%22%3Atrue%7D%7D",
                "tid": "",
                "isLiving": false,
                "isVR": false,
                "distanceDesc": "",
                "isGoldOwner": false
            },
            {
                "infoid": "1821013962629134",
                "cateName": "整租",
                "cateID": "11",
                "title": "南北通透，空气流畅，整租",
                "price": "360",
                "img": "https://pic1.ajkimg.com/display/anjuke/2d0bf97ddb464f7efd36f58987c3c869/240x180c.jpg?t=1&srotate=1",
                "unit": "元/月",
                "coll": "0",
                "subwayDistance": "距8号线-中大站步行1.8km",
                "fangwuLiangDian": [
                    {
                        "text": "整租",
                        "textColor": "#7397C1",
                        "bgColor": "#7397C1"
                    },
                    {
                        "text": "朝南",
                        "textColor": "#7397C1",
                        "bgColor": "#7397C1"
                    },
                    {
                        "text": "近地铁",
                        "textColor": "#7397C1",
                        "bgColor": "#7397C1"
                    },
                    {
                        "text": "精装修",
                        "textColor": "#7397C1",
                        "bgColor": "#7397C1"
                    }
                ],
                "area": "30㎡",
                "toward": "朝南",
                "shangquan": "中大",
                "isAnxuan": "0",
                "location": false,
                "source_type": "1",
                "isauction": "1",
                "auctionShowText": "",
                "legoAuction": "%7B%22isauction%22%3A%221%22%2C%22lego%22%3A%7B%22lego_tid%22%3A%22%22%2C%22lego_ad_click_url%22%3A%22%22%2C%22lego_sid%22%3A%22110967499212892323512397686%22%2C%22platformCompany%22%3A%22ajk%22%2C%22lego_show_track_url_base%22%3A%22%22%2C%22lego_version%22%3A%221.0.1%22%2C%22legoHuiDu%22%3Atrue%7D%7D",
                "tid": "",
                "isLiving": false,
                "isVR": false,
                "distanceDesc": "",
                "isGoldOwner": false
            },
            {
                "infoid": "1980624245496846",
                "cateName": "整租",
                "cateID": "11",
                "title": "怡乐路小区 1室1厅1卫 1900元月 精装修 30平",
                "price": "1900",
                "img": "https://pic1.ajkimg.com/display/anjuke/ac2b6b303e2013865c858d744c394917/240x180c.jpg?t=1&srotate=1",
                "unit": "元/月",
                "coll": "0",
                "subwayDistance": "距8号线-中大站步行656m",
                "fangwuLiangDian": [
                    {
                        "text": "整租",
                        "textColor": "#7397C1",
                        "bgColor": "#7397C1"
                    },
                    {
                        "text": "朝南",
                        "textColor": "#7397C1",
                        "bgColor": "#7397C1"
                    },
                    {
                        "text": "近地铁",
                        "textColor": "#7397C1",
                        "bgColor": "#7397C1"
                    },
                    {
                        "text": "精装修",
                        "textColor": "#7397C1",
                        "bgColor": "#7397C1"
                    }
                ],
                "area": "30㎡",
                "toward": "朝南",
                "shangquan": "江南西",
                "isAnxuan": "0",
                "location": false,
                "source_type": "1",
                "isauction": "1",
                "auctionShowText": "",
                "legoAuction": "%7B%22isauction%22%3A%221%22%2C%22lego%22%3A%7B%22lego_tid%22%3A%22%22%2C%22lego_ad_click_url%22%3A%22%22%2C%22lego_sid%22%3A%22110967499212892323512397686%22%2C%22platformCompany%22%3A%22ajk%22%2C%22lego_show_track_url_base%22%3A%22%22%2C%22lego_version%22%3A%221.0.1%22%2C%22legoHuiDu%22%3Atrue%7D%7D",
                "tid": "",
                "isLiving": false,
                "isVR": false,
                "distanceDesc": "",
                "isGoldOwner": false
            },
            {
                "infoid": "2025061322120204",
                "cateName": "整租",
                "cateID": "11",
                "title": "3号线 地铁口 毕业季特惠 海珠区 绿码地区 直租 拎包入住",
                "price": "1820",
                "img": "https://pic1.ajkimg.com/display/anjuke/5e11899351172614491fad7f2aeb3db5/240x180c.jpg?t=1&srotate=1",
                "unit": "元/月",
                "coll": "0",
                "subwayDistance": "距8号线-中大站步行1.2km",
                "fangwuLiangDian": [
                    {
                        "text": "整租",
                        "textColor": "#7397C1",
                        "bgColor": "#7397C1"
                    },
                    {
                        "text": "朝北",
                        "textColor": "#7397C1",
                        "bgColor": "#7397C1"
                    },
                    {
                        "text": "近地铁",
                        "textColor": "#7397C1",
                        "bgColor": "#7397C1"
                    },
                    {
                        "text": "有电梯",
                        "textColor": "#7397C1",
                        "bgColor": "#7397C1"
                    },
                    {
                        "text": "精装修",
                        "textColor": "#7397C1",
                        "bgColor": "#7397C1"
                    }
                ],
                "area": "28㎡",
                "toward": "朝北",
                "shangquan": "东晓南",
                "isAnxuan": "0",
                "location": false,
                "source_type": "1",
                "isauction": "1",
                "auctionShowText": "",
                "legoAuction": "%7B%22isauction%22%3A%221%22%2C%22lego%22%3A%7B%22lego_tid%22%3A%22%22%2C%22lego_ad_click_url%22%3A%22%22%2C%22lego_sid%22%3A%22110967499212892323512397686%22%2C%22platformCompany%22%3A%22ajk%22%2C%22lego_show_track_url_base%22%3A%22%22%2C%22lego_version%22%3A%221.0.1%22%2C%22legoHuiDu%22%3Atrue%7D%7D",
                "tid": "",
                "isLiving": false,
                "isVR": false,
                "distanceDesc": "",
                "isGoldOwner": false
            },
            {
                "infoid": "2004709330625550",
                "cateName": "整租",
                "cateID": "11",
                "title": "怡乐路  一房一厅  近中大地铁口  家私齐全 租1900",
                "price": "1900",
                "img": "https://pic1.ajkimg.com/display/anjuke/e87f53e0c56d3598bcbb929654ef25d4/240x180c.jpg?t=1&srotate=1",
                "unit": "元/月",
                "coll": "0",
                "subwayDistance": "距8号线-中大站步行656m",
                "fangwuLiangDian": [
                    {
                        "text": "整租",
                        "textColor": "#7397C1",
                        "bgColor": "#7397C1"
                    },
                    {
                        "text": "朝南",
                        "textColor": "#7397C1",
                        "bgColor": "#7397C1"
                    },
                    {
                        "text": "近地铁",
                        "textColor": "#7397C1",
                        "bgColor": "#7397C1"
                    },
                    {
                        "text": "精装修",
                        "textColor": "#7397C1",
                        "bgColor": "#7397C1"
                    }
                ],
                "area": "35㎡",
                "toward": "朝南",
                "shangquan": "江南西",
                "isAnxuan": "0",
                "location": false,
                "source_type": "1",
                "isauction": "1",
                "auctionShowText": "",
                "legoAuction": "%7B%22isauction%22%3A%221%22%2C%22lego%22%3A%7B%22lego_tid%22%3A%22%22%2C%22lego_ad_click_url%22%3A%22%22%2C%22lego_sid%22%3A%22110967499212892323512397686%22%2C%22platformCompany%22%3A%22ajk%22%2C%22lego_show_track_url_base%22%3A%22%22%2C%22lego_version%22%3A%221.0.1%22%2C%22legoHuiDu%22%3Atrue%7D%7D",
                "tid": "",
                "isLiving": false,
                "isVR": false,
                "distanceDesc": "",
                "isGoldOwner": false
            },
            {
                "infoid": "1890092113110016",
                "cateName": "整租",
                "cateID": "11",
                "title": "大单间近地铁押一付一大阳台带可养宠物 单身情侣温馨公寓免中介",
                "price": "1000",
                "img": "https://pic1.ajkimg.com/display/anjuke/ba10a0ec92cd164a3f3770712efca708/240x180c.jpg?t=1&srotate=1",
                "unit": "元/月",
                "coll": "0",
                "subwayDistance": "距8号线-中大站步行1.0km",
                "fangwuLiangDian": [
                    {
                        "text": "整租",
                        "textColor": "#7397C1",
                        "bgColor": "#7397C1"
                    },
                    {
                        "text": "南北",
                        "textColor": "#7397C1",
                        "bgColor": "#7397C1"
                    },
                    {
                        "text": "近地铁",
                        "textColor": "#7397C1",
                        "bgColor": "#7397C1"
                    },
                    {
                        "text": "有电梯",
                        "textColor": "#7397C1",
                        "bgColor": "#7397C1"
                    },
                    {
                        "text": "精装修",
                        "textColor": "#7397C1",
                        "bgColor": "#7397C1"
                    }
                ],
                "area": "23㎡",
                "toward": "南北",
                "shangquan": "新港西",
                "isAnxuan": "0",
                "location": false,
                "source_type": "1",
                "isauction": "1",
                "auctionShowText": "",
                "legoAuction": "%7B%22isauction%22%3A%221%22%2C%22lego%22%3A%7B%22lego_tid%22%3A%22%22%2C%22lego_ad_click_url%22%3A%22%22%2C%22lego_sid%22%3A%22110967499212892323512397686%22%2C%22platformCompany%22%3A%22ajk%22%2C%22lego_show_track_url_base%22%3A%22%22%2C%22lego_version%22%3A%221.0.1%22%2C%22legoHuiDu%22%3Atrue%7D%7D",
                "tid": "",
                "isLiving": false,
                "isVR": false,
                "distanceDesc": "",
                "isGoldOwner": false
            },
            {
                "infoid": "2043320670904328",
                "cateName": "整租",
                "cateID": "11",
                "title": "地铁站附近 出行方便 精美装修 南北通风 先到先得 手慢无",
                "price": "404",
                "img": "https://pic1.ajkimg.com/display/anjuke/69ae5e5aa3bc6f6b616b9a6590cc9819/240x180c.jpg?t=1&srotate=1",
                "unit": "元/月",
                "coll": "0",
                "subwayDistance": "距8号线-中大站步行1.8km",
                "fangwuLiangDian": [
                    {
                        "text": "整租",
                        "textColor": "#7397C1",
                        "bgColor": "#7397C1"
                    },
                    {
                        "text": "朝南",
                        "textColor": "#7397C1",
                        "bgColor": "#7397C1"
                    },
                    {
                        "text": "近地铁",
                        "textColor": "#7397C1",
                        "bgColor": "#7397C1"
                    },
                    {
                        "text": "精装修",
                        "textColor": "#7397C1",
                        "bgColor": "#7397C1"
                    }
                ],
                "area": "32㎡",
                "toward": "朝南",
                "shangquan": "中大",
                "isAnxuan": "0",
                "location": false,
                "source_type": "1",
                "isauction": "1",
                "auctionShowText": "",
                "legoAuction": "%7B%22isauction%22%3A%221%22%2C%22lego%22%3A%7B%22lego_tid%22%3A%22%22%2C%22lego_ad_click_url%22%3A%22%22%2C%22lego_sid%22%3A%22110967499212892323512397686%22%2C%22platformCompany%22%3A%22ajk%22%2C%22lego_show_track_url_base%22%3A%22%22%2C%22lego_version%22%3A%221.0.1%22%2C%22legoHuiDu%22%3Atrue%7D%7D",
                "tid": "",
                "isLiving": false,
                "isVR": false,
                "distanceDesc": "",
                "isGoldOwner": false
            },
            {
                "infoid": "2023423121268745",
                "cateName": "整租",
                "cateID": "11",
                "title": ".地铁附近 两房一厅精装房 采光好 南北通风 出行方便 手慢",
                "price": "899",
                "img": "https://pic1.ajkimg.com/display/anjuke/f75c97709d740ceee1ca625971fdb0dc/240x180c.jpg?t=1&srotate=1",
                "unit": "元/月",
                "coll": "0",
                "subwayDistance": "距8号线-中大站步行1.8km",
                "fangwuLiangDian": [
                    {
                        "text": "整租",
                        "textColor": "#7397C1",
                        "bgColor": "#7397C1"
                    },
                    {
                        "text": "朝西",
                        "textColor": "#7397C1",
                        "bgColor": "#7397C1"
                    },
                    {
                        "text": "近地铁",
                        "textColor": "#7397C1",
                        "bgColor": "#7397C1"
                    },
                    {
                        "text": "精装修",
                        "textColor": "#7397C1",
                        "bgColor": "#7397C1"
                    }
                ],
                "area": "55㎡",
                "toward": "朝西",
                "shangquan": "中大",
                "isAnxuan": "0",
                "location": false,
                "source_type": "1",
                "isauction": "1",
                "auctionShowText": "",
                "legoAuction": "%7B%22isauction%22%3A%221%22%2C%22lego%22%3A%7B%22lego_tid%22%3A%22%22%2C%22lego_ad_click_url%22%3A%22%22%2C%22lego_sid%22%3A%22110967499212892323512397686%22%2C%22platformCompany%22%3A%22ajk%22%2C%22lego_show_track_url_base%22%3A%22%22%2C%22lego_version%22%3A%221.0.1%22%2C%22legoHuiDu%22%3Atrue%7D%7D",
                "tid": "",
                "isLiving": false,
                "isVR": false,
                "distanceDesc": "",
                "isGoldOwner": false
            },
            {
                "infoid": "2044350440087554",
                "cateName": "整租",
                "cateID": "11",
                "title": "晓怡居 全新精装修公寓，性价比高，采光好拎包入住",
                "price": "560",
                "img": "https://pic1.ajkimg.com/display/anjuke/bc356e9d64b1ee66ecbb6c488aab52c6/240x180c.jpg?t=1&srotate=1",
                "unit": "元/月",
                "coll": "0",
                "subwayDistance": "距8号线-中大站步行1.2km",
                "fangwuLiangDian": [
                    {
                        "text": "整租",
                        "textColor": "#7397C1",
                        "bgColor": "#7397C1"
                    },
                    {
                        "text": "东南",
                        "textColor": "#7397C1",
                        "bgColor": "#7397C1"
                    },
                    {
                        "text": "近地铁",
                        "textColor": "#7397C1",
                        "bgColor": "#7397C1"
                    },
                    {
                        "text": "新上",
                        "textColor": "#7397C1",
                        "bgColor": "#7397C1"
                    }
                ],
                "area": "20㎡",
                "toward": "东南",
                "shangquan": "晓港",
                "isAnxuan": "0",
                "location": false,
                "source_type": "1",
                "isauction": "1",
                "auctionShowText": "",
                "legoAuction": "%7B%22isauction%22%3A%221%22%2C%22lego%22%3A%7B%22lego_tid%22%3A%22%22%2C%22lego_ad_click_url%22%3A%22%22%2C%22lego_sid%22%3A%22110967499212892323512397686%22%2C%22platformCompany%22%3A%22ajk%22%2C%22lego_show_track_url_base%22%3A%22%22%2C%22lego_version%22%3A%221.0.1%22%2C%22legoHuiDu%22%3Atrue%7D%7D",
                "tid": "",
                "isLiving": false,
                "isVR": false,
                "distanceDesc": "",
                "isGoldOwner": false
            },
            {
                "infoid": "2041838783963151",
                "cateName": "整租",
                "cateID": "11",
                "title": "远洋宿舍，晓港，中大交通方便，精装大单间，装修好，价格便宜",
                "price": "1700",
                "img": "https://pic1.ajkimg.com/display/anjuke/a251d9b809588812135a5f93952fbecf/240x180c.jpg?t=1&srotate=1",
                "unit": "元/月",
                "coll": "0",
                "subwayDistance": "距8号线-中大站步行720m",
                "fangwuLiangDian": [
                    {
                        "text": "整租",
                        "textColor": "#7397C1",
                        "bgColor": "#7397C1"
                    },
                    {
                        "text": "南北",
                        "textColor": "#7397C1",
                        "bgColor": "#7397C1"
                    },
                    {
                        "text": "近地铁",
                        "textColor": "#7397C1",
                        "bgColor": "#7397C1"
                    },
                    {
                        "text": "精装修",
                        "textColor": "#7397C1",
                        "bgColor": "#7397C1"
                    }
                ],
                "area": "30㎡",
                "toward": "南北",
                "shangquan": "新港西",
                "isAnxuan": "0",
                "location": false,
                "source_type": "1",
                "isauction": "1",
                "auctionShowText": "",
                "legoAuction": "%7B%22isauction%22%3A%221%22%2C%22lego%22%3A%7B%22lego_tid%22%3A%22%22%2C%22lego_ad_click_url%22%3A%22%22%2C%22lego_sid%22%3A%22110967499212892323512397686%22%2C%22platformCompany%22%3A%22ajk%22%2C%22lego_show_track_url_base%22%3A%22%22%2C%22lego_version%22%3A%221.0.1%22%2C%22legoHuiDu%22%3Atrue%7D%7D",
                "tid": "",
                "isLiving": false,
                "isVR": false,
                "distanceDesc": "",
                "isGoldOwner": false
            },
            {
                "infoid": "1992512044490757",
                "cateName": "整租",
                "cateID": "11",
                "title": "昌岗地铁口晓港地铁口月付可短租)无中介(家电全)",
                "price": "500",
                "img": "https://pic1.ajkimg.com/display/anjuke/33c1847f40ab79c3f672ae6074042bb3/240x180c.jpg?t=1&srotate=1",
                "unit": "元/月",
                "coll": "0",
                "subwayDistance": "距8号线-中大站步行1.3km",
                "fangwuLiangDian": [
                    {
                        "text": "整租",
                        "textColor": "#7397C1",
                        "bgColor": "#7397C1"
                    },
                    {
                        "text": "朝南",
                        "textColor": "#7397C1",
                        "bgColor": "#7397C1"
                    },
                    {
                        "text": "近地铁",
                        "textColor": "#7397C1",
                        "bgColor": "#7397C1"
                    },
                    {
                        "text": "精装修",
                        "textColor": "#7397C1",
                        "bgColor": "#7397C1"
                    }
                ],
                "area": "20㎡",
                "toward": "朝南",
                "shangquan": "江南西",
                "isAnxuan": "0",
                "location": false,
                "source_type": "1",
                "isauction": "1",
                "auctionShowText": "",
                "legoAuction": "%7B%22isauction%22%3A%221%22%2C%22lego%22%3A%7B%22lego_tid%22%3A%22%22%2C%22lego_ad_click_url%22%3A%22%22%2C%22lego_sid%22%3A%22110967499212892323512397686%22%2C%22platformCompany%22%3A%22ajk%22%2C%22lego_show_track_url_base%22%3A%22%22%2C%22lego_version%22%3A%221.0.1%22%2C%22legoHuiDu%22%3Atrue%7D%7D",
                "tid": "",
                "isLiving": false,
                "isVR": false,
                "distanceDesc": "",
                "isGoldOwner": false
            },
            {
                "infoid": "2023751548264457",
                "cateName": "整租",
                "cateID": "11",
                "title": "一个月起租 全新公寓房欧式简约风格 家私家电齐全赠送wifi",
                "price": "1500",
                "img": "https://pic1.ajkimg.com/display/anjuke/79dbc8fe294eb6c4cade3c5a2142ab93/240x180c.jpg?t=1&srotate=1",
                "unit": "元/月",
                "coll": "0",
                "subwayDistance": "距8号线-中大站步行831m",
                "fangwuLiangDian": [
                    {
                        "text": "整租",
                        "textColor": "#7397C1",
                        "bgColor": "#7397C1"
                    },
                    {
                        "text": "南北",
                        "textColor": "#7397C1",
                        "bgColor": "#7397C1"
                    },
                    {
                        "text": "近地铁",
                        "textColor": "#7397C1",
                        "bgColor": "#7397C1"
                    },
                    {
                        "text": "有电梯",
                        "textColor": "#7397C1",
                        "bgColor": "#7397C1"
                    },
                    {
                        "text": "精装修",
                        "textColor": "#7397C1",
                        "bgColor": "#7397C1"
                    }
                ],
                "area": "24㎡",
                "toward": "南北",
                "shangquan": "中大",
                "isAnxuan": "0",
                "location": false,
                "source_type": "1",
                "isauction": "1",
                "auctionShowText": "",
                "legoAuction": "%7B%22isauction%22%3A%221%22%2C%22lego%22%3A%7B%22lego_tid%22%3A%22%22%2C%22lego_ad_click_url%22%3A%22%22%2C%22lego_sid%22%3A%22110967499212892323512397686%22%2C%22platformCompany%22%3A%22ajk%22%2C%22lego_show_track_url_base%22%3A%22%22%2C%22lego_version%22%3A%221.0.1%22%2C%22legoHuiDu%22%3Atrue%7D%7D",
                "tid": "",
                "isLiving": false,
                "isVR": false,
                "distanceDesc": "",
                "isGoldOwner": false
            },
            {
                "infoid": "2042923389771788",
                "cateName": "整租",
                "cateID": "11",
                "title": "昌岗东一巷小区 1室0厅1卫 600元月 23平",
                "price": "600",
                "img": "https://pic1.ajkimg.com/display/anjuke/f477c17f3f02c7b799b80bc7c07bbbbe/240x180c.jpg?t=1&srotate=1",
                "unit": "元/月",
                "coll": "0",
                "subwayDistance": "距8号线-中大站步行1.3km",
                "fangwuLiangDian": [
                    {
                        "text": "整租",
                        "textColor": "#7397C1",
                        "bgColor": "#7397C1"
                    },
                    {
                        "text": "朝南",
                        "textColor": "#7397C1",
                        "bgColor": "#7397C1"
                    },
                    {
                        "text": "近地铁",
                        "textColor": "#7397C1",
                        "bgColor": "#7397C1"
                    }
                ],
                "area": "23㎡",
                "toward": "朝南",
                "shangquan": "江南西",
                "isAnxuan": "0",
                "location": false,
                "source_type": "1",
                "isauction": "1",
                "auctionShowText": "",
                "legoAuction": "%7B%22isauction%22%3A%221%22%2C%22lego%22%3A%7B%22lego_tid%22%3A%22%22%2C%22lego_ad_click_url%22%3A%22%22%2C%22lego_sid%22%3A%22110967499212892323512397686%22%2C%22platformCompany%22%3A%22ajk%22%2C%22lego_show_track_url_base%22%3A%22%22%2C%22lego_version%22%3A%221.0.1%22%2C%22legoHuiDu%22%3Atrue%7D%7D",
                "tid": "",
                "isLiving": false,
                "isVR": false,
                "distanceDesc": "",
                "isGoldOwner": false
            },
            {
                "infoid": "2043497141969931",
                "cateName": "整租",
                "cateID": "11",
                "title": "毕业季可短租整租房青年白领公寓3号线大塘站地铁不限流",
                "price": "1730",
                "img": "https://pic1.ajkimg.com/display/anjuke/5623299d7c046ab5060b36a3c18629ff/240x180c.jpg?t=1&srotate=1",
                "unit": "元/月",
                "coll": "0",
                "subwayDistance": "距8号线-中大站步行2.0km",
                "fangwuLiangDian": [
                    {
                        "text": "整租",
                        "textColor": "#7397C1",
                        "bgColor": "#7397C1"
                    },
                    {
                        "text": "南北",
                        "textColor": "#7397C1",
                        "bgColor": "#7397C1"
                    },
                    {
                        "text": "近地铁",
                        "textColor": "#7397C1",
                        "bgColor": "#7397C1"
                    },
                    {
                        "text": "有电梯",
                        "textColor": "#7397C1",
                        "bgColor": "#7397C1"
                    },
                    {
                        "text": "精装修",
                        "textColor": "#7397C1",
                        "bgColor": "#7397C1"
                    }
                ],
                "area": "32㎡",
                "toward": "南北",
                "shangquan": "广州大道南",
                "isAnxuan": "0",
                "location": false,
                "source_type": "1",
                "isauction": "1",
                "auctionShowText": "",
                "legoAuction": "%7B%22isauction%22%3A%221%22%2C%22lego%22%3A%7B%22lego_tid%22%3A%22%22%2C%22lego_ad_click_url%22%3A%22%22%2C%22lego_sid%22%3A%22110967499212892323512397686%22%2C%22platformCompany%22%3A%22ajk%22%2C%22lego_show_track_url_base%22%3A%22%22%2C%22lego_version%22%3A%221.0.1%22%2C%22legoHuiDu%22%3Atrue%7D%7D",
                "tid": "",
                "isLiving": false,
                "isVR": false,
                "distanceDesc": "",
                "isGoldOwner": false
            },
            {
                "infoid": "1827210178371593",
                "cateName": "整租",
                "cateID": "11",
                "title": "直租海珠区泰沙路大马路边电梯房，无中介，家私家电齐全",
                "price": "1500",
                "img": "https://pic1.ajkimg.com/display/anjuke/f327c7676426c874200cabadf0c78346/240x180c.jpg?t=1&srotate=1",
                "unit": "元/月",
                "coll": "0",
                "subwayDistance": "距8号线-中大站步行1.7km",
                "fangwuLiangDian": [
                    {
                        "text": "整租",
                        "textColor": "#7397C1",
                        "bgColor": "#7397C1"
                    },
                    {
                        "text": "朝西",
                        "textColor": "#7397C1",
                        "bgColor": "#7397C1"
                    },
                    {
                        "text": "近地铁",
                        "textColor": "#7397C1",
                        "bgColor": "#7397C1"
                    },
                    {
                        "text": "有电梯",
                        "textColor": "#7397C1",
                        "bgColor": "#7397C1"
                    },
                    {
                        "text": "精装修",
                        "textColor": "#7397C1",
                        "bgColor": "#7397C1"
                    }
                ],
                "area": "35㎡",
                "toward": "朝西",
                "shangquan": "江燕路",
                "isAnxuan": "0",
                "location": false,
                "source_type": "1",
                "isauction": "1",
                "auctionShowText": "",
                "legoAuction": "%7B%22isauction%22%3A%221%22%2C%22lego%22%3A%7B%22lego_tid%22%3A%22%22%2C%22lego_ad_click_url%22%3A%22%22%2C%22lego_sid%22%3A%22110967499212892323512397686%22%2C%22platformCompany%22%3A%22ajk%22%2C%22lego_show_track_url_base%22%3A%22%22%2C%22lego_version%22%3A%221.0.1%22%2C%22legoHuiDu%22%3Atrue%7D%7D",
                "tid": "",
                "isLiving": false,
                "isVR": false,
                "distanceDesc": "",
                "isGoldOwner": false
            },
            {
                "infoid": "2037298558051330",
                "cateName": "整租",
                "cateID": "11",
                "title": "宝岗大道地铁口(月付)(可短租)拎包住(家电全)",
                "price": "500",
                "img": "https://pic1.ajkimg.com/display/anjuke/f94afd1cbf1a1bde6fd70acaf0b99fbb/240x180c.jpg?t=1&srotate=1",
                "unit": "元/月",
                "coll": "0",
                "subwayDistance": "距8号线-中大站步行1.9km",
                "fangwuLiangDian": [
                    {
                        "text": "整租",
                        "textColor": "#7397C1",
                        "bgColor": "#7397C1"
                    },
                    {
                        "text": "朝南",
                        "textColor": "#7397C1",
                        "bgColor": "#7397C1"
                    },
                    {
                        "text": "近地铁",
                        "textColor": "#7397C1",
                        "bgColor": "#7397C1"
                    }
                ],
                "area": "12㎡",
                "toward": "朝南",
                "shangquan": "昌岗",
                "isAnxuan": "0",
                "location": false,
                "source_type": "1",
                "isauction": "1",
                "auctionShowText": "",
                "legoAuction": "%7B%22isauction%22%3A%221%22%2C%22lego%22%3A%7B%22lego_tid%22%3A%22%22%2C%22lego_ad_click_url%22%3A%22%22%2C%22lego_sid%22%3A%22110967499212892323512397686%22%2C%22platformCompany%22%3A%22ajk%22%2C%22lego_show_track_url_base%22%3A%22%22%2C%22lego_version%22%3A%221.0.1%22%2C%22legoHuiDu%22%3Atrue%7D%7D",
                "tid": "",
                "isLiving": false,
                "isVR": false,
                "distanceDesc": "",
                "isGoldOwner": false
            },
            {
                "infoid": "1975453740704783",
                "cateName": "整租",
                "cateID": "11",
                "title": "海珠区泰沙路金紫里电梯房超靓电梯单间，家电齐全拎包入住",
                "price": "1700",
                "img": "https://pic1.ajkimg.com/display/anjuke/6e72822e526bea1451337d9cc54cd7ec/240x180c.jpg?t=1&srotate=1",
                "unit": "元/月",
                "coll": "0",
                "subwayDistance": "距8号线-中大站步行1.7km",
                "fangwuLiangDian": [
                    {
                        "text": "整租",
                        "textColor": "#7397C1",
                        "bgColor": "#7397C1"
                    },
                    {
                        "text": "东南",
                        "textColor": "#7397C1",
                        "bgColor": "#7397C1"
                    },
                    {
                        "text": "近地铁",
                        "textColor": "#7397C1",
                        "bgColor": "#7397C1"
                    },
                    {
                        "text": "有电梯",
                        "textColor": "#7397C1",
                        "bgColor": "#7397C1"
                    },
                    {
                        "text": "精装修",
                        "textColor": "#7397C1",
                        "bgColor": "#7397C1"
                    }
                ],
                "area": "28㎡",
                "toward": "东南",
                "shangquan": "江燕路",
                "isAnxuan": "0",
                "location": false,
                "source_type": "1",
                "isauction": "1",
                "auctionShowText": "",
                "legoAuction": "%7B%22isauction%22%3A%221%22%2C%22lego%22%3A%7B%22lego_tid%22%3A%22%22%2C%22lego_ad_click_url%22%3A%22%22%2C%22lego_sid%22%3A%22110967499212892323512397686%22%2C%22platformCompany%22%3A%22ajk%22%2C%22lego_show_track_url_base%22%3A%22%22%2C%22lego_version%22%3A%221.0.1%22%2C%22legoHuiDu%22%3Atrue%7D%7D",
                "tid": "",
                "isLiving": false,
                "isVR": false,
                "distanceDesc": "",
                "isGoldOwner": false
            },
            {
                "infoid": "1939537161462798",
                "cateName": "整租",
                "cateID": "11",
                "title": "全新豪华公寓特价单间580元，拎包入住，交通便利",
                "price": "580",
                "img": "https://pic1.ajkimg.com/display/anjuke/516b9eba2f7e6cb92744a9a3540706b1/240x180c.jpg?t=1&srotate=1",
                "unit": "元/月",
                "coll": "0",
                "subwayDistance": "距8号线-中大站步行1.8km",
                "fangwuLiangDian": [
                    {
                        "text": "整租",
                        "textColor": "#7397C1",
                        "bgColor": "#7397C1"
                    },
                    {
                        "text": "南北",
                        "textColor": "#7397C1",
                        "bgColor": "#7397C1"
                    },
                    {
                        "text": "近地铁",
                        "textColor": "#7397C1",
                        "bgColor": "#7397C1"
                    },
                    {
                        "text": "有电梯",
                        "textColor": "#7397C1",
                        "bgColor": "#7397C1"
                    },
                    {
                        "text": "精装修",
                        "textColor": "#7397C1",
                        "bgColor": "#7397C1"
                    }
                ],
                "area": "18㎡",
                "toward": "南北",
                "shangquan": "晓港",
                "isAnxuan": "0",
                "location": false,
                "source_type": "1",
                "isauction": "1",
                "auctionShowText": "",
                "legoAuction": "%7B%22isauction%22%3A%221%22%2C%22lego%22%3A%7B%22lego_tid%22%3A%22%22%2C%22lego_ad_click_url%22%3A%22%22%2C%22lego_sid%22%3A%22110967499212892323512397686%22%2C%22platformCompany%22%3A%22ajk%22%2C%22lego_show_track_url_base%22%3A%22%22%2C%22lego_version%22%3A%221.0.1%22%2C%22legoHuiDu%22%3Atrue%7D%7D",
                "tid": "",
                "isLiving": false,
                "isVR": false,
                "distanceDesc": "",
                "isGoldOwner": false
            },
            {
                "infoid": "1961176851095567",
                "cateName": "整租",
                "cateID": "11",
                "title": "昌岗 晓园北中层一房 家电齐 拎包可入住",
                "price": "2000",
                "img": "https://pic1.ajkimg.com/display/anjuke/7b09412533b54640d5b7c07ca9f25a6b/240x180c.jpg?t=1&srotate=1",
                "unit": "元/月",
                "coll": "0",
                "subwayDistance": "距8号线-中大站步行1.7km",
                "fangwuLiangDian": [
                    {
                        "text": "整租",
                        "textColor": "#7397C1",
                        "bgColor": "#7397C1"
                    },
                    {
                        "text": "朝北",
                        "textColor": "#7397C1",
                        "bgColor": "#7397C1"
                    },
                    {
                        "text": "近地铁",
                        "textColor": "#7397C1",
                        "bgColor": "#7397C1"
                    }
                ],
                "area": "30㎡",
                "toward": "朝北",
                "shangquan": "昌岗",
                "isAnxuan": "0",
                "location": false,
                "source_type": "1",
                "isauction": "1",
                "auctionShowText": "",
                "legoAuction": "%7B%22isauction%22%3A%221%22%2C%22lego%22%3A%7B%22lego_tid%22%3A%22%22%2C%22lego_ad_click_url%22%3A%22%22%2C%22lego_sid%22%3A%22110967499212892323512397686%22%2C%22platformCompany%22%3A%22ajk%22%2C%22lego_show_track_url_base%22%3A%22%22%2C%22lego_version%22%3A%221.0.1%22%2C%22legoHuiDu%22%3Atrue%7D%7D",
                "tid": "",
                "isLiving": false,
                "isVR": false,
                "distanceDesc": "",
                "isGoldOwner": false
            },
            {
                "infoid": "1999640464169986",
                "cateName": "整租",
                "cateID": "11",
                "title": "桥东街小区 2室1厅1卫 2000元月 南北通透 精装修",
                "price": "2000",
                "img": "https://pic1.ajkimg.com/display/anjuke/4b6bfe1470b7df801c7012202bf34a17/240x180c.jpg?t=1&srotate=1",
                "unit": "元/月",
                "coll": "0",
                "subwayDistance": "距8号线-中大站步行2.0km",
                "fangwuLiangDian": [
                    {
                        "text": "整租",
                        "textColor": "#7397C1",
                        "bgColor": "#7397C1"
                    },
                    {
                        "text": "朝南",
                        "textColor": "#7397C1",
                        "bgColor": "#7397C1"
                    },
                    {
                        "text": "近地铁",
                        "textColor": "#7397C1",
                        "bgColor": "#7397C1"
                    },
                    {
                        "text": "精装修",
                        "textColor": "#7397C1",
                        "bgColor": "#7397C1"
                    }
                ],
                "area": "60㎡",
                "toward": "朝南",
                "shangquan": "官洲",
                "isAnxuan": "0",
                "location": false,
                "source_type": "1",
                "isauction": "1",
                "auctionShowText": "",
                "legoAuction": "%7B%22isauction%22%3A%221%22%2C%22lego%22%3A%7B%22lego_tid%22%3A%22%22%2C%22lego_ad_click_url%22%3A%22%22%2C%22lego_sid%22%3A%22110967499212892323512397686%22%2C%22platformCompany%22%3A%22ajk%22%2C%22lego_show_track_url_base%22%3A%22%22%2C%22lego_version%22%3A%221.0.1%22%2C%22legoHuiDu%22%3Atrue%7D%7D",
                "tid": "",
                "isLiving": false,
                "isVR": false,
                "distanceDesc": "",
                "isGoldOwner": false
            },
            {
                "infoid": "2014633629442057",
                "cateName": "整租",
                "cateID": "11",
                "title": "康泰花园 江泰地铁口 楼梯8楼 只租2200 只租2200",
                "price": "2000",
                "img": "https://pic1.ajkimg.com/display/anjuke/321038b73747ed2c9be2218c4e3d4c45/240x180c.jpg?t=1&srotate=1",
                "unit": "元/月",
                "coll": "0",
                "subwayDistance": "距8号线-中大站步行2.0km",
                "fangwuLiangDian": [
                    {
                        "text": "整租",
                        "textColor": "#7397C1",
                        "bgColor": "#7397C1"
                    },
                    {
                        "text": "朝南",
                        "textColor": "#7397C1",
                        "bgColor": "#7397C1"
                    },
                    {
                        "text": "近地铁",
                        "textColor": "#7397C1",
                        "bgColor": "#7397C1"
                    },
                    {
                        "text": "有电梯",
                        "textColor": "#7397C1",
                        "bgColor": "#7397C1"
                    }
                ],
                "area": "55㎡",
                "toward": "朝南",
                "shangquan": "江南西",
                "isAnxuan": "0",
                "location": false,
                "source_type": "1",
                "isauction": "1",
                "auctionShowText": "",
                "legoAuction": "%7B%22isauction%22%3A%221%22%2C%22lego%22%3A%7B%22lego_tid%22%3A%22%22%2C%22lego_ad_click_url%22%3A%22%22%2C%22lego_sid%22%3A%22110967499212892323512397686%22%2C%22platformCompany%22%3A%22ajk%22%2C%22lego_show_track_url_base%22%3A%22%22%2C%22lego_version%22%3A%221.0.1%22%2C%22legoHuiDu%22%3Atrue%7D%7D",
                "tid": "",
                "isLiving": false,
                "isVR": false,
                "distanceDesc": "",
                "isGoldOwner": false
            },
            {
                "infoid": "1961030394454023",
                "cateName": "整租",
                "cateID": "11",
                "title": "真实盘源 江南大道南生活超便利低层",
                "price": "1900",
                "img": "https://pic1.ajkimg.com/display/anjuke/3c48291b4072bac0bc75974331555e00/240x180c.jpg?t=1&srotate=1",
                "unit": "元/月",
                "coll": "0",
                "subwayDistance": "距8号线-中大站步行1.7km",
                "fangwuLiangDian": [
                    {
                        "text": "整租",
                        "textColor": "#7397C1",
                        "bgColor": "#7397C1"
                    },
                    {
                        "text": "朝南",
                        "textColor": "#7397C1",
                        "bgColor": "#7397C1"
                    },
                    {
                        "text": "近地铁",
                        "textColor": "#7397C1",
                        "bgColor": "#7397C1"
                    }
                ],
                "area": "48㎡",
                "toward": "朝南",
                "shangquan": "江南大道中",
                "isAnxuan": "0",
                "location": false,
                "source_type": "1",
                "isauction": "1",
                "auctionShowText": "",
                "legoAuction": "%7B%22isauction%22%3A%221%22%2C%22lego%22%3A%7B%22lego_tid%22%3A%22%22%2C%22lego_ad_click_url%22%3A%22%22%2C%22lego_sid%22%3A%22110967499212892323512397686%22%2C%22platformCompany%22%3A%22ajk%22%2C%22lego_show_track_url_base%22%3A%22%22%2C%22lego_version%22%3A%221.0.1%22%2C%22legoHuiDu%22%3Atrue%7D%7D",
                "tid": "",
                "isLiving": false,
                "isVR": false,
                "distanceDesc": "",
                "isGoldOwner": false
            },
            {
                "infoid": "2040823684604942",
                "cateName": "整租",
                "cateID": "11",
                "title": "达镖国际中心 1室1厅1卫 700元月 24平 南北通透",
                "price": "700",
                "img": "https://pic1.ajkimg.com/display/anjuke/9c6c2b80d33f358666280dae6ba0a255/240x180c.jpg?t=1&srotate=1",
                "unit": "元/月",
                "coll": "0",
                "subwayDistance": "距8号线-中大站步行1.9km",
                "fangwuLiangDian": [
                    {
                        "text": "整租",
                        "textColor": "#7397C1",
                        "bgColor": "#7397C1"
                    },
                    {
                        "text": "朝东",
                        "textColor": "#7397C1",
                        "bgColor": "#7397C1"
                    },
                    {
                        "text": "近地铁",
                        "textColor": "#7397C1",
                        "bgColor": "#7397C1"
                    },
                    {
                        "text": "有电梯",
                        "textColor": "#7397C1",
                        "bgColor": "#7397C1"
                    }
                ],
                "area": "14㎡",
                "toward": "朝东",
                "shangquan": "昌岗",
                "isAnxuan": "0",
                "location": false,
                "source_type": "1",
                "isauction": "1",
                "auctionShowText": "",
                "legoAuction": "%7B%22isauction%22%3A%221%22%2C%22lego%22%3A%7B%22lego_tid%22%3A%22%22%2C%22lego_ad_click_url%22%3A%22%22%2C%22lego_sid%22%3A%22110967499212892323512397686%22%2C%22platformCompany%22%3A%22ajk%22%2C%22lego_show_track_url_base%22%3A%22%22%2C%22lego_version%22%3A%221.0.1%22%2C%22legoHuiDu%22%3Atrue%7D%7D",
                "tid": "",
                "isLiving": false,
                "isVR": false,
                "distanceDesc": "",
                "isGoldOwner": false
            },
            {
                "infoid": "1965251226870785",
                "cateName": "整租",
                "cateID": "11",
                "title": "晓港地铁口（月付可短租）无中介（家电全）",
                "price": "500",
                "img": "https://pic1.ajkimg.com/display/anjuke/6a02e791fc4d5d5128301c5e3e1e8eb0/240x180c.jpg?t=1&srotate=1",
                "unit": "元/月",
                "coll": "0",
                "subwayDistance": "距8号线-中大站步行1.2km",
                "fangwuLiangDian": [
                    {
                        "text": "整租",
                        "textColor": "#7397C1",
                        "bgColor": "#7397C1"
                    },
                    {
                        "text": "朝南",
                        "textColor": "#7397C1",
                        "bgColor": "#7397C1"
                    },
                    {
                        "text": "近地铁",
                        "textColor": "#7397C1",
                        "bgColor": "#7397C1"
                    },
                    {
                        "text": "有电梯",
                        "textColor": "#7397C1",
                        "bgColor": "#7397C1"
                    }
                ],
                "area": "35㎡",
                "toward": "朝南",
                "shangquan": "晓港",
                "isAnxuan": "0",
                "location": false,
                "source_type": "1",
                "isauction": "1",
                "auctionShowText": "",
                "legoAuction": "%7B%22isauction%22%3A%221%22%2C%22lego%22%3A%7B%22lego_tid%22%3A%22%22%2C%22lego_ad_click_url%22%3A%22%22%2C%22lego_sid%22%3A%22110967499212892323512397686%22%2C%22platformCompany%22%3A%22ajk%22%2C%22lego_show_track_url_base%22%3A%22%22%2C%22lego_version%22%3A%221.0.1%22%2C%22legoHuiDu%22%3Atrue%7D%7D",
                "tid": "",
                "isLiving": false,
                "isVR": false,
                "distanceDesc": "",
                "isGoldOwner": false
            },
            {
                "infoid": "2034448012813327",
                "cateName": "整租",
                "cateID": "11",
                "title": "单身不是狗，一房一厅，邻近地铁，环境舒适，独立厨卫！",
                "price": "800",
                "img": "https://pic1.ajkimg.com/display/anjuke/ddddad8bcbb58128f7b0f6ce4d4f96da/240x180c.jpg?t=1&srotate=1",
                "unit": "元/月",
                "coll": "0",
                "subwayDistance": "距8号线-中大站步行2.0km",
                "fangwuLiangDian": [
                    {
                        "text": "整租",
                        "textColor": "#7397C1",
                        "bgColor": "#7397C1"
                    },
                    {
                        "text": "朝西",
                        "textColor": "#7397C1",
                        "bgColor": "#7397C1"
                    },
                    {
                        "text": "近地铁",
                        "textColor": "#7397C1",
                        "bgColor": "#7397C1"
                    },
                    {
                        "text": "精装修",
                        "textColor": "#7397C1",
                        "bgColor": "#7397C1"
                    }
                ],
                "area": "35㎡",
                "toward": "朝西",
                "shangquan": "东晓南",
                "isAnxuan": "0",
                "location": false,
                "source_type": "1",
                "isauction": "1",
                "auctionShowText": "",
                "legoAuction": "%7B%22isauction%22%3A%221%22%2C%22lego%22%3A%7B%22lego_tid%22%3A%22%22%2C%22lego_ad_click_url%22%3A%22%22%2C%22lego_sid%22%3A%22110967499212892323512397686%22%2C%22platformCompany%22%3A%22ajk%22%2C%22lego_show_track_url_base%22%3A%22%22%2C%22lego_version%22%3A%221.0.1%22%2C%22legoHuiDu%22%3Atrue%7D%7D",
                "tid": "",
                "isLiving": false,
                "isVR": false,
                "distanceDesc": "",
                "isGoldOwner": false
            },
            {
                "infoid": "1968316116247552",
                "cateName": "整租",
                "cateID": "11",
                "title": "洋弯岛创投小镇唯品同创汇附近 loft单间 家具齐 品牌公寓",
                "price": "1500",
                "img": "https://pic1.ajkimg.com/display/anjuke/46e6a7914bba28ac44357c47d94f54d1/240x180c.jpg?t=1&srotate=1",
                "unit": "元/月",
                "coll": "0",
                "subwayDistance": "距8号线-中大站步行2.0km",
                "fangwuLiangDian": [
                    {
                        "text": "整租",
                        "textColor": "#7397C1",
                        "bgColor": "#7397C1"
                    },
                    {
                        "text": "朝南",
                        "textColor": "#7397C1",
                        "bgColor": "#7397C1"
                    },
                    {
                        "text": "近地铁",
                        "textColor": "#7397C1",
                        "bgColor": "#7397C1"
                    },
                    {
                        "text": "有电梯",
                        "textColor": "#7397C1",
                        "bgColor": "#7397C1"
                    },
                    {
                        "text": "精装修",
                        "textColor": "#7397C1",
                        "bgColor": "#7397C1"
                    }
                ],
                "area": "32㎡",
                "toward": "朝南",
                "shangquan": "广州大道南",
                "isAnxuan": "0",
                "location": false,
                "source_type": "1",
                "isauction": "1",
                "auctionShowText": "",
                "legoAuction": "%7B%22isauction%22%3A%221%22%2C%22lego%22%3A%7B%22lego_tid%22%3A%22%22%2C%22lego_ad_click_url%22%3A%22%22%2C%22lego_sid%22%3A%22110967499212892323512397686%22%2C%22platformCompany%22%3A%22ajk%22%2C%22lego_show_track_url_base%22%3A%22%22%2C%22lego_version%22%3A%221.0.1%22%2C%22legoHuiDu%22%3Atrue%7D%7D",
                "tid": "",
                "isLiving": false,
                "isVR": false,
                "distanceDesc": "",
                "isGoldOwner": false
            },
            {
                "infoid": "1938432783309828",
                "cateName": "整租",
                "cateID": "11",
                "title": "鹭江 近地铁 可短租 精装修公寓 采光好 直租",
                "price": "800",
                "img": "https://pic1.ajkimg.com/display/anjuke/18045e30ecd0c1354a796048b3ca0944/240x180c.jpg?t=1&srotate=1",
                "unit": "元/月",
                "coll": "0",
                "subwayDistance": "距8号线-中大站步行1.8km",
                "fangwuLiangDian": [
                    {
                        "text": "整租",
                        "textColor": "#7397C1",
                        "bgColor": "#7397C1"
                    },
                    {
                        "text": "南北",
                        "textColor": "#7397C1",
                        "bgColor": "#7397C1"
                    },
                    {
                        "text": "近地铁",
                        "textColor": "#7397C1",
                        "bgColor": "#7397C1"
                    },
                    {
                        "text": "有电梯",
                        "textColor": "#7397C1",
                        "bgColor": "#7397C1"
                    },
                    {
                        "text": "精装修",
                        "textColor": "#7397C1",
                        "bgColor": "#7397C1"
                    }
                ],
                "area": "28㎡",
                "toward": "南北",
                "shangquan": "中大",
                "isAnxuan": "0",
                "location": false,
                "source_type": "1",
                "isauction": "1",
                "auctionShowText": "",
                "legoAuction": "%7B%22isauction%22%3A%221%22%2C%22lego%22%3A%7B%22lego_tid%22%3A%22%22%2C%22lego_ad_click_url%22%3A%22%22%2C%22lego_sid%22%3A%22110967499212892323512397686%22%2C%22platformCompany%22%3A%22ajk%22%2C%22lego_show_track_url_base%22%3A%22%22%2C%22lego_version%22%3A%221.0.1%22%2C%22legoHuiDu%22%3Atrue%7D%7D",
                "tid": "",
                "isLiving": false,
                "isVR": false,
                "distanceDesc": "",
                "isGoldOwner": false
            },
            {
                "infoid": "2038756538358789",
                "cateName": "整租",
                "cateID": "11",
                "title": "手慢无出租 价格优惠 无中介 出行方便 安心无忧 拎包入住",
                "price": "399",
                "img": "https://pic1.ajkimg.com/display/anjuke/ea0ff61c940c0ef9e248ad965151c7c0/240x180c.jpg?t=1&srotate=1",
                "unit": "元/月",
                "coll": "0",
                "subwayDistance": "距8号线-中大站步行1.8km",
                "fangwuLiangDian": [
                    {
                        "text": "整租",
                        "textColor": "#7397C1",
                        "bgColor": "#7397C1"
                    },
                    {
                        "text": "朝南",
                        "textColor": "#7397C1",
                        "bgColor": "#7397C1"
                    },
                    {
                        "text": "近地铁",
                        "textColor": "#7397C1",
                        "bgColor": "#7397C1"
                    },
                    {
                        "text": "精装修",
                        "textColor": "#7397C1",
                        "bgColor": "#7397C1"
                    }
                ],
                "area": "31㎡",
                "toward": "朝南",
                "shangquan": "中大",
                "isAnxuan": "0",
                "location": false,
                "source_type": "1",
                "isauction": "1",
                "auctionShowText": "",
                "legoAuction": "%7B%22isauction%22%3A%221%22%2C%22lego%22%3A%7B%22lego_tid%22%3A%22%22%2C%22lego_ad_click_url%22%3A%22%22%2C%22lego_sid%22%3A%22110967499212892323512397686%22%2C%22platformCompany%22%3A%22ajk%22%2C%22lego_show_track_url_base%22%3A%22%22%2C%22lego_version%22%3A%221.0.1%22%2C%22legoHuiDu%22%3Atrue%7D%7D",
                "tid": "",
                "isLiving": false,
                "isVR": false,
                "distanceDesc": "",
                "isGoldOwner": false
            },
            {
                "infoid": "1997730598036495",
                "cateName": "整租",
                "cateID": "11",
                "title": "与之 中大 1室0厅1卫 1600元月 南北通透 精装修",
                "price": "1500",
                "img": "https://pic1.ajkimg.com/display/anjuke/8c0a1e552ba854ce802dbe44137b99d6/240x180c.jpg?t=1&srotate=1",
                "unit": "元/月",
                "coll": "0",
                "subwayDistance": "距8号线-中大站步行1.3km",
                "fangwuLiangDian": [
                    {
                        "text": "整租",
                        "textColor": "#7397C1",
                        "bgColor": "#7397C1"
                    },
                    {
                        "text": "朝南",
                        "textColor": "#7397C1",
                        "bgColor": "#7397C1"
                    },
                    {
                        "text": "近地铁",
                        "textColor": "#7397C1",
                        "bgColor": "#7397C1"
                    },
                    {
                        "text": "精装修",
                        "textColor": "#7397C1",
                        "bgColor": "#7397C1"
                    }
                ],
                "area": "30㎡",
                "toward": "朝南",
                "shangquan": "江南西",
                "isAnxuan": "0",
                "location": false,
                "source_type": "1",
                "isauction": "1",
                "auctionShowText": "",
                "legoAuction": "%7B%22isauction%22%3A%221%22%2C%22lego%22%3A%7B%22lego_tid%22%3A%22%22%2C%22lego_ad_click_url%22%3A%22%22%2C%22lego_sid%22%3A%22110967499212892323512397686%22%2C%22platformCompany%22%3A%22ajk%22%2C%22lego_show_track_url_base%22%3A%22%22%2C%22lego_version%22%3A%221.0.1%22%2C%22legoHuiDu%22%3Atrue%7D%7D",
                "tid": "",
                "isLiving": false,
                "isVR": false,
                "distanceDesc": "",
                "isGoldOwner": false
            },
            {
                "infoid": "2044344995700745",
                "cateName": "整租",
                "cateID": "11",
                "title": "昌岗东一巷小区 2室1厅1卫 1200元月 配套齐全",
                "price": "1200",
                "img": "https://pic1.ajkimg.com/display/anjuke/d1342319d9162b505585208c41980e83/240x180c.jpg?t=1&srotate=1",
                "unit": "元/月",
                "coll": "0",
                "subwayDistance": "距8号线-中大站步行1.3km",
                "fangwuLiangDian": [
                    {
                        "text": "整租",
                        "textColor": "#7397C1",
                        "bgColor": "#7397C1"
                    },
                    {
                        "text": "朝南",
                        "textColor": "#7397C1",
                        "bgColor": "#7397C1"
                    },
                    {
                        "text": "近地铁",
                        "textColor": "#7397C1",
                        "bgColor": "#7397C1"
                    },
                    {
                        "text": "新上",
                        "textColor": "#7397C1",
                        "bgColor": "#7397C1"
                    }
                ],
                "area": "41㎡",
                "toward": "朝南",
                "shangquan": "江南西",
                "isAnxuan": "0",
                "location": false,
                "source_type": "1",
                "isauction": "1",
                "auctionShowText": "",
                "legoAuction": "%7B%22isauction%22%3A%221%22%2C%22lego%22%3A%7B%22lego_tid%22%3A%22%22%2C%22lego_ad_click_url%22%3A%22%22%2C%22lego_sid%22%3A%22110967499212892323512397686%22%2C%22platformCompany%22%3A%22ajk%22%2C%22lego_show_track_url_base%22%3A%22%22%2C%22lego_version%22%3A%221.0.1%22%2C%22legoHuiDu%22%3Atrue%7D%7D",
                "tid": "",
                "isLiving": false,
                "isVR": false,
                "distanceDesc": "",
                "isGoldOwner": false
            },
            {
                "infoid": "2037367815036940",
                "cateName": "整租",
                "cateID": "11",
                "title": "整租阳光一房一厅，小区环境",
                "price": "1950",
                "img": "https://pic1.ajkimg.com/display/anjuke/359123067535927902c6935f0c570b15/240x180c.jpg?t=1&srotate=1",
                "unit": "元/月",
                "coll": "0",
                "subwayDistance": "距8号线-中大站步行1.8km",
                "fangwuLiangDian": [
                    {
                        "text": "整租",
                        "textColor": "#7397C1",
                        "bgColor": "#7397C1"
                    },
                    {
                        "text": "东南",
                        "textColor": "#7397C1",
                        "bgColor": "#7397C1"
                    },
                    {
                        "text": "近地铁",
                        "textColor": "#7397C1",
                        "bgColor": "#7397C1"
                    },
                    {
                        "text": "精装修",
                        "textColor": "#7397C1",
                        "bgColor": "#7397C1"
                    }
                ],
                "area": "35㎡",
                "toward": "东南",
                "shangquan": "前进路",
                "isAnxuan": "0",
                "location": false,
                "source_type": "1",
                "isauction": "1",
                "auctionShowText": "",
                "legoAuction": "%7B%22isauction%22%3A%221%22%2C%22lego%22%3A%7B%22lego_tid%22%3A%22%22%2C%22lego_ad_click_url%22%3A%22%22%2C%22lego_sid%22%3A%22110967499212892323512397686%22%2C%22platformCompany%22%3A%22ajk%22%2C%22lego_show_track_url_base%22%3A%22%22%2C%22lego_version%22%3A%221.0.1%22%2C%22legoHuiDu%22%3Atrue%7D%7D",
                "tid": "",
                "isLiving": false,
                "isVR": false,
                "distanceDesc": "",
                "isGoldOwner": false
            },
            {
                "infoid": "1578803529063438",
                "cateName": "整租",
                "cateID": "11",
                "title": "晓园新村    精装一房    家电齐全      治安好",
                "price": "2000",
                "img": "https://pic1.ajkimg.com/display/anjuke/ab512846787c0aa4ea42477a9450956d/240x180c.jpg?t=1&srotate=1",
                "unit": "元/月",
                "coll": "0",
                "subwayDistance": "距8号线-中大站步行2.0km",
                "fangwuLiangDian": [
                    {
                        "text": "整租",
                        "textColor": "#7397C1",
                        "bgColor": "#7397C1"
                    },
                    {
                        "text": "朝南",
                        "textColor": "#7397C1",
                        "bgColor": "#7397C1"
                    },
                    {
                        "text": "近地铁",
                        "textColor": "#7397C1",
                        "bgColor": "#7397C1"
                    },
                    {
                        "text": "豪华装修",
                        "textColor": "#7397C1",
                        "bgColor": "#7397C1"
                    }
                ],
                "area": "45㎡",
                "toward": "朝南",
                "shangquan": "昌岗",
                "isAnxuan": "0",
                "location": false,
                "source_type": "1",
                "isauction": "1",
                "auctionShowText": "",
                "legoAuction": "%7B%22isauction%22%3A%221%22%2C%22lego%22%3A%7B%22lego_tid%22%3A%22%22%2C%22lego_ad_click_url%22%3A%22%22%2C%22lego_sid%22%3A%22110967499212892323512397686%22%2C%22platformCompany%22%3A%22ajk%22%2C%22lego_show_track_url_base%22%3A%22%22%2C%22lego_version%22%3A%221.0.1%22%2C%22legoHuiDu%22%3Atrue%7D%7D",
                "tid": "",
                "isLiving": false,
                "isVR": false,
                "distanceDesc": "",
                "isGoldOwner": false
            },
            {
                "infoid": "2007550430585866",
                "cateName": "整租",
                "cateID": "11",
                "title": "鹭江地铁口（月付）可短租 无中介 家电 拎包入住 先到先得",
                "price": "500",
                "img": "https://pic1.ajkimg.com/display/anjuke/02799c474baabd44683c95f7ca30caaf/240x180c.jpg?t=1&srotate=1",
                "unit": "元/月",
                "coll": "0",
                "subwayDistance": "距8号线-中大站步行1.8km",
                "fangwuLiangDian": [
                    {
                        "text": "整租",
                        "textColor": "#7397C1",
                        "bgColor": "#7397C1"
                    },
                    {
                        "text": "朝南",
                        "textColor": "#7397C1",
                        "bgColor": "#7397C1"
                    },
                    {
                        "text": "近地铁",
                        "textColor": "#7397C1",
                        "bgColor": "#7397C1"
                    },
                    {
                        "text": "有电梯",
                        "textColor": "#7397C1",
                        "bgColor": "#7397C1"
                    },
                    {
                        "text": "精装修",
                        "textColor": "#7397C1",
                        "bgColor": "#7397C1"
                    }
                ],
                "area": "14㎡",
                "toward": "朝南",
                "shangquan": "中大",
                "isAnxuan": "0",
                "location": false,
                "source_type": "1",
                "isauction": "1",
                "auctionShowText": "",
                "legoAuction": "%7B%22isauction%22%3A%221%22%2C%22lego%22%3A%7B%22lego_tid%22%3A%22%22%2C%22lego_ad_click_url%22%3A%22%22%2C%22lego_sid%22%3A%22110967499212892323512397686%22%2C%22platformCompany%22%3A%22ajk%22%2C%22lego_show_track_url_base%22%3A%22%22%2C%22lego_version%22%3A%221.0.1%22%2C%22legoHuiDu%22%3Atrue%7D%7D",
                "tid": "",
                "isLiving": false,
                "isVR": false,
                "distanceDesc": "",
                "isGoldOwner": false
            },
            {
                "infoid": "2027681947672576",
                "cateName": "整租",
                "cateID": "11",
                "title": "精装 独厨卫清爽温馨宽敞明亮拎包入住",
                "price": "501",
                "img": "https://pic1.ajkimg.com/display/anjuke/f56094bcb602b75c1e16c2b3f8c87178/240x180c.jpg?t=1&srotate=1",
                "unit": "元/月",
                "coll": "0",
                "subwayDistance": "距8号线-中大站步行1.8km",
                "fangwuLiangDian": [
                    {
                        "text": "整租",
                        "textColor": "#7397C1",
                        "bgColor": "#7397C1"
                    },
                    {
                        "text": "朝南",
                        "textColor": "#7397C1",
                        "bgColor": "#7397C1"
                    },
                    {
                        "text": "近地铁",
                        "textColor": "#7397C1",
                        "bgColor": "#7397C1"
                    },
                    {
                        "text": "精装修",
                        "textColor": "#7397C1",
                        "bgColor": "#7397C1"
                    }
                ],
                "area": "45㎡",
                "toward": "朝南",
                "shangquan": "中大",
                "isAnxuan": "0",
                "location": false,
                "source_type": "1",
                "isauction": "1",
                "auctionShowText": "",
                "legoAuction": "%7B%22isauction%22%3A%221%22%2C%22lego%22%3A%7B%22lego_tid%22%3A%22%22%2C%22lego_ad_click_url%22%3A%22%22%2C%22lego_sid%22%3A%22110967499212892323512397686%22%2C%22platformCompany%22%3A%22ajk%22%2C%22lego_show_track_url_base%22%3A%22%22%2C%22lego_version%22%3A%221.0.1%22%2C%22legoHuiDu%22%3Atrue%7D%7D",
                "tid": "",
                "isLiving": false,
                "isVR": false,
                "distanceDesc": "",
                "isGoldOwner": false
            },
            {
                "infoid": "2021439652750336",
                "cateName": "整租",
                "cateID": "11",
                "title": "3号线 近地铁 大阳台 采光好 家电齐全 给你一个温馨的家",
                "price": "850",
                "img": "https://pic1.ajkimg.com/display/anjuke/f30316643edba97086ed68d4b8be24c6/240x180c.jpg?t=1&srotate=1",
                "unit": "元/月",
                "coll": "0",
                "subwayDistance": "距8号线-中大站步行1.8km",
                "fangwuLiangDian": [
                    {
                        "text": "整租",
                        "textColor": "#7397C1",
                        "bgColor": "#7397C1"
                    },
                    {
                        "text": "朝南",
                        "textColor": "#7397C1",
                        "bgColor": "#7397C1"
                    },
                    {
                        "text": "近地铁",
                        "textColor": "#7397C1",
                        "bgColor": "#7397C1"
                    },
                    {
                        "text": "有电梯",
                        "textColor": "#7397C1",
                        "bgColor": "#7397C1"
                    },
                    {
                        "text": "精装修",
                        "textColor": "#7397C1",
                        "bgColor": "#7397C1"
                    }
                ],
                "area": "28㎡",
                "toward": "朝南",
                "shangquan": "中大",
                "isAnxuan": "0",
                "location": false,
                "source_type": "1",
                "isauction": "1",
                "auctionShowText": "",
                "legoAuction": "%7B%22isauction%22%3A%221%22%2C%22lego%22%3A%7B%22lego_tid%22%3A%22%22%2C%22lego_ad_click_url%22%3A%22%22%2C%22lego_sid%22%3A%22110967499212892323512397686%22%2C%22platformCompany%22%3A%22ajk%22%2C%22lego_show_track_url_base%22%3A%22%22%2C%22lego_version%22%3A%221.0.1%22%2C%22legoHuiDu%22%3Atrue%7D%7D",
                "tid": "",
                "isLiving": false,
                "isVR": false,
                "distanceDesc": "",
                "isGoldOwner": false
            },
            {
                "infoid": "2014508195159041",
                "cateName": "整租",
                "cateID": "11",
                "title": "晓怡居 1室0厅1卫 321元月 电梯房 配套齐全",
                "price": "321",
                "img": "https://pic1.ajkimg.com/display/anjuke/50a2903ec777707f41eec865494d85ad/240x180c.jpg?t=1&srotate=1",
                "unit": "元/月",
                "coll": "0",
                "subwayDistance": "距8号线-中大站步行1.2km",
                "fangwuLiangDian": [
                    {
                        "text": "整租",
                        "textColor": "#7397C1",
                        "bgColor": "#7397C1"
                    },
                    {
                        "text": "朝南",
                        "textColor": "#7397C1",
                        "bgColor": "#7397C1"
                    },
                    {
                        "text": "近地铁",
                        "textColor": "#7397C1",
                        "bgColor": "#7397C1"
                    },
                    {
                        "text": "有电梯",
                        "textColor": "#7397C1",
                        "bgColor": "#7397C1"
                    },
                    {
                        "text": "精装修",
                        "textColor": "#7397C1",
                        "bgColor": "#7397C1"
                    }
                ],
                "area": "30㎡",
                "toward": "朝南",
                "shangquan": "晓港",
                "isAnxuan": "0",
                "location": false,
                "source_type": "1",
                "isauction": "1",
                "auctionShowText": "",
                "legoAuction": "%7B%22isauction%22%3A%221%22%2C%22lego%22%3A%7B%22lego_tid%22%3A%22%22%2C%22lego_ad_click_url%22%3A%22%22%2C%22lego_sid%22%3A%22110967499212892323512397686%22%2C%22platformCompany%22%3A%22ajk%22%2C%22lego_show_track_url_base%22%3A%22%22%2C%22lego_version%22%3A%221.0.1%22%2C%22legoHuiDu%22%3Atrue%7D%7D",
                "tid": "",
                "isLiving": false,
                "isVR": false,
                "distanceDesc": "",
                "isGoldOwner": false
            },
            {
                "infoid": "1990866062695436",
                "cateName": "整租",
                "cateID": "11",
                "title": "中大远洋宿舍，精装一房一厅，拎包入住，交通便利，价格便宜",
                "price": "2000",
                "img": "https://pic1.ajkimg.com/display/anjuke/f62ae49dd3c0742625c26b43176c1b9f/240x180c.jpg?t=1&srotate=1",
                "unit": "元/月",
                "coll": "0",
                "subwayDistance": "距8号线-中大站步行720m",
                "fangwuLiangDian": [
                    {
                        "text": "整租",
                        "textColor": "#7397C1",
                        "bgColor": "#7397C1"
                    },
                    {
                        "text": "朝南",
                        "textColor": "#7397C1",
                        "bgColor": "#7397C1"
                    },
                    {
                        "text": "近地铁",
                        "textColor": "#7397C1",
                        "bgColor": "#7397C1"
                    },
                    {
                        "text": "精装修",
                        "textColor": "#7397C1",
                        "bgColor": "#7397C1"
                    }
                ],
                "area": "30㎡",
                "toward": "朝南",
                "shangquan": "新港西",
                "isAnxuan": "0",
                "location": false,
                "source_type": "1",
                "isauction": "1",
                "auctionShowText": "",
                "legoAuction": "%7B%22isauction%22%3A%221%22%2C%22lego%22%3A%7B%22lego_tid%22%3A%22%22%2C%22lego_ad_click_url%22%3A%22%22%2C%22lego_sid%22%3A%22110967499212892323512397686%22%2C%22platformCompany%22%3A%22ajk%22%2C%22lego_show_track_url_base%22%3A%22%22%2C%22lego_version%22%3A%221.0.1%22%2C%22legoHuiDu%22%3Atrue%7D%7D",
                "tid": "",
                "isLiving": false,
                "isVR": false,
                "distanceDesc": "",
                "isGoldOwner": false
            },
            {
                "infoid": "1863885230285828",
                "cateName": "整租",
                "cateID": "11",
                "title": "不走巷子，大马路边电梯大单间，直租无中介",
                "price": "1500",
                "img": "https://pic1.ajkimg.com/display/anjuke/7147b6b714cc8b41726a251416f482e3/240x180c.jpg?t=1&srotate=1",
                "unit": "元/月",
                "coll": "0",
                "subwayDistance": "距8号线-中大站步行2.0km",
                "fangwuLiangDian": [
                    {
                        "text": "整租",
                        "textColor": "#7397C1",
                        "bgColor": "#7397C1"
                    },
                    {
                        "text": "东南",
                        "textColor": "#7397C1",
                        "bgColor": "#7397C1"
                    },
                    {
                        "text": "近地铁",
                        "textColor": "#7397C1",
                        "bgColor": "#7397C1"
                    },
                    {
                        "text": "有电梯",
                        "textColor": "#7397C1",
                        "bgColor": "#7397C1"
                    },
                    {
                        "text": "豪华装修",
                        "textColor": "#7397C1",
                        "bgColor": "#7397C1"
                    }
                ],
                "area": "32㎡",
                "toward": "东南",
                "shangquan": "江南西",
                "isAnxuan": "0",
                "location": false,
                "source_type": "1",
                "isauction": "1",
                "auctionShowText": "",
                "legoAuction": "%7B%22isauction%22%3A%221%22%2C%22lego%22%3A%7B%22lego_tid%22%3A%22%22%2C%22lego_ad_click_url%22%3A%22%22%2C%22lego_sid%22%3A%22110967499212892323512397686%22%2C%22platformCompany%22%3A%22ajk%22%2C%22lego_show_track_url_base%22%3A%22%22%2C%22lego_version%22%3A%221.0.1%22%2C%22legoHuiDu%22%3Atrue%7D%7D",
                "tid": "",
                "isLiving": false,
                "isVR": false,
                "distanceDesc": "",
                "isGoldOwner": false
            },
            {
                "infoid": "2026680059913219",
                "cateName": "整租",
                "cateID": "11",
                "title": "与之8号线中大地铁 大单间独卫单身公寓 拎包入住 可短租",
                "price": "1300",
                "img": "https://pic1.ajkimg.com/display/anjuke/5ff0e3337df6d8b5e1a6f47ef47be438/240x180c.jpg?t=1&srotate=1",
                "unit": "元/月",
                "coll": "0",
                "subwayDistance": "距8号线-中大站步行1.2km",
                "fangwuLiangDian": [
                    {
                        "text": "整租",
                        "textColor": "#7397C1",
                        "bgColor": "#7397C1"
                    },
                    {
                        "text": "朝南",
                        "textColor": "#7397C1",
                        "bgColor": "#7397C1"
                    },
                    {
                        "text": "近地铁",
                        "textColor": "#7397C1",
                        "bgColor": "#7397C1"
                    },
                    {
                        "text": "有电梯",
                        "textColor": "#7397C1",
                        "bgColor": "#7397C1"
                    },
                    {
                        "text": "精装修",
                        "textColor": "#7397C1",
                        "bgColor": "#7397C1"
                    }
                ],
                "area": "30㎡",
                "toward": "朝南",
                "shangquan": "滨江东",
                "isAnxuan": "0",
                "location": false,
                "source_type": "1",
                "isauction": "1",
                "auctionShowText": "",
                "legoAuction": "%7B%22isauction%22%3A%221%22%2C%22lego%22%3A%7B%22lego_tid%22%3A%22%22%2C%22lego_ad_click_url%22%3A%22%22%2C%22lego_sid%22%3A%22110967499212892323512397686%22%2C%22platformCompany%22%3A%22ajk%22%2C%22lego_show_track_url_base%22%3A%22%22%2C%22lego_version%22%3A%221.0.1%22%2C%22legoHuiDu%22%3Atrue%7D%7D",
                "tid": "",
                "isLiving": false,
                "isVR": false,
                "distanceDesc": "",
                "isGoldOwner": false
            },
            {
                "infoid": "1940993126326287",
                "cateName": "整租",
                "cateID": "11",
                "title": "海联路大院中等装修正规 2000元",
                "price": "2000",
                "img": "https://pic1.ajkimg.com/display/anjuke/e06381fa2d9d40d1684a92bf70ffdd1e/240x180c.jpg?t=1&srotate=1",
                "unit": "元/月",
                "coll": "0",
                "subwayDistance": "距8号线-中大站步行2.0km",
                "fangwuLiangDian": [
                    {
                        "text": "整租",
                        "textColor": "#7397C1",
                        "bgColor": "#7397C1"
                    },
                    {
                        "text": "朝北",
                        "textColor": "#7397C1",
                        "bgColor": "#7397C1"
                    },
                    {
                        "text": "近地铁",
                        "textColor": "#7397C1",
                        "bgColor": "#7397C1"
                    },
                    {
                        "text": "有电梯",
                        "textColor": "#7397C1",
                        "bgColor": "#7397C1"
                    }
                ],
                "area": "40㎡",
                "toward": "朝北",
                "shangquan": "滨江东",
                "isAnxuan": "0",
                "location": false,
                "source_type": "1",
                "isauction": "1",
                "auctionShowText": "",
                "legoAuction": "%7B%22isauction%22%3A%221%22%2C%22lego%22%3A%7B%22lego_tid%22%3A%22%22%2C%22lego_ad_click_url%22%3A%22%22%2C%22lego_sid%22%3A%22110967499212892323512397686%22%2C%22platformCompany%22%3A%22ajk%22%2C%22lego_show_track_url_base%22%3A%22%22%2C%22lego_version%22%3A%221.0.1%22%2C%22legoHuiDu%22%3Atrue%7D%7D",
                "tid": "",
                "isLiving": false,
                "isVR": false,
                "distanceDesc": "",
                "isGoldOwner": false
            },
            {
                "infoid": "2021657614361613",
                "cateName": "整租",
                "cateID": "11",
                "title": "近地铁公寓  采光通风好 独立阳台 独立厨卫 专人管理",
                "price": "1500",
                "img": "https://pic1.ajkimg.com/display/anjuke/fbb6a15b1dad02923176427c1f41c224/240x180c.jpg?t=1&srotate=1",
                "unit": "元/月",
                "coll": "0",
                "subwayDistance": "距8号线-中大站步行831m",
                "fangwuLiangDian": [
                    {
                        "text": "整租",
                        "textColor": "#7397C1",
                        "bgColor": "#7397C1"
                    },
                    {
                        "text": "朝南",
                        "textColor": "#7397C1",
                        "bgColor": "#7397C1"
                    },
                    {
                        "text": "近地铁",
                        "textColor": "#7397C1",
                        "bgColor": "#7397C1"
                    },
                    {
                        "text": "有电梯",
                        "textColor": "#7397C1",
                        "bgColor": "#7397C1"
                    },
                    {
                        "text": "精装修",
                        "textColor": "#7397C1",
                        "bgColor": "#7397C1"
                    }
                ],
                "area": "25㎡",
                "toward": "朝南",
                "shangquan": "中大",
                "isAnxuan": "0",
                "location": false,
                "source_type": "1",
                "isauction": "1",
                "auctionShowText": "",
                "legoAuction": "%7B%22isauction%22%3A%221%22%2C%22lego%22%3A%7B%22lego_tid%22%3A%22%22%2C%22lego_ad_click_url%22%3A%22%22%2C%22lego_sid%22%3A%22110967499212892323512397686%22%2C%22platformCompany%22%3A%22ajk%22%2C%22lego_show_track_url_base%22%3A%22%22%2C%22lego_version%22%3A%221.0.1%22%2C%22legoHuiDu%22%3Atrue%7D%7D",
                "tid": "",
                "isLiving": false,
                "isVR": false,
                "distanceDesc": "",
                "isGoldOwner": false
            }
        ]
    }
}