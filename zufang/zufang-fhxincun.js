var ZUFANG_3 = {
    "code": "0",
    "message": "OK",
    "data": {
      "isToast": false,
      "activity": {
        "title": ""
      },
      "configs": {
        "banner": [],
        "houseSubject": []
      },
      "tkd": {
        "title": "8号线凤凰新村整租租房网，广州8号线凤凰新村整租租房信息，8号线凤凰新村整租房屋出租价格-广州58安居客",
        "keywords": "8号线凤凰新村整租租房，8号线凤凰新村整租出租房，8号线凤凰新村整租租房子",
        "description": "安居客广州租房网为您提供8号线凤凰新村整租租房信息,包括广州二手房出租、新房出租，租房子,找广州出租房,就上58安居客广州租房网。"
      },
      "gongyu": {
        "isShow": true
      },
      "goldOwnerCity": false,
      "list": [
        {
          "infoid": "2036145743257603",
          "cateName": "整租",
          "cateID": "11",
          "title": "初次出租（钥匙房）电梯三房精装修（大阳台）宝岗怡雅苑",
          "price": "3800",
          "img": "https://pic1.ajkimg.com/display/anjuke/8e3bda05427df8b20e2ed99f2b9b014d/240x180c.jpg?t=1&srotate=1",
          "unit": "元/月",
          "coll": "0",
          "subwayDistance": "距8号线-凤凰新村站步行1.1km",
          "fangwuLiangDian": [
            {
              "text": "整租",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "朝北",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "近地铁",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "有电梯",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "精装修",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            }
          ],
          "area": "71.9㎡",
          "toward": "朝北",
          "shangquan": "宝岗",
          "isAnxuan": "0",
          "location": false,
          "source_type": "1",
          "isauction": "2",
          "auctionShowText": "广告",
          "legoAuction": "%7B%22isauction%22%3A%222%22%2C%22lego%22%3A%7B%22lego_tid%22%3A%22edae00fc-c044-464d-8060-cc8303c678c2%22%2C%22lego_ad_click_url%22%3A%22https%3A%5C%2F%5C%2Flegoclick.58.com%5C%2Fjump%3Ftarget%3DpZwY0ZnlsztdraOWUvYKuaY3mhFhrHnvmBY1rjK6sHEQnjTVmHb1PBYYrj03mh7bnvFBn19KnWNdPHmzPWNzrjN1rjcKTHckn1mQPjNLPjnzPH0vnjnKnHcYnjTKnHcYnjTKnikQnED1THDvnWn3PjTLnHTQPHDKnE7AEzdPsN72iSXMMSpcfSpEMrXYC8yc-SB6Jrh6VEDQTHDKTHnKUvPHPLDkHD-ogLIvibPOINIimYIaI1IgXNPdwEDQPjTYn1N1rH91PjEzPHbdrjEkTHmK0A7zmyd1n1NYrWcdP1n_0A7zmyd1n1N1rWnvTHDKmhcdPW9zmWnVuA7bmzYYPHDOsHbQuWcVujmLrjbkPhNduhu6THDYnjE1PHnOrjndrH0LrjNkrj9KnHEkPjndn1b3n1NknHnQPWTOP9DKnikQnEDKTEDVTEDKTEDdra3zPj98PWm8nHm1THTKnTDKnikQnE7exEDQnjT1P9DQnjTQPWbzTyRbmyNknAuWsynkPjEVPjmYuaY3njmksyPWrjnknvnvP16Wn9DKPT7LXjK-nHPBuHw6PHn1PW9YPWmKmyGoRvRWpA7YTHTKnzkQPWNLsjcLnj0KnE78IyQ_TH6-ujFWujK6uWubnANQnjn%22%2C%22lego_sid%22%3A%22168604811212840049557069323%22%2C%22platformCompany%22%3A%22ajk%22%2C%22lego_show_track_url_base%22%3A%22https%3A%5C%2F%5C%2Fadtrack.58.com%5C%2F1001692%5C%2F%22%2C%22lego_version%22%3A%221.0.1%22%2C%22legoHuiDu%22%3Atrue%7D%7D",
          "tid": "edae00fc-c044-464d-8060-cc8303c678c2",
          "isLiving": false,
          "isVR": false,
          "distanceDesc": "",
          "isGoldOwner": false
        },
        {
          "infoid": "2027726916345860",
          "cateName": "整租",
          "cateID": "11",
          "title": "宝岗大道 精装修三房 家电齐全 拎包入住",
          "price": "3999",
          "img": "https://pic1.ajkimg.com/display/anjuke/a1ef5c513b120c9a04145bba6aa9ab8b/240x180c.jpg?t=1&srotate=1",
          "unit": "元/月",
          "coll": "0",
          "subwayDistance": "距8号线-凤凰新村站步行1.4km",
          "fangwuLiangDian": [
            {
              "text": "整租",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "南北",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "近地铁",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            }
          ],
          "area": "71.2㎡",
          "toward": "南北",
          "shangquan": "宝岗",
          "isAnxuan": "0",
          "location": false,
          "source_type": "1",
          "isauction": "2",
          "auctionShowText": "",
          "legoAuction": "%7B%22isauction%22%3A%222%22%2C%22lego%22%3A%7B%22lego_tid%22%3A%227ff4cc89-466e-465b-9e5f-d62b526a5165%22%2C%22lego_ad_click_url%22%3A%22https%3A%5C%2F%5C%2Flegoclick.58.com%5C%2Fjump%3Ftarget%3DpZwY0ZnlsztdraOWUvYKuaY3mhFhrHnvmBY1rjK6sHEQnjTVmHb1PBYYrj03mh7bnvFBn19KPW01PjTOnHNYPj0dP1EKTHcknW0LnWmOnHm1PjN3PWTKnHcYnjTKnHcYnjTKnikQnED1THDvnWn3PjTLnHTQPHDKn97AEzdPsN72iSXMMSpcfSpEMrXYC8yc-SB6Jrh6VEDQTHDKTHnKUvPHPLDkHD-ogLIvibPOINIimYIaI1IgXNPdwEDQPjTYn1cLrH0zrj9OrHmYPHEYTHmK0A7zmyd1n1NYrWcdP1n_0A7zmyd1n1N1rWnvTHDKPW-hrHELrynVnj0dPBYYrAcvsH6-PHbVPWEQPjFBuH0zPW6bTHDYnjE1nW0OP1nkPHndn191njEKnHEkPjnzP1bLnWbvPHEdP1bznTDKnikQnEDKTEDVTEDKTEDdra3zPj98PWm8nHm1THTKnTDKnikQnE7exEDQnjT1P9DQnjTQPWbzTHIhuWwWm19OsHEvPhNVPjmdmBYOuHRhsyEvnhcdnWu6PHDvPEDKPT7LXjK-nHPBuHw6PHn1PW9YPWmKmyGoRvRWpA7YTHTKnzkQPWNLsjcLnj0KnE78IyQ_THcQPHbduHTduj66njK6Pyn%22%2C%22lego_sid%22%3A%22168604811212840049557069323%22%2C%22platformCompany%22%3A%22ajk%22%2C%22lego_show_track_url_base%22%3A%22https%3A%5C%2F%5C%2Fadtrack.58.com%5C%2F1001692%5C%2F%22%2C%22lego_version%22%3A%221.0.1%22%2C%22legoHuiDu%22%3Atrue%7D%7D",
          "tid": "7ff4cc89-466e-465b-9e5f-d62b526a5165",
          "isLiving": false,
          "isVR": false,
          "distanceDesc": "",
          "isGoldOwner": false
        },
        {
          "infoid": "2015965501612036",
          "cateName": "整租",
          "cateID": "11",
          "title": "怡龙苑 3室2厅1卫 3500元月 68平 豪华装修",
          "price": "3500",
          "img": "https://pic1.ajkimg.com/display/anjuke/4c98a386af7b2fbb2fc31f3b79681bb3/240x180c.jpg?t=1&srotate=1",
          "unit": "元/月",
          "coll": "0",
          "subwayDistance": "距8号线-凤凰新村站步行936m",
          "fangwuLiangDian": [
            {
              "text": "安选房源",
              "textColor": "#3EC77B",
              "bgColor": "#3EC77B"
            },
            {
              "text": "整租",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "东西",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "近地铁",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "有电梯",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "豪华装修",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            }
          ],
          "area": "68㎡",
          "toward": "东西",
          "shangquan": "宝岗",
          "isAnxuan": "1",
          "location": false,
          "source_type": "1",
          "isauction": "2",
          "auctionShowText": "",
          "legoAuction": "%7B%22isauction%22%3A%222%22%2C%22lego%22%3A%7B%22lego_tid%22%3A%22b164d24a-68d2-4804-8901-16c6986d31f5%22%2C%22lego_ad_click_url%22%3A%22https%3A%5C%2F%5C%2Flegoclick.58.com%5C%2Fjump%3Ftarget%3DpZwY0ZnlsztdraOWUvYKuaY3mhFhrHnvmBY1rjK6sHEQnjTVmHb1PBYYrj03mh7bnvFBn19KPjNOPjD1P1mOPWc3n1DKTHcknHNOPWNdnjDvnHckn1mKrj0knTD3P1TkTHD_nHDKnkDQPWc1rjEkP1DknHNQTHnKwbnVHidKibfMGO4hBs4hbF1MV2s-BFxCCpWGCUNKnEDQTED1TyqWN1IQnDQFpdqLIbGjXgRZNhPZEM0LRL-jINNKnHEkPHTLnHN1nj0dPWEQrHN3PEDvTgK60h7V01ndPjCzPH01sZK60h7V01ndn1C1P9DQTHK-nj9OrycdsHcOnhEVPjPBuid6Pvn1sHmdn1DdPAnQrjDQm9DQPjTdnj0QPHnQnj0On10vrjb3THDYnjNkP1Ddn1TOP1EdnWn1rHcKTHD_nHDKTEDKsEDKTEDKPH98nWE3sWmvsWDvnkDkTHTKTHD_nHDKXLYKnHTkn1mKnHTknHmOn97BnHmYujcYmiYvrAEzsHE3njEVrjbkniYQPhnvrH9vujnQuWNKTHEKIL9kuHD1mhNYmHN1n1m3PjmvTy7JpdI-mv66ITDkTHn_nHmdPzkzP1TLTHDKUMR_UT7-uHbYryw6rjnvuH03njFW%22%2C%22lego_sid%22%3A%22168604811212840049557069323%22%2C%22platformCompany%22%3A%22ajk%22%2C%22lego_show_track_url_base%22%3A%22https%3A%5C%2F%5C%2Fadtrack.58.com%5C%2F1001692%5C%2F%22%2C%22lego_version%22%3A%221.0.1%22%2C%22legoHuiDu%22%3Atrue%7D%7D",
          "tid": "b164d24a-68d2-4804-8901-16c6986d31f5",
          "isLiving": false,
          "isVR": false,
          "distanceDesc": "",
          "isGoldOwner": false
        },
        {
          "infoid": "2031962717316107",
          "cateName": "整租",
          "cateID": "11",
          "title": "8号线凤凰新村地铁 精装两房 家私齐全 大飘窗",
          "price": "2800",
          "img": "https://pic1.ajkimg.com/display/anjuke/e5b94fd39964d45c5948d9cbb299cb74/240x180c.jpg?t=1&srotate=1",
          "unit": "元/月",
          "coll": "0",
          "subwayDistance": "距8号线-凤凰新村站步行760m",
          "fangwuLiangDian": [
            {
              "text": "安选房源",
              "textColor": "#3EC77B",
              "bgColor": "#3EC77B"
            },
            {
              "text": "整租",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "朝南",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "近地铁",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "精装修",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            }
          ],
          "area": "55㎡",
          "toward": "朝南",
          "shangquan": "宝岗",
          "isAnxuan": "1",
          "location": false,
          "source_type": "1",
          "isauction": "2",
          "auctionShowText": "",
          "legoAuction": "%7B%22isauction%22%3A%222%22%2C%22lego%22%3A%7B%22lego_tid%22%3A%222194dee9-5be5-4165-8c43-908c1af20203%22%2C%22lego_ad_click_url%22%3A%22https%3A%5C%2F%5C%2Flegoclick.58.com%5C%2Fjump%3Ftarget%3DpZwY0ZnlsztdraOWUvYKuaY3mhFhrHnvmBY1rjK6sHEQnjTVmHb1PBYYrj03mh7bnvFBn19KPHEYrHT1rHmvrH9OnjDKTHckn1DOPWcLnH01nHmQnj0KnHcYnjTKnHcYnjTKnikQnED1THDvnWn3PjTLnHTQPHDKPT7AEzdPsN72iSXMMSpcfSpEMrXYC8yc-SB6Jrh6VEDQTHDKTHnKUvPHPLDkHD-ogLIvibPOINIimYIaI1IgXNPdwEDQPjTYP1Nkrjbvnjn1P1TdrH9YTHmK0A7zmyd1n1NYrWcdP1n_0A7zmyd1n1N1rWnvTHDKmHNzmHcQn1mVPjE3riYYnAPhsy7huHbVmHczPhu6mhmOP1u6THDYnjELPHT3rHmQPjmOPj9krHmKnHEkPj0dnj9OPWTdPjmLn1EkrTDKnikQnEDKTEDVTEDKTEDdra3zPj98PWm8nHm1THTKnTDKnikQnE7exEDQnjT1P9DQnjTQPWbzTHcQrHwbuyNOsHRBuHNVPjDvPiY3m1E1sHbkrAnQmymznjcknkDKPT7LXjK-nHPBuHw6PHn1PW9YPWmKmyGoRvRWpA7YTHTKnzkQPWNLsjcLnj0KnE78IyQ_THbYnHczuAFhnyEkuHu6uHb%22%2C%22lego_sid%22%3A%22168604811212840049557069323%22%2C%22platformCompany%22%3A%22ajk%22%2C%22lego_show_track_url_base%22%3A%22https%3A%5C%2F%5C%2Fadtrack.58.com%5C%2F1001692%5C%2F%22%2C%22lego_version%22%3A%221.0.1%22%2C%22legoHuiDu%22%3Atrue%7D%7D",
          "tid": "2194dee9-5be5-4165-8c43-908c1af20203",
          "isLiving": false,
          "isVR": false,
          "distanceDesc": "",
          "isGoldOwner": false
        },
        {
          "infoid": "2033214487157769",
          "cateName": "整租",
          "cateID": "11",
          "title": "南北广场实用两房 环境安静舒适 家私电器齐全 治安管理好",
          "price": "3500",
          "img": "https://pic1.ajkimg.com/display/anjuke/df720c82223ad2101a466a7bdfdcef4c/240x180c.jpg?t=1&srotate=1",
          "unit": "元/月",
          "coll": "0",
          "subwayDistance": "距8号线-凤凰新村站步行1.1km",
          "fangwuLiangDian": [
            {
              "text": "整租",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "朝东",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "近地铁",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "有电梯",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "精装修",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            }
          ],
          "area": "70.4㎡",
          "toward": "朝东",
          "shangquan": "宝岗",
          "isAnxuan": "0",
          "location": false,
          "source_type": "1",
          "isauction": "2",
          "auctionShowText": "",
          "legoAuction": "%7B%22isauction%22%3A%222%22%2C%22lego%22%3A%7B%22lego_tid%22%3A%22af95c539-df9d-4e96-9e93-c0fead9f8ee4%22%2C%22lego_ad_click_url%22%3A%22https%3A%5C%2F%5C%2Flegoclick.58.com%5C%2Fjump%3Ftarget%3DpZwY0ZnlsztdraOWUvYKuaY3mhFhrHnvmBY1rjK6sHEQnjTVmHb1PBYYrj03mh7bnvFBn19KPjD1rHD1n1ELnH0dnTDKnWT1n1cQPjE3P1DdP10vrEDQnWEknTDQnWEknTDQsjDQTHnKnHmzn19Ynj0QnjDdnEDdTNujsNYVENGsOlXxOCB4OGa0OeiBOmBgl2AClpAdTHDKnEDKnk7fmdnL0HKniyVxILu2EL-dwdFWwYFLPdIOELR7THDYnjnvrHD1njckPHEzP1nknWEKP97kmgF6Ugn1PHElnWNLnzQkmgF6Ugn1PHnln1mKnED1PA7-mWPBniY1rjcYsHEvrH0VrjP6PzdbPyE1uymzP1DQuyDKnHEkn1mOnHnknWcQn1mdPWNLP9DQPjT1PWbQn1TznHDzrHb1nW9QTEDQsjDQTEDKTiYKTEDKTHN3sWcYra3vPB3QPWnKnTDkTEDQsjDQTgVqTHDknjnvTHDknjDvrHcKmymOPyndn1bVuAmOuaYYuHbvsH--rHnVm1Khuy7brym3uyNYTEDYTgI3nANQnvF-PADdn1nvrjEvP976phVguyPCmgEKnTD1sjDvPH0_nW0kPkDQTyOdUAkKujEzmWT1n1wbPH6hPjnvnT%22%2C%22lego_sid%22%3A%22168604811212840049557069323%22%2C%22platformCompany%22%3A%22ajk%22%2C%22lego_show_track_url_base%22%3A%22https%3A%5C%2F%5C%2Fadtrack.58.com%5C%2F1001692%5C%2F%22%2C%22lego_version%22%3A%221.0.1%22%2C%22legoHuiDu%22%3Atrue%7D%7D",
          "tid": "af95c539-df9d-4e96-9e93-c0fead9f8ee4",
          "isLiving": false,
          "isVR": false,
          "distanceDesc": "",
          "isGoldOwner": false
        },
        {
          "infoid": "2032964947567631",
          "cateName": "整租",
          "cateID": "11",
          "title": "江南花园 2室2厅1卫 4500元月 电梯房 配套齐全",
          "price": "4500",
          "img": "https://pic1.ajkimg.com/display/anjuke/b71a2bb2ede2a4bdbb537ceacd72b3c5/240x180c.jpg?t=1&srotate=1",
          "unit": "元/月",
          "coll": "0",
          "subwayDistance": "距8号线-凤凰新村站步行1.6km",
          "fangwuLiangDian": [
            {
              "text": "整租",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "朝南",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "近地铁",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "有电梯",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            }
          ],
          "area": "58㎡",
          "toward": "朝南",
          "shangquan": "宝岗",
          "isAnxuan": "0",
          "location": false,
          "source_type": "1",
          "isauction": "2",
          "auctionShowText": "",
          "legoAuction": "%7B%22isauction%22%3A%222%22%2C%22lego%22%3A%7B%22lego_tid%22%3A%22c7a1b7bc-2324-4330-a602-9fcb2655e939%22%2C%22lego_ad_click_url%22%3A%22https%3A%5C%2F%5C%2Flegoclick.58.com%5C%2Fjump%3Ftarget%3DpZwY0ZnlsztdraOWUvYKuaY3mhFhrHnvmBY1rjK6sHEQnjTVmHb1PBYYrj03mh7bnvFBn19KP1D3PHDdrjb1P1bvnjDKTHckn1cOPWEOPj0dPW0vn1DKnHcYnjTKnHcYnjTKnikQnED1THDvnWn3PjTLnHTQPHDKP97AEzdPsN72iSXMMSpcfSpEMrXYC8yc-SB6Jrh6VEDQTHDKTHnKUvPHPLDkHD-ogLIvibPOINIimYIaI1IgXNPdwEDQPjTYrHmzrHN3PjEdrHDvnHmkTHmK0A7zmyd1n1NYrWcdP1n_0A7zmyd1n1N1rWnvTHDKn1-hujDknhcVmynzmiYYrjbvsH6hP19VnjEQuyw-n1wbnHTkTHDYnjEOPWcOPHbdnHDzPWNzrjTKnHEkPjbvnWbdrHEdPW0Yn1EzPTDKnikQnEDKTEDVTEDKTEDdra3zPj98PWm8nHm1THTKnTDKnikQnE7exEDQnjT1P9DQnjTQPWbzTynLmH7BPvFWsHc1nWEVPjn1nad6PWTzsH-hmvczPWNduHb1rEDKPT7LXjK-nHPBuHw6PHn1PW9YPWmKmyGoRvRWpA7YTHTKnzkQPWNLsjcLnj0KnE78IyQ_Tyu6rHD1uhPbPjwbuAPhn10%22%2C%22lego_sid%22%3A%22168604811212840049557069323%22%2C%22platformCompany%22%3A%22ajk%22%2C%22lego_show_track_url_base%22%3A%22https%3A%5C%2F%5C%2Fadtrack.58.com%5C%2F1001692%5C%2F%22%2C%22lego_version%22%3A%221.0.1%22%2C%22legoHuiDu%22%3Atrue%7D%7D",
          "tid": "c7a1b7bc-2324-4330-a602-9fcb2655e939",
          "isLiving": false,
          "isVR": false,
          "distanceDesc": "",
          "isGoldOwner": false
        },
        {
          "infoid": "1993798360065024",
          "cateName": "整租",
          "cateID": "11",
          "title": "光大花园  业主原自住两房  家私配套齐全  有锁匙",
          "price": "4700",
          "img": "https://pic1.ajkimg.com/display/anjuke/4a839190b13e9f2f21461ea678741fee/240x180c.jpg?t=1&srotate=1",
          "unit": "元/月",
          "coll": "0",
          "subwayDistance": "距8号线-凤凰新村站步行1.2km",
          "fangwuLiangDian": [
            {
              "text": "安选房源",
              "textColor": "#3EC77B",
              "bgColor": "#3EC77B"
            },
            {
              "text": "整租",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "东北",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "近地铁",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "有电梯",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "精装修",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            }
          ],
          "area": "72㎡",
          "toward": "东北",
          "shangquan": "工业大道北",
          "isAnxuan": "1",
          "location": false,
          "source_type": "1",
          "isauction": "2",
          "auctionShowText": "",
          "legoAuction": "%7B%22isauction%22%3A%222%22%2C%22lego%22%3A%7B%22lego_tid%22%3A%2238648623-a13c-4bca-bd90-3c2efc5f06ac%22%2C%22lego_ad_click_url%22%3A%22https%3A%5C%2F%5C%2Flegoclick.58.com%5C%2Fjump%3Ftarget%3DpZwY0ZnlsztdraOWUvYKuaY3mhFhrHnvmBY1rjK6sHEQnjTVmHb1PBYYrj03mh7bnvFBn19KnWbLn19OnHTvnW9vnH0KTHDOrHnLrH91PWTkPWNknWEKnHD1rHbKnHD1rHbKnikQnED1THDvnWn3PjTLnHTQPHDKPk7AEzdPsN72iSXMMSpcfSpEMrXYC8yc-SB6Jrh6VEDQTHDKTHnKUvPHPLDkHD-ogLIvibPOINIimYIaI1IgXNPdwEDQPjTdnjmYn10Ln10dnHnLP1bzTHmK0A7zmyd1n1NYrWcdP1n_0A7zmyd1n1N1rWnvTHDKnhFBP1bknHEVnADQnzYYrAcdsH-bnhnVuhEvrHcvmHbkPW6bTHDYnjNkPWE1P19vrj0OPHbkPjTKnHEkPHTvPjnLrjNLnjNQPjE1n9DKnikQnEDKTEDVTEDKTEDdra3zPj98PWm8nHm1THTKnTDKnikQnE7exEDQnjT1P9DQnjTQPWbzTHn3PWE3PWc1syDQnvnVPAFWmidBujbksHPWnhRhm1Rhnju6mkDKPT7LXjK-nHPBuHw6PHn1PW9YPWmKmyGoRvRWpA7YTHTKnzkQPWNLsjcLnjNKnE78IyQ_TyRBnjE3n1mLmhu6mvP6nWN%22%2C%22lego_sid%22%3A%22168604811212840049557069323%22%2C%22platformCompany%22%3A%22ajk%22%2C%22lego_show_track_url_base%22%3A%22https%3A%5C%2F%5C%2Fadtrack.58.com%5C%2F1001692%5C%2F%22%2C%22lego_version%22%3A%221.0.1%22%2C%22legoHuiDu%22%3Atrue%7D%7D",
          "tid": "38648623-a13c-4bca-bd90-3c2efc5f06ac",
          "isLiving": false,
          "isVR": false,
          "distanceDesc": "",
          "isGoldOwner": false
        },
        {
          "infoid": "2033487094293504",
          "cateName": "整租",
          "cateID": "11",
          "title": "福苑小区 2室1厅1卫 4800元月 60平 电梯房",
          "price": "4800",
          "img": "https://pic1.ajkimg.com/display/anjuke/05f6dd2eb78930184bf28dabb0590f25/240x180c.jpg?t=1&srotate=1",
          "unit": "元/月",
          "coll": "0",
          "subwayDistance": "距8号线-凤凰新村站步行673m",
          "fangwuLiangDian": [
            {
              "text": "整租",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "朝南",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "近地铁",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "有电梯",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            }
          ],
          "area": "60㎡",
          "toward": "朝南",
          "shangquan": "宝岗",
          "isAnxuan": "0",
          "location": false,
          "source_type": "1",
          "isauction": "2",
          "auctionShowText": "",
          "legoAuction": "%7B%22isauction%22%3A%222%22%2C%22lego%22%3A%7B%22lego_tid%22%3A%227b1c9b4c-4547-47d6-b1e4-baa887ed46d0%22%2C%22lego_ad_click_url%22%3A%22https%3A%5C%2F%5C%2Flegoclick.58.com%5C%2Fjump%3Ftarget%3DpZwY0ZnlsztdraOWUvYKuaY3mhFhrHnvmBY1rjK6sHEQnjTVmHb1PBYYrj03mh7bnvFBn19KPjNOPjD1P1mOPWc3n1DKTHckn1nYrj0krHEzrHndnjEKnHcYnjTKnHcYnjTKnikQnED1THDvnWn3PjTLnHTQPHDKrT7AEzdPsN72iSXMMSpcfSpEMrXYC8yc-SB6Jrh6VEDQTHDKTHnKUvPHPLDkHD-ogLIvibPOINIimYIaI1IgXNPdwEDQPjTdnjmdrHmOrjNQP1TLn1bzTHmK0A7zmyd1n1NYrWcdP1n_0A7zmyd1n1N1rWnvTHDKn1E1mymYPWNVuWnYmiYYuj03sHbzmWNVrjc3uyDLPjKbuWn3THDYnjNkPWNOP1TYP1cYPWTzrj9KnHEkPHTvPHbLnjDYrHNknWbLP9DKnikQnEDKTEDVTEDKTEDdra3zPj98PWm8nHm1THTKnTDKnikQnE7exEDQnjT1P9DQnjTQPWbzTHIBnynOmWwWsHEdPj0VPjIbPBdBnyNYsyF6mH93PvRbPjubnTDKPT7LXjK-nHPBuHw6PHn1PW9YPWmKmyGoRvRWpA7YTHTKnzkQPWNLsjcLnj0KnE78IyQ_THP-rH91myFhn1DdPywhnWE%22%2C%22lego_sid%22%3A%22168604811212840049557069323%22%2C%22platformCompany%22%3A%22ajk%22%2C%22lego_show_track_url_base%22%3A%22https%3A%5C%2F%5C%2Fadtrack.58.com%5C%2F1001692%5C%2F%22%2C%22lego_version%22%3A%221.0.1%22%2C%22legoHuiDu%22%3Atrue%7D%7D",
          "tid": "7b1c9b4c-4547-47d6-b1e4-baa887ed46d0",
          "isLiving": false,
          "isVR": false,
          "distanceDesc": "",
          "isGoldOwner": false
        },
        {
          "infoid": "2020257927341059",
          "cateName": "整租",
          "cateID": "11",
          "title": "可逸豪苑三房 园林小区 越秀物业 家私齐全 人车分流",
          "price": "6600",
          "img": "https://pic1.ajkimg.com/display/anjuke/6e95688b335557baa799638f400747aa/240x180c.jpg?t=1&srotate=1",
          "unit": "元/月",
          "coll": "0",
          "subwayDistance": "距8号线-凤凰新村站步行1.2km",
          "fangwuLiangDian": [
            {
              "text": "整租",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "朝南",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "近地铁",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "有电梯",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            }
          ],
          "area": "82㎡",
          "toward": "朝南",
          "shangquan": "江南西",
          "isAnxuan": "0",
          "location": false,
          "source_type": "1",
          "isauction": "2",
          "auctionShowText": "",
          "legoAuction": "%7B%22isauction%22%3A%222%22%2C%22lego%22%3A%7B%22lego_tid%22%3A%2277daf154-ca1d-4d17-a619-7cfddcde9904%22%2C%22lego_ad_click_url%22%3A%22https%3A%5C%2F%5C%2Flegoclick.58.com%5C%2Fjump%3Ftarget%3DpZwY0ZnlsztdraOWUvYKuaY3mhFhrHnvmBY1rjK6sHEQnjTVmHb1PBYYrj03mh7bnvFBn19KP1D3PHDdrjb1P1bvnjDKTHcknWTzPH0OnW01PjDkPHbKnHn3rHbKnHn3rHbKnikQnED1THDvnWn3PjTLnHTQPHDKrE7AEzdPsN72iSXMMSpcfSpEMrXYC8yc-SB6Jrh6VEDQTHDKTHnKUvPHPLDkHD-ogLIvibPOINIimYIaI1IgXNPdwEDQPjTYrHm1njndn1Eknj9LnWbvTHmK0A7zmyd1n1NYrWcdP1n_0A7zmyd1n1N1rWnvTHDKn17hujmzPjmVm1EOPiYYnjTvsHbOuhcVmWPWP1wWrH-bnAN3THDYnjEOPWnkn1mdPjn3PHmvPjDKnHEkPjbvn1T1PWEQrjTzn1EzPTDKnikQnEDKTEDVTEDKTEDdra3zPj98PWm8nHm1THTKnTDKnikQnE7exEDQnjT1P9DQnjTQPWbzTH0LuA7hnHNYsyP6nyEVPAEQPzd6PWDOsHIWuhwbmvw-rHbkPTDKPT7LXjK-nHPBuHw6PHn1PW9YPWmKmyGoRvRWpA7YTHTKnzkQPWNLsj0OP1bKnE78IyQ_TyckmWbkrAcvn1FBuWP-nHE%22%2C%22lego_sid%22%3A%22168604811212840049557069323%22%2C%22platformCompany%22%3A%22ajk%22%2C%22lego_show_track_url_base%22%3A%22https%3A%5C%2F%5C%2Fadtrack.58.com%5C%2F1001692%5C%2F%22%2C%22lego_version%22%3A%221.0.1%22%2C%22legoHuiDu%22%3Atrue%7D%7D",
          "tid": "77daf154-ca1d-4d17-a619-7cfddcde9904",
          "isLiving": false,
          "isVR": false,
          "distanceDesc": "",
          "isGoldOwner": false
        },
        {
          "infoid": "2020664219099136",
          "cateName": "整租",
          "cateID": "11",
          "title": "南华西大厦 2室1厅1卫 4200元月 配套齐全 电梯房",
          "price": "4200",
          "img": "https://pic1.ajkimg.com/display/anjuke/ca02352a357b3993da18874495fca396/240x180c.jpg?t=1&srotate=1",
          "unit": "元/月",
          "coll": "0",
          "subwayDistance": "距8号线-凤凰新村站步行1.3km",
          "fangwuLiangDian": [
            {
              "text": "安选房源",
              "textColor": "#3EC77B",
              "bgColor": "#3EC77B"
            },
            {
              "text": "整租",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "朝南",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "近地铁",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "有电梯",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "精装修",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            }
          ],
          "area": "70㎡",
          "toward": "朝南",
          "shangquan": "宝岗",
          "isAnxuan": "1",
          "location": false,
          "source_type": "1",
          "isauction": "2",
          "auctionShowText": "",
          "legoAuction": "%7B%22isauction%22%3A%222%22%2C%22lego%22%3A%7B%22lego_tid%22%3A%22191f4cb7-aba1-41f7-a818-d9f877c292f3%22%2C%22lego_ad_click_url%22%3A%22https%3A%5C%2F%5C%2Flegoclick.58.com%5C%2Fjump%3Ftarget%3DpZwY0ZnlsztdraOWUvYKuaY3mhFhrHnvmBY1rjK6sHEQnjTVmHb1PBYYrj03mh7bnvFBn19KPjNOPjD1P1mOPWc3n1DKTHcknWTvPWEznHbkrHbQn1mKnHcYnjTKnHcYnjTKnikQnED1THDvnWn3PjTLnHTQPHDKnHTKwbnVHidKibfMGO4hBs4hbF1MV2s-BFxCCpWGCUNKnEDQTED1TyqWN1IQnDQFpdqLIbGjXgRZNhPZEM0LRL-jINNKnHEkPHTvPWDknHnkPHn3rHTdP9DvTgK60h7V01ndPjCzPH01sZK60h7V01ndn1C1P9DQTH93PjNQnhDvsH6bmWTVPAu-uBdBmHbQsyP-Pj7hn1nYnWNdu9DQPjTdnjmvnHTQP1EOrH9dnW9kTHDYnjNkPWmQnjDvPWDOnjT3njTKTHD_nHDKTEDKsEDKTEDKPH98nWE3sWmvsWDvnkDkTHTKTHD_nHDKXLYKnHTkn1mKnHTknHmOn9DQrH7hPAPBPzd6mhDQsHEQuW0VmH9Qradbrym3P1IWnWbzuWnKTHEKIL9kuHD1mhNYmHN1n1m3PjmvTy7JpdI-mv66ITDkTHn_nHmdPzkzP1TLTHDKUMR_UT7-uywBmWPBuyn1njRhPhDv%22%2C%22lego_sid%22%3A%22168604811212840049557069323%22%2C%22platformCompany%22%3A%22ajk%22%2C%22lego_show_track_url_base%22%3A%22https%3A%5C%2F%5C%2Fadtrack.58.com%5C%2F1001692%5C%2F%22%2C%22lego_version%22%3A%221.0.1%22%2C%22legoHuiDu%22%3Atrue%7D%7D",
          "tid": "191f4cb7-aba1-41f7-a818-d9f877c292f3",
          "isLiving": false,
          "isVR": false,
          "distanceDesc": "",
          "isGoldOwner": false
        },
        {
          "infoid": "2038897745486863",
          "cateName": "整租",
          "cateID": "11",
          "title": "富力现代广场 精致3房东南向 超值抵租 只需5500元",
          "price": "5500",
          "img": "https://pic1.ajkimg.com/display/anjuke/cc64ba8b5e714d067421804547d55302/240x180c.jpg?t=1&srotate=1",
          "unit": "元/月",
          "coll": "0",
          "subwayDistance": "距8号线-凤凰新村站步行444m",
          "fangwuLiangDian": [
            {
              "text": "整租",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "南北",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "近地铁",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "新上",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "有电梯",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "精装修",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            }
          ],
          "area": "98㎡",
          "toward": "南北",
          "shangquan": "工业大道北",
          "isAnxuan": "0",
          "location": false,
          "source_type": "1",
          "isauction": "2",
          "auctionShowText": "",
          "legoAuction": "%7B%22isauction%22%3A%222%22%2C%22lego%22%3A%7B%22lego_tid%22%3A%2277c7cc92-238f-4858-9560-949e5f1c1c2b%22%2C%22lego_ad_click_url%22%3A%22https%3A%5C%2F%5C%2Flegoclick.58.com%5C%2Fjump%3Ftarget%3DpZwY0ZnlsztdraOWUvYKuaY3mhFhrHnvmBY1rjK6sHEQnjTVmHb1PBYYrj03mh7bnvFBn19KPjn3PH0zn10YPHTzPHEKTHckn193rH0LPjNYrjm3PWnKnHD1rHbKnHD1rHbKnikQnED1THDvnWn3PjTLnHTQPHDKnHDKwbnVHidKibfMGO4hBs4hbF1MV2s-BFxCCpWGCUNKnEDQTED1TyqWN1IQnDQFpdqLIbGjXgRZNhPZEM0LRL-jINNKnHEkPHTvnHT1n1EvnWTLrHE3rEDvTgK60h7V01ndPjCzPH01sZK60h7V01ndn1C1P9DQTHK6PyDdrym1syDduADVPjTdnzd6mHbksyDOmhEvmymLnAw6n9DQPjTdnjmQnjn1PW9knH9Ln1bzTHDYnjNkPWDkn1ndrj0Onj9vnj9KTHD_nHDKTEDKsEDKTEDKPH98nWE3sWmvsWDvnkDkTHTKTHD_nHDKXLYKnHTkn1mKnHTknHmOn9DLPvnLmvnOnBYzn16hsHE3PH9VrHNvnaYOPj--PymQm17WnhcKTHEKIL9kuHD1mhNYmHN1n1m3PjmvTy7JpdI-mv66ITDkTHn_nHmdPzkzP1TdTHDKUMR_UTDdm17brAuhPjwhPhDdnWmv%22%2C%22lego_sid%22%3A%22168604811212840049557069323%22%2C%22platformCompany%22%3A%22ajk%22%2C%22lego_show_track_url_base%22%3A%22https%3A%5C%2F%5C%2Fadtrack.58.com%5C%2F1001692%5C%2F%22%2C%22lego_version%22%3A%221.0.1%22%2C%22legoHuiDu%22%3Atrue%7D%7D",
          "tid": "77c7cc92-238f-4858-9560-949e5f1c1c2b",
          "isLiving": false,
          "isVR": false,
          "distanceDesc": "",
          "isGoldOwner": false
        },
        {
          "infoid": "2037697376672781",
          "cateName": "整租",
          "cateID": "11",
          "title": "出租，家电家电齐全。精装修，可随时看房",
          "price": "5500",
          "img": "https://pic1.ajkimg.com/display/anjuke/d4fa44088a24ac8a5e2572230a7aa01c/240x180c.jpg?t=1&srotate=1",
          "unit": "元/月",
          "coll": "0",
          "subwayDistance": "距8号线-凤凰新村站步行1.2km",
          "fangwuLiangDian": [
            {
              "text": "整租",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "东南",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "近地铁",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "有电梯",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "精装修",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            }
          ],
          "area": "98㎡",
          "toward": "东南",
          "shangquan": "江南西",
          "isAnxuan": "0",
          "location": false,
          "source_type": "1",
          "isauction": "2",
          "auctionShowText": "",
          "legoAuction": "%7B%22isauction%22%3A%222%22%2C%22lego%22%3A%7B%22lego_tid%22%3A%2294083f05-4879-47bf-8da8-cdbad5c351b9%22%2C%22lego_ad_click_url%22%3A%22https%3A%5C%2F%5C%2Flegoclick.58.com%5C%2Fjump%3Ftarget%3DpZwY0ZnlsztdraOWUvYKuaY3mhFhrHnvmBY1rjK6sHEQnjTVmHb1PBYYrj03mh7bnvFBn19KPHT3nWcOPjckPj0LPWnKTHckn10vrH01P1mvP1cLrjDKnHn3rHbKnHn3rHbKnikQnED1THDvnWn3PjTLnHTQPHDKnHcKwbnVHidKibfMGO4hBs4hbF1MV2s-BFxCCpWGCUNKnEDQTED1TyqWN1IQnDQFpdqLIbGjXgRZNhPZEM0LRL-jINNKnHEkPHT1nj0drjN1PHEkPHNvrTDvTgK60h7V01ndPjCzPH01sZK60h7V01ndn1C1P9DQTHE1rjnQPvwbsHcvmhnVPjNkniYOrAmdsH9QnyD1ryDkuj-BP9DQPjTdnjnkP1N3rj0krHEOrj93THDYnjNkn1TLPH9LP19vP1NznjTKTHD_nHDKTEDKsEDKTEDKPH98nWE3sWmvsWDvnkDkTHTKTHD_nHDKXLYKnHTkn1mKnHTknHmOn9DOPjT3nvmkPiYYrj0OsHELmhmVrAw6radWuAF6ujRWn1NQmWbKTHEKIL9kuHD1mhNYmHN1n1m3PjmvTy7JpdI-mv66ITDkTHn_nHmdPzkLrH0OTHDKUMR_UTD3uADvPyc3n17hPym3P1Td%22%2C%22lego_sid%22%3A%22168604811212840049557069323%22%2C%22platformCompany%22%3A%22ajk%22%2C%22lego_show_track_url_base%22%3A%22https%3A%5C%2F%5C%2Fadtrack.58.com%5C%2F1001692%5C%2F%22%2C%22lego_version%22%3A%221.0.1%22%2C%22legoHuiDu%22%3Atrue%7D%7D",
          "tid": "94083f05-4879-47bf-8da8-cdbad5c351b9",
          "isLiving": false,
          "isVR": false,
          "distanceDesc": "",
          "isGoldOwner": false
        },
        {
          "infoid": "2023714652380174",
          "cateName": "整租",
          "cateID": "11",
          "title": "八号线广佛线交汇急租两房！交通便利 繁华商圈 家私家电齐全",
          "price": "6000",
          "img": "https://pic1.ajkimg.com/display/anjuke/1d5dba9ba0464f329646221e915440bc/240x180c.jpg?t=1&srotate=1",
          "unit": "元/月",
          "coll": "0",
          "subwayDistance": "距8号线-凤凰新村站步行444m",
          "fangwuLiangDian": [
            {
              "text": "整租",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "朝西",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "近地铁",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "有电梯",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "精装修",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            }
          ],
          "area": "79㎡",
          "toward": "朝西",
          "shangquan": "工业大道北",
          "isAnxuan": "0",
          "location": false,
          "source_type": "1",
          "isauction": "2",
          "auctionShowText": "",
          "legoAuction": "%7B%22isauction%22%3A%222%22%2C%22lego%22%3A%7B%22lego_tid%22%3A%228d5e1207-605e-41b7-8c57-5b19af5d95e2%22%2C%22lego_ad_click_url%22%3A%22https%3A%5C%2F%5C%2Flegoclick.58.com%5C%2Fjump%3Ftarget%3DpZwY0ZnlsztdraOWUvYKuaY3mhFhrHnvmBY1rjK6sHEQnjTVmHb1PBYYrj03mh7bnvFBn19KrjTknHc1PWb1rH03njNKTHcknWnLnHEvPHc1rjTQP1EKnHD1rHbKnHD1rHbKnikQnED1THDvnWn3PjTLnHTQPHDKnHnKwbnVHidKibfMGO4hBs4hbF1MV2s-BFxCCpWGCUNKnEDQTED1TyqWN1IQnDQFpdqLIbGjXgRZNhPZEM0LRL-jINNKnHEknHDLn1D3PHNYPHmYPWT3nTDvTgK60h7V01ndPjCzPH01sZK60h7V01ndn1C1P9DQTH0vn10QuW-WsycQuAnVPjD1mBdBPvRbsHKBmhwbmhFWuWPbPTDQPjTQnH01nH9dPWbvPWEQnjcYTHDYnjDQP1nQrjNvnjE1PWm1n1mKTHD_nHDKTEDKsEDKTEDKPH98nWE3sWmvsWDvnkDkTHTKTHD_nHDKXLYKnHTkn1mKnHTknHmOn9D3ujR-nHckPzYvnjR-sHEQmW0VrAndPzYdmWDOmymdujbduHcKTHEKIL9kuHD1mhNYmHN1n1m3PjmvTy7JpdI-mv66ITDkTHn_nHmdPzkzP1TdTHDKUMR_UT76uyFbujFhnjF-mhF6nhc3%22%2C%22lego_sid%22%3A%22168604811212840049557069323%22%2C%22platformCompany%22%3A%22ajk%22%2C%22lego_show_track_url_base%22%3A%22https%3A%5C%2F%5C%2Fadtrack.58.com%5C%2F1001692%5C%2F%22%2C%22lego_version%22%3A%221.0.1%22%2C%22legoHuiDu%22%3Atrue%7D%7D",
          "tid": "8d5e1207-605e-41b7-8c57-5b19af5d95e2",
          "isLiving": false,
          "isVR": false,
          "distanceDesc": "",
          "isGoldOwner": false
        },
        {
          "infoid": "1931015190912015",
          "cateName": "整租",
          "cateID": "11",
          "title": "富力现代广场豪华装修两房出租 有外阳台采光好 适宜居住",
          "price": "5200",
          "img": "https://pic1.ajkimg.com/display/anjuke/1d5dba9ba0464f329646221e915440bc/240x180c.jpg?t=1&srotate=1",
          "unit": "元/月",
          "coll": "0",
          "subwayDistance": "距8号线-凤凰新村站步行444m",
          "fangwuLiangDian": [
            {
              "text": "整租",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "朝西",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "近地铁",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "有电梯",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "精装修",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            }
          ],
          "area": "79㎡",
          "toward": "朝西",
          "shangquan": "工业大道北",
          "isAnxuan": "0",
          "location": false,
          "source_type": "1",
          "isauction": "2",
          "auctionShowText": "",
          "legoAuction": "%7B%22isauction%22%3A%222%22%2C%22lego%22%3A%7B%22lego_tid%22%3A%224dbf75fd-61bd-40de-a767-386e80302e32%22%2C%22lego_ad_click_url%22%3A%22https%3A%5C%2F%5C%2Flegoclick.58.com%5C%2Fjump%3Ftarget%3DpZwY0ZnlsztdraOWUvYKuaY3mhFhrHnvmBY1rjK6sHEQnjTVmHb1PBYYrj03mh7bnvFBn19KP19YnH91n1EdPWEQnjnKTHDOn1DknHNQrHTOnHcknHNKnHD1rHbKnHD1rHbKnikQnED1THDvnWn3PjTLnHTQPHDKnHEKwbnVHidKibfMGO4hBs4hbF1MV2s-BFxCCpWGCUNKnEDQTED1TyqWN1IQnDQFpdqLIbGjXgRZNhPZEM0LRL-jINNKnHEkPj0zrjDOPjbvn1T3P1nvnTDvTgK60h7V01ndPjCzPH01sZK60h7V01ndn1C1P9DQTHTYPWmvPADLsyP-mHmVPj-bPzY3PHb3sHcQPAm1uHIbnW9QuEDQPjTYP1c3nHbdn1bdnHTkPW0zTHDYnjELnW9QrHNzrH9vn1DvrjcKTHD_nHDKTEDKsEDKTEDKPH98nWE3sWmvsWDvnkDkTHTKTHD_nHDKXLYKnHTkn1mKnHTknHmOn9DYuAFhP1RhuaYvnyFbsHEkuANVmH0vPzY1rju-rjT1njF-n1cKTHEKIL9kuHD1mhNYmHN1n1m3PjmvTy7JpdI-mv66ITDkTHn_nHmdPzkzP1TdTHDKUMR_UTDkrjRhPADkn1KWuywBnhDL%22%2C%22lego_sid%22%3A%22168604811212840049557069323%22%2C%22platformCompany%22%3A%22ajk%22%2C%22lego_show_track_url_base%22%3A%22https%3A%5C%2F%5C%2Fadtrack.58.com%5C%2F1001692%5C%2F%22%2C%22lego_version%22%3A%221.0.1%22%2C%22legoHuiDu%22%3Atrue%7D%7D",
          "tid": "4dbf75fd-61bd-40de-a767-386e80302e32",
          "isLiving": false,
          "isVR": false,
          "distanceDesc": "",
          "isGoldOwner": false
        },
        {
          "infoid": "2034426617061382",
          "cateName": "整租",
          "cateID": "11",
          "title": "南康阁 3室2厅1卫 4600元月 精装修 南北通透",
          "price": "4600",
          "img": "https://pic1.ajkimg.com/display/anjuke/036fd163a5fcca18986ae5c8a70bab47/240x180c.jpg?t=1&srotate=1",
          "unit": "元/月",
          "coll": "0",
          "subwayDistance": "距8号线-凤凰新村站步行1.6km",
          "fangwuLiangDian": [
            {
              "text": "整租",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "朝南",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "近地铁",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "有电梯",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "精装修",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            }
          ],
          "area": "96㎡",
          "toward": "朝南",
          "shangquan": "江南西",
          "isAnxuan": "0",
          "location": false,
          "source_type": "1",
          "isauction": "2",
          "auctionShowText": "",
          "legoAuction": "%7B%22isauction%22%3A%222%22%2C%22lego%22%3A%7B%22lego_tid%22%3A%22ceddc4f0-aa7e-4c3e-8366-6b0ce77ddcc1%22%2C%22lego_ad_click_url%22%3A%22https%3A%5C%2F%5C%2Flegoclick.58.com%5C%2Fjump%3Ftarget%3DpZwY0ZnlsztdraOWUvYKuaY3mhFhrHnvmBY1rjK6sHEQnjTVmHb1PBYYrj03mh7bnvFBn19KPjD1rHD1n1ELnH0dnTDKnWT1PjEzPWmQP1TvnHn3n9DQn19OrEDQn19OrEDQsjDQTHnKnHmzn19Ynj0QnjDdnEDQPE7AEzdPsN72iSXMMSpcfSpEMrXYC8yc-SB6Jrh6VEDQTHDKTHnKUvPHPLDkHD-ogLIvibPOINIimYIaI1IgXNPdwEDQPjT1rHDYPHm1PHnYrHbQn1mkTHmK0A7zmyd1n1NYrWcdP1n_0A7zmyd1n1N1rWnvTHDKnWFBmH0YuH0VrHFBuaYYuHuhsH-WnyNVnvNQrHKhrHn1rjTLTHDYnjnOnHEdPWnvrHTQrjTvnj9KnHEkn1bQPjNvn1mkPWcOnjE1n9DKnikQnEDKTEDVTEDKTEDdra3zPj98PWm8nHm1THTKnTDKnikQnE7exEDQnjT1P9DQnjTQPWbzTyP-uAwWPAmksy76PvNVPAn1uiY3n1mvsHuBnAP-P1IbuAPWnEDKPT7LXjK-nHPBuHw6PHn1PW9YPWmKmyGoRvRWpA7YTHTKnzkQPWNLsj0OP1bKnE78IyQ_TyD1nWFBuAFbrAnvP10kujN%22%2C%22lego_sid%22%3A%22168604811212840049557069323%22%2C%22platformCompany%22%3A%22ajk%22%2C%22lego_show_track_url_base%22%3A%22https%3A%5C%2F%5C%2Fadtrack.58.com%5C%2F1001692%5C%2F%22%2C%22lego_version%22%3A%221.0.1%22%2C%22legoHuiDu%22%3Atrue%7D%7D",
          "tid": "ceddc4f0-aa7e-4c3e-8366-6b0ce77ddcc1",
          "isLiving": false,
          "isVR": false,
          "distanceDesc": "",
          "isGoldOwner": false
        },
        {
          "infoid": "2028706899682311",
          "cateName": "整租",
          "cateID": "11",
          "title": "江南西 电梯 精装修三房一厅 拎包入住 采光好 近地铁！",
          "price": "5600",
          "img": "https://pic1.ajkimg.com/display/anjuke/479efb490d98fbb72b3d73d4e10303f0/240x180c.jpg?t=1&srotate=1",
          "unit": "元/月",
          "coll": "0",
          "subwayDistance": "距8号线-凤凰新村站步行1.7km",
          "fangwuLiangDian": [
            {
              "text": "整租",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "东南",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "近地铁",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "精装修",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            }
          ],
          "area": "70㎡",
          "toward": "东南",
          "shangquan": "江南西",
          "isAnxuan": "0",
          "location": false,
          "source_type": "1",
          "isauction": "2",
          "auctionShowText": "",
          "legoAuction": "%7B%22isauction%22%3A%222%22%2C%22lego%22%3A%7B%22lego_tid%22%3A%2219347dc6-b968-4c29-8b4a-54303d68172c%22%2C%22lego_ad_click_url%22%3A%22https%3A%5C%2F%5C%2Flegoclick.58.com%5C%2Fjump%3Ftarget%3DpZwY0ZnlsztdraOWUvYKuaY3mhFhrHnvmBY1rjK6sHEQnjTVmHb1PBYYrj03mh7bnvFBn19KPjD1rHD1n1ELnH0dnTDKnWTzrj0kPW9OrHm3nWnQnEDQn19OrEDQn19OrEDQsjDQTHnKnHmzn19Ynj0QnjDdnEDQP97AEzdPsN72iSXMMSpcfSpEMrXYC8yc-SB6Jrh6VEDQTHDKTHnKUvPHPLDkHD-ogLIvibPOINIimYIaI1IgXNPdwEDQPjTzPjEOnHTznHNkPH03nH0vTHmK0A7zmyd1n1NYrWcdP1n_0A7zmyd1n1N1rWnvTHDKP1nvPyEkuhnVm1wbuaYYnvubsH-WnAnVrAmdPj6bn1IBrARBTHDYnjcYPjbQnjc1n1TOnWbQPHnKnHEknWEYrHDknWckrHcOrjE1n9DKnikQnEDKTEDVTEDKTEDdra3zPj98PWm8nHm1THTKnTDKnikQnE7exEDQnjT1P9DQnjTQPWbzTHDOn1ELuAnvsycOPW9VPAnzriY3mWw6sHNYn1T1ujm3nH0zmkDKPT7LXjK-nHPBuHw6PHn1PW9YPWmKmyGoRvRWpA7YTHTKnzkQPWNLsj0OP1bKnE78IyQ_THNzmHbOPH01rHFbmhN1rjc%22%2C%22lego_sid%22%3A%22168604811212840049557069323%22%2C%22platformCompany%22%3A%22ajk%22%2C%22lego_show_track_url_base%22%3A%22https%3A%5C%2F%5C%2Fadtrack.58.com%5C%2F1001692%5C%2F%22%2C%22lego_version%22%3A%221.0.1%22%2C%22legoHuiDu%22%3Atrue%7D%7D",
          "tid": "19347dc6-b968-4c29-8b4a-54303d68172c",
          "isLiving": false,
          "isVR": false,
          "distanceDesc": "",
          "isGoldOwner": false
        },
        {
          "infoid": "2023719282188300",
          "cateName": "整租",
          "cateID": "11",
          "title": "双线地铁口物业，距离市二宫凤凰新村地铁口都很近！宝岗大厦诚租",
          "price": "5000",
          "img": "https://pic1.ajkimg.com/display/anjuke/2cc84208af5a23f4a71164a46d2267bd/240x180c.jpg?t=1&srotate=1",
          "unit": "元/月",
          "coll": "0",
          "subwayDistance": "距8号线-凤凰新村站步行811m",
          "fangwuLiangDian": [
            {
              "text": "整租",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "朝南",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "近地铁",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "有电梯",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            }
          ],
          "area": "95㎡",
          "toward": "朝南",
          "shangquan": "宝岗",
          "isAnxuan": "0",
          "location": false,
          "source_type": "1",
          "isauction": "2",
          "auctionShowText": "",
          "legoAuction": "%7B%22isauction%22%3A%222%22%2C%22lego%22%3A%7B%22lego_tid%22%3A%22a1d0352b-ac70-42ab-89fc-cd35f05fd194%22%2C%22lego_ad_click_url%22%3A%22https%3A%5C%2F%5C%2Flegoclick.58.com%5C%2Fjump%3Ftarget%3DpZwY0ZnlsztdraOWUvYKuaY3mhFhrHnvmBY1rjK6sHEQnjTVmHb1PBYYrj03mh7bnvFBn19KP1b3nHczPj9LnjnLrHcKTHcknWnLnHbzrjcQrj91njTKnHcYnjTKnHcYnjTKnikQnED1THDvnWn3PjTLnHTQPHcKnH0KwbnVHidKibfMGO4hBs4hbF1MV2s-BFxCCpWGCUNKnEDQTED1TyqWN1IQnDQFpdqLIbGjXgRZNhPZEM0LRL-jINNKnHEkPjbOrHEdnWDYnHcOP1mvPEDvTgK60h7V01ndPjCzPH01sZK60h7V01ndn1C1P9DQTHRhPHIBuy7hsyRhnycVPA7bmiY3mh7WsyR6PhPWujEdPHc1PkDQPjTYrHbOPjNzn1TkPW9QnWDvTHDYnjEOrHbYPHcznjEznHcznWEKTHD_nHDKTEDKsEDKTEDKPH98nWE3sWmvsWDvnkDkTHTKTHD_nHDKXLYKnHTkn1mKnHTknHmOn976nyEkn1NzmBd6m10ksHEzmycVrj-hmzdWujnduWTduhEQrHEKTHEKIL9kuHD1mhNYmHN1n1m3PjmvTy7JpdI-mv66ITDkTHn_nHmdPzkzP1TLTHDKUMR_UTD3nHbLnyDznH66nhDduH9Y%22%2C%22lego_sid%22%3A%22168604811212840049557069323%22%2C%22platformCompany%22%3A%22ajk%22%2C%22lego_show_track_url_base%22%3A%22https%3A%5C%2F%5C%2Fadtrack.58.com%5C%2F1001692%5C%2F%22%2C%22lego_version%22%3A%221.0.1%22%2C%22legoHuiDu%22%3Atrue%7D%7D",
          "tid": "a1d0352b-ac70-42ab-89fc-cd35f05fd194",
          "isLiving": false,
          "isVR": false,
          "distanceDesc": "",
          "isGoldOwner": false
        },
        {
          "infoid": "2023719369071624",
          "cateName": "整租",
          "cateID": "11",
          "title": "推荐好房 阳光名苑大三房 外露阳台有电梯 现在4888 诚租",
          "price": "4888",
          "img": "https://pic1.ajkimg.com/display/anjuke/cdb189e53854e433fae0f65c865b0e8b/240x180c.jpg?t=1&srotate=1",
          "unit": "元/月",
          "coll": "0",
          "subwayDistance": "距8号线-凤凰新村站步行1.3km",
          "fangwuLiangDian": [
            {
              "text": "安选房源",
              "textColor": "#3EC77B",
              "bgColor": "#3EC77B"
            },
            {
              "text": "整租",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "西南",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "近地铁",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "有电梯",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "精装修",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            }
          ],
          "area": "88㎡",
          "toward": "西南",
          "shangquan": "工业大道北",
          "isAnxuan": "1",
          "location": false,
          "source_type": "1",
          "isauction": "2",
          "auctionShowText": "",
          "legoAuction": "%7B%22isauction%22%3A%222%22%2C%22lego%22%3A%7B%22lego_tid%22%3A%2292b46c7f-488e-4a93-b74d-97488795210c%22%2C%22lego_ad_click_url%22%3A%22https%3A%5C%2F%5C%2Flegoclick.58.com%5C%2Fjump%3Ftarget%3DpZwY0ZnlsztdraOWUvYKuaY3mhFhrHnvmBY1rjK6sHEQnjTVmHb1PBYYrj03mh7bnvFBn19KP19YnH91n1EdPWEQnjnKTHcknWnLnHb1PWbkP1DvnWEKrjTknTD3njTkTHD_nHDKnkDQPWc1rjEkP1DknHNzTHD3TNujsNYVENGsOlXxOCB4OGa0OeiBOmBgl2AClpAdTHDKnEDKnk7fmdnL0HKniyVxILu2EL-dwdFWwYFLPdIOELR7THDYnjELnW9QnWDOnHNkrj93rHmKP97kmgF6Ugn1PHElnWNLnzQkmgF6Ugn1PHnln1mKnED3mWwhn1mOnzYLny7bsHw6PWbVmhP-naYOuH9zuW7hnW6WPvNKnHEkPj0zrjDznWTdn1NkPHTzPTDQPjTYP1c3nHcznjbOPWn3nW0zTEDQsjDQTEDKTiYKTEDKTHN3sWcYra3vPB3QPWnKnTDkTEDQsjDQTgVqTHDknjnvTHDknjDvrHcKrHFBPjuWPvmVPj93uiYYmHb1sycLPAEVrH0Yrj9LrHNznHKWTEDYTgI3nANQnvF-PADdn1nvrjEvP976phVguyPCmgEKnTD1sjDvPH0_nW0kPEDQTyOdUAkKnAFBnH0dPWK-nH6hnj9dP9%22%2C%22lego_sid%22%3A%22168604811212840049557069323%22%2C%22platformCompany%22%3A%22ajk%22%2C%22lego_show_track_url_base%22%3A%22https%3A%5C%2F%5C%2Fadtrack.58.com%5C%2F1001692%5C%2F%22%2C%22lego_version%22%3A%221.0.1%22%2C%22legoHuiDu%22%3Atrue%7D%7D",
          "tid": "92b46c7f-488e-4a93-b74d-97488795210c",
          "isLiving": false,
          "isVR": false,
          "distanceDesc": "",
          "isGoldOwner": false
        },
        {
          "infoid": "2023077425830926",
          "cateName": "整租",
          "cateID": "11",
          "title": "宝岗大道怡龙苑 电梯两房 光线户型好 拎包入住 家电齐",
          "price": "2700",
          "img": "https://pic1.ajkimg.com/display/anjuke/259eee3a57a2b6dee33de3dc1073541c/240x180c.jpg?t=1&srotate=1",
          "unit": "元/月",
          "coll": "0",
          "subwayDistance": "距8号线-凤凰新村站步行936m",
          "fangwuLiangDian": [
            {
              "text": "整租",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "东南",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "近地铁",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "有电梯",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "精装修",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            }
          ],
          "area": "61㎡",
          "toward": "东南",
          "shangquan": "宝岗",
          "isAnxuan": "0",
          "location": false,
          "source_type": "1",
          "isauction": "2",
          "auctionShowText": "",
          "legoAuction": "%7B%22isauction%22%3A%222%22%2C%22lego%22%3A%7B%22lego_tid%22%3A%228d4a9dd9-4a34-41a6-b3d3-72b01a6ea7d3%22%2C%22lego_ad_click_url%22%3A%22https%3A%5C%2F%5C%2Flegoclick.58.com%5C%2Fjump%3Ftarget%3DpZwY0ZnlsztdraOWUvYKuaY3mhFhrHnvmBY1rjK6sHEQnjTVmHb1PBYYrj03mh7bnvFBn19KPHNvrjczPHNOn1DQPjbKTHcknWnkP10YnWN3n1TOnWmKnHcYnjTKnHcYnjTKnikQnED1THDvnWn3PjTLnHTQPHcKnHbKwbnVHidKibfMGO4hBs4hbF1MV2s-BFxCCpWGCUNKnEDQTED1TyqWN1IQnDQFpdqLIbGjXgRZNhPZEM0LRL-jINNKnHEknH0znWn1P10zn1m3PWbQn9DvTgK60h7V01ndPjCzPH01sZK60h7V01ndn1C1P9DQTymOmvc3PvDYsyN3rADVPj7bPiY3uH0dsyDYuWczmvNLrADzn9DQPjTQP1czn1nLrj91nj0kPjmYTHDYnjDLnWc1n10LrHEOrHEQP1mKTHD_nHDKTEDKsEDKTEDKPH98nWE3sWmvsWDvnkDkTHTKTHD_nHDKXLYKnHTkn1mKnHTknHmOn9D3ujw6rywbriYYmHnYsHEQmHmVmWPbnzYLnhcknyDvuyDLujnKTHEKIL9kuHD1mhNYmHN1n1m3PjmvTy7JpdI-mv66ITDkTHn_nHmdPzkzP1TLTHDKUMR_UTDOnycYuHnzmhPWP16-nyPW%22%2C%22lego_sid%22%3A%22168604811212840049557069323%22%2C%22platformCompany%22%3A%22ajk%22%2C%22lego_show_track_url_base%22%3A%22https%3A%5C%2F%5C%2Fadtrack.58.com%5C%2F1001692%5C%2F%22%2C%22lego_version%22%3A%221.0.1%22%2C%22legoHuiDu%22%3Atrue%7D%7D",
          "tid": "8d4a9dd9-4a34-41a6-b3d3-72b01a6ea7d3",
          "isLiving": false,
          "isVR": false,
          "distanceDesc": "",
          "isGoldOwner": false
        },
        {
          "infoid": "2027227895045128",
          "cateName": "整租",
          "cateID": "11",
          "title": "改善型家庭必看租房 业主之前自住 保养好 环境宜人 适合居住",
          "price": "7100",
          "img": "https://pic1.ajkimg.com/display/anjuke/c4b114b4fece185cc5f2a5f6ae8c1018/240x180c.jpg?t=1&srotate=1",
          "unit": "元/月",
          "coll": "0",
          "subwayDistance": "距8号线-凤凰新村站步行1.5km",
          "fangwuLiangDian": [
            {
              "text": "整租",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "朝南",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "近地铁",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "有电梯",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            }
          ],
          "area": "105㎡",
          "toward": "朝南",
          "shangquan": "工业大道南",
          "isAnxuan": "0",
          "location": false,
          "source_type": "1",
          "isauction": "2",
          "auctionShowText": "",
          "legoAuction": "%7B%22isauction%22%3A%222%22%2C%22lego%22%3A%7B%22lego_tid%22%3A%222f2f2730-bb16-4b9c-9c99-5e3576dd3058%22%2C%22lego_ad_click_url%22%3A%22https%3A%5C%2F%5C%2Flegoclick.58.com%5C%2Fjump%3Ftarget%3DpZwY0ZnlsztdraOWUvYKuaY3mhFhrHnvmBY1rjK6sHEQnjTVmHb1PBYYrj03mh7bnvFBn19KP19YnH91n1EdPWEQnjnKTHcknW0znW03rHNkPjNQnW9KnHEYnjTKnHEYnjTKnikQnED1THDvnWn3PjTLnHTQPHcKnWTKwbnVHidKibfMGO4hBs4hbF1MV2s-BFxCCpWGCUNKnEDQTED1TyqWN1IQnDQFpdqLIbGjXgRZNhPZEM0LRL-jINNKnHEkPj0zrjTkPWmdPHmQPjbLPkDvTgK60h7V01ndPjCzPH01sZK60h7V01ndn1C1P9DQTyPhPyDQnWP6sHT3ujbVPjTLPzd6n1ubsy7-mvmknH7buhwWP9DQPjTYP1c3njTvrHc3nWE3rjn1THDYnjELnW9knjmOPWNOrHnYP1cKTHD_nHDKTEDKsEDKTEDKPH98nWE3sWmvsWDvnkDkTHTKTHD_nHDKXLYKnHTkn1mKnHTknHmOn9DzuWFhnW01nadBmWDvsHwBrynVrynOriYduHndP1ubujnkPH9KTHEKIL9kuHD1mhNYmHN1n1m3PjmvTy7JpdI-mv66ITDkTHn_nHmdPzkQnHEdnkDQTyOdUAkKrAFBmhn3nHNzPHI6PAnOrE%22%2C%22lego_sid%22%3A%22168604811212840049557069323%22%2C%22platformCompany%22%3A%22ajk%22%2C%22lego_show_track_url_base%22%3A%22https%3A%5C%2F%5C%2Fadtrack.58.com%5C%2F1001692%5C%2F%22%2C%22lego_version%22%3A%221.0.1%22%2C%22legoHuiDu%22%3Atrue%7D%7D",
          "tid": "2f2f2730-bb16-4b9c-9c99-5e3576dd3058",
          "isLiving": false,
          "isVR": false,
          "distanceDesc": "",
          "isGoldOwner": false
        },
        {
          "infoid": "2023604027547648",
          "cateName": "整租",
          "cateID": "11",
          "title": "邻近凤凰新村地铁口，随时可以拎包入住的新公寓，革新路铭创优居",
          "price": "2400",
          "img": "https://pic1.ajkimg.com/display/anjuke/33f0fb2c5d6d033088ffbbac6c752e41/240x180c.jpg?t=1&srotate=1",
          "unit": "元/月",
          "coll": "0",
          "subwayDistance": "距8号线-凤凰新村站步行491m",
          "fangwuLiangDian": [
            {
              "text": "整租",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "朝南",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "近地铁",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "精装修",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            }
          ],
          "area": "35㎡",
          "toward": "朝南",
          "shangquan": "工业大道北",
          "isAnxuan": "0",
          "location": false,
          "source_type": "1",
          "isauction": "2",
          "auctionShowText": "",
          "legoAuction": "%7B%22isauction%22%3A%222%22%2C%22lego%22%3A%7B%22lego_tid%22%3A%22b90e3b76-db0d-4686-9d18-6bdb12ca65f2%22%2C%22lego_ad_click_url%22%3A%22https%3A%5C%2F%5C%2Flegoclick.58.com%5C%2Fjump%3Ftarget%3DpZwY0ZnlsztdraOWUvYKuaY3mhFhrHnvmBY1rjK6sHEQnjTVmHb1PBYYrj03mh7bnvFBn19KP1b3nHczPj9LnjnLrHcKTHcknWnvnjEknW0dPj0vPj9KnHD1rHbKnHD1rHbKnikQnED1THDvnWn3PjTLnHTQPHcKnWDKwbnVHidKibfMGO4hBs4hbF1MV2s-BFxCCpWGCUNKnEDQTED1TyqWN1IQnDQFpdqLIbGjXgRZNhPZEM0LRL-jINNKnHEkPjbOrHNdnHc1PHbzn1bvrTDvTgK60h7V01ndPjCzPH01sZK60h7V01ndn1C1P9DQTHmLnjKWmvDdsHDOPANVPjubnzYOmWmOsHTLujm1mvmzuW0YPkDQPjTYrHbOPHNQnWDYrHNvPHEvTHDYnjEOrHbdPHDQnH9Yrj0dPHnKTHD_nHDKTEDKsEDKTEDKPH98nWE3sWmvsWDvnkDkTHTKTHD_nHDKXLYKnHTkn1mKnHTknHmOn97BrHK-nvcLPBdbmWKbsHEvrjmVryEQraYvmhwBnHFWmHmduWcKTHEKIL9kuHD1mhNYmHN1n1m3PjmvTy7JpdI-mv66ITDkTHn_nHmdPzkzP1TdTHDKUMR_UT7WmWKbmWEzrjTLrHNYPAw6%22%2C%22lego_sid%22%3A%22168604811212840049557069323%22%2C%22platformCompany%22%3A%22ajk%22%2C%22lego_show_track_url_base%22%3A%22https%3A%5C%2F%5C%2Fadtrack.58.com%5C%2F1001692%5C%2F%22%2C%22lego_version%22%3A%221.0.1%22%2C%22legoHuiDu%22%3Atrue%7D%7D",
          "tid": "b90e3b76-db0d-4686-9d18-6bdb12ca65f2",
          "isLiving": false,
          "isVR": false,
          "distanceDesc": "",
          "isGoldOwner": false
        },
        {
          "infoid": "2013167137121292",
          "cateName": "整租",
          "cateID": "11",
          "title": "恒大金碧湾 中高层  保养新  临近太古仓 有匙随时看",
          "price": "5000",
          "img": "https://pic1.ajkimg.com/display/anjuke/c55565c10a1835d0cf5587d1d8cb33fe/240x180c.jpg?t=1&srotate=1",
          "unit": "元/月",
          "coll": "0",
          "subwayDistance": "距8号线-凤凰新村站步行751m",
          "fangwuLiangDian": [
            {
              "text": "安选房源",
              "textColor": "#3EC77B",
              "bgColor": "#3EC77B"
            },
            {
              "text": "整租",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "朝东",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "近地铁",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "有电梯",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "精装修",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            }
          ],
          "area": "68㎡",
          "toward": "朝东",
          "shangquan": "工业大道北",
          "isAnxuan": "1",
          "location": false,
          "source_type": "1",
          "isauction": "2",
          "auctionShowText": "",
          "legoAuction": "%7B%22isauction%22%3A%222%22%2C%22lego%22%3A%7B%22lego_tid%22%3A%2250d39d06-0657-4d56-a4ea-be3584246734%22%2C%22lego_ad_click_url%22%3A%22https%3A%5C%2F%5C%2Flegoclick.58.com%5C%2Fjump%3Ftarget%3DpZwY0ZnlsztdraOWUvYKuaY3mhFhrHnvmBY1rjK6sHEQnjTVmHb1PBYYrj03mh7bnvFBn19KnWEdPWD1nW9KTHcknHnQPW0Qn10QnWDzrHcKnHD1rHbKnHD1rHbKnikQnED1THDvnWn3PjTLnHTQPHcKnWcKwbnVHidKibfMGO4hBs4hbF1MV2s-BFxCCpWGCUNKnEDQTED1TyqWN1IQnDQFpdqLIbGjXgRZNhPZEM0LRL-jINNKnHEkPj03rjm3P1NknHcLnHTYnTDvTgK60h7V01ndPjCzPH01sZK60h7V01ndn1C1P9DQTHP-PjK6mvnOsHPbuHEVPjczmidBPhmOsH9dnHEYmvcQrHnvP9DQPjTYP193PW9LP1TzPWTQP1c3THDYnjELrj9vrj0vnjDOn19Yn1cKTHD_nHDKTEDKsEDKTEDKPH98nWE3sWmvsWDvnkDkTHTKTHD_nHDKXLYKnHTkn1mKnHTknHmOn9DdnAE1ryEkPBYkPWNLsHwbPHmVmHw-midBuHndrjEzPjmLn1EKTHEKIL9kuHD1mhNYmHN1n1m3PjmvTy7JpdI-mv66ITDkTHn_nHmdPzkzP1TdTHDKUMR_UTDkmWDLuWnYm1ndm1nQuyDO%22%2C%22lego_sid%22%3A%22168604811212840049557069323%22%2C%22platformCompany%22%3A%22ajk%22%2C%22lego_show_track_url_base%22%3A%22https%3A%5C%2F%5C%2Fadtrack.58.com%5C%2F1001692%5C%2F%22%2C%22lego_version%22%3A%221.0.1%22%2C%22legoHuiDu%22%3Atrue%7D%7D",
          "tid": "50d39d06-0657-4d56-a4ea-be3584246734",
          "isLiving": false,
          "isVR": false,
          "distanceDesc": "",
          "isGoldOwner": false
        },
        {
          "infoid": "1942924179660812",
          "cateName": "整租",
          "cateID": "11",
          "title": "市二宫地铁站 小区电梯房 高层看广州塔 三房出租",
          "price": "8500",
          "img": "https://pic1.ajkimg.com/display/anjuke/3118658435e17891d28d3b13941380d2/240x180c.jpg?t=1&srotate=1",
          "unit": "元/月",
          "coll": "0",
          "subwayDistance": "距8号线-凤凰新村站步行2.0km",
          "fangwuLiangDian": [
            {
              "text": "整租",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "西南",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "近地铁",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "有电梯",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "精装修",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            }
          ],
          "area": "93㎡",
          "toward": "西南",
          "shangquan": "江南西",
          "isAnxuan": "0",
          "location": false,
          "source_type": "1",
          "isauction": "2",
          "auctionShowText": "",
          "legoAuction": "%7B%22isauction%22%3A%222%22%2C%22lego%22%3A%7B%22lego_tid%22%3A%227468bbe2-4037-40bf-b908-bf08d0e324e3%22%2C%22lego_ad_click_url%22%3A%22https%3A%5C%2F%5C%2Flegoclick.58.com%5C%2Fjump%3Ftarget%3DpZwY0ZnlsztdraOWUvYKuaY3mhFhrHnvmBY1rjK6sHEQnjTVmHb1PBYYrj03mh7bnvFBn19Kn1nYnjbzPj9OrH0vPjnKTHDOPjcOnWEQP1bvPWT3nHcKnHn3rHbKnHn3rHbKnikQnED1THDvnWn3PjTLnHTQPHcKnWnKwbnVHidKibfMGO4hBs4hbF1MV2s-BFxCCpWGCUNKnEDQTED1TyqWN1IQnDQFpdqLIbGjXgRZNhPZEM0LRL-jINNKnHn3njE3rHTOnHDLPjnLPHEzPTDvTgK60h7V01ndPjCzPH01sZK60h7V01ndn1C1P9DQTHN3PyEYrHR-synduWcVPjDvPBYOrynQsHuBPHmzmvDduhDYuEDQn19kPj9OnjbQPHndnj9OPWmYTHD1rjTYrjbkrHDYn1TznW0OPW9KTHD_nHDKTEDKsEDKTEDKPH98nWE3sWmvsWDvnkDkTHTKTHD_nHDKXLYKnHTkn1mKnHTknHmOn9DLPjm3mhF-nBYYnjnLsHEkmhmVmWbkradBuWT3ujK-n1cYuHnKTHEKIL9kuHD1mhNYmHN1n1m3PjmvTy7JpdI-mv66ITDkTHn_nHmdPzkLrH0OTHDKUMR_UTDQrHuBnH7WnAckPhP6PvP6%22%2C%22lego_sid%22%3A%22168604811212840049557069323%22%2C%22platformCompany%22%3A%22ajk%22%2C%22lego_show_track_url_base%22%3A%22https%3A%5C%2F%5C%2Fadtrack.58.com%5C%2F1001692%5C%2F%22%2C%22lego_version%22%3A%221.0.1%22%2C%22legoHuiDu%22%3Atrue%7D%7D",
          "tid": "7468bbe2-4037-40bf-b908-bf08d0e324e3",
          "isLiving": false,
          "isVR": false,
          "distanceDesc": "",
          "isGoldOwner": false
        },
        {
          "infoid": "1935275310226441",
          "cateName": "整租",
          "cateID": "11",
          "title": "光大花园四期三室精装电梯房 空气清新采光好 保养好居住舒适",
          "price": "7000",
          "img": "https://pic1.ajkimg.com/display/anjuke/fe8dbcb1cbf9594774d0b8644da405aa/240x180c.jpg?t=1&srotate=1",
          "unit": "元/月",
          "coll": "0",
          "subwayDistance": "距8号线-凤凰新村站步行1.5km",
          "fangwuLiangDian": [
            {
              "text": "整租",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "朝南",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "近地铁",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "有电梯",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "精装修",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            }
          ],
          "area": "126㎡",
          "toward": "朝南",
          "shangquan": "工业大道南",
          "isAnxuan": "0",
          "location": false,
          "source_type": "1",
          "isauction": "2",
          "auctionShowText": "",
          "legoAuction": "%7B%22isauction%22%3A%222%22%2C%22lego%22%3A%7B%22lego_tid%22%3A%22f09e88fc-8908-4809-bc24-be8ab72e1d87%22%2C%22lego_ad_click_url%22%3A%22https%3A%5C%2F%5C%2Flegoclick.58.com%5C%2Fjump%3Ftarget%3DpZwY0ZnlsztdraOWUvYKuaY3mhFhrHnvmBY1rjK6sHEQnjTVmHb1PBYYrj03mh7bnvFBn19KP19YnH91n1EdPWEQnjnKTHDOn1NzP1N1nHTznWmYPjDKnHEYnjTKnHEYnjTKnikQnED1THDvnWn3PjTLnHTQPHcKnWEKwbnVHidKibfMGO4hBs4hbF1MV2s-BFxCCpWGCUNKnEDQTED1TyqWN1IQnDQFpdqLIbGjXgRZNhPZEM0LRL-jINNKnHEkPj0zrjTvnH0znWm1PWc3rTDvTgK60h7V01ndPjCzPH01sZK60h7V01ndn1C1P9DQTHTQnj0kn1TOsHDYuyDVPAc3mBYOnvELsHRhuhmOmvNvPHmOrEDQPjTYP1c3njmQrH0knHTknWcYTHDYnjELnW9kPWcknj03PjE3PWEKTHD_nHDKTEDKsEDKTEDKPH98nWE3sWmvsWDvnkDkTHTKTHD_nHDKXLYKnHTkn1mKnHTknHmOn97hnj--rj6hmzY3rHT3sHE3njbVmhnzPadBuH66mW0zuH7brj0KTHEKIL9kuHD1mhNYmHN1n1m3PjmvTy7JpdI-mv66ITDkTHn_nHmdPzkQnHEdnkDQTyOdUAkKnAwBPjcvuj03mHKWnvRhuT%22%2C%22lego_sid%22%3A%22168604811212840049557069323%22%2C%22platformCompany%22%3A%22ajk%22%2C%22lego_show_track_url_base%22%3A%22https%3A%5C%2F%5C%2Fadtrack.58.com%5C%2F1001692%5C%2F%22%2C%22lego_version%22%3A%221.0.1%22%2C%22legoHuiDu%22%3Atrue%7D%7D",
          "tid": "f09e88fc-8908-4809-bc24-be8ab72e1d87",
          "isLiving": false,
          "isVR": false,
          "distanceDesc": "",
          "isGoldOwner": false
        },
        {
          "infoid": "1996249375597582",
          "cateName": "整租",
          "cateID": "11",
          "title": "南北广场  新净三房两厅  拎包入住",
          "price": "4600",
          "img": "https://pic1.ajkimg.com/display/anjuke/3e6daeab8cc1866e4aa08546e7c1ed5f/240x180c.jpg?t=1&srotate=1",
          "unit": "元/月",
          "coll": "0",
          "subwayDistance": "距8号线-凤凰新村站步行1.1km",
          "fangwuLiangDian": [
            {
              "text": "整租",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "朝东",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "近地铁",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "有电梯",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "精装修",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            }
          ],
          "area": "90㎡",
          "toward": "朝东",
          "shangquan": "宝岗",
          "isAnxuan": "0",
          "location": false,
          "source_type": "1",
          "isauction": "2",
          "auctionShowText": "",
          "legoAuction": "%7B%22isauction%22%3A%222%22%2C%22lego%22%3A%7B%22lego_tid%22%3A%22918f4489-0ebf-49a3-a5c6-c723227a1b4c%22%2C%22lego_ad_click_url%22%3A%22https%3A%5C%2F%5C%2Flegoclick.58.com%5C%2Fjump%3Ftarget%3DpZwY0ZnlsztdraOWUvYKuaY3mhFhrHnvmBY1rjK6sHEQnjTVmHb1PBYYrj03mh7bnvFBn19KPjmQrHmkn1Evrjmzn1TKTHDOrHmzPjb1P1NdrH0drjcKnHcYnjTKnHcYnjTKnikQnED1THDvnWn3PjTLnHTQPHcKnWNKwbnVHidKibfMGO4hBs4hbF1MV2s-BFxCCpWGCUNKnEDQTED1TyqWN1IQnDQFpdqLIbGjXgRZNhPZEM0LRL-jINNKnHEkPjmvP1NYn1n3PHc3PjmkrTDvTgK60h7V01ndPjCzPH01sZK60h7V01ndn1C1P9DQTHndP1KBrj9LsH--uH0VPAcQnid6ujwhsH66P1mQmW7huWKWrTDQPjTYPWmLPHE1PHmOrjn3nj9kTHDYnjEvPW0dPjnYPWTLrjmQP1mKTHD_nHDKTEDKsEDKTEDKPH98nWE3sWmvsWDvnkDkTHTKTHD_nHDKXLYKnHTkn1mKnHTknHmOn9DOnH6hPjE3riYkuyFhsHEOmHnVmHRWPBdWP1c1nWcLmH7BPAnKTHEKIL9kuHD1mhNYmHN1n1m3PjmvTy7JpdI-mv66ITDkTHn_nHmdPzkzP1TLTHDKUMR_UTDvuhEOuyn3P1-buWu-rHcQ%22%2C%22lego_sid%22%3A%22168604811212840049557069323%22%2C%22platformCompany%22%3A%22ajk%22%2C%22lego_show_track_url_base%22%3A%22https%3A%5C%2F%5C%2Fadtrack.58.com%5C%2F1001692%5C%2F%22%2C%22lego_version%22%3A%221.0.1%22%2C%22legoHuiDu%22%3Atrue%7D%7D",
          "tid": "918f4489-0ebf-49a3-a5c6-c723227a1b4c",
          "isLiving": false,
          "isVR": false,
          "distanceDesc": "",
          "isGoldOwner": false
        },
        {
          "infoid": "2021708827385870",
          "cateName": "整租",
          "cateID": "11",
          "title": "广佛八号线！超值南向三房两厅精装电梯房 拎包入住周边配套齐全",
          "price": "6400",
          "img": "https://pic1.ajkimg.com/display/anjuke/2e2e429824d0d14e2d2d50f539fb2d15/240x180c.jpg?t=1&srotate=1",
          "unit": "元/月",
          "coll": "0",
          "subwayDistance": "距8号线-凤凰新村站步行1.2km",
          "fangwuLiangDian": [
            {
              "text": "安选房源",
              "textColor": "#3EC77B",
              "bgColor": "#3EC77B"
            },
            {
              "text": "整租",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "朝南",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "近地铁",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "有电梯",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "精装修",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            }
          ],
          "area": "121㎡",
          "toward": "朝南",
          "shangquan": "工业大道北",
          "isAnxuan": "1",
          "location": false,
          "source_type": "1",
          "isauction": "2",
          "auctionShowText": "",
          "legoAuction": "%7B%22isauction%22%3A%222%22%2C%22lego%22%3A%7B%22lego_tid%22%3A%2285d71158-95be-443e-af8f-9f4d1ccba536%22%2C%22lego_ad_click_url%22%3A%22https%3A%5C%2F%5C%2Flegoclick.58.com%5C%2Fjump%3Ftarget%3DpZwY0ZnlsztdraOWUvYKuaY3mhFhrHnvmBY1rjK6sHEQnjTVmHb1PBYYrj03mh7bnvFBn19KrjTknHc1PWb1rH03njNKTHcknWDLnj93nW01rjN3P1TKrjTknTD3njTkTHD_nHDKnkDQPWc1rjEkP1DknHNzTHcvTNujsNYVENGsOlXxOCB4OGa0OeiBOmBgl2AClpAdTHDKnEDKnk7fmdnL0HKniyVxILu2EL-dwdFWwYFLPdIOELR7THDYnjTOrj9OnHmOPjELPjT1PHnKP97kmgF6Ugn1PHElnWNLnzQkmgF6Ugn1PHnln1mKnE7Wryw6nvDkPBYkuH0OsHEvPH9VrjbLuiYQmWcvuhuhuH93uHTKnHEknjb3rjbQP1DzrHcOn19zPTDQPjTkrH93rHDLnjnLnjDOnHnvTEDQsjDQTEDKTiYKTEDKTHN3sWcYra3vPB3QPWnKnTDkTEDQsjDQTgVqTHDknjnvTHDknjDvrHcKrjRbP1DQPH9VrHRBuiYYPjP-sy7hrAmVrymYuj7WmvF6PHnvTEDYTgI3nANQnvF-PADdn1nvrjEvP976phVguyPCmgEKnTD1sjDvPH0_nW0kPEDQTyOdUAkKPjEznAwhmWDdnHbzmywbnk%22%2C%22lego_sid%22%3A%22168604811212840049557069323%22%2C%22platformCompany%22%3A%22ajk%22%2C%22lego_show_track_url_base%22%3A%22https%3A%5C%2F%5C%2Fadtrack.58.com%5C%2F1001692%5C%2F%22%2C%22lego_version%22%3A%221.0.1%22%2C%22legoHuiDu%22%3Atrue%7D%7D",
          "tid": "85d71158-95be-443e-af8f-9f4d1ccba536",
          "isLiving": false,
          "isVR": false,
          "distanceDesc": "",
          "isGoldOwner": false
        },
        {
          "infoid": "1982001226408968",
          "cateName": "整租",
          "cateID": "11",
          "title": "光大花园三期榕城尚品  沙园地铁站 复式楼精装修   免仲介",
          "price": "2000",
          "img": "https://pic1.ajkimg.com/display/anjuke/c24954dd2338aef7adf8b4db53a670a8/240x180c.jpg?t=1&srotate=1",
          "unit": "元/月",
          "coll": "0",
          "subwayDistance": "距8号线-凤凰新村站步行1.2km",
          "fangwuLiangDian": [
            {
              "text": "整租",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "朝西",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "近地铁",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "有电梯",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "精装修",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            }
          ],
          "area": "29㎡",
          "toward": "朝西",
          "shangquan": "工业大道北",
          "isAnxuan": "0",
          "location": false,
          "source_type": "1",
          "isauction": "2",
          "auctionShowText": "",
          "legoAuction": "%7B%22isauction%22%3A%222%22%2C%22lego%22%3A%7B%22lego_tid%22%3A%2277c2b14e-5f37-40e6-b19f-c3a750dd288c%22%2C%22lego_ad_click_url%22%3A%22https%3A%5C%2F%5C%2Flegoclick.58.com%5C%2Fjump%3Ftarget%3DpZwY0ZnlsztdraOWUvYKuaY3mhFhrHnvmBY1rjK6sHEQnjTVmHb1PBYYrj03mh7bnvFBn19KP1mLPWEkPWNOnH0OP19KTHDOrjcknjDznWmYnj9OPW9KPW9knTDvrjTkTHD_nHDKnkDQPWc1rjEkP1DknHNzTHcLTNujsNYVENGsOlXxOCB4OGa0OeiBOmBgl2AClpAdTHDKnEDKnk7fmdnL0HKniyVxILu2EL-dwdFWwYFLPdIOELR7THDYnjELPWc1Pjn1njE1n1bYPHmKP97kmgF6Ugn1PHElnWNLnzQkmgF6Ugn1PHnln1mKnE7hm16hPjR-nBd-uyDksHwbnjnVmWc3PBdBmWubrH66uWn3mvnKnHEkPj0vnWnYPjEYnjbOPH9YnTDQPjTYP1mzn1EYn1E3P1cQnHNzTEDQsjDQTEDKTiYKTEDKTHN3sWcYra3vPB3QPWnKnTDkTEDQsjDQTgVqTHDknjnvTHDknjDvrHcKP1IWnhcQPANVPym1PzYYnANvsycQrymVm1P6P1NkuAEzrj6WTEDYTgI3nANQnvF-PADdn1nvrjEvP976phVguyPCmgEKnTD1sjDvPH0_nW0kPEDQTyOdUAkKnAmOmhE1rAEknyRBnjc1P9%22%2C%22lego_sid%22%3A%22168604811212840049557069323%22%2C%22platformCompany%22%3A%22ajk%22%2C%22lego_show_track_url_base%22%3A%22https%3A%5C%2F%5C%2Fadtrack.58.com%5C%2F1001692%5C%2F%22%2C%22lego_version%22%3A%221.0.1%22%2C%22legoHuiDu%22%3Atrue%7D%7D",
          "tid": "77c2b14e-5f37-40e6-b19f-c3a750dd288c",
          "isLiving": false,
          "isVR": false,
          "distanceDesc": "",
          "isGoldOwner": false
        },
        {
          "infoid": "2033383123950599",
          "cateName": "整租",
          "cateID": "11",
          "title": "凤凰新村 精装电梯高层 4房仅租 7000元 南北向位置安静",
          "price": "7000",
          "img": "https://pic1.ajkimg.com/display/anjuke/06aa2ecdc22190a0235967ea994464a1/240x180c.jpg?t=1&srotate=1",
          "unit": "元/月",
          "coll": "0",
          "subwayDistance": "距8号线-凤凰新村站步行1m",
          "fangwuLiangDian": [
            {
              "text": "整租",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "南北",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "近地铁",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "有电梯",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "精装修",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            }
          ],
          "area": "112㎡",
          "toward": "南北",
          "shangquan": "工业大道北",
          "isAnxuan": "0",
          "location": false,
          "source_type": "1",
          "isauction": "2",
          "auctionShowText": "",
          "legoAuction": "%7B%22isauction%22%3A%222%22%2C%22lego%22%3A%7B%22lego_tid%22%3A%22103cb4de-890d-4e5a-9028-7e9b46dddb4e%22%2C%22lego_ad_click_url%22%3A%22https%3A%5C%2F%5C%2Flegoclick.58.com%5C%2Fjump%3Ftarget%3DpZwY0ZnlsztdraOWUvYKuaY3mhFhrHnvmBY1rjK6sHEQnjTVmHb1PBYYrj03mh7bnvFBn19KP19kPjEzrjDOrHTYnWDKTHckn1n1rjnQnWnOPHTdrHbKnHD1rHbKnHD1rHbKnikQnED1THDvnWn3PjTLnHTQPHcKnW9KwbnVHidKibfMGO4hBs4hbF1MV2s-BFxCCpWGCUNKnEDQTED1TyqWN1IQnDQFpdqLIbGjXgRZNhPZEM0LRL-jINNKnHEkPHTznjNQrHEQP1cdP1b3PTDvTgK60h7V01ndPjCzPH01sZK60h7V01ndn1C1P9DQTHPWrAcQP17WsHFBPj9VPjDkrid6mvF6sH0OrHuWnH--nWm3nkDQPjTdnjckPHDOPHm3nWNzrHc3THDYnjNknWTdnHbYP1NOP19zPjTKTHD_nHDKTEDKsEDKTEDKPH98nWE3sWmvsWDvnkDkTHTKTHD_nHDKXLYKnHTkn1mKnHTknHmOn9DQnjPWmWwbuiY3rHKbsHw-PyDVrHTzraYLuH-BPjubuAwBPANKTHEKIL9kuHD1mhNYmHN1n1m3PjmvTy7JpdI-mv66ITDkTHn_nHmdPzkzP1TdTHDKUMR_UTDYnW9dmymOrHT3uAELmH--%22%2C%22lego_sid%22%3A%22168604811212840049557069323%22%2C%22platformCompany%22%3A%22ajk%22%2C%22lego_show_track_url_base%22%3A%22https%3A%5C%2F%5C%2Fadtrack.58.com%5C%2F1001692%5C%2F%22%2C%22lego_version%22%3A%221.0.1%22%2C%22legoHuiDu%22%3Atrue%7D%7D",
          "tid": "103cb4de-890d-4e5a-9028-7e9b46dddb4e",
          "isLiving": false,
          "isVR": false,
          "distanceDesc": "",
          "isGoldOwner": false
        },
        {
          "infoid": "2033374328987653",
          "cateName": "整租",
          "cateID": "11",
          "title": "凤凰新村 4室2厅2卫 电梯房 仅租6000 陪读必备 笋！",
          "price": "6000",
          "img": "https://pic1.ajkimg.com/display/anjuke/8944c527d98079cc39b61dd81ed695cb/240x180c.jpg?t=1&srotate=1",
          "unit": "元/月",
          "coll": "0",
          "subwayDistance": "距8号线-凤凰新村站步行1m",
          "fangwuLiangDian": [
            {
              "text": "整租",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "南北",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "近地铁",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "有电梯",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "精装修",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            }
          ],
          "area": "117㎡",
          "toward": "南北",
          "shangquan": "工业大道北",
          "isAnxuan": "0",
          "location": false,
          "source_type": "1",
          "isauction": "2",
          "auctionShowText": "",
          "legoAuction": "%7B%22isauction%22%3A%222%22%2C%22lego%22%3A%7B%22lego_tid%22%3A%223662dd53-0191-4462-93e9-181d5e76a4b6%22%2C%22lego_ad_click_url%22%3A%22https%3A%5C%2F%5C%2Flegoclick.58.com%5C%2Fjump%3Ftarget%3DpZwY0ZnlsztdraOWUvYKuaY3mhFhrHnvmBY1rjK6sHEQnjTVmHb1PBYYrj03mh7bnvFBn19KP19kPjEzrjDOrHTYnWDKTHckn1n1P1E1nW9Orj0vPHnKnHD1rHbKnHD1rHbKnikQnED1THDvnWn3PjTLnHTQPHcKnWbKwbnVHidKibfMGO4hBs4hbF1MV2s-BFxCCpWGCUNKnEDQTED1TyqWN1IQnDQFpdqLIbGjXgRZNhPZEM0LRL-jINNKnHEkPHTznjNYnWDYPWDOnHnvnTDvTgK60h7V01ndPjCzPH01sZK60h7V01ndn1C1P9DQTyR6mh76PWRBsHN1mWmVPAuWriY3rANzsycvP1mvuhRhrAFhnTDQPjTdnjckPHEzn1TOP1mOnWDvTHDYnjNknWTdPjcznHn1njTznWEKTHD_nHDKTEDKsEDKTEDKPH98nWE3sWmvsWDvnkDkTHTKTHD_nHDKXLYKnHTkn1mKnHTknHmOn9D1PWmzuAEdnzYknHbQsHEYPWcVrHP-riYQrj7bPyNLPhDYmWmKTHEKIL9kuHD1mhNYmHN1n1m3PjmvTy7JpdI-mv66ITDkTHn_nHmdPzkzP1TdTHDKUMR_UT7hnhnvnjmdrj9dnWuWuyc1%22%2C%22lego_sid%22%3A%22168604811212840049557069323%22%2C%22platformCompany%22%3A%22ajk%22%2C%22lego_show_track_url_base%22%3A%22https%3A%5C%2F%5C%2Fadtrack.58.com%5C%2F1001692%5C%2F%22%2C%22lego_version%22%3A%221.0.1%22%2C%22legoHuiDu%22%3Atrue%7D%7D",
          "tid": "3662dd53-0191-4462-93e9-181d5e76a4b6",
          "isLiving": false,
          "isVR": false,
          "distanceDesc": "",
          "isGoldOwner": false
        },
        {
          "infoid": "2036220626082822",
          "cateName": "整租",
          "cateID": "11",
          "title": "真实 慎德里小区 准电梯 精装两房 近地铁 家私家电齐",
          "price": "3200",
          "img": "https://pic1.ajkimg.com/display/anjuke/c54c5d5449e0e0d86a44d241c244eae2/240x180c.jpg?t=1&srotate=1",
          "unit": "元/月",
          "coll": "0",
          "subwayDistance": "距8号线-凤凰新村站步行2.0km",
          "fangwuLiangDian": [
            {
              "text": "安选房源",
              "textColor": "#3EC77B",
              "bgColor": "#3EC77B"
            },
            {
              "text": "整租",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "朝北",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "近地铁",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "精装修",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            }
          ],
          "area": "50㎡",
          "toward": "朝北",
          "shangquan": "昌岗",
          "isAnxuan": "1",
          "location": false,
          "source_type": "1",
          "isauction": "6",
          "auctionShowText": "",
          "legoAuction": "%7B%22isauction%22%3A%226%22%2C%22lego%22%3A%7B%22lego_tid%22%3A%229e13a8e9-f356-428e-b8da-479b65a01fe1%22%2C%22lego_ad_click_url%22%3A%22https%3A%5C%2F%5C%2Flegoclick.58.com%5C%2Fjump%3Ftarget%3DpZwY0ZnlsztdraOWUvYKuadbn19LPjubnaYOm191sHEzm1EVrym3PaYQPyN3ujNvP1cdnHEKnWTknHmdPj0YTEDznjnvnWckPWcvnj9zrjczTHTKnHTkTHD_nHDKnkDQPWc1rjEkP1DknHbzTHDKOplFOUA7OplBOlXxOCB4OUa5OlBsOUJ5sXi38Syc-SB6JrpCCXX1B8pEMrXYC9DQTHDKTHnKUvPHPLDkHD-ogLIvibPOINIimYIaI1IgXNPdwEDQPjTdnjTzrHmvP191PHTYn19dTHmK0A7zmyd1n1NYrWcdP1n_0A7zmyd1n1N1rWnvTHDKujmvn1DzuHNVmyRbPiYYrARbsyF6uhmVPhu6mWDzuHb3uWPhTHDYnjNknjcOPWmOPjc3rj0On1mKnHEkPHTknWbvPW9zPHEYn1nzrTDKnikQnEDKTEDVTEDKTEDdra3zPj98PWm8nHm1THTKnTDKnikQnE7exEDQnjTdPTDQnjTzn1NOTH--nHP6rANOsym1PHmVPjc3uidBrAw6sHELrycvPyDknyu-nEDKPT7LXjK-nHPBuHw6PHn1PW9YPWmKmyGoRvRWpA7YTHnKnzkQPWNLsjm1rjNKnE78IyQ_THF6PhNOuyDQPWDdrHTLn10%22%2C%22lego_sid%22%3A%22168604811212840049557069323%22%2C%22platformCompany%22%3A%22ajk%22%2C%22lego_show_track_url_base%22%3A%22https%3A%5C%2F%5C%2Fadtrack.58.com%5C%2F1002359%5C%2F%22%2C%22lego_version%22%3A%221.0.1%22%2C%22legoHuiDu%22%3Atrue%7D%7D",
          "tid": "9e13a8e9-f356-428e-b8da-479b65a01fe1",
          "isLiving": false,
          "isVR": true,
          "distanceDesc": "",
          "isGoldOwner": false
        },
        {
          "infoid": "1959986130050050",
          "cateName": "整租",
          "cateID": "11",
          "title": "市二宫 精装 家私家电齐全 交通方便 超市市场美食一条龙",
          "price": "1480",
          "img": "https://pic1.ajkimg.com/display/anjuke/f7bd7575b47ca8bc15542511aab770d5/240x180c.jpg?t=1&srotate=1",
          "unit": "元/月",
          "coll": "0",
          "subwayDistance": "",
          "fangwuLiangDian": [
            {
              "text": "整租",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "南北",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "近地铁",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "有电梯",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "精装修",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            }
          ],
          "area": "25㎡",
          "toward": "南北",
          "shangquan": "前进路",
          "isAnxuan": "0",
          "location": false,
          "source_type": "1",
          "isauction": "6",
          "auctionShowText": "",
          "legoAuction": "%7B%22isauction%22%3A%226%22%2C%22lego%22%3A%7B%22lego_tid%22%3A%22116ea90a-bfa8-4f17-9cc1-9103c1acb92f%22%2C%22lego_ad_click_url%22%3A%22https%3A%5C%2F%5C%2Flegoclick.58.com%5C%2Fjump%3Ftarget%3DpZwY0ZnlsztdraOWUvYKuadbn19LPjubnaYOm191sHEzm1EVrym3PaYQPyN3ujNvP1cdnHEKnHTQnHnOnWTQTEDQrHNOrH9vnHnknjNknjNkTHTKnHTkTHD_nHDKnkDQPWc1rjEkP1DknHbzTHcKOplFOUA7OplBOlXxOCB4OUa5OlBsOUJ5sXi38Syc-SB6JrpCCXX1B8pEMrXYC9DQTHDKTHnKUvPHPLDkHD-ogLIvibPOINIimYIaI1IgXNPdwEDQPjTdnjbYnHbkPHT3rHEQn1DzTHmK0A7zmyd1n1NYrWcdP1n_0A7zmyd1n1N1rWnvTHDKPhE1uhnkn1mVryc3riYYnAmvsH91rHTVPhDdmW9YuANLuH0OTHDYnjNkrHEQrHTvPHDdPHDLPjEKnHEkPHTOPjDOnjNvP1mvnHNvrEDKnikQnEDKTEDVTEDKTEDdra3zPj98PWm8nHm1THTKnTDKnikQnE7exEDQnjTdPTDQnjTzn1NOTHDQPhR6rHK6syFhmH9VPAmQPzYOmvnQsHbQnjPWny7WmWbzu9DKPT7LXjK-nHPBuHw6PHn1PW9YPWmKmyGoRvRWpA7YTHnKnzkQPWNLsjm1rHDKnE78IyQ_THcYrH0vPjTvnvnzmHIbuWc%22%2C%22lego_sid%22%3A%22168604811212840049557069323%22%2C%22platformCompany%22%3A%22ajk%22%2C%22lego_show_track_url_base%22%3A%22https%3A%5C%2F%5C%2Fadtrack.58.com%5C%2F1002359%5C%2F%22%2C%22lego_version%22%3A%221.0.1%22%2C%22legoHuiDu%22%3Atrue%7D%7D",
          "tid": "116ea90a-bfa8-4f17-9cc1-9103c1acb92f",
          "isLiving": false,
          "isVR": false,
          "distanceDesc": "",
          "isGoldOwner": false
        },
        {
          "infoid": "2034912770647042",
          "cateName": "整租",
          "cateID": "11",
          "title": "出租怡雅苑宝岗大道路段黄金三楼两房一厅精装2700",
          "price": "2700",
          "img": "https://pic1.ajkimg.com/display/anjuke/a0ba6102df557cb40a9a85b99ce06bd2/240x180c.jpg?t=1&srotate=1",
          "unit": "元/月",
          "coll": "0",
          "subwayDistance": "距8号线-凤凰新村站步行1.1km",
          "fangwuLiangDian": [
            {
              "text": "整租",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "西南",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "近地铁",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "精装修",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            }
          ],
          "area": "65㎡",
          "toward": "西南",
          "shangquan": "宝岗",
          "isAnxuan": "0",
          "location": false,
          "source_type": "1",
          "isauction": "6",
          "auctionShowText": "",
          "legoAuction": "%7B%22isauction%22%3A%226%22%2C%22lego%22%3A%7B%22lego_tid%22%3A%22ddd035b7-f22a-483c-8b5a-8af208717910%22%2C%22lego_ad_click_url%22%3A%22https%3A%5C%2F%5C%2Flegoclick.58.com%5C%2Fjump%3Ftarget%3DpZwY0ZnlsztdraOWUvYKuadbn19LPjubnaYOm191sHEzm1EVrym3PaYQPyN3ujNvP1cdnHEKnWTkrjbdrj9LTEDznjnYrHDzP10kPWELnjEzTHTKnHTkTHD_nHDKnkDQPWc1rjEkP1DknHbzTHnKOplFOUA7OplBOlXxOCB4OUa5OlBsOUJ5sXi38Syc-SB6JrpCCXX1B8pEMrXYC9DQTHDKTHnKUvPHPLDkHD-ogLIvibPOINIimYIaI1IgXNPdwEDQPjTdnj0YnW03rj9OPHmvnWT3THmK0A7zmyd1n1NYrWcdP1n_0A7zmyd1n1N1rWnvTHDKP1czmWD3nWTVrHFbmBYYm1NvsycvrAnVn1ndnjRWmWFBry7-THDYnjNkP1EzP1b1nHnQrjm3nH0KnHEkPHTLPjcLrHc1n1EOPHTYnTDKnikQnEDKTEDVTEDKTEDdra3zPj98PWm8nHm1THTKnTDKnikQnE7exEDQnjTdPTDQnjTzn1NOTywbujT1PycLsymznhDVPj91mzY3mWR6sH66uWckrj0QP1bQnTDKPT7LXjK-nHPBuHw6PHn1PW9YPWmKmyGoRvRWpA7YTHnKnzkQPWNLsjcLnj0KnE78IyQ_THTOPhcdPyDQuH7hm1nkuyc%22%2C%22lego_sid%22%3A%22168604811212840049557069323%22%2C%22platformCompany%22%3A%22ajk%22%2C%22lego_show_track_url_base%22%3A%22https%3A%5C%2F%5C%2Fadtrack.58.com%5C%2F1002359%5C%2F%22%2C%22lego_version%22%3A%221.0.1%22%2C%22legoHuiDu%22%3Atrue%7D%7D",
          "tid": "ddd035b7-f22a-483c-8b5a-8af208717910",
          "isLiving": false,
          "isVR": false,
          "distanceDesc": "",
          "isGoldOwner": false
        },
        {
          "infoid": "1879418436723726",
          "cateName": "整租",
          "cateID": "11",
          "title": "全新豪华装修大单间，直租无中介，配置齐全，有阳台",
          "price": "1900",
          "img": "https://pic1.ajkimg.com/display/anjuke/e81323119cf1acf268134c3d761bcd88/240x180c.jpg?t=1&srotate=1",
          "unit": "元/月",
          "coll": "0",
          "subwayDistance": "距8号线-凤凰新村站步行665m",
          "fangwuLiangDian": [
            {
              "text": "整租",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "东南",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "近地铁",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "有电梯",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "豪华装修",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            }
          ],
          "area": "35㎡",
          "toward": "东南",
          "shangquan": "工业大道北",
          "isAnxuan": "0",
          "location": false,
          "source_type": "1",
          "isauction": "6",
          "auctionShowText": "",
          "legoAuction": "%7B%22isauction%22%3A%226%22%2C%22lego%22%3A%7B%22lego_tid%22%3A%222a402941-fa80-48a2-bf05-5087fea241ce%22%2C%22lego_ad_click_url%22%3A%22https%3A%5C%2F%5C%2Flegoclick.58.com%5C%2Fjump%3Ftarget%3DpZwY0ZnlsztdraOWUvYKuadbn19LPjubnaYOm191sHEzm1EVrym3PaYQPyN3ujNvP1cdnHEKnHTQPj0Yn19kTEDQrj0OPjD3PjnvP1c1P1cvTHTKnHTkTHD_nHDKnkDQPWc1rjEkP1DknHbzTHEKOplFOUA7OplBOlXxOCB4OUa5OlBsOUJ5sXi38Syc-SB6JrpCCXX1B8pEMrXYC9DQTHDKTHnKUvPHPLDkHD-ogLIvibPOINIimYIaI1IgXNPdwEDQPjTdnjNznHDYnHmQn10LnW9kTHmK0A7zmyd1n1NYrWcdP1n_0A7zmyd1n1N1rWnvTHDKmhNQn1D3nWTVPj0OraYYnWbLsH9LnjDVujcOmHTOuWbdPyR6THDYnjNkPHcQnHE1nHmdP1TvnWNKnHEkPHTdnWDQPjcYnHTLn1DdnkDKnikQnEDKTEDVTEDKTEDdra3zPj98PWm8nHm1THTKnTDKnikQnE7exEDQnjTdPTDQnjTzn1NOTHF6PjTzrHEQsyu6rjTVPj66nBdBuWTdsHNkrjIhuyDzPj7WuEDKPT7LXjK-nHPBuHw6PHn1PW9YPWmKmyGoRvRWpA7YTHnKnzkQPWNLsjcLnjNKnE78IyQ_THFWuHTYPHRBPhwbuWwBn19%22%2C%22lego_sid%22%3A%22168604811212840049557069323%22%2C%22platformCompany%22%3A%22ajk%22%2C%22lego_show_track_url_base%22%3A%22https%3A%5C%2F%5C%2Fadtrack.58.com%5C%2F1002359%5C%2F%22%2C%22lego_version%22%3A%221.0.1%22%2C%22legoHuiDu%22%3Atrue%7D%7D",
          "tid": "2a402941-fa80-48a2-bf05-5087fea241ce",
          "isLiving": false,
          "isVR": false,
          "distanceDesc": "",
          "isGoldOwner": false
        },
        {
          "infoid": "2038645585550340",
          "cateName": "整租",
          "cateID": "11",
          "title": "江南西 广百新一城  旁一房一厅电梯楼  出租",
          "price": "2400",
          "img": "https://pic1.ajkimg.com/display/anjuke/78fe4793a86064dff3af98e75a72fd23/240x180c.jpg?t=1&srotate=1",
          "unit": "元/月",
          "coll": "0",
          "subwayDistance": "距8号线-凤凰新村站步行885m",
          "fangwuLiangDian": [
            {
              "text": "整租",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "朝南",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "近地铁",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "新上",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "有电梯",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "精装修",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            }
          ],
          "area": "40㎡",
          "toward": "朝南",
          "shangquan": "宝岗",
          "isAnxuan": "0",
          "location": false,
          "source_type": "1",
          "isauction": "6",
          "auctionShowText": "",
          "legoAuction": "%7B%22isauction%22%3A%226%22%2C%22lego%22%3A%7B%22lego_tid%22%3A%22832543f7-e3bc-41c9-b85e-58ad9c03f8e2%22%2C%22lego_ad_click_url%22%3A%22https%3A%5C%2F%5C%2Flegoclick.58.com%5C%2Fjump%3Ftarget%3DpZwY0ZnlsztdraOWUvYKuadbn19LPjubnaYOm191sHEzm1EVrym3PaYQPyN3ujNvP1cdnHEKP1mQPHbzTEDznjn3PWEdPH9dPHNkn1EkTHTKnHTkTHD_nHDKnkDQPWc1rjEkP1DknHbzTHNKOplFOUA7OplBOlXxOCB4OUa5OlBsOUJ5sXi38Syc-SB6JrpCCXX1B8pEMrXYC9DQTHDKTHnKUvPHPLDkHD-ogLIvibPOINIimYIaI1IgXNPdwEDQPjTYrHbdP1mzPjT1PW9knWNLTHmK0A7zmyd1n1NYrWcdP1n_0A7zmyd1n1N1rWnvTHDKn1cOmWb1uWTVnHwbuaYYnjDYsHbLuymVrHDkm1NYm101nH91THDYnjEOrHNLPWcYrHNOPHbkPjTKnHEkPjbOPH0vnWELrHDLP10zrTDKnikQnEDKTEDVTEDKTEDdra3zPj98PWm8nHm1THTKnTDKnikQnE7exEDQnjTdPTDQnjTzn1NOTH91nWNYnvmLsyN1mhnVPj7WridBrjR-sHN3myEOm1T1uW6-n9DKPT7LXjK-nHPBuHw6PHn1PW9YPWmKmyGoRvRWpA7YTHnKnzkQPWNLsjcLnj0KnE78IyQ_THTQnHmzPH6BnjnLnWTLnhN%22%2C%22lego_sid%22%3A%22168604811212840049557069323%22%2C%22platformCompany%22%3A%22ajk%22%2C%22lego_show_track_url_base%22%3A%22https%3A%5C%2F%5C%2Fadtrack.58.com%5C%2F1002359%5C%2F%22%2C%22lego_version%22%3A%221.0.1%22%2C%22legoHuiDu%22%3Atrue%7D%7D",
          "tid": "832543f7-e3bc-41c9-b85e-58ad9c03f8e2",
          "isLiving": false,
          "isVR": false,
          "distanceDesc": "",
          "isGoldOwner": false
        },
        {
          "infoid": "1979846425748481",
          "cateName": "整租",
          "cateID": "11",
          "title": "宝岗大道地铁站旁  两房一厅出租，拎包入住",
          "price": "2200",
          "img": "https://pic1.ajkimg.com/display/anjuke/b35099df0c16601388ef9abe9350020f/240x180c.jpg?t=1&srotate=1",
          "unit": "元/月",
          "coll": "0",
          "subwayDistance": "",
          "fangwuLiangDian": [
            {
              "text": "整租",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "朝南",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "近地铁",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "精装修",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            }
          ],
          "area": "45㎡",
          "toward": "朝南",
          "shangquan": "江南西",
          "isAnxuan": "0",
          "location": false,
          "source_type": "1",
          "isauction": "6",
          "auctionShowText": "",
          "legoAuction": "%7B%22isauction%22%3A%226%22%2C%22lego%22%3A%7B%22lego_tid%22%3A%22db31b46d-5944-49e0-b5bf-d9d0314a1567%22%2C%22lego_ad_click_url%22%3A%22https%3A%5C%2F%5C%2Flegoclick.58.com%5C%2Fjump%3Ftarget%3DpZwY0ZnlsztdraOWUvYKuadbn19LPjubnaYOm191sHEzm1EVrym3PaYQPyN3ujNvP1cdnHEKP1mQPHbzTEDQrH0OrjEvPjcdP1E3Pj9QTHTKnHTkTHD_nHDKnkDQPWc1rjEkP1DknHbzTHmKOplFOUA7OplBOlXxOCB4OUa5OlBsOUJ5sXi38Syc-SB6JrpCCXX1B8pEMrXYC9DQTHDKTHnKUvPHPLDkHD-ogLIvibPOINIimYIaI1IgXNPdwEDQPjTYrHbvnjcLnjcvPHD3njDvTHmK0A7zmyd1n1NYrWcdP1n_0A7zmyd1n1N1rWnvTHDKnAEYuyw6mHNVnH9knBYYrADdsycOmhDVmWu-uHRbmyDYPjEQTHDYnjEOrHmknW0zn1mznWbQnWTKnHEkPjbOPWTzP1TOP19znHD3PTDKnikQnEDKTEDVTEDKTEDdra3zPj98PWm8nHm1THTKnTDKnikQnE7exEDQnjTdPTDQnjTzn1NOTywBn17BPjubsHNOPjEVPj--nadBPyFhsyEOujT1nHw6nHNvPkDKPT7LXjK-nHPBuHw6PHn1PW9YPWmKmyGoRvRWpA7YTHnKnzkQPWNLsj0OP1bKnE78IyQ_THnzPyDkm1cvm1cLm176PW9%22%2C%22lego_sid%22%3A%22168604811212840049557069323%22%2C%22platformCompany%22%3A%22ajk%22%2C%22lego_show_track_url_base%22%3A%22https%3A%5C%2F%5C%2Fadtrack.58.com%5C%2F1002359%5C%2F%22%2C%22lego_version%22%3A%221.0.1%22%2C%22legoHuiDu%22%3Atrue%7D%7D",
          "tid": "db31b46d-5944-49e0-b5bf-d9d0314a1567",
          "isLiving": false,
          "isVR": false,
          "distanceDesc": "",
          "isGoldOwner": false
        },
        {
          "infoid": "2023155042868238",
          "cateName": "整租",
          "cateID": "11",
          "title": "凤凰新村，低楼层，精装两房",
          "price": "3000",
          "img": "https://pic1.ajkimg.com/display/anjuke/6e49f1f9b8f62d28a5c9421262d96646/240x180c.jpg?t=1&srotate=1",
          "unit": "元/月",
          "coll": "0",
          "subwayDistance": "距8号线-凤凰新村站步行1m",
          "fangwuLiangDian": [
            {
              "text": "整租",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "朝南",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "近地铁",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "精装修",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            }
          ],
          "area": "60㎡",
          "toward": "朝南",
          "shangquan": "工业大道北",
          "isAnxuan": "0",
          "location": false,
          "source_type": "1",
          "isauction": "6",
          "auctionShowText": "",
          "legoAuction": "%7B%22isauction%22%3A%226%22%2C%22lego%22%3A%7B%22lego_tid%22%3A%22771aea2b-b71f-4de2-bbdd-bf386ca59797%22%2C%22lego_ad_click_url%22%3A%22https%3A%5C%2F%5C%2Flegoclick.58.com%5C%2Fjump%3Ftarget%3DpZwY0ZnlsztdraOWUvYKuadbn19LPjubnaYOm191sHEzm1EVrym3PaYQPyN3ujNvP1cdnHEKP1mQPHbzTEDznjc1nHNdnjEzrjm3nWn3THTKnHTkTHD_nHDKnkDQPWc1rjEkP1DknHbzTH0KOplFOUA7OplBOlXxOCB4OUa5OlBsOUJ5sXi38Syc-SB6JrpCCXX1B8pEMrXYC9DQTHDKTHnKUvPHPLDkHD-ogLIvibPOINIimYIaI1IgXNPdwEDQPjTYrHbvn1NQrHbzP1bOnWnzTHmK0A7zmyd1n1NYrWcdP1n_0A7zmyd1n1N1rWnvTHDKrHPbPAFhmhEVrHbzniYYmyPhsyDvrAmVuhRWnHF-uW9zuW76THDYnjEOrHm1PHckP1mvrjbYnj9KnHEkPjbOPWndnWTknHDOnHb1P9DKnikQnEDKTEDVTEDKTEDdra3zPj98PWm8nHm1THTKnTDKnikQnE7exEDQnjTdPTDQnjTzn1NOTH0Lny7-mHFBsycLnymVPAw-nBdBmhwbsyFhn19vmvDdrH0OPkDKPT7LXjK-nHPBuHw6PHn1PW9YPWmKmyGoRvRWpA7YTHnKnzkQPWNLsjcLnjNKnE78IyQ_TH66PHm1ujT3ryRhuADYmHc%22%2C%22lego_sid%22%3A%22168604811212840049557069323%22%2C%22platformCompany%22%3A%22ajk%22%2C%22lego_show_track_url_base%22%3A%22https%3A%5C%2F%5C%2Fadtrack.58.com%5C%2F1002359%5C%2F%22%2C%22lego_version%22%3A%221.0.1%22%2C%22legoHuiDu%22%3Atrue%7D%7D",
          "tid": "771aea2b-b71f-4de2-bbdd-bf386ca59797",
          "isLiving": false,
          "isVR": false,
          "distanceDesc": "",
          "isGoldOwner": false
        },
        {
          "infoid": "2037708071115779",
          "cateName": "整租",
          "cateID": "11",
          "title": "出租。全新装修，全新家私家电。拎包入住，可随时看房",
          "price": "5700",
          "img": "https://pic1.ajkimg.com/display/anjuke/3fa8a44a5086719cf8d0e0d1e6c32815/240x180c.jpg?t=1&srotate=1",
          "unit": "元/月",
          "coll": "0",
          "subwayDistance": "",
          "fangwuLiangDian": [
            {
              "text": "整租",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "东南",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "近地铁",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "有电梯",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "精装修",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            }
          ],
          "area": "103.4㎡",
          "toward": "东南",
          "shangquan": "江南西",
          "isAnxuan": "0",
          "location": false,
          "source_type": "1",
          "isauction": "6",
          "auctionShowText": "",
          "legoAuction": "%7B%22isauction%22%3A%226%22%2C%22lego%22%3A%7B%22lego_tid%22%3A%229e99f6f8-2bdc-4cfc-8979-68892c1f006e%22%2C%22lego_ad_click_url%22%3A%22https%3A%5C%2F%5C%2Flegoclick.58.com%5C%2Fjump%3Ftarget%3DpZwY0ZnlsztdraOWUvYKuadbn19LPjubnaYOm191sHEzm1EVrym3PaYQPyN3ujNvP1cdnHEKPHn1nWm3nTDKnWT1P10krjTLnHDQPH0LrEDkTHDknTDQsjDQTHnKnHmzn19Ynj0QnjDOn9DOTXy8BXyQ6Xy8C8XMMSpcfSykWSXCBSylWzLb8sf-BFxCCpWhJ2ZM_3ohbF1MV2cKnEDQTED1TyqWN1IQnDQFpdqLIbGjXgRZNhPZEM0LRL-jINNKnHEkPHT1njmOPWNQPjc1nWnznTDvTgK60h7V01ndPjCzPH01sZK60h7V01ndn1C1P9DQTyPbPWbduhEYsHDdmHTVPjNOnidBujc1sH6hn1EQPAcLPjbYP9DQPjTdnjnkPWbvPWmOPjcQPHm3THDYnjNkn1TvrHmdrjbLnWbLrHnKTHD_nHDKTEDKsEDKTEDKPH98nWE3sWmvsWDvnkDkTHTKTHD_nHDKXLYKnHTkPHEKnHTknWndrEDOuHbOuWuhraYzmhwWsHwWuhnVrjbLriYvrj9OnhnQuWTkPhNKTHEKIL9kuHD1mhNYmHN1n1m3PjmvTy7JpdI-mv66ITD1THn_nHmdPzkLrH0OTHDKUMR_UT76PH6WuWnkrAm3uWbYnWnQ%22%2C%22lego_sid%22%3A%22168604811212840049557069323%22%2C%22platformCompany%22%3A%22ajk%22%2C%22lego_show_track_url_base%22%3A%22https%3A%5C%2F%5C%2Fadtrack.58.com%5C%2F1002359%5C%2F%22%2C%22lego_version%22%3A%221.0.1%22%2C%22legoHuiDu%22%3Atrue%7D%7D",
          "tid": "9e99f6f8-2bdc-4cfc-8979-68892c1f006e",
          "isLiving": false,
          "isVR": false,
          "distanceDesc": "",
          "isGoldOwner": false
        },
        {
          "infoid": "2019078274931723",
          "cateName": "整租",
          "cateID": "11",
          "title": "8号线地铁旁 实拍图 品牌家电 有阳台厨卫 房东租 无仲介",
          "price": "1000",
          "img": "https://pic1.ajkimg.com/display/anjuke/2b31ae701d22c4c4cbd883793707f358/240x180c.jpg?t=1&srotate=1",
          "unit": "元/月",
          "coll": "0",
          "subwayDistance": "距8号线-凤凰新村站步行1m",
          "fangwuLiangDian": [
            {
              "text": "整租",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "朝南",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "近地铁",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "精装修",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            }
          ],
          "area": "30㎡",
          "toward": "朝南",
          "shangquan": "工业大道北",
          "isAnxuan": "0",
          "location": false,
          "source_type": "1",
          "isauction": "6",
          "auctionShowText": "",
          "legoAuction": "%7B%22isauction%22%3A%226%22%2C%22lego%22%3A%7B%22lego_tid%22%3A%221b6e44b5-4511-48f5-9649-0b10c9b36d3a%22%2C%22lego_ad_click_url%22%3A%22https%3A%5C%2F%5C%2Flegoclick.58.com%5C%2Fjump%3Ftarget%3DpZwY0ZnlsztdraOWUvYKuadbn19LPjubnaYOm191sHEzm1EVrym3PaYQPyN3ujNvP1cdnHEKP1DdrjDYPTDKnWTQrHTLrjcLPjb1nH0znkDkTHDknTDQsjDQTHnKnHmzn19Ynj0QnjDOn9DQnTZ-oCM-_mg-oJsMGO4hBs4-_c4MJcf-8CtVOsBeOmBgl2ACOJB6Oer2OGa0OeiBTHDKnEDKnk7fmdnL0HKniyVxILu2EL-dwdFWwYFLPdIOELR7THDYnjELnjE1rHDzPj0vrHbOPWbKP97kmgF6Ugn1PHElnWNLnzQkmgF6Ugn1PHnln1mKnEDQnvmYnju-mid-uWIhsHw-rjbVmyFBrad-PHTYnhn1PHw-nyEKnHEkPj0kPjnOnHEOPHDvrjTknEDQPjTYP1TYn1bQn1DYrjT3rjnzTEDQsjDQTEDKTiYKTEDKTHN3sWcYra3vPB3QPWnKnTDkTEDQsjDQTgVqTHDknjNYTHDknjc1PHbKnycvuHEYmWNVPjNQniYYrAmdsHbvPjbVnAcQnAnOmWnvujP6TEDYTgI3nANQnvF-PADdn1nvrjEvP976phVguyPCmgEKnkD1sjDvPH0_nW0kPEDQTyOdUAkKnW0YnHmLnjKbrAc1rAuWnE%22%2C%22lego_sid%22%3A%22168604811212840049557069323%22%2C%22platformCompany%22%3A%22ajk%22%2C%22lego_show_track_url_base%22%3A%22https%3A%5C%2F%5C%2Fadtrack.58.com%5C%2F1002359%5C%2F%22%2C%22lego_version%22%3A%221.0.1%22%2C%22legoHuiDu%22%3Atrue%7D%7D",
          "tid": "1b6e44b5-4511-48f5-9649-0b10c9b36d3a",
          "isLiving": false,
          "isVR": false,
          "distanceDesc": "",
          "isGoldOwner": false
        },
        {
          "infoid": "2038577566670851",
          "cateName": "整租",
          "cateID": "11",
          "title": "凤凰新村安歆公寓，南北通透安全卫生，拎包入住，欢迎入住",
          "price": "400",
          "img": "https://pic1.ajkimg.com/display/anjuke/b427d9331a84718b5006d160f957b12a/240x180c.jpg?t=1&srotate=1",
          "unit": "元/月",
          "coll": "0",
          "subwayDistance": "距8号线-凤凰新村站步行602m",
          "fangwuLiangDian": [
            {
              "text": "整租",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "南北",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "近地铁",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "新上",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "精装修",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            }
          ],
          "area": "30㎡",
          "toward": "南北",
          "shangquan": "江南西",
          "isAnxuan": "0",
          "location": false,
          "source_type": "1",
          "isauction": "6",
          "auctionShowText": "",
          "legoAuction": "%7B%22isauction%22%3A%226%22%2C%22lego%22%3A%7B%22lego_tid%22%3A%227a0b3fcd-09e4-4471-a220-fef055ede1df%22%2C%22lego_ad_click_url%22%3A%22https%3A%5C%2F%5C%2Flegoclick.58.com%5C%2Fjump%3Ftarget%3DpZwY0ZnlsztdraOWUvYKuadbn19LPjubnaYOm191sHEzm1EVrym3PaYQPyN3ujNvP1cdnHEKnWTkrH0LrjmQTEDznjn3PH0LPHmvPW0krjNQTHTKnHTkTHD_nHDKnkDQPWc1rjEkP1DknHbzTHDQTXy8BXyQ6Xy8C8XMMSpcfSykWSXCBSylWzLb8sf-BFxCCpWhJ2ZM_3ohbF1MV2cKnEDQTED1TyqWN1IQnDQFpdqLIbGjXgRZNhPZEM0LRL-jINNKnHEkPjb3P1bQrHnzn1m1rH9krTDvTgK60h7V01ndPjCzPH01sZK60h7V01ndn1C1P9DQTH6huWbLnHm3sHTkPH0VPjcdmBY3PHm3sHDzPhD1uhnvnvmOnTDQPjTYrH9LrHDOPjN1PWm1nWn1THDYnjEOrj0OnHbYn1cvrj0vnH0KTHD_nHDKTEDKsEDKTEDKPH98nWE3sWmvsWDvnkDkTHTKTHD_nHDKXLYKnHTkPHEKnHTknWndrEDLmHKBnvuWuaYkryNYsHEYP1DVmHcznadhuymkPHR-uANQuAmKTHEKIL9kuHD1mhNYmHN1n1m3PjmvTy7JpdI-mv66ITD1THn_nHmdPzkLrH0OTHDKUMR_UTDvP1c3uHubnvnQnHIbPvmO%22%2C%22lego_sid%22%3A%22168604811212840049557069323%22%2C%22platformCompany%22%3A%22ajk%22%2C%22lego_show_track_url_base%22%3A%22https%3A%5C%2F%5C%2Fadtrack.58.com%5C%2F1002359%5C%2F%22%2C%22lego_version%22%3A%221.0.1%22%2C%22legoHuiDu%22%3Atrue%7D%7D",
          "tid": "7a0b3fcd-09e4-4471-a220-fef055ede1df",
          "isLiving": false,
          "isVR": false,
          "distanceDesc": "",
          "isGoldOwner": false
        },
        {
          "infoid": "2037619263872011",
          "cateName": "整租",
          "cateID": "11",
          "title": "天鹅湾 精装一房 家电齐全 拎包入住",
          "price": "4000",
          "img": "https://pic1.ajkimg.com/display/anjuke/c0f5e5e7b0e6e7197463470bec0cf900/240x180c.jpg?t=1&srotate=1",
          "unit": "元/月",
          "coll": "0",
          "subwayDistance": "距8号线-凤凰新村站步行1.1km",
          "fangwuLiangDian": [
            {
              "text": "整租",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "东南",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "近地铁",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "有电梯",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "精装修",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            }
          ],
          "area": "48㎡",
          "toward": "东南",
          "shangquan": "工业大道北",
          "isAnxuan": "0",
          "location": false,
          "source_type": "1",
          "isauction": "6",
          "auctionShowText": "",
          "legoAuction": "%7B%22isauction%22%3A%226%22%2C%22lego%22%3A%7B%22lego_tid%22%3A%2235cdedc8-a7be-4665-979b-693d29cbc0ff%22%2C%22lego_ad_click_url%22%3A%22https%3A%5C%2F%5C%2Flegoclick.58.com%5C%2Fjump%3Ftarget%3DpZwY0ZnlsztdraOWUvYKuadbn19LPjubnaYOm191sHEzm1EVrym3PaYQPyN3ujNvP1cdnHEKnHTQnHcdnjcYTEDznjnLPWDOnWm1rj0znjDQTHTKnHTkTHD_nHDKnkDQPWc1rjEkP1DknHb1THD1TXy8BXyQ6Xy8C8XMMSpcfSykWSXCBSylWzLb8sf-BFxCCpWhJ2ZM_3ohbF1MV2cKnEDQTED1TyqWN1IQnDQFpdqLIbGjXgRZNhPZEM0LRL-jINNKnHEkPj01njmzrjEYP1bYnjmkrTDvTgK60h7V01ndPjCzPH01sZK60h7V01ndn1C1P9DQTHDQPyDkuHDdsH0vujTVPjNzuaYOnAP-sH6WujRWP1ckrjbYP9DQPjTYP1nkPWc3PWT1nHcdP1mkTHDYnjELn1TvnW9dnWnYn19krjTKTHD_nHDKTEDKsEDKTEDKPH98nWE3sWmvsWDvnkDkTHTKTHD_nHDKXLYKnHTkPHEKnHTknWndrED1PyPbuywWrad6PvF-sHEvPWNVrH0OmBYvrHPbnW-WmhnkuhmKTHEKIL9kuHD1mhNYmHN1n1m3PjmvTy7JpdI-mv66ITD1THn_nHmdPzkzP1TdTHDKUMR_UT7bPH7-Ph7hP1ndujNQPANQ%22%2C%22lego_sid%22%3A%22168604811212840049557069323%22%2C%22platformCompany%22%3A%22ajk%22%2C%22lego_show_track_url_base%22%3A%22https%3A%5C%2F%5C%2Fadtrack.58.com%5C%2F1002359%5C%2F%22%2C%22lego_version%22%3A%221.0.1%22%2C%22legoHuiDu%22%3Atrue%7D%7D",
          "tid": "35cdedc8-a7be-4665-979b-693d29cbc0ff",
          "isLiving": false,
          "isVR": false,
          "distanceDesc": "",
          "isGoldOwner": false
        },
        {
          "infoid": "2029301794512901",
          "cateName": "整租",
          "cateID": "11",
          "title": "实地拍摄 沙园市场附近 近地铁两房一厅 看上业主请人搞卫生",
          "price": "2000",
          "img": "https://pic1.ajkimg.com/display/anjuke/1e929158cb053055027f36bba7cefa05/240x180c.jpg?t=1&srotate=1",
          "unit": "元/月",
          "coll": "0",
          "subwayDistance": "距8号线-凤凰新村站步行1.5km",
          "fangwuLiangDian": [
            {
              "text": "整租",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "朝南",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "近地铁",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "精装修",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            }
          ],
          "area": "58㎡",
          "toward": "朝南",
          "shangquan": "沙园",
          "isAnxuan": "0",
          "location": false,
          "source_type": "1",
          "isauction": "6",
          "auctionShowText": "",
          "legoAuction": "%7B%22isauction%22%3A%226%22%2C%22lego%22%3A%7B%22lego_tid%22%3A%22b1418771-28a1-4e19-a303-2fe734190373%22%2C%22lego_ad_click_url%22%3A%22https%3A%5C%2F%5C%2Flegoclick.58.com%5C%2Fjump%3Ftarget%3DpZwY0ZnlsztdraOWUvYKuadbn19LPjubnaYOm191sHEzm1EVrym3PaYQPyN3ujNvP1cdnHEKnWTkPWbdP1DLTEDznjcOn1TQP1bYPHDzrHTQTHTKnHTkTHD_nHDKnkDQPWc1rjEkP1DknHb1THDYTXy8BXyQ6Xy8C8XMMSpcfSykWSXCBSylWzLb8sf-BFxCCpWhJ2ZM_3ohbF1MV2cKnEDQTED1TyqWN1IQnDQFpdqLIbGjXgRZNhPZEM0LRL-jINNKnHEkPjm3n1DQrjbQnWcznHD3PTDvTgK60h7V01ndPjCzPH01sZK60h7V01ndn1C1P9DQTyn1PWndPj7hsHb3mhnVPAP-naYOujnOsycOmWEvnjRbmyF6PkDQPjTYPW91nHDOn1ndrjEQP1bzTHDYnjEvrjnQnHbzn1b1P1c3njTKTHD_nHDKTEDKsEDKTEDKPH98nWE3sWmvsWDvnkDkTHTKTHD_nHDKXLYKnHTkPHEKnHTknWndrE7BnHEQrj0LniYzrADQsHw-nHbVmHnknzYzuhNLn1EQrHT1P1nKTHEKIL9kuHD1mhNYmHN1n1m3PjmvTy7JpdI-mv66ITD1THn_nHmdPzkvn19OTHDKUMR_UT7bn1NQnycznWF-uhF6uHRW%22%2C%22lego_sid%22%3A%22168604811212840049557069323%22%2C%22platformCompany%22%3A%22ajk%22%2C%22lego_show_track_url_base%22%3A%22https%3A%5C%2F%5C%2Fadtrack.58.com%5C%2F1002359%5C%2F%22%2C%22lego_version%22%3A%221.0.1%22%2C%22legoHuiDu%22%3Atrue%7D%7D",
          "tid": "b1418771-28a1-4e19-a303-2fe734190373",
          "isLiving": false,
          "isVR": false,
          "distanceDesc": "",
          "isGoldOwner": false
        },
        {
          "infoid": "2032889714826250",
          "cateName": "整租",
          "cateID": "11",
          "title": "富力金禧花园 东南向  精装修  拎包入住",
          "price": "6000",
          "img": "https://pic1.ajkimg.com/display/anjuke/3bdea95f6522e9983317f3588616b948/240x180c.jpg?t=1&srotate=1",
          "unit": "元/月",
          "coll": "0",
          "subwayDistance": "距8号线-凤凰新村站步行1.7km",
          "fangwuLiangDian": [
            {
              "text": "整租",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "东南",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "近地铁",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "有电梯",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "精装修",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            }
          ],
          "area": "85㎡",
          "toward": "东南",
          "shangquan": "宝岗",
          "isAnxuan": "0",
          "location": false,
          "source_type": "1",
          "isauction": "6",
          "auctionShowText": "",
          "legoAuction": "%7B%22isauction%22%3A%226%22%2C%22lego%22%3A%7B%22lego_tid%22%3A%222a28708c-ebc3-4618-acc8-be77f28f79e8%22%2C%22lego_ad_click_url%22%3A%22https%3A%5C%2F%5C%2Flegoclick.58.com%5C%2Fjump%3Ftarget%3DpZwY0ZnlsztdraOWUvYKuadbn19LPjubnaYOm191sHEzm1EVrym3PaYQPyN3ujNvP1cdnHEKnWTkPW91n19kTEDznjnzrj9OP1DYrjcvnWNkTHTKnHTkTHD_nHDKnkDQPWc1rjEkP1DknHb1THDdTXy8BXyQ6Xy8C8XMMSpcfSykWSXCBSylWzLb8sf-BFxCCpWhJ2ZM_3ohbF1MV2cKnEDQTED1TyqWN1IQnDQFpdqLIbGjXgRZNhPZEM0LRL-jINNKnHEkPjmvP1n3rH01PHndnjcLn9DvTgK60h7V01ndPjCzPH01sZK60h7V01ndn1C1P9DQTHcvPA76m1bksHu-rjTVPjTOuBYOPhnYsHubPhndnAnvmHEdnkDQPjTYPWmLn19Orj9znHEvrjDvTHDYnjEvPW01rjb3nHNkPjckPjbKTHD_nHDKTEDKsEDKTEDKPH98nWE3sWmvsWDvnkDkTHTKTHD_nHDKXLYKnHTkPHEKnHTknWndrEDzmHc3P1T3mzd-mhn1sHEvnH9VmyPWradBuH0LuWc3uW0OuH9KTHEKIL9kuHD1mhNYmHN1n1m3PjmvTy7JpdI-mv66ITD1THn_nHmdPzkzP1TLTHDKUMR_UTDzrHmvuyNkn1Fbnvn3uWTv%22%2C%22lego_sid%22%3A%22168604811212840049557069323%22%2C%22platformCompany%22%3A%22ajk%22%2C%22lego_show_track_url_base%22%3A%22https%3A%5C%2F%5C%2Fadtrack.58.com%5C%2F1002359%5C%2F%22%2C%22lego_version%22%3A%221.0.1%22%2C%22legoHuiDu%22%3Atrue%7D%7D",
          "tid": "2a28708c-ebc3-4618-acc8-be77f28f79e8",
          "isLiving": false,
          "isVR": false,
          "distanceDesc": "",
          "isGoldOwner": false
        },
        {
          "infoid": "1941530289937418",
          "cateName": "整租",
          "cateID": "11",
          "title": "凤凰新村精装阳台大单间",
          "price": "2400",
          "img": "https://pic1.ajkimg.com/display/anjuke/a97466b844547099bc84970a50e9fe09/240x180c.jpg?t=1&srotate=1",
          "unit": "元/月",
          "coll": "0",
          "subwayDistance": "距8号线-凤凰新村站步行655m",
          "fangwuLiangDian": [
            {
              "text": "整租",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "朝南",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "近地铁",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "精装修",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            }
          ],
          "area": "30㎡",
          "toward": "朝南",
          "shangquan": "工业大道北",
          "isAnxuan": "0",
          "location": false,
          "source_type": "1",
          "isauction": "6",
          "auctionShowText": "",
          "legoAuction": "%7B%22isauction%22%3A%226%22%2C%22lego%22%3A%7B%22lego_tid%22%3A%22415d4196-0bcc-4b84-a7ef-1ebebfde0251%22%2C%22lego_ad_click_url%22%3A%22https%3A%5C%2F%5C%2Flegoclick.58.com%5C%2Fjump%3Ftarget%3DpZwY0ZnlsztdraOWUvYKuadbn19LPjubnaYOm191sHEzm1EVrym3PaYQPyN3ujNvP1cdnHEKnWTkP1czP19kTEDQrHEQPHnknW9OrHnLPjD3THTKnHTkTHD_nHDKnkDQPWc1rjEkP1DknHb1THDvTXy8BXyQ6Xy8C8XMMSpcfSykWSXCBSylWzLb8sf-BFxCCpWhJ2ZM_3ohbF1MV2cKnEDQTED1TyqWN1IQnDQFpdqLIbGjXgRZNhPZEM0LRL-jINNKnHEknjb3Pj0drHmOP1EQPWDOn9DvTgK60h7V01ndPjCzPH01sZK60h7V01ndn1C1P9DQTyR6Pjc3PvNOsHK-uHDVPjKWrad6P1NdsyDzmWRhmH-hPj9LmEDQPjTkrH9YP1NOrjbkn1N3nW0zTHDYnjTOrjELPHb3nHbkPHNQnjEKTHD_nHDKTEDKsEDKTEDKPH98nWE3sWmvsWDvnkDkTHTKTHD_nHDKXLYKnHTkPHEKnHTknWndrEDYnHRbPjDOPBYkmhPWsHwBrjEVmHI-uBYQuyF-mhubuHTzPHDKTHEKIL9kuHD1mhNYmHN1n1m3PjmvTy7JpdI-mv66ITD1THn_nHmdPzkzP1TdTHDKUMR_UT76uj93mW0OPjNQmHbYPhcv%22%2C%22lego_sid%22%3A%22168604811212840049557069323%22%2C%22platformCompany%22%3A%22ajk%22%2C%22lego_show_track_url_base%22%3A%22https%3A%5C%2F%5C%2Fadtrack.58.com%5C%2F1002359%5C%2F%22%2C%22lego_version%22%3A%221.0.1%22%2C%22legoHuiDu%22%3Atrue%7D%7D",
          "tid": "415d4196-0bcc-4b84-a7ef-1ebebfde0251",
          "isLiving": false,
          "isVR": false,
          "distanceDesc": "",
          "isGoldOwner": false
        },
        {
          "infoid": "2028727103104008",
          "cateName": "整租",
          "cateID": "11",
          "title": "沙园商圈！必租超值舒适三房！宽敞透亮 南北通透",
          "price": "4800",
          "img": "https://pic1.ajkimg.com/display/anjuke/50c019fc6b4eccb3a54dfdf2a7b0ef97/240x180c.jpg?t=1&srotate=1",
          "unit": "元/月",
          "coll": "0",
          "subwayDistance": "距8号线-凤凰新村站步行1.3km",
          "fangwuLiangDian": [
            {
              "text": "安选房源",
              "textColor": "#3EC77B",
              "bgColor": "#3EC77B"
            },
            {
              "text": "整租",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "西南",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "近地铁",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "有电梯",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "精装修",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            }
          ],
          "area": "95.5㎡",
          "toward": "西南",
          "shangquan": "工业大道北",
          "isAnxuan": "1",
          "location": false,
          "source_type": "1",
          "isauction": "1",
          "auctionShowText": "",
          "legoAuction": "%7B%22isauction%22%3A%221%22%2C%22lego%22%3A%7B%22lego_tid%22%3A%22%22%2C%22lego_ad_click_url%22%3A%22%22%2C%22lego_sid%22%3A%22168604811212840049557069323%22%2C%22platformCompany%22%3A%22ajk%22%2C%22lego_show_track_url_base%22%3A%22https%3A%5C%2F%5C%2Fadtrack.58.com%5C%2F1001692%5C%2F%22%2C%22lego_version%22%3A%221.0.1%22%2C%22legoHuiDu%22%3Atrue%7D%7D",
          "tid": "",
          "isLiving": false,
          "isVR": false,
          "distanceDesc": "",
          "isGoldOwner": false
        },
        {
          "infoid": "2030517480202242",
          "cateName": "整租",
          "cateID": "11",
          "title": "江南西地铁口电梯两房出租，全新装修，通风采光好近地铁。",
          "price": "4200",
          "img": "https://pic1.ajkimg.com/display/anjuke/4f4de661aa89e713e6867474f5ed3a79/240x180c.jpg?t=1&srotate=1",
          "unit": "元/月",
          "coll": "0",
          "subwayDistance": "距8号线-凤凰新村站步行1.7km",
          "fangwuLiangDian": [
            {
              "text": "安选房源",
              "textColor": "#3EC77B",
              "bgColor": "#3EC77B"
            },
            {
              "text": "整租",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "朝南",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "近地铁",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "有电梯",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "精装修",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            }
          ],
          "area": "95㎡",
          "toward": "朝南",
          "shangquan": "江南西",
          "isAnxuan": "1",
          "location": false,
          "source_type": "1",
          "isauction": "1",
          "auctionShowText": "",
          "legoAuction": "%7B%22isauction%22%3A%221%22%2C%22lego%22%3A%7B%22lego_tid%22%3A%22%22%2C%22lego_ad_click_url%22%3A%22%22%2C%22lego_sid%22%3A%22168604811212840049557069323%22%2C%22platformCompany%22%3A%22ajk%22%2C%22lego_show_track_url_base%22%3A%22https%3A%5C%2F%5C%2Fadtrack.58.com%5C%2F1001692%5C%2F%22%2C%22lego_version%22%3A%221.0.1%22%2C%22legoHuiDu%22%3Atrue%7D%7D",
          "tid": "",
          "isLiving": false,
          "isVR": false,
          "distanceDesc": "",
          "isGoldOwner": false
        },
        {
          "infoid": "2031661882139660",
          "cateName": "整租",
          "cateID": "11",
          "title": "毕业季 学生优惠特价 押一付一 全新公寓平台直租 欢迎咨询",
          "price": "1980",
          "img": "https://pic1.ajkimg.com/display/anjuke/87a4c69b30e0b3e4c0b252a3e28ea81b/240x180c.jpg?t=1&srotate=1",
          "unit": "元/月",
          "coll": "0",
          "subwayDistance": "距8号线-凤凰新村站步行1m",
          "fangwuLiangDian": [
            {
              "text": "安选房源",
              "textColor": "#3EC77B",
              "bgColor": "#3EC77B"
            },
            {
              "text": "整租",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "朝南",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "近地铁",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "精装修",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            }
          ],
          "area": "26㎡",
          "toward": "朝南",
          "shangquan": "工业大道北",
          "isAnxuan": "1",
          "location": false,
          "source_type": "1",
          "isauction": "1",
          "auctionShowText": "",
          "legoAuction": "%7B%22isauction%22%3A%221%22%2C%22lego%22%3A%7B%22lego_tid%22%3A%22%22%2C%22lego_ad_click_url%22%3A%22%22%2C%22lego_sid%22%3A%22168604811212840049557069323%22%2C%22platformCompany%22%3A%22ajk%22%2C%22lego_show_track_url_base%22%3A%22https%3A%5C%2F%5C%2Fadtrack.58.com%5C%2F1001692%5C%2F%22%2C%22lego_version%22%3A%221.0.1%22%2C%22legoHuiDu%22%3Atrue%7D%7D",
          "tid": "",
          "isLiving": false,
          "isVR": false,
          "distanceDesc": "",
          "isGoldOwner": false
        },
        {
          "infoid": "2032959892972554",
          "cateName": "整租",
          "cateID": "11",
          "title": "力荐!南北对流户型方正！近地铁口出行便利，北向望花园景观舒适",
          "price": "4400",
          "img": "https://pic1.ajkimg.com/display/anjuke/1fe3e65dfe7251710ea210222a43e2c9/240x180c.jpg?t=1&srotate=1",
          "unit": "元/月",
          "coll": "0",
          "subwayDistance": "距8号线-凤凰新村站步行1.5km",
          "fangwuLiangDian": [
            {
              "text": "安选房源",
              "textColor": "#3EC77B",
              "bgColor": "#3EC77B"
            },
            {
              "text": "整租",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "朝北",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "近地铁",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "有电梯",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "精装修",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            }
          ],
          "area": "65㎡",
          "toward": "朝北",
          "shangquan": "工业大道北",
          "isAnxuan": "1",
          "location": false,
          "source_type": "1",
          "isauction": "1",
          "auctionShowText": "",
          "legoAuction": "%7B%22isauction%22%3A%221%22%2C%22lego%22%3A%7B%22lego_tid%22%3A%22%22%2C%22lego_ad_click_url%22%3A%22%22%2C%22lego_sid%22%3A%22168604811212840049557069323%22%2C%22platformCompany%22%3A%22ajk%22%2C%22lego_show_track_url_base%22%3A%22https%3A%5C%2F%5C%2Fadtrack.58.com%5C%2F1001692%5C%2F%22%2C%22lego_version%22%3A%221.0.1%22%2C%22legoHuiDu%22%3Atrue%7D%7D",
          "tid": "",
          "isLiving": false,
          "isVR": false,
          "distanceDesc": "",
          "isGoldOwner": false
        },
        {
          "infoid": "2024753690795010",
          "cateName": "整租",
          "cateID": "11",
          "title": "光大花园一期，二房二厅，拎包即住，户型方正",
          "price": "5200",
          "img": "https://pic1.ajkimg.com/display/anjuke/8be252ea460c73ef1bd4b60bfc4ab1ac/240x180c.jpg?t=1&srotate=1",
          "unit": "元/月",
          "coll": "0",
          "subwayDistance": "距8号线-凤凰新村站步行1.2km",
          "fangwuLiangDian": [
            {
              "text": "安选房源",
              "textColor": "#3EC77B",
              "bgColor": "#3EC77B"
            },
            {
              "text": "整租",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "东南",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "近地铁",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "有电梯",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "精装修",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            }
          ],
          "area": "75㎡",
          "toward": "东南",
          "shangquan": "工业大道北",
          "isAnxuan": "1",
          "location": false,
          "source_type": "1",
          "isauction": "1",
          "auctionShowText": "",
          "legoAuction": "%7B%22isauction%22%3A%221%22%2C%22lego%22%3A%7B%22lego_tid%22%3A%22%22%2C%22lego_ad_click_url%22%3A%22%22%2C%22lego_sid%22%3A%22168604811212840049557069323%22%2C%22platformCompany%22%3A%22ajk%22%2C%22lego_show_track_url_base%22%3A%22https%3A%5C%2F%5C%2Fadtrack.58.com%5C%2F1001692%5C%2F%22%2C%22lego_version%22%3A%221.0.1%22%2C%22legoHuiDu%22%3Atrue%7D%7D",
          "tid": "",
          "isLiving": false,
          "isVR": false,
          "distanceDesc": "",
          "isGoldOwner": false
        },
        {
          "infoid": "2024573299696653",
          "cateName": "整租",
          "cateID": "11",
          "title": "光大花园 电梯房 小区管理户型方正实用采光好交通便利近地铁",
          "price": "6300",
          "img": "https://pic1.ajkimg.com/display/anjuke/a9759e2c2e1dab1995ebbbbb0464f6a0/240x180c.jpg?t=1&srotate=1",
          "unit": "元/月",
          "coll": "0",
          "subwayDistance": "距8号线-凤凰新村站步行1.2km",
          "fangwuLiangDian": [
            {
              "text": "安选房源",
              "textColor": "#3EC77B",
              "bgColor": "#3EC77B"
            },
            {
              "text": "整租",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "东南",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "近地铁",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "有电梯",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "精装修",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            }
          ],
          "area": "104㎡",
          "toward": "东南",
          "shangquan": "工业大道北",
          "isAnxuan": "1",
          "location": false,
          "source_type": "1",
          "isauction": "1",
          "auctionShowText": "",
          "legoAuction": "%7B%22isauction%22%3A%221%22%2C%22lego%22%3A%7B%22lego_tid%22%3A%22%22%2C%22lego_ad_click_url%22%3A%22%22%2C%22lego_sid%22%3A%22168604811212840049557069323%22%2C%22platformCompany%22%3A%22ajk%22%2C%22lego_show_track_url_base%22%3A%22https%3A%5C%2F%5C%2Fadtrack.58.com%5C%2F1001692%5C%2F%22%2C%22lego_version%22%3A%221.0.1%22%2C%22legoHuiDu%22%3Atrue%7D%7D",
          "tid": "",
          "isLiving": false,
          "isVR": false,
          "distanceDesc": "",
          "isGoldOwner": false
        },
        {
          "infoid": "2024471280671749",
          "cateName": "整租",
          "cateID": "11",
          "title": "高品质小区 一线江景 高层视野无遮挡 家私齐全 居住舒适",
          "price": "7500",
          "img": "https://pic1.ajkimg.com/display/anjuke/e779bc6fb80d5978f97a6dcd586e7a21/240x180c.jpg?t=1&srotate=1",
          "unit": "元/月",
          "coll": "0",
          "subwayDistance": "距8号线-凤凰新村站步行1.6km",
          "fangwuLiangDian": [
            {
              "text": "安选房源",
              "textColor": "#3EC77B",
              "bgColor": "#3EC77B"
            },
            {
              "text": "整租",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "西北",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "近地铁",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "有电梯",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "精装修",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            }
          ],
          "area": "121㎡",
          "toward": "西北",
          "shangquan": "工业大道北",
          "isAnxuan": "1",
          "location": false,
          "source_type": "1",
          "isauction": "1",
          "auctionShowText": "",
          "legoAuction": "%7B%22isauction%22%3A%221%22%2C%22lego%22%3A%7B%22lego_tid%22%3A%22%22%2C%22lego_ad_click_url%22%3A%22%22%2C%22lego_sid%22%3A%22168604811212840049557069323%22%2C%22platformCompany%22%3A%22ajk%22%2C%22lego_show_track_url_base%22%3A%22https%3A%5C%2F%5C%2Fadtrack.58.com%5C%2F1001692%5C%2F%22%2C%22lego_version%22%3A%221.0.1%22%2C%22legoHuiDu%22%3Atrue%7D%7D",
          "tid": "",
          "isLiving": false,
          "isVR": false,
          "distanceDesc": "",
          "isGoldOwner": false
        },
        {
          "infoid": "2023692403121161",
          "cateName": "整租",
          "cateID": "11",
          "title": "近地铁江南西（正南）初次出租两房（全新全齐）江南西南丰阁急租",
          "price": "4000",
          "img": "https://pic1.ajkimg.com/display/anjuke/8068d3321fac03c732142d692613f2de/240x180c.jpg?t=1&srotate=1",
          "unit": "元/月",
          "coll": "0",
          "subwayDistance": "距8号线-凤凰新村站步行1.9km",
          "fangwuLiangDian": [
            {
              "text": "安选房源",
              "textColor": "#3EC77B",
              "bgColor": "#3EC77B"
            },
            {
              "text": "整租",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "朝南",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "近地铁",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "有电梯",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "精装修",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            }
          ],
          "area": "56㎡",
          "toward": "朝南",
          "shangquan": "江南西",
          "isAnxuan": "1",
          "location": false,
          "source_type": "1",
          "isauction": "1",
          "auctionShowText": "",
          "legoAuction": "%7B%22isauction%22%3A%221%22%2C%22lego%22%3A%7B%22lego_tid%22%3A%22%22%2C%22lego_ad_click_url%22%3A%22%22%2C%22lego_sid%22%3A%22168604811212840049557069323%22%2C%22platformCompany%22%3A%22ajk%22%2C%22lego_show_track_url_base%22%3A%22https%3A%5C%2F%5C%2Fadtrack.58.com%5C%2F1001692%5C%2F%22%2C%22lego_version%22%3A%221.0.1%22%2C%22legoHuiDu%22%3Atrue%7D%7D",
          "tid": "",
          "isLiving": false,
          "isVR": false,
          "distanceDesc": "",
          "isGoldOwner": false
        },
        {
          "infoid": "2023511635125260",
          "cateName": "整租",
          "cateID": "11",
          "title": "南北广场很好的户型，现在是吉屋，居家好选择。",
          "price": "6200",
          "img": "https://pic1.ajkimg.com/display/anjuke/9e9cac2f5f1c190f0843c72cb2527e69/240x180c.jpg?t=1&srotate=1",
          "unit": "元/月",
          "coll": "0",
          "subwayDistance": "距8号线-凤凰新村站步行1.1km",
          "fangwuLiangDian": [
            {
              "text": "安选房源",
              "textColor": "#3EC77B",
              "bgColor": "#3EC77B"
            },
            {
              "text": "整租",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "朝东",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "近地铁",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "有电梯",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            }
          ],
          "area": "136㎡",
          "toward": "朝东",
          "shangquan": "宝岗",
          "isAnxuan": "1",
          "location": false,
          "source_type": "1",
          "isauction": "1",
          "auctionShowText": "",
          "legoAuction": "%7B%22isauction%22%3A%221%22%2C%22lego%22%3A%7B%22lego_tid%22%3A%22%22%2C%22lego_ad_click_url%22%3A%22%22%2C%22lego_sid%22%3A%22168604811212840049557069323%22%2C%22platformCompany%22%3A%22ajk%22%2C%22lego_show_track_url_base%22%3A%22https%3A%5C%2F%5C%2Fadtrack.58.com%5C%2F1001692%5C%2F%22%2C%22lego_version%22%3A%221.0.1%22%2C%22legoHuiDu%22%3Atrue%7D%7D",
          "tid": "",
          "isLiving": false,
          "isVR": false,
          "distanceDesc": "",
          "isGoldOwner": false
        },
        {
          "infoid": "2023001107717127",
          "cateName": "整租",
          "cateID": "11",
          "title": "广佛八号线交汇超笋三房！光大花园精装电梯房 家私家电齐全",
          "price": "6400",
          "img": "https://pic1.ajkimg.com/display/anjuke/6d2214f76413f8ec8bee8c8fc755552a/240x180c.jpg?t=1&srotate=1",
          "unit": "元/月",
          "coll": "0",
          "subwayDistance": "距8号线-凤凰新村站步行1.2km",
          "fangwuLiangDian": [
            {
              "text": "安选房源",
              "textColor": "#3EC77B",
              "bgColor": "#3EC77B"
            },
            {
              "text": "整租",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "朝南",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "近地铁",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "有电梯",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "精装修",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            }
          ],
          "area": "121㎡",
          "toward": "朝南",
          "shangquan": "工业大道北",
          "isAnxuan": "1",
          "location": false,
          "source_type": "1",
          "isauction": "1",
          "auctionShowText": "",
          "legoAuction": "%7B%22isauction%22%3A%221%22%2C%22lego%22%3A%7B%22lego_tid%22%3A%22%22%2C%22lego_ad_click_url%22%3A%22%22%2C%22lego_sid%22%3A%22168604811212840049557069323%22%2C%22platformCompany%22%3A%22ajk%22%2C%22lego_show_track_url_base%22%3A%22https%3A%5C%2F%5C%2Fadtrack.58.com%5C%2F1001692%5C%2F%22%2C%22lego_version%22%3A%221.0.1%22%2C%22legoHuiDu%22%3Atrue%7D%7D",
          "tid": "",
          "isLiving": false,
          "isVR": false,
          "distanceDesc": "",
          "isGoldOwner": false
        },
        {
          "infoid": "2020815174885389",
          "cateName": "整租",
          "cateID": "11",
          "title": "南向三房带主套，诚信出租，看中价格可议，临近乐峰商场！笋！！",
          "price": "6000",
          "img": "https://pic1.ajkimg.com/display/anjuke/beb3744d85aedfac945f10e7eb9eec68/240x180c.jpg?t=1&srotate=1",
          "unit": "元/月",
          "coll": "0",
          "subwayDistance": "距8号线-凤凰新村站步行1.2km",
          "fangwuLiangDian": [
            {
              "text": "安选房源",
              "textColor": "#3EC77B",
              "bgColor": "#3EC77B"
            },
            {
              "text": "整租",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "朝南",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "近地铁",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "有电梯",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "精装修",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            }
          ],
          "area": "105㎡",
          "toward": "朝南",
          "shangquan": "工业大道北",
          "isAnxuan": "1",
          "location": false,
          "source_type": "1",
          "isauction": "1",
          "auctionShowText": "",
          "legoAuction": "%7B%22isauction%22%3A%221%22%2C%22lego%22%3A%7B%22lego_tid%22%3A%22%22%2C%22lego_ad_click_url%22%3A%22%22%2C%22lego_sid%22%3A%22168604811212840049557069323%22%2C%22platformCompany%22%3A%22ajk%22%2C%22lego_show_track_url_base%22%3A%22https%3A%5C%2F%5C%2Fadtrack.58.com%5C%2F1001692%5C%2F%22%2C%22lego_version%22%3A%221.0.1%22%2C%22legoHuiDu%22%3Atrue%7D%7D",
          "tid": "",
          "isLiving": false,
          "isVR": false,
          "distanceDesc": "",
          "isGoldOwner": false
        },
        {
          "infoid": "2020810408666127",
          "cateName": "整租",
          "cateID": "11",
          "title": "光大精装修三房出租，业主好说话，临近地铁站，大型商圈！！！！",
          "price": "6400",
          "img": "https://pic1.ajkimg.com/display/anjuke/0de16a830f0e90ba219648a5f4f0cb3d/240x180c.jpg?t=1&srotate=1",
          "unit": "元/月",
          "coll": "0",
          "subwayDistance": "距8号线-凤凰新村站步行1.2km",
          "fangwuLiangDian": [
            {
              "text": "安选房源",
              "textColor": "#3EC77B",
              "bgColor": "#3EC77B"
            },
            {
              "text": "整租",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "西南",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "近地铁",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "有电梯",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "精装修",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            }
          ],
          "area": "95㎡",
          "toward": "西南",
          "shangquan": "工业大道北",
          "isAnxuan": "1",
          "location": false,
          "source_type": "1",
          "isauction": "1",
          "auctionShowText": "",
          "legoAuction": "%7B%22isauction%22%3A%221%22%2C%22lego%22%3A%7B%22lego_tid%22%3A%22%22%2C%22lego_ad_click_url%22%3A%22%22%2C%22lego_sid%22%3A%22168604811212840049557069323%22%2C%22platformCompany%22%3A%22ajk%22%2C%22lego_show_track_url_base%22%3A%22https%3A%5C%2F%5C%2Fadtrack.58.com%5C%2F1001692%5C%2F%22%2C%22lego_version%22%3A%221.0.1%22%2C%22legoHuiDu%22%3Atrue%7D%7D",
          "tid": "",
          "isLiving": false,
          "isVR": false,
          "distanceDesc": "",
          "isGoldOwner": false
        },
        {
          "infoid": "2020664190541830",
          "cateName": "整租",
          "cateID": "11",
          "title": "南北广场 2室2厅1卫 4000元月 配套齐全 电梯房",
          "price": "4000",
          "img": "https://pic1.ajkimg.com/display/anjuke/c96b670b1c4f43b5fcaf464e6d37c44b/240x180c.jpg?t=1&srotate=1",
          "unit": "元/月",
          "coll": "0",
          "subwayDistance": "距8号线-凤凰新村站步行1.1km",
          "fangwuLiangDian": [
            {
              "text": "安选房源",
              "textColor": "#3EC77B",
              "bgColor": "#3EC77B"
            },
            {
              "text": "整租",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "朝西",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "近地铁",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "有电梯",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            }
          ],
          "area": "70㎡",
          "toward": "朝西",
          "shangquan": "宝岗",
          "isAnxuan": "1",
          "location": false,
          "source_type": "1",
          "isauction": "1",
          "auctionShowText": "",
          "legoAuction": "%7B%22isauction%22%3A%221%22%2C%22lego%22%3A%7B%22lego_tid%22%3A%22%22%2C%22lego_ad_click_url%22%3A%22%22%2C%22lego_sid%22%3A%22168604811212840049557069323%22%2C%22platformCompany%22%3A%22ajk%22%2C%22lego_show_track_url_base%22%3A%22https%3A%5C%2F%5C%2Fadtrack.58.com%5C%2F1001692%5C%2F%22%2C%22lego_version%22%3A%221.0.1%22%2C%22legoHuiDu%22%3Atrue%7D%7D",
          "tid": "",
          "isLiving": false,
          "isVR": false,
          "distanceDesc": "",
          "isGoldOwner": false
        },
        {
          "infoid": "2020647143087111",
          "cateName": "整租",
          "cateID": "11",
          "title": "力荐租房！精装拎包入住！一线江景视野无遮挡！近地铁口！",
          "price": "4500",
          "img": "https://pic1.ajkimg.com/display/anjuke/ac49a92566422f1a6c41565860cdd3c2/240x180c.jpg?t=1&srotate=1",
          "unit": "元/月",
          "coll": "0",
          "subwayDistance": "距8号线-凤凰新村站步行1.6km",
          "fangwuLiangDian": [
            {
              "text": "安选房源",
              "textColor": "#3EC77B",
              "bgColor": "#3EC77B"
            },
            {
              "text": "整租",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "西北",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "近地铁",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "有电梯",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "精装修",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            }
          ],
          "area": "120㎡",
          "toward": "西北",
          "shangquan": "工业大道北",
          "isAnxuan": "1",
          "location": false,
          "source_type": "1",
          "isauction": "1",
          "auctionShowText": "",
          "legoAuction": "%7B%22isauction%22%3A%221%22%2C%22lego%22%3A%7B%22lego_tid%22%3A%22%22%2C%22lego_ad_click_url%22%3A%22%22%2C%22lego_sid%22%3A%22168604811212840049557069323%22%2C%22platformCompany%22%3A%22ajk%22%2C%22lego_show_track_url_base%22%3A%22https%3A%5C%2F%5C%2Fadtrack.58.com%5C%2F1001692%5C%2F%22%2C%22lego_version%22%3A%221.0.1%22%2C%22legoHuiDu%22%3Atrue%7D%7D",
          "tid": "",
          "isLiving": false,
          "isVR": false,
          "distanceDesc": "",
          "isGoldOwner": false
        },
        {
          "infoid": "2034692034749454",
          "cateName": "整租",
          "cateID": "11",
          "title": "洪德雅苑 2室2厅1卫 3000元月 73平 南北通透",
          "price": "3000",
          "img": "https://pic1.ajkimg.com/display/anjuke/aaefc59bf484f6c078feb7af0eeca11f/240x180c.jpg?t=1&srotate=1",
          "unit": "元/月",
          "coll": "0",
          "subwayDistance": "距8号线-凤凰新村站步行636m",
          "fangwuLiangDian": [
            {
              "text": "安选房源",
              "textColor": "#3EC77B",
              "bgColor": "#3EC77B"
            },
            {
              "text": "整租",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "东南",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "近地铁",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "精装修",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            }
          ],
          "area": "73㎡",
          "toward": "东南",
          "shangquan": "工业大道北",
          "isAnxuan": "1",
          "location": false,
          "source_type": "1",
          "isauction": "1",
          "auctionShowText": "",
          "legoAuction": "%7B%22isauction%22%3A%221%22%2C%22lego%22%3A%7B%22lego_tid%22%3A%22%22%2C%22lego_ad_click_url%22%3A%22%22%2C%22lego_sid%22%3A%22168604811212840049557069323%22%2C%22platformCompany%22%3A%22ajk%22%2C%22lego_show_track_url_base%22%3A%22https%3A%5C%2F%5C%2Fadtrack.58.com%5C%2F1001692%5C%2F%22%2C%22lego_version%22%3A%221.0.1%22%2C%22legoHuiDu%22%3Atrue%7D%7D",
          "tid": "",
          "isLiving": false,
          "isVR": false,
          "distanceDesc": "",
          "isGoldOwner": false
        },
        {
          "infoid": "2030161156054018",
          "cateName": "整租",
          "cateID": "11",
          "title": "金碧湾 新上两房 室内整洁家具家电齐全 有钥匙随时可看",
          "price": "5000",
          "img": "https://pic1.ajkimg.com/display/anjuke/b8ff79a3d40d7f0233502b06c4f64714/240x180c.jpg?t=1&srotate=1",
          "unit": "元/月",
          "coll": "0",
          "subwayDistance": "距8号线-凤凰新村站步行751m",
          "fangwuLiangDian": [
            {
              "text": "安选房源",
              "textColor": "#3EC77B",
              "bgColor": "#3EC77B"
            },
            {
              "text": "整租",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "西南",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "近地铁",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "有电梯",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "精装修",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            }
          ],
          "area": "65㎡",
          "toward": "西南",
          "shangquan": "工业大道北",
          "isAnxuan": "1",
          "location": false,
          "source_type": "1",
          "isauction": "1",
          "auctionShowText": "",
          "legoAuction": "%7B%22isauction%22%3A%221%22%2C%22lego%22%3A%7B%22lego_tid%22%3A%22%22%2C%22lego_ad_click_url%22%3A%22%22%2C%22lego_sid%22%3A%22168604811212840049557069323%22%2C%22platformCompany%22%3A%22ajk%22%2C%22lego_show_track_url_base%22%3A%22https%3A%5C%2F%5C%2Fadtrack.58.com%5C%2F1001692%5C%2F%22%2C%22lego_version%22%3A%221.0.1%22%2C%22legoHuiDu%22%3Atrue%7D%7D",
          "tid": "",
          "isLiving": false,
          "isVR": false,
          "distanceDesc": "",
          "isGoldOwner": false
        },
        {
          "infoid": "2027632652427270",
          "cateName": "整租",
          "cateID": "11",
          "title": "天鹅湾 望江精装3房2卫 拎包入住 非常漂亮  租8200月",
          "price": "8200",
          "img": "https://pic1.ajkimg.com/display/anjuke/a22998814c37343a20f43bb5a6e33376/240x180c.jpg?t=1&srotate=1",
          "unit": "元/月",
          "coll": "0",
          "subwayDistance": "距8号线-凤凰新村站步行1.1km",
          "fangwuLiangDian": [
            {
              "text": "安选房源",
              "textColor": "#3EC77B",
              "bgColor": "#3EC77B"
            },
            {
              "text": "整租",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "朝南",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "近地铁",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "有电梯",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "精装修",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            }
          ],
          "area": "141.8㎡",
          "toward": "朝南",
          "shangquan": "工业大道北",
          "isAnxuan": "1",
          "location": false,
          "source_type": "1",
          "isauction": "1",
          "auctionShowText": "",
          "legoAuction": "%7B%22isauction%22%3A%221%22%2C%22lego%22%3A%7B%22lego_tid%22%3A%22%22%2C%22lego_ad_click_url%22%3A%22%22%2C%22lego_sid%22%3A%22168604811212840049557069323%22%2C%22platformCompany%22%3A%22ajk%22%2C%22lego_show_track_url_base%22%3A%22https%3A%5C%2F%5C%2Fadtrack.58.com%5C%2F1001692%5C%2F%22%2C%22lego_version%22%3A%221.0.1%22%2C%22legoHuiDu%22%3Atrue%7D%7D",
          "tid": "",
          "isLiving": false,
          "isVR": false,
          "distanceDesc": "",
          "isGoldOwner": false
        },
        {
          "infoid": "2026537580602374",
          "cateName": "整租",
          "cateID": "11",
          "title": "光大花园一期推荐两房 花园小区 环境优美 保养好拎包即可入住",
          "price": "5000",
          "img": "https://pic1.ajkimg.com/display/anjuke/68bff0b0dbedc0fdc29945027af1b82d/240x180c.jpg?t=1&srotate=1",
          "unit": "元/月",
          "coll": "0",
          "subwayDistance": "距8号线-凤凰新村站步行1.2km",
          "fangwuLiangDian": [
            {
              "text": "安选房源",
              "textColor": "#3EC77B",
              "bgColor": "#3EC77B"
            },
            {
              "text": "整租",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "朝南",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "近地铁",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "有电梯",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            }
          ],
          "area": "63㎡",
          "toward": "朝南",
          "shangquan": "工业大道北",
          "isAnxuan": "1",
          "location": false,
          "source_type": "1",
          "isauction": "1",
          "auctionShowText": "",
          "legoAuction": "%7B%22isauction%22%3A%221%22%2C%22lego%22%3A%7B%22lego_tid%22%3A%22%22%2C%22lego_ad_click_url%22%3A%22%22%2C%22lego_sid%22%3A%22168604811212840049557069323%22%2C%22platformCompany%22%3A%22ajk%22%2C%22lego_show_track_url_base%22%3A%22https%3A%5C%2F%5C%2Fadtrack.58.com%5C%2F1001692%5C%2F%22%2C%22lego_version%22%3A%221.0.1%22%2C%22legoHuiDu%22%3Atrue%7D%7D",
          "tid": "",
          "isLiving": false,
          "isVR": false,
          "distanceDesc": "",
          "isGoldOwner": false
        },
        {
          "infoid": "2023413612848132",
          "cateName": "整租",
          "cateID": "11",
          "title": "沙园地铁站旁高层电梯精装三房拎包入住乐峰购物广场",
          "price": "7500",
          "img": "https://pic1.ajkimg.com/display/anjuke/3a0a3ba57882c2bca775bf687170d8ec/240x180c.jpg?t=1&srotate=1",
          "unit": "元/月",
          "coll": "0",
          "subwayDistance": "距8号线-凤凰新村站步行1.6km",
          "fangwuLiangDian": [
            {
              "text": "安选房源",
              "textColor": "#3EC77B",
              "bgColor": "#3EC77B"
            },
            {
              "text": "整租",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "西北",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "近地铁",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "有电梯",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "精装修",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            }
          ],
          "area": "122㎡",
          "toward": "西北",
          "shangquan": "工业大道北",
          "isAnxuan": "1",
          "location": false,
          "source_type": "1",
          "isauction": "1",
          "auctionShowText": "",
          "legoAuction": "%7B%22isauction%22%3A%221%22%2C%22lego%22%3A%7B%22lego_tid%22%3A%22%22%2C%22lego_ad_click_url%22%3A%22%22%2C%22lego_sid%22%3A%22168604811212840049557069323%22%2C%22platformCompany%22%3A%22ajk%22%2C%22lego_show_track_url_base%22%3A%22https%3A%5C%2F%5C%2Fadtrack.58.com%5C%2F1001692%5C%2F%22%2C%22lego_version%22%3A%221.0.1%22%2C%22legoHuiDu%22%3Atrue%7D%7D",
          "tid": "",
          "isLiving": false,
          "isVR": false,
          "distanceDesc": "",
          "isGoldOwner": false
        },
        {
          "infoid": "2020664211627008",
          "cateName": "整租",
          "cateID": "11",
          "title": "江南花园 2室1厅1卫 4500元月 电梯房 72平",
          "price": "4500",
          "img": "https://pic1.ajkimg.com/display/anjuke/05ea3750858fc35c9d2e8c12998b3da7/240x180c.jpg?t=1&srotate=1",
          "unit": "元/月",
          "coll": "0",
          "subwayDistance": "距8号线-凤凰新村站步行1.6km",
          "fangwuLiangDian": [
            {
              "text": "安选房源",
              "textColor": "#3EC77B",
              "bgColor": "#3EC77B"
            },
            {
              "text": "整租",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "东南",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "近地铁",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "有电梯",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "精装修",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            }
          ],
          "area": "72㎡",
          "toward": "东南",
          "shangquan": "宝岗",
          "isAnxuan": "1",
          "location": false,
          "source_type": "1",
          "isauction": "1",
          "auctionShowText": "",
          "legoAuction": "%7B%22isauction%22%3A%221%22%2C%22lego%22%3A%7B%22lego_tid%22%3A%22%22%2C%22lego_ad_click_url%22%3A%22%22%2C%22lego_sid%22%3A%22168604811212840049557069323%22%2C%22platformCompany%22%3A%22ajk%22%2C%22lego_show_track_url_base%22%3A%22https%3A%5C%2F%5C%2Fadtrack.58.com%5C%2F1001692%5C%2F%22%2C%22lego_version%22%3A%221.0.1%22%2C%22legoHuiDu%22%3Atrue%7D%7D",
          "tid": "",
          "isLiving": false,
          "isVR": false,
          "distanceDesc": "",
          "isGoldOwner": false
        },
        {
          "infoid": "2023696156140552",
          "cateName": "整租",
          "cateID": "11",
          "title": "8号线广佛线急租！家私齐全 装修干净 有钥匙 随时约看",
          "price": "4900",
          "img": "https://pic1.ajkimg.com/display/anjuke/cdb189e53854e433fae0f65c865b0e8b/240x180c.jpg?t=1&srotate=1",
          "unit": "元/月",
          "coll": "0",
          "subwayDistance": "距8号线-凤凰新村站步行1.3km",
          "fangwuLiangDian": [
            {
              "text": "安选房源",
              "textColor": "#3EC77B",
              "bgColor": "#3EC77B"
            },
            {
              "text": "整租",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "东南",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "近地铁",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "有电梯",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "精装修",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            }
          ],
          "area": "96㎡",
          "toward": "东南",
          "shangquan": "工业大道北",
          "isAnxuan": "1",
          "location": false,
          "source_type": "1",
          "isauction": "1",
          "auctionShowText": "",
          "legoAuction": "%7B%22isauction%22%3A%221%22%2C%22lego%22%3A%7B%22lego_tid%22%3A%22%22%2C%22lego_ad_click_url%22%3A%22%22%2C%22lego_sid%22%3A%22168604811212840049557069323%22%2C%22platformCompany%22%3A%22ajk%22%2C%22lego_show_track_url_base%22%3A%22https%3A%5C%2F%5C%2Fadtrack.58.com%5C%2F1001692%5C%2F%22%2C%22lego_version%22%3A%221.0.1%22%2C%22legoHuiDu%22%3Atrue%7D%7D",
          "tid": "",
          "isLiving": false,
          "isVR": false,
          "distanceDesc": "",
          "isGoldOwner": false
        },
        {
          "infoid": "2032956527729675",
          "cateName": "整租",
          "cateID": "11",
          "title": "2号线江南西地铁口 家私电齐全拎包入住 精致两房看房方便！！",
          "price": "2800",
          "img": "https://pic1.ajkimg.com/display/anjuke/86d468c181edc7ae7356fb090d1300ae/240x180c.jpg?t=1&srotate=1",
          "unit": "元/月",
          "coll": "0",
          "subwayDistance": "距8号线-凤凰新村站步行1.8km",
          "fangwuLiangDian": [
            {
              "text": "安选房源",
              "textColor": "#3EC77B",
              "bgColor": "#3EC77B"
            },
            {
              "text": "整租",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "朝东",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "近地铁",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "有电梯",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "精装修",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            }
          ],
          "area": "53㎡",
          "toward": "朝东",
          "shangquan": "江南西",
          "isAnxuan": "1",
          "location": false,
          "source_type": "1",
          "isauction": "1",
          "auctionShowText": "",
          "legoAuction": "%7B%22isauction%22%3A%221%22%2C%22lego%22%3A%7B%22lego_tid%22%3A%22%22%2C%22lego_ad_click_url%22%3A%22%22%2C%22lego_sid%22%3A%22168604811212840049557069323%22%2C%22platformCompany%22%3A%22ajk%22%2C%22lego_show_track_url_base%22%3A%22https%3A%5C%2F%5C%2Fadtrack.58.com%5C%2F1001692%5C%2F%22%2C%22lego_version%22%3A%221.0.1%22%2C%22legoHuiDu%22%3Atrue%7D%7D",
          "tid": "",
          "isLiving": false,
          "isVR": false,
          "distanceDesc": "",
          "isGoldOwner": false
        },
        {
          "infoid": "2032930422218760",
          "cateName": "整租",
          "cateID": "11",
          "title": "全齐正规一房（精装修）近地铁8号线（配套成熟）近宝岗江南西",
          "price": "2400",
          "img": "https://pic1.ajkimg.com/display/anjuke/53cfd3e715198bfac5d9d9e92fd7e5f5/240x180c.jpg?t=1&srotate=1",
          "unit": "元/月",
          "coll": "0",
          "subwayDistance": "距8号线-凤凰新村站步行957m",
          "fangwuLiangDian": [
            {
              "text": "安选房源",
              "textColor": "#3EC77B",
              "bgColor": "#3EC77B"
            },
            {
              "text": "整租",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "朝南",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "近地铁",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "有电梯",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "精装修",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            }
          ],
          "area": "25㎡",
          "toward": "朝南",
          "shangquan": "宝岗",
          "isAnxuan": "1",
          "location": false,
          "source_type": "1",
          "isauction": "1",
          "auctionShowText": "",
          "legoAuction": "%7B%22isauction%22%3A%221%22%2C%22lego%22%3A%7B%22lego_tid%22%3A%22%22%2C%22lego_ad_click_url%22%3A%22%22%2C%22lego_sid%22%3A%22168604811212840049557069323%22%2C%22platformCompany%22%3A%22ajk%22%2C%22lego_show_track_url_base%22%3A%22https%3A%5C%2F%5C%2Fadtrack.58.com%5C%2F1001692%5C%2F%22%2C%22lego_version%22%3A%221.0.1%22%2C%22legoHuiDu%22%3Atrue%7D%7D",
          "tid": "",
          "isLiving": false,
          "isVR": false,
          "distanceDesc": "",
          "isGoldOwner": false
        },
        {
          "infoid": "2033004655059983",
          "cateName": "整租",
          "cateID": "11",
          "title": "江南西 中层两房 间隔方正 厅大房大 可拎包入住",
          "price": "3300",
          "img": "https://pic1.ajkimg.com/display/anjuke/5fab33b4a2239082cd977536b038ce65/240x180c.jpg?t=1&srotate=1",
          "unit": "元/月",
          "coll": "0",
          "subwayDistance": "距8号线-凤凰新村站步行1.8km",
          "fangwuLiangDian": [
            {
              "text": "安选房源",
              "textColor": "#3EC77B",
              "bgColor": "#3EC77B"
            },
            {
              "text": "整租",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "朝东",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "近地铁",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            }
          ],
          "area": "74㎡",
          "toward": "朝东",
          "shangquan": "江南西",
          "isAnxuan": "1",
          "location": false,
          "source_type": "1",
          "isauction": "1",
          "auctionShowText": "",
          "legoAuction": "%7B%22isauction%22%3A%221%22%2C%22lego%22%3A%7B%22lego_tid%22%3A%22%22%2C%22lego_ad_click_url%22%3A%22%22%2C%22lego_sid%22%3A%22168604811212840049557069323%22%2C%22platformCompany%22%3A%22ajk%22%2C%22lego_show_track_url_base%22%3A%22https%3A%5C%2F%5C%2Fadtrack.58.com%5C%2F1001692%5C%2F%22%2C%22lego_version%22%3A%221.0.1%22%2C%22legoHuiDu%22%3Atrue%7D%7D",
          "tid": "",
          "isLiving": false,
          "isVR": false,
          "distanceDesc": "",
          "isGoldOwner": false
        },
        {
          "infoid": "2028700589022216",
          "cateName": "整租",
          "cateID": "11",
          "title": "沙园地铁口三房急租 原装电梯楼龄新 南北通透采光好 拎包入住",
          "price": "4780",
          "img": "https://pic1.ajkimg.com/display/anjuke/52bee49f719598ab7379316d7264a296/240x180c.jpg?t=1&srotate=1",
          "unit": "元/月",
          "coll": "0",
          "subwayDistance": "距8号线-凤凰新村站步行1.3km",
          "fangwuLiangDian": [
            {
              "text": "安选房源",
              "textColor": "#3EC77B",
              "bgColor": "#3EC77B"
            },
            {
              "text": "整租",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "西南",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "近地铁",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "有电梯",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "精装修",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            }
          ],
          "area": "88㎡",
          "toward": "西南",
          "shangquan": "工业大道北",
          "isAnxuan": "1",
          "location": false,
          "source_type": "1",
          "isauction": "1",
          "auctionShowText": "",
          "legoAuction": "%7B%22isauction%22%3A%221%22%2C%22lego%22%3A%7B%22lego_tid%22%3A%22%22%2C%22lego_ad_click_url%22%3A%22%22%2C%22lego_sid%22%3A%22168604811212840049557069323%22%2C%22platformCompany%22%3A%22ajk%22%2C%22lego_show_track_url_base%22%3A%22https%3A%5C%2F%5C%2Fadtrack.58.com%5C%2F1001692%5C%2F%22%2C%22lego_version%22%3A%221.0.1%22%2C%22legoHuiDu%22%3Atrue%7D%7D",
          "tid": "",
          "isLiving": false,
          "isVR": false,
          "distanceDesc": "",
          "isGoldOwner": false
        },
        {
          "infoid": "2026328927002638",
          "cateName": "整租",
          "cateID": "11",
          "title": "天鹅湾 一线江景 诚心出租复式三房南北通透光线充足温馨舒适",
          "price": "8200",
          "img": "https://pic1.ajkimg.com/display/anjuke/bbb20ca10b6e59669185f6e8faaa8c5a/240x180c.jpg?t=1&srotate=1",
          "unit": "元/月",
          "coll": "0",
          "subwayDistance": "距8号线-凤凰新村站步行1.1km",
          "fangwuLiangDian": [
            {
              "text": "安选房源",
              "textColor": "#3EC77B",
              "bgColor": "#3EC77B"
            },
            {
              "text": "整租",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "西北",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "近地铁",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "有电梯",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            }
          ],
          "area": "145㎡",
          "toward": "西北",
          "shangquan": "工业大道北",
          "isAnxuan": "1",
          "location": false,
          "source_type": "1",
          "isauction": "1",
          "auctionShowText": "",
          "legoAuction": "%7B%22isauction%22%3A%221%22%2C%22lego%22%3A%7B%22lego_tid%22%3A%22%22%2C%22lego_ad_click_url%22%3A%22%22%2C%22lego_sid%22%3A%22168604811212840049557069323%22%2C%22platformCompany%22%3A%22ajk%22%2C%22lego_show_track_url_base%22%3A%22https%3A%5C%2F%5C%2Fadtrack.58.com%5C%2F1001692%5C%2F%22%2C%22lego_version%22%3A%221.0.1%22%2C%22legoHuiDu%22%3Atrue%7D%7D",
          "tid": "",
          "isLiving": false,
          "isVR": false,
          "distanceDesc": "",
          "isGoldOwner": false
        },
        {
          "infoid": "2025952329686025",
          "cateName": "整租",
          "cateID": "11",
          "title": "精装三房进地铁太古仓上下班方便",
          "price": "5800",
          "img": "https://pic1.ajkimg.com/display/anjuke/ddd5d5fa5139cd20e07c8f4efd722313/240x180c.jpg?t=1&srotate=1",
          "unit": "元/月",
          "coll": "0",
          "subwayDistance": "距8号线-凤凰新村站步行1.2km",
          "fangwuLiangDian": [
            {
              "text": "安选房源",
              "textColor": "#3EC77B",
              "bgColor": "#3EC77B"
            },
            {
              "text": "整租",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "朝南",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "近地铁",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "有电梯",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "精装修",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            }
          ],
          "area": "103㎡",
          "toward": "朝南",
          "shangquan": "工业大道北",
          "isAnxuan": "1",
          "location": false,
          "source_type": "1",
          "isauction": "1",
          "auctionShowText": "",
          "legoAuction": "%7B%22isauction%22%3A%221%22%2C%22lego%22%3A%7B%22lego_tid%22%3A%22%22%2C%22lego_ad_click_url%22%3A%22%22%2C%22lego_sid%22%3A%22168604811212840049557069323%22%2C%22platformCompany%22%3A%22ajk%22%2C%22lego_show_track_url_base%22%3A%22https%3A%5C%2F%5C%2Fadtrack.58.com%5C%2F1001692%5C%2F%22%2C%22lego_version%22%3A%221.0.1%22%2C%22legoHuiDu%22%3Atrue%7D%7D",
          "tid": "",
          "isLiving": false,
          "isVR": false,
          "distanceDesc": "",
          "isGoldOwner": false
        },
        {
          "infoid": "2023373117154304",
          "cateName": "整租",
          "cateID": "11",
          "title": "光大花园 3室2厅2卫 6000元月 电梯房 精装修",
          "price": "6000",
          "img": "https://pic1.ajkimg.com/display/anjuke/bcf9a29daad7da20157b3cb86bf12af6/240x180c.jpg?t=1&srotate=1",
          "unit": "元/月",
          "coll": "0",
          "subwayDistance": "距8号线-凤凰新村站步行1.2km",
          "fangwuLiangDian": [
            {
              "text": "安选房源",
              "textColor": "#3EC77B",
              "bgColor": "#3EC77B"
            },
            {
              "text": "整租",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "朝南",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "近地铁",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "有电梯",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "精装修",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            }
          ],
          "area": "104㎡",
          "toward": "朝南",
          "shangquan": "工业大道北",
          "isAnxuan": "1",
          "location": false,
          "source_type": "1",
          "isauction": "1",
          "auctionShowText": "",
          "legoAuction": "%7B%22isauction%22%3A%221%22%2C%22lego%22%3A%7B%22lego_tid%22%3A%22%22%2C%22lego_ad_click_url%22%3A%22%22%2C%22lego_sid%22%3A%22168604811212840049557069323%22%2C%22platformCompany%22%3A%22ajk%22%2C%22lego_show_track_url_base%22%3A%22https%3A%5C%2F%5C%2Fadtrack.58.com%5C%2F1001692%5C%2F%22%2C%22lego_version%22%3A%221.0.1%22%2C%22legoHuiDu%22%3Atrue%7D%7D",
          "tid": "",
          "isLiving": false,
          "isVR": false,
          "distanceDesc": "",
          "isGoldOwner": false
        },
        {
          "infoid": "2020634652434444",
          "cateName": "整租",
          "cateID": "11",
          "title": "笋盘力荐！中层一线江景！视野无遮挡！高绿化天然氧吧！",
          "price": "6800",
          "img": "https://pic1.ajkimg.com/display/anjuke/a4de933dece8b0cebd65a88e2aec5be6/240x180c.jpg?t=1&srotate=1",
          "unit": "元/月",
          "coll": "0",
          "subwayDistance": "距8号线-凤凰新村站步行1.6km",
          "fangwuLiangDian": [
            {
              "text": "安选房源",
              "textColor": "#3EC77B",
              "bgColor": "#3EC77B"
            },
            {
              "text": "整租",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "西北",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "近地铁",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "有电梯",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "精装修",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            }
          ],
          "area": "158㎡",
          "toward": "西北",
          "shangquan": "工业大道北",
          "isAnxuan": "1",
          "location": false,
          "source_type": "1",
          "isauction": "1",
          "auctionShowText": "",
          "legoAuction": "%7B%22isauction%22%3A%221%22%2C%22lego%22%3A%7B%22lego_tid%22%3A%22%22%2C%22lego_ad_click_url%22%3A%22%22%2C%22lego_sid%22%3A%22168604811212840049557069323%22%2C%22platformCompany%22%3A%22ajk%22%2C%22lego_show_track_url_base%22%3A%22https%3A%5C%2F%5C%2Fadtrack.58.com%5C%2F1001692%5C%2F%22%2C%22lego_version%22%3A%221.0.1%22%2C%22legoHuiDu%22%3Atrue%7D%7D",
          "tid": "",
          "isLiving": false,
          "isVR": false,
          "distanceDesc": "",
          "isGoldOwner": false
        },
        {
          "infoid": "1955744998611974",
          "cateName": "整租",
          "cateID": "11",
          "title": "富力金禧花园 二房一厅 厅出阳台 采光好通风 旺中带静",
          "price": "6400",
          "img": "https://pic1.ajkimg.com/display/anjuke/2cffa3c4189f0008e847f343298ed4bf/240x180c.jpg?t=1&srotate=1",
          "unit": "元/月",
          "coll": "0",
          "subwayDistance": "距8号线-凤凰新村站步行1.7km",
          "fangwuLiangDian": [
            {
              "text": "安选房源",
              "textColor": "#3EC77B",
              "bgColor": "#3EC77B"
            },
            {
              "text": "整租",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "东南",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "近地铁",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "有电梯",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "精装修",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            }
          ],
          "area": "85㎡",
          "toward": "东南",
          "shangquan": "宝岗",
          "isAnxuan": "1",
          "location": false,
          "source_type": "1",
          "isauction": "1",
          "auctionShowText": "",
          "legoAuction": "%7B%22isauction%22%3A%221%22%2C%22lego%22%3A%7B%22lego_tid%22%3A%22%22%2C%22lego_ad_click_url%22%3A%22%22%2C%22lego_sid%22%3A%22168604811212840049557069323%22%2C%22platformCompany%22%3A%22ajk%22%2C%22lego_show_track_url_base%22%3A%22https%3A%5C%2F%5C%2Fadtrack.58.com%5C%2F1001692%5C%2F%22%2C%22lego_version%22%3A%221.0.1%22%2C%22legoHuiDu%22%3Atrue%7D%7D",
          "tid": "",
          "isLiving": false,
          "isVR": false,
          "distanceDesc": "",
          "isGoldOwner": false
        },
        {
          "infoid": "2038964965008390",
          "cateName": "整租",
          "cateID": "11",
          "title": "小区笋租 宝岗地铁 精装电梯两房 拎包即住 有匙即睇！！",
          "price": "3000",
          "img": "https://pic1.ajkimg.com/display/anjuke/85d5d861f6a03f89a8df7e16eea6962f/240x180c.jpg?t=1&srotate=1",
          "unit": "元/月",
          "coll": "0",
          "subwayDistance": "距8号线-凤凰新村站步行2.0km",
          "fangwuLiangDian": [
            {
              "text": "安选房源",
              "textColor": "#3EC77B",
              "bgColor": "#3EC77B"
            },
            {
              "text": "整租",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "朝南",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "近地铁",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "新上",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "有电梯",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "精装修",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            }
          ],
          "area": "44㎡",
          "toward": "朝南",
          "shangquan": "昌岗",
          "isAnxuan": "1",
          "location": false,
          "source_type": "1",
          "isauction": "1",
          "auctionShowText": "",
          "legoAuction": "%7B%22isauction%22%3A%221%22%2C%22lego%22%3A%7B%22lego_tid%22%3A%22%22%2C%22lego_ad_click_url%22%3A%22%22%2C%22lego_sid%22%3A%22168604811212840049557069323%22%2C%22platformCompany%22%3A%22ajk%22%2C%22lego_show_track_url_base%22%3A%22https%3A%5C%2F%5C%2Fadtrack.58.com%5C%2F1001692%5C%2F%22%2C%22lego_version%22%3A%221.0.1%22%2C%22legoHuiDu%22%3Atrue%7D%7D",
          "tid": "",
          "isLiving": false,
          "isVR": false,
          "distanceDesc": "",
          "isGoldOwner": false
        },
        {
          "infoid": "2037805003645952",
          "cateName": "整租",
          "cateID": "11",
          "title": "光大花园  近地铁  南向三房 安静舒适 有钥匙随时看",
          "price": "5800",
          "img": "https://pic1.ajkimg.com/display/anjuke/1cc3417a16d743da6a015a3a6afeaec7/240x180c.jpg?t=1&srotate=1",
          "unit": "元/月",
          "coll": "0",
          "subwayDistance": "距8号线-凤凰新村站步行1.2km",
          "fangwuLiangDian": [
            {
              "text": "安选房源",
              "textColor": "#3EC77B",
              "bgColor": "#3EC77B"
            },
            {
              "text": "整租",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "朝南",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "近地铁",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "新上",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "有电梯",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            }
          ],
          "area": "101㎡",
          "toward": "朝南",
          "shangquan": "工业大道北",
          "isAnxuan": "1",
          "location": false,
          "source_type": "1",
          "isauction": "1",
          "auctionShowText": "",
          "legoAuction": "%7B%22isauction%22%3A%221%22%2C%22lego%22%3A%7B%22lego_tid%22%3A%22%22%2C%22lego_ad_click_url%22%3A%22%22%2C%22lego_sid%22%3A%22168604811212840049557069323%22%2C%22platformCompany%22%3A%22ajk%22%2C%22lego_show_track_url_base%22%3A%22https%3A%5C%2F%5C%2Fadtrack.58.com%5C%2F1001692%5C%2F%22%2C%22lego_version%22%3A%221.0.1%22%2C%22legoHuiDu%22%3Atrue%7D%7D",
          "tid": "",
          "isLiving": false,
          "isVR": false,
          "distanceDesc": "",
          "isGoldOwner": false
        },
        {
          "infoid": "2024439631111174",
          "cateName": "整租",
          "cateID": "11",
          "title": "邻近双线地铁口！业主诚心出租，价格可以谈！南北广场可拎包入住",
          "price": "6000",
          "img": "https://pic1.ajkimg.com/display/anjuke/f801eef065ba22f0840d1037ca2f094f/240x180c.jpg?t=1&srotate=1",
          "unit": "元/月",
          "coll": "0",
          "subwayDistance": "距8号线-凤凰新村站步行1.1km",
          "fangwuLiangDian": [
            {
              "text": "安选房源",
              "textColor": "#3EC77B",
              "bgColor": "#3EC77B"
            },
            {
              "text": "整租",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "朝南",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "近地铁",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "有电梯",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            }
          ],
          "area": "136㎡",
          "toward": "朝南",
          "shangquan": "宝岗",
          "isAnxuan": "1",
          "location": false,
          "source_type": "1",
          "isauction": "1",
          "auctionShowText": "",
          "legoAuction": "%7B%22isauction%22%3A%221%22%2C%22lego%22%3A%7B%22lego_tid%22%3A%22%22%2C%22lego_ad_click_url%22%3A%22%22%2C%22lego_sid%22%3A%22168604811212840049557069323%22%2C%22platformCompany%22%3A%22ajk%22%2C%22lego_show_track_url_base%22%3A%22https%3A%5C%2F%5C%2Fadtrack.58.com%5C%2F1001692%5C%2F%22%2C%22lego_version%22%3A%221.0.1%22%2C%22legoHuiDu%22%3Atrue%7D%7D",
          "tid": "",
          "isLiving": false,
          "isVR": false,
          "distanceDesc": "",
          "isGoldOwner": false
        },
        {
          "infoid": "2023707933025283",
          "cateName": "整租",
          "cateID": "11",
          "title": "金碧湾好房推荐 2室1厅1卫 5000元月 太古仓商圈 赞",
          "price": "5000",
          "img": "https://pic1.ajkimg.com/display/anjuke/0188f5f5c361d6c2b85665a132d7117b/240x180c.jpg?t=1&srotate=1",
          "unit": "元/月",
          "coll": "0",
          "subwayDistance": "距8号线-凤凰新村站步行751m",
          "fangwuLiangDian": [
            {
              "text": "安选房源",
              "textColor": "#3EC77B",
              "bgColor": "#3EC77B"
            },
            {
              "text": "整租",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "西北",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "近地铁",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "有电梯",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "精装修",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            }
          ],
          "area": "64㎡",
          "toward": "西北",
          "shangquan": "工业大道北",
          "isAnxuan": "1",
          "location": false,
          "source_type": "1",
          "isauction": "1",
          "auctionShowText": "",
          "legoAuction": "%7B%22isauction%22%3A%221%22%2C%22lego%22%3A%7B%22lego_tid%22%3A%22%22%2C%22lego_ad_click_url%22%3A%22%22%2C%22lego_sid%22%3A%22168604811212840049557069323%22%2C%22platformCompany%22%3A%22ajk%22%2C%22lego_show_track_url_base%22%3A%22https%3A%5C%2F%5C%2Fadtrack.58.com%5C%2F1001692%5C%2F%22%2C%22lego_version%22%3A%221.0.1%22%2C%22legoHuiDu%22%3Atrue%7D%7D",
          "tid": "",
          "isLiving": false,
          "isVR": false,
          "distanceDesc": "",
          "isGoldOwner": false
        },
        {
          "infoid": "2020303270880270",
          "cateName": "整租",
          "cateID": "11",
          "title": "南华西大厦电梯两房 装修保养好 中高层厅出阳台视野开阔采光好",
          "price": "4200",
          "img": "https://pic1.ajkimg.com/display/anjuke/ca02352a357b3993da18874495fca396/240x180c.jpg?t=1&srotate=1",
          "unit": "元/月",
          "coll": "0",
          "subwayDistance": "距8号线-凤凰新村站步行1.3km",
          "fangwuLiangDian": [
            {
              "text": "安选房源",
              "textColor": "#3EC77B",
              "bgColor": "#3EC77B"
            },
            {
              "text": "整租",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "朝南",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "近地铁",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "有电梯",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "精装修",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            }
          ],
          "area": "72㎡",
          "toward": "朝南",
          "shangquan": "宝岗",
          "isAnxuan": "1",
          "location": false,
          "source_type": "1",
          "isauction": "1",
          "auctionShowText": "",
          "legoAuction": "%7B%22isauction%22%3A%221%22%2C%22lego%22%3A%7B%22lego_tid%22%3A%22%22%2C%22lego_ad_click_url%22%3A%22%22%2C%22lego_sid%22%3A%22168604811212840049557069323%22%2C%22platformCompany%22%3A%22ajk%22%2C%22lego_show_track_url_base%22%3A%22https%3A%5C%2F%5C%2Fadtrack.58.com%5C%2F1001692%5C%2F%22%2C%22lego_version%22%3A%221.0.1%22%2C%22legoHuiDu%22%3Atrue%7D%7D",
          "tid": "",
          "isLiving": false,
          "isVR": false,
          "distanceDesc": "",
          "isGoldOwner": false
        },
        {
          "infoid": "2020609324033035",
          "cateName": "整租",
          "cateID": "11",
          "title": "怡龙苑 电梯3房 家私家电齐全 拎包入住 交通便利 随时看房",
          "price": "3500",
          "img": "https://pic1.ajkimg.com/display/anjuke/02e6a2923049f9f3b8fe1f047471964e/240x180c.jpg?t=1&srotate=1",
          "unit": "元/月",
          "coll": "0",
          "subwayDistance": "距8号线-凤凰新村站步行936m",
          "fangwuLiangDian": [
            {
              "text": "安选房源",
              "textColor": "#3EC77B",
              "bgColor": "#3EC77B"
            },
            {
              "text": "整租",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "东西",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "近地铁",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "有电梯",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "精装修",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            }
          ],
          "area": "89㎡",
          "toward": "东西",
          "shangquan": "宝岗",
          "isAnxuan": "1",
          "location": false,
          "source_type": "1",
          "isauction": "1",
          "auctionShowText": "",
          "legoAuction": "%7B%22isauction%22%3A%221%22%2C%22lego%22%3A%7B%22lego_tid%22%3A%22%22%2C%22lego_ad_click_url%22%3A%22%22%2C%22lego_sid%22%3A%22168604811212840049557069323%22%2C%22platformCompany%22%3A%22ajk%22%2C%22lego_show_track_url_base%22%3A%22https%3A%5C%2F%5C%2Fadtrack.58.com%5C%2F1001692%5C%2F%22%2C%22lego_version%22%3A%221.0.1%22%2C%22legoHuiDu%22%3Atrue%7D%7D",
          "tid": "",
          "isLiving": false,
          "isVR": false,
          "distanceDesc": "",
          "isGoldOwner": false
        },
        {
          "infoid": "2036188166333452",
          "cateName": "整租",
          "cateID": "11",
          "title": "实地拍摄 宝岗精装两房一厅 电梯加装中 有钥匙随时看房",
          "price": "3200",
          "img": "https://pic1.ajkimg.com/display/anjuke/7983d828722c2ea9359725e3a77571bd/240x180c.jpg?t=1&srotate=1",
          "unit": "元/月",
          "coll": "0",
          "subwayDistance": "距8号线-凤凰新村站步行2.0km",
          "fangwuLiangDian": [
            {
              "text": "安选房源",
              "textColor": "#3EC77B",
              "bgColor": "#3EC77B"
            },
            {
              "text": "整租",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "南北",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "近地铁",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "精装修",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            }
          ],
          "area": "45㎡",
          "toward": "南北",
          "shangquan": "昌岗",
          "isAnxuan": "1",
          "location": false,
          "source_type": "1",
          "isauction": "1",
          "auctionShowText": "",
          "legoAuction": "%7B%22isauction%22%3A%221%22%2C%22lego%22%3A%7B%22lego_tid%22%3A%22%22%2C%22lego_ad_click_url%22%3A%22%22%2C%22lego_sid%22%3A%22168604811212840049557069323%22%2C%22platformCompany%22%3A%22ajk%22%2C%22lego_show_track_url_base%22%3A%22https%3A%5C%2F%5C%2Fadtrack.58.com%5C%2F1001692%5C%2F%22%2C%22lego_version%22%3A%221.0.1%22%2C%22legoHuiDu%22%3Atrue%7D%7D",
          "tid": "",
          "isLiving": false,
          "isVR": true,
          "distanceDesc": "",
          "isGoldOwner": false
        },
        {
          "infoid": "2030358693519370",
          "cateName": "整租",
          "cateID": "11",
          "title": "金碧湾精装两房出租 拎包入住即可 采光充足通风透气",
          "price": "5300",
          "img": "https://pic1.ajkimg.com/display/anjuke/51b7f086a0b49ee2c59a2f25e28e5e95/240x180c.jpg?t=1&srotate=1",
          "unit": "元/月",
          "coll": "0",
          "subwayDistance": "距8号线-凤凰新村站步行751m",
          "fangwuLiangDian": [
            {
              "text": "安选房源",
              "textColor": "#3EC77B",
              "bgColor": "#3EC77B"
            },
            {
              "text": "整租",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "朝西",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "近地铁",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "有电梯",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "精装修",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            }
          ],
          "area": "65㎡",
          "toward": "朝西",
          "shangquan": "工业大道北",
          "isAnxuan": "1",
          "location": false,
          "source_type": "1",
          "isauction": "1",
          "auctionShowText": "",
          "legoAuction": "%7B%22isauction%22%3A%221%22%2C%22lego%22%3A%7B%22lego_tid%22%3A%22%22%2C%22lego_ad_click_url%22%3A%22%22%2C%22lego_sid%22%3A%22168604811212840049557069323%22%2C%22platformCompany%22%3A%22ajk%22%2C%22lego_show_track_url_base%22%3A%22https%3A%5C%2F%5C%2Fadtrack.58.com%5C%2F1001692%5C%2F%22%2C%22lego_version%22%3A%221.0.1%22%2C%22legoHuiDu%22%3Atrue%7D%7D",
          "tid": "",
          "isLiving": false,
          "isVR": false,
          "distanceDesc": "",
          "isGoldOwner": false
        },
        {
          "infoid": "2027884883696650",
          "cateName": "整租",
          "cateID": "11",
          "title": "南华西大厦 两房两厅 家私家电齐全 环境好 交通便利看房方便",
          "price": "4200",
          "img": "https://pic1.ajkimg.com/display/anjuke/dad663605e05e7333d8ca0ee80839bfc/240x180c.jpg?t=1&srotate=1",
          "unit": "元/月",
          "coll": "0",
          "subwayDistance": "距8号线-凤凰新村站步行1.3km",
          "fangwuLiangDian": [
            {
              "text": "安选房源",
              "textColor": "#3EC77B",
              "bgColor": "#3EC77B"
            },
            {
              "text": "整租",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "朝南",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "近地铁",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "有电梯",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "精装修",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            }
          ],
          "area": "72㎡",
          "toward": "朝南",
          "shangquan": "宝岗",
          "isAnxuan": "1",
          "location": false,
          "source_type": "1",
          "isauction": "1",
          "auctionShowText": "",
          "legoAuction": "%7B%22isauction%22%3A%221%22%2C%22lego%22%3A%7B%22lego_tid%22%3A%22%22%2C%22lego_ad_click_url%22%3A%22%22%2C%22lego_sid%22%3A%22168604811212840049557069323%22%2C%22platformCompany%22%3A%22ajk%22%2C%22lego_show_track_url_base%22%3A%22https%3A%5C%2F%5C%2Fadtrack.58.com%5C%2F1001692%5C%2F%22%2C%22lego_version%22%3A%221.0.1%22%2C%22legoHuiDu%22%3Atrue%7D%7D",
          "tid": "",
          "isLiving": false,
          "isVR": false,
          "distanceDesc": "",
          "isGoldOwner": false
        },
        {
          "infoid": "2021634933825544",
          "cateName": "整租",
          "cateID": "11",
          "title": "昌盛小区电梯两房 装修保养好 安静舒适 拎包入住即可",
          "price": "3500",
          "img": "https://pic1.ajkimg.com/display/anjuke/423f0d37e1767c2437c1cf88cf16ee6f/240x180c.jpg?t=1&srotate=1",
          "unit": "元/月",
          "coll": "0",
          "subwayDistance": "距8号线-凤凰新村站步行957m",
          "fangwuLiangDian": [
            {
              "text": "安选房源",
              "textColor": "#3EC77B",
              "bgColor": "#3EC77B"
            },
            {
              "text": "整租",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "朝南",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "近地铁",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "有电梯",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "精装修",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            }
          ],
          "area": "61.7㎡",
          "toward": "朝南",
          "shangquan": "宝岗",
          "isAnxuan": "1",
          "location": false,
          "source_type": "1",
          "isauction": "1",
          "auctionShowText": "",
          "legoAuction": "%7B%22isauction%22%3A%221%22%2C%22lego%22%3A%7B%22lego_tid%22%3A%22%22%2C%22lego_ad_click_url%22%3A%22%22%2C%22lego_sid%22%3A%22168604811212840049557069323%22%2C%22platformCompany%22%3A%22ajk%22%2C%22lego_show_track_url_base%22%3A%22https%3A%5C%2F%5C%2Fadtrack.58.com%5C%2F1001692%5C%2F%22%2C%22lego_version%22%3A%221.0.1%22%2C%22legoHuiDu%22%3Atrue%7D%7D",
          "tid": "",
          "isLiving": false,
          "isVR": false,
          "distanceDesc": "",
          "isGoldOwner": false
        },
        {
          "infoid": "2020251897012234",
          "cateName": "整租",
          "cateID": "11",
          "title": "怡龙苑电梯三房 装修保养新净 户型通透 采光充足 即租即入住",
          "price": "3500",
          "img": "https://pic1.ajkimg.com/display/anjuke/23e20817a26345a2da64fca64c3926a4/240x180c.jpg?t=1&srotate=1",
          "unit": "元/月",
          "coll": "0",
          "subwayDistance": "距8号线-凤凰新村站步行936m",
          "fangwuLiangDian": [
            {
              "text": "安选房源",
              "textColor": "#3EC77B",
              "bgColor": "#3EC77B"
            },
            {
              "text": "整租",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "东西",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "近地铁",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "有电梯",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            }
          ],
          "area": "68㎡",
          "toward": "东西",
          "shangquan": "宝岗",
          "isAnxuan": "1",
          "location": false,
          "source_type": "1",
          "isauction": "1",
          "auctionShowText": "",
          "legoAuction": "%7B%22isauction%22%3A%221%22%2C%22lego%22%3A%7B%22lego_tid%22%3A%22%22%2C%22lego_ad_click_url%22%3A%22%22%2C%22lego_sid%22%3A%22168604811212840049557069323%22%2C%22platformCompany%22%3A%22ajk%22%2C%22lego_show_track_url_base%22%3A%22https%3A%5C%2F%5C%2Fadtrack.58.com%5C%2F1001692%5C%2F%22%2C%22lego_version%22%3A%221.0.1%22%2C%22legoHuiDu%22%3Atrue%7D%7D",
          "tid": "",
          "isLiving": false,
          "isVR": false,
          "distanceDesc": "",
          "isGoldOwner": false
        },
        {
          "infoid": "2037410442648576",
          "cateName": "整租",
          "cateID": "11",
          "title": "恒龙苑 电梯大两房 乐居方正 装修新净  拎包入住",
          "price": "3600",
          "img": "https://pic1.ajkimg.com/display/anjuke/bc160ec3da5e06a03f91a7f0f161567a/240x180c.jpg?t=1&srotate=1",
          "unit": "元/月",
          "coll": "0",
          "subwayDistance": "距8号线-凤凰新村站步行1.6km",
          "fangwuLiangDian": [
            {
              "text": "安选房源",
              "textColor": "#3EC77B",
              "bgColor": "#3EC77B"
            },
            {
              "text": "整租",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "朝东",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "近地铁",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "有电梯",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "精装修",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            }
          ],
          "area": "76㎡",
          "toward": "朝东",
          "shangquan": "宝岗",
          "isAnxuan": "1",
          "location": false,
          "source_type": "1",
          "isauction": "1",
          "auctionShowText": "",
          "legoAuction": "%7B%22isauction%22%3A%221%22%2C%22lego%22%3A%7B%22lego_tid%22%3A%22%22%2C%22lego_ad_click_url%22%3A%22%22%2C%22lego_sid%22%3A%22168604811212840049557069323%22%2C%22platformCompany%22%3A%22ajk%22%2C%22lego_show_track_url_base%22%3A%22https%3A%5C%2F%5C%2Fadtrack.58.com%5C%2F1001692%5C%2F%22%2C%22lego_version%22%3A%221.0.1%22%2C%22legoHuiDu%22%3Atrue%7D%7D",
          "tid": "",
          "isLiving": false,
          "isVR": false,
          "distanceDesc": "",
          "isGoldOwner": false
        },
        {
          "infoid": "2024422359964680",
          "cateName": "整租",
          "cateID": "11",
          "title": "2号线江南西地铁口 精装大两房 家私电齐全 只要3500！！",
          "price": "3500",
          "img": "https://pic1.ajkimg.com/display/anjuke/e2f2a04037da28848cd4aed495595096/240x180c.jpg?t=1&srotate=1",
          "unit": "元/月",
          "coll": "0",
          "subwayDistance": "距8号线-凤凰新村站步行1.9km",
          "fangwuLiangDian": [
            {
              "text": "安选房源",
              "textColor": "#3EC77B",
              "bgColor": "#3EC77B"
            },
            {
              "text": "整租",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "朝南",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "近地铁",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "有电梯",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "精装修",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            }
          ],
          "area": "55㎡",
          "toward": "朝南",
          "shangquan": "江南西",
          "isAnxuan": "1",
          "location": false,
          "source_type": "1",
          "isauction": "1",
          "auctionShowText": "",
          "legoAuction": "%7B%22isauction%22%3A%221%22%2C%22lego%22%3A%7B%22lego_tid%22%3A%22%22%2C%22lego_ad_click_url%22%3A%22%22%2C%22lego_sid%22%3A%22168604811212840049557069323%22%2C%22platformCompany%22%3A%22ajk%22%2C%22lego_show_track_url_base%22%3A%22https%3A%5C%2F%5C%2Fadtrack.58.com%5C%2F1001692%5C%2F%22%2C%22lego_version%22%3A%221.0.1%22%2C%22legoHuiDu%22%3Atrue%7D%7D",
          "tid": "",
          "isLiving": false,
          "isVR": false,
          "distanceDesc": "",
          "isGoldOwner": false
        },
        {
          "infoid": "2033015494959110",
          "cateName": "整租",
          "cateID": "11",
          "title": "高档天鹅湾小区 全新精装3房2卫 家私家电可配齐 租1万",
          "price": "10000",
          "img": "https://pic1.ajkimg.com/display/anjuke/7f2a8bd5d972d49bce6a8c9a0f4df712/240x180c.jpg?t=1&srotate=1",
          "unit": "元/月",
          "coll": "0",
          "subwayDistance": "距8号线-凤凰新村站步行1.1km",
          "fangwuLiangDian": [
            {
              "text": "安选房源",
              "textColor": "#3EC77B",
              "bgColor": "#3EC77B"
            },
            {
              "text": "整租",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "朝南",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "近地铁",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "有电梯",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "精装修",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            }
          ],
          "area": "153㎡",
          "toward": "朝南",
          "shangquan": "工业大道北",
          "isAnxuan": "1",
          "location": false,
          "source_type": "1",
          "isauction": "1",
          "auctionShowText": "",
          "legoAuction": "%7B%22isauction%22%3A%221%22%2C%22lego%22%3A%7B%22lego_tid%22%3A%22%22%2C%22lego_ad_click_url%22%3A%22%22%2C%22lego_sid%22%3A%22168604811212840049557069323%22%2C%22platformCompany%22%3A%22ajk%22%2C%22lego_show_track_url_base%22%3A%22https%3A%5C%2F%5C%2Fadtrack.58.com%5C%2F1001692%5C%2F%22%2C%22lego_version%22%3A%221.0.1%22%2C%22legoHuiDu%22%3Atrue%7D%7D",
          "tid": "",
          "isLiving": false,
          "isVR": false,
          "distanceDesc": "",
          "isGoldOwner": false
        },
        {
          "infoid": "2032943336743943",
          "cateName": "整租",
          "cateID": "11",
          "title": "8号线凤凰新村站 初次出租精装修2房 拎包入住 只要4200",
          "price": "4200",
          "img": "https://pic1.ajkimg.com/display/anjuke/6e25f4ff6615a07392c1b1521eaf911a/240x180c.jpg?t=1&srotate=1",
          "unit": "元/月",
          "coll": "0",
          "subwayDistance": "距8号线-凤凰新村站步行673m",
          "fangwuLiangDian": [
            {
              "text": "安选房源",
              "textColor": "#3EC77B",
              "bgColor": "#3EC77B"
            },
            {
              "text": "整租",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "朝南",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "近地铁",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "有电梯",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            }
          ],
          "area": "50㎡",
          "toward": "朝南",
          "shangquan": "宝岗",
          "isAnxuan": "1",
          "location": false,
          "source_type": "1",
          "isauction": "1",
          "auctionShowText": "",
          "legoAuction": "%7B%22isauction%22%3A%221%22%2C%22lego%22%3A%7B%22lego_tid%22%3A%22%22%2C%22lego_ad_click_url%22%3A%22%22%2C%22lego_sid%22%3A%22168604811212840049557069323%22%2C%22platformCompany%22%3A%22ajk%22%2C%22lego_show_track_url_base%22%3A%22https%3A%5C%2F%5C%2Fadtrack.58.com%5C%2F1001692%5C%2F%22%2C%22lego_version%22%3A%221.0.1%22%2C%22legoHuiDu%22%3Atrue%7D%7D",
          "tid": "",
          "isLiving": false,
          "isVR": false,
          "distanceDesc": "",
          "isGoldOwner": false
        },
        {
          "infoid": "2023045499593739",
          "cateName": "整租",
          "cateID": "11",
          "title": "荟贤大厦 3室2厅1卫 5100元月 电梯房 91平",
          "price": "5100",
          "img": "https://pic1.ajkimg.com/display/anjuke/8b0d58bdd4c84ba65bfcaad2c453d662/240x180c.jpg?t=1&srotate=1",
          "unit": "元/月",
          "coll": "0",
          "subwayDistance": "距8号线-凤凰新村站步行1.4km",
          "fangwuLiangDian": [
            {
              "text": "安选房源",
              "textColor": "#3EC77B",
              "bgColor": "#3EC77B"
            },
            {
              "text": "整租",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "东南",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "近地铁",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "有电梯",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            }
          ],
          "area": "91㎡",
          "toward": "东南",
          "shangquan": "宝岗",
          "isAnxuan": "1",
          "location": false,
          "source_type": "1",
          "isauction": "1",
          "auctionShowText": "",
          "legoAuction": "%7B%22isauction%22%3A%221%22%2C%22lego%22%3A%7B%22lego_tid%22%3A%22%22%2C%22lego_ad_click_url%22%3A%22%22%2C%22lego_sid%22%3A%22168604811212840049557069323%22%2C%22platformCompany%22%3A%22ajk%22%2C%22lego_show_track_url_base%22%3A%22https%3A%5C%2F%5C%2Fadtrack.58.com%5C%2F1001692%5C%2F%22%2C%22lego_version%22%3A%221.0.1%22%2C%22legoHuiDu%22%3Atrue%7D%7D",
          "tid": "",
          "isLiving": false,
          "isVR": false,
          "distanceDesc": "",
          "isGoldOwner": false
        },
        {
          "infoid": "1969519903219720",
          "cateName": "整租",
          "cateID": "11",
          "title": "光大花园 精装三房带主套 业主初次出租 拎包入住",
          "price": "6000",
          "img": "https://pic1.ajkimg.com/display/anjuke/833244f02884db5c013125109df78822/240x180c.jpg?t=1&srotate=1",
          "unit": "元/月",
          "coll": "0",
          "subwayDistance": "距8号线-凤凰新村站步行1.2km",
          "fangwuLiangDian": [
            {
              "text": "安选房源",
              "textColor": "#3EC77B",
              "bgColor": "#3EC77B"
            },
            {
              "text": "整租",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "东南",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "近地铁",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "有电梯",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "精装修",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            }
          ],
          "area": "93㎡",
          "toward": "东南",
          "shangquan": "工业大道北",
          "isAnxuan": "1",
          "location": false,
          "source_type": "1",
          "isauction": "1",
          "auctionShowText": "",
          "legoAuction": "%7B%22isauction%22%3A%221%22%2C%22lego%22%3A%7B%22lego_tid%22%3A%22%22%2C%22lego_ad_click_url%22%3A%22%22%2C%22lego_sid%22%3A%22168604811212840049557069323%22%2C%22platformCompany%22%3A%22ajk%22%2C%22lego_show_track_url_base%22%3A%22https%3A%5C%2F%5C%2Fadtrack.58.com%5C%2F1001692%5C%2F%22%2C%22lego_version%22%3A%221.0.1%22%2C%22legoHuiDu%22%3Atrue%7D%7D",
          "tid": "",
          "isLiving": false,
          "isVR": false,
          "distanceDesc": "",
          "isGoldOwner": false
        },
        {
          "infoid": "2030146931481611",
          "cateName": "整租",
          "cateID": "11",
          "title": "靓租天誉半岛望江大五房家电齐全拎包入住原租户准备到期",
          "price": "32000",
          "img": "https://pic1.ajkimg.com/display/anjuke/0108f0ca011f917230ea0cabf8fa9a77/240x180c.jpg?t=1&srotate=1",
          "unit": "元/月",
          "coll": "0",
          "subwayDistance": "距8号线-凤凰新村站步行1.5km",
          "fangwuLiangDian": [
            {
              "text": "安选房源",
              "textColor": "#3EC77B",
              "bgColor": "#3EC77B"
            },
            {
              "text": "整租",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "东北",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "近地铁",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "有电梯",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "精装修",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            }
          ],
          "area": "370㎡",
          "toward": "东北",
          "shangquan": "工业大道北",
          "isAnxuan": "1",
          "location": false,
          "source_type": "1",
          "isauction": "1",
          "auctionShowText": "",
          "legoAuction": "%7B%22isauction%22%3A%221%22%2C%22lego%22%3A%7B%22lego_tid%22%3A%22%22%2C%22lego_ad_click_url%22%3A%22%22%2C%22lego_sid%22%3A%22168604811212840049557069323%22%2C%22platformCompany%22%3A%22ajk%22%2C%22lego_show_track_url_base%22%3A%22https%3A%5C%2F%5C%2Fadtrack.58.com%5C%2F1001692%5C%2F%22%2C%22lego_version%22%3A%221.0.1%22%2C%22legoHuiDu%22%3Atrue%7D%7D",
          "tid": "",
          "isLiving": false,
          "isVR": false,
          "distanceDesc": "",
          "isGoldOwner": false
        },
        {
          "infoid": "2021674318717967",
          "cateName": "整租",
          "cateID": "11",
          "title": "光大花园 精装三房望江 家私家电齐全 随时入住",
          "price": "6500",
          "img": "https://pic1.ajkimg.com/display/anjuke/2d5040e35081ba9a4ad56a51dce08dd9/240x180c.jpg?t=1&srotate=1",
          "unit": "元/月",
          "coll": "0",
          "subwayDistance": "距8号线-凤凰新村站步行1.2km",
          "fangwuLiangDian": [
            {
              "text": "安选房源",
              "textColor": "#3EC77B",
              "bgColor": "#3EC77B"
            },
            {
              "text": "整租",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "朝南",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "近地铁",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "有电梯",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "精装修",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            }
          ],
          "area": "104㎡",
          "toward": "朝南",
          "shangquan": "工业大道北",
          "isAnxuan": "1",
          "location": false,
          "source_type": "1",
          "isauction": "1",
          "auctionShowText": "",
          "legoAuction": "%7B%22isauction%22%3A%221%22%2C%22lego%22%3A%7B%22lego_tid%22%3A%22%22%2C%22lego_ad_click_url%22%3A%22%22%2C%22lego_sid%22%3A%22168604811212840049557069323%22%2C%22platformCompany%22%3A%22ajk%22%2C%22lego_show_track_url_base%22%3A%22https%3A%5C%2F%5C%2Fadtrack.58.com%5C%2F1001692%5C%2F%22%2C%22lego_version%22%3A%221.0.1%22%2C%22legoHuiDu%22%3Atrue%7D%7D",
          "tid": "",
          "isLiving": false,
          "isVR": false,
          "distanceDesc": "",
          "isGoldOwner": false
        },
        {
          "infoid": "2024785723417612",
          "cateName": "整租",
          "cateID": "11",
          "title": "宝岗大道 低层3房 全新装修 南向采光通风好 随时方便看房",
          "price": "4500",
          "img": "https://pic1.ajkimg.com/display/anjuke/5db72b8e65833e0f7effe2c7cedf182e/240x180c.jpg?t=1&srotate=1",
          "unit": "元/月",
          "coll": "0",
          "subwayDistance": "距8号线-凤凰新村站步行1.1km",
          "fangwuLiangDian": [
            {
              "text": "安选房源",
              "textColor": "#3EC77B",
              "bgColor": "#3EC77B"
            },
            {
              "text": "整租",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "南北",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "近地铁",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "精装修",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            }
          ],
          "area": "89㎡",
          "toward": "南北",
          "shangquan": "宝岗",
          "isAnxuan": "1",
          "location": false,
          "source_type": "1",
          "isauction": "1",
          "auctionShowText": "",
          "legoAuction": "%7B%22isauction%22%3A%221%22%2C%22lego%22%3A%7B%22lego_tid%22%3A%22%22%2C%22lego_ad_click_url%22%3A%22%22%2C%22lego_sid%22%3A%22168604811212840049557069323%22%2C%22platformCompany%22%3A%22ajk%22%2C%22lego_show_track_url_base%22%3A%22https%3A%5C%2F%5C%2Fadtrack.58.com%5C%2F1001692%5C%2F%22%2C%22lego_version%22%3A%221.0.1%22%2C%22legoHuiDu%22%3Atrue%7D%7D",
          "tid": "",
          "isLiving": false,
          "isVR": false,
          "distanceDesc": "",
          "isGoldOwner": false
        },
        {
          "infoid": "2021637103902732",
          "cateName": "整租",
          "cateID": "11",
          "title": "正规三房出租，一线江景房，临近太古仓码头，吃喝玩乐应有尽有！",
          "price": "7000",
          "img": "https://pic1.ajkimg.com/display/anjuke/36900bf33afa8c0edf4ac2d3d9968f4e/240x180c.jpg?t=1&srotate=1",
          "unit": "元/月",
          "coll": "0",
          "subwayDistance": "距8号线-凤凰新村站步行1.6km",
          "fangwuLiangDian": [
            {
              "text": "安选房源",
              "textColor": "#3EC77B",
              "bgColor": "#3EC77B"
            },
            {
              "text": "整租",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "朝西",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "近地铁",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "有电梯",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "精装修",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            }
          ],
          "area": "104㎡",
          "toward": "朝西",
          "shangquan": "工业大道北",
          "isAnxuan": "1",
          "location": false,
          "source_type": "1",
          "isauction": "1",
          "auctionShowText": "",
          "legoAuction": "%7B%22isauction%22%3A%221%22%2C%22lego%22%3A%7B%22lego_tid%22%3A%22%22%2C%22lego_ad_click_url%22%3A%22%22%2C%22lego_sid%22%3A%22168604811212840049557069323%22%2C%22platformCompany%22%3A%22ajk%22%2C%22lego_show_track_url_base%22%3A%22https%3A%5C%2F%5C%2Fadtrack.58.com%5C%2F1001692%5C%2F%22%2C%22lego_version%22%3A%221.0.1%22%2C%22legoHuiDu%22%3Atrue%7D%7D",
          "tid": "",
          "isLiving": false,
          "isVR": false,
          "distanceDesc": "",
          "isGoldOwner": false
        },
        {
          "infoid": "2030480266124301",
          "cateName": "整租",
          "cateID": "11",
          "title": "毕业季 学生优惠特价 押一付一 全新公寓平台直租 欢迎咨询",
          "price": "2210",
          "img": "https://pic1.ajkimg.com/display/anjuke/9413c2a2f60f33618ed4dda7e392e74f/240x180c.jpg?t=1&srotate=1",
          "unit": "元/月",
          "coll": "0",
          "subwayDistance": "距8号线-凤凰新村站步行673m",
          "fangwuLiangDian": [
            {
              "text": "安选房源",
              "textColor": "#3EC77B",
              "bgColor": "#3EC77B"
            },
            {
              "text": "整租",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "朝南",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "近地铁",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "精装修",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            }
          ],
          "area": "26㎡",
          "toward": "朝南",
          "shangquan": "宝岗",
          "isAnxuan": "1",
          "location": false,
          "source_type": "1",
          "isauction": "1",
          "auctionShowText": "",
          "legoAuction": "%7B%22isauction%22%3A%221%22%2C%22lego%22%3A%7B%22lego_tid%22%3A%22%22%2C%22lego_ad_click_url%22%3A%22%22%2C%22lego_sid%22%3A%22168604811212840049557069323%22%2C%22platformCompany%22%3A%22ajk%22%2C%22lego_show_track_url_base%22%3A%22https%3A%5C%2F%5C%2Fadtrack.58.com%5C%2F1001692%5C%2F%22%2C%22lego_version%22%3A%221.0.1%22%2C%22legoHuiDu%22%3Atrue%7D%7D",
          "tid": "",
          "isLiving": false,
          "isVR": false,
          "distanceDesc": "",
          "isGoldOwner": false
        },
        {
          "infoid": "2024680824019973",
          "cateName": "整租",
          "cateID": "11",
          "title": "光大花园 2室2厅1卫  电梯房 户型方正实用 采光好近地铁",
          "price": "5000",
          "img": "https://pic1.ajkimg.com/display/anjuke/8be252ea460c73ef1bd4b60bfc4ab1ac/240x180c.jpg?t=1&srotate=1",
          "unit": "元/月",
          "coll": "0",
          "subwayDistance": "距8号线-凤凰新村站步行1.2km",
          "fangwuLiangDian": [
            {
              "text": "安选房源",
              "textColor": "#3EC77B",
              "bgColor": "#3EC77B"
            },
            {
              "text": "整租",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "东南",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "近地铁",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "有电梯",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            }
          ],
          "area": "74㎡",
          "toward": "东南",
          "shangquan": "工业大道北",
          "isAnxuan": "1",
          "location": false,
          "source_type": "1",
          "isauction": "1",
          "auctionShowText": "",
          "legoAuction": "%7B%22isauction%22%3A%221%22%2C%22lego%22%3A%7B%22lego_tid%22%3A%22%22%2C%22lego_ad_click_url%22%3A%22%22%2C%22lego_sid%22%3A%22168604811212840049557069323%22%2C%22platformCompany%22%3A%22ajk%22%2C%22lego_show_track_url_base%22%3A%22https%3A%5C%2F%5C%2Fadtrack.58.com%5C%2F1001692%5C%2F%22%2C%22lego_version%22%3A%221.0.1%22%2C%22legoHuiDu%22%3Atrue%7D%7D",
          "tid": "",
          "isLiving": false,
          "isVR": false,
          "distanceDesc": "",
          "isGoldOwner": false
        },
        {
          "infoid": "2034403681640453",
          "cateName": "整租",
          "cateID": "11",
          "title": "阳光名苑 3室2厅1卫 5000元月 精装修 96平",
          "price": "5000",
          "img": "https://pic1.ajkimg.com/display/anjuke/bcbf0843dcf6e65545360521f3eb457b/240x180c.jpg?t=1&srotate=1",
          "unit": "元/月",
          "coll": "0",
          "subwayDistance": "距8号线-凤凰新村站步行1.3km",
          "fangwuLiangDian": [
            {
              "text": "安选房源",
              "textColor": "#3EC77B",
              "bgColor": "#3EC77B"
            },
            {
              "text": "整租",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "东南",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "近地铁",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "有电梯",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "精装修",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            }
          ],
          "area": "96㎡",
          "toward": "东南",
          "shangquan": "工业大道北",
          "isAnxuan": "1",
          "location": false,
          "source_type": "1",
          "isauction": "1",
          "auctionShowText": "",
          "legoAuction": "%7B%22isauction%22%3A%221%22%2C%22lego%22%3A%7B%22lego_tid%22%3A%22%22%2C%22lego_ad_click_url%22%3A%22%22%2C%22lego_sid%22%3A%22168604811212840049557069323%22%2C%22platformCompany%22%3A%22ajk%22%2C%22lego_show_track_url_base%22%3A%22https%3A%5C%2F%5C%2Fadtrack.58.com%5C%2F1001692%5C%2F%22%2C%22lego_version%22%3A%221.0.1%22%2C%22legoHuiDu%22%3Atrue%7D%7D",
          "tid": "",
          "isLiving": false,
          "isVR": false,
          "distanceDesc": "",
          "isGoldOwner": false
        }
      ]
    }
  }