var ZUFANG_1 = {
    "code": "0",
    "message": "OK",
    "data": {
      "isToast": false,
      "activity": {
        "title": ""
      },
      "configs": {
        "banner": [],
        "houseSubject": []
      },
      "tkd": {
        "title": "广州整租租房网，广州整租租房信息，广州整租房屋出租价格-广州58安居客",
        "keywords": "广州整租租房，广州整租出租房，广州整租租房子",
        "description": "安居客广州租房网为您提供整租租房信息,包括广州二手房出租、新房出租，租房子,找广州出租房,就上58安居客广州租房网。"
      },
      "gongyu": {
        "isShow": true
      },
      "goldOwnerCity": false,
      "list": [
        {
          "infoid": "2039062186992643",
          "cateName": "整租",
          "cateID": "11",
          "title": "柯木塱a口 两房一厅 格局方正 不需要过马路",
          "price": "1750",
          "img": "https://pic1.ajkimg.com/display/anjuke/e84abbf34001aa4928debeafb57d9054/240x180c.jpg?t=1&srotate=1",
          "unit": "元/月",
          "coll": "0",
          "subwayDistance": "距6号线-柯木塱站步行234m",
          "fangwuLiangDian": [
            {
              "text": "整租",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "朝南",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "近地铁",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "新上",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            }
          ],
          "area": "50㎡",
          "toward": "朝南",
          "shangquan": "岑村",
          "isAnxuan": "0",
          "location": false,
          "source_type": "1",
          "isauction": "2",
          "auctionShowText": "广告",
          "legoAuction": "%7B%22isauction%22%3A%222%22%2C%22lego%22%3A%7B%22lego_tid%22%3A%225889f4b2-4648-4866-8f9e-67c78f049433%22%2C%22lego_ad_click_url%22%3A%22https%3A%5C%2F%5C%2Flegoclick.58.com%5C%2Fjump%3Ftarget%3DpZwY0ZnlsztdraOWUvYKuadBnjNzrjEvnBYLmhuhsHw6nvmVrjbvraYzuhwhmHRhmvn1rHEKP1EQnWbLP1ckrjbQnWEKTHckn1bkPWcQrjmOrHcvPjnKrHbknTDOrHTkTHD_nHDKnzkQPWNYsjDdn1NvTHDvnWn3n1b3PWNLn1nKnE7AEzdPsN72iSXMMSpcfSpEMrXYC8yc-SB6Jrh6VEDQTHDKTHnKUvPHPLDkHD-ogLIvibPOINIimYIaI1IgXNPdwEDQPjTdnHTknjn3PjTYP1DYPjbvTHmKTHDKn1bQuWEzm1DVryRhmiYYPj7BsHbLrycVnHT1nAnOuW0duyNOTHDYnjNQnjTkn19dPHNLnjbYPjTKnHEkPHDknjT1rjEznHE3P1mQP9DKnikQnEDKTEDVTEDKTEDdra3zPj98PWm8nHm1THTKnTDKnikQnE7exEDQnjT1P9DQnjTQPWbzTHN3rj-hPAczsHEvPj9VPj9vPBY3uW--sHmLm103uWTYrHE1nkDKPT7LXjK-nHPBuHw6PHn1PW9YPWmKmyGoRvRWpA7YTHTKnzkQPWNYsjDdn1NvTHDKUMR_UT7-nH9zmHKBnWR-PyN3uym1%22%2C%22lego_sid%22%3A%22143716796212839938880118877%22%2C%22platformCompany%22%3A%22ajk%22%2C%22lego_show_track_url_base%22%3A%22https%3A%5C%2F%5C%2Fadtrack.58.com%5C%2F1001692%5C%2F%22%2C%22lego_version%22%3A%221.0.1%22%2C%22legoHuiDu%22%3Atrue%7D%7D",
          "tid": "5889f4b2-4648-4866-8f9e-67c78f049433",
          "isLiving": false,
          "isVR": false,
          "distanceDesc": "",
          "isGoldOwner": false
        },
        {
          "infoid": "2003739993267210",
          "cateName": "整租",
          "cateID": "11",
          "title": "天河SOHO公寓 绿码社区 24H门禁 拎包入住 免费健身",
          "price": "850",
          "img": "https://pic1.ajkimg.com/display/anjuke/8a655f5a3494a1ccc5b796b6c74aace1/240x180c.jpg?t=1&srotate=1",
          "unit": "元/月",
          "coll": "0",
          "subwayDistance": "距21号线-天河智慧城站步行2.0km",
          "fangwuLiangDian": [
            {
              "text": "整租",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "朝南",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "近地铁",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "有电梯",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "精装修",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            }
          ],
          "area": "18㎡",
          "toward": "朝南",
          "shangquan": "岑村",
          "isAnxuan": "0",
          "location": false,
          "source_type": "1",
          "isauction": "6",
          "auctionShowText": "",
          "legoAuction": "%7B%22isauction%22%3A%226%22%2C%22lego%22%3A%7B%22lego_tid%22%3A%22231f431e-7abd-494f-be74-693ee0d855c3%22%2C%22lego_ad_click_url%22%3A%22https%3A%5C%2F%5C%2Flegoclick.58.com%5C%2Fjump%3Ftarget%3DpZwY0ZnlsztdraOWUvYKuaYYnHP6nh76uaYvnH9LsHEzP1nVrARWnBdBP1EYnAmYPWT3PH0KnHTQnjT1rHDkTEDznjT1P1nOrHb1nWmLnWDkTHTKnHTkTHD_nHDKnzkQPWNYsjDdn1NvTHDvnWn3n1b3PWNLPWTKnEZ-oCM-_mg-oJsMGO4hBs4-_c4MJcf-8CtVOsBeOmBgl2ACOJB6Oer2OGa0OeiBTHDKnEDKnk7fmdnL0HKniyVxILu2EL-dwdFWwYFLPdIOELR7THDYnjNkrjEvPHTvrjDOn1n3nWEKP9DKnE7Wm1EQryNvmiYQrjcYsHEzPvnVmhckraY1myw-mH7bnHTzPjDKnHEkPHT3Pjmdnj9znjnYPH9dP9DQPjTdnj9YPWNkP1nvPjm1rj0zTEDQsjDQTEDKTiYKTEDKTHN3sWcYra3vPB3QPWnKnTDkTEDQsjDQTgVqTHDknjNYTHDknjc1PHbKnWnQuWE1nyNVPv7BuaYYrHwhsyF-P1EVPWb1uyNkuj9dPyn1TEDYTgI3nANQnvF-PADdn1nvrjEvP976phVguyPCmgEKnkD1sjDvPHE_nHN1PHmKnE78IyQ_THcvnvmYnWTLPh7BnW-WPhE%22%2C%22lego_sid%22%3A%22143716796212839938880118877%22%2C%22platformCompany%22%3A%22ajk%22%2C%22lego_show_track_url_base%22%3A%22https%3A%5C%2F%5C%2Fadtrack.58.com%5C%2F1002359%5C%2F%22%2C%22lego_version%22%3A%221.0.1%22%2C%22legoHuiDu%22%3Atrue%7D%7D",
          "tid": "231f431e-7abd-494f-be74-693ee0d855c3",
          "isLiving": false,
          "isVR": false,
          "distanceDesc": "",
          "isGoldOwner": false
        },
        {
          "infoid": "1874905766077441",
          "cateName": "整租",
          "cateID": "11",
          "title": "非中介房屋直租6号线高塘石地铁口交通便利停车方便",
          "price": "950",
          "img": "https://pic1.ajkimg.com/display/anjuke/a934ea41f11e200d10a3792351c2500f/240x180c.jpg?t=1&srotate=1",
          "unit": "元/月",
          "coll": "0",
          "subwayDistance": "距6号线-高塘石站步行1.0km",
          "fangwuLiangDian": [
            {
              "text": "整租",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "朝南",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "近地铁",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "精装修",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            }
          ],
          "area": "25㎡",
          "toward": "朝南",
          "shangquan": "岑村",
          "isAnxuan": "0",
          "location": false,
          "source_type": "1",
          "isauction": "6",
          "auctionShowText": "",
          "legoAuction": "%7B%22isauction%22%3A%226%22%2C%22lego%22%3A%7B%22lego_tid%22%3A%22fbaecd62-ff79-44ba-b9b8-5b51f4e39b9c%22%2C%22lego_ad_click_url%22%3A%22https%3A%5C%2F%5C%2Flegoclick.58.com%5C%2Fjump%3Ftarget%3DpZwY0ZnlsztdraOWUvYKuaYYnHP6nh76uaYvnH9LsHEzP1nVrARWnBdBP1EYnAmYPWT3PH0KnHTQn1cLnWTvTEDQrj0YrHTdP1mvnj0LPjEQTHTKnHTkTHD_nHDKnzkQPWNYsjDdn1NvTHDvnWn3n1b3PWNLPWTKn9Z-oCM-_mg-oJsMGO4hBs4-_c4MJcf-8CtVOsBeOmBgl2ACOJB6Oer2OGa0OeiBTHDKnEDKnk7fmdnL0HKniyVxILu2EL-dwdFWwYFLPdIOELR7THDYnjEOrHbLnHcvnWE1PHnzrjTKP9DKnEDLuWu-n1cYuaYOPjD3sHEvmycVrH0dniYzn10krHNznvRbPjnKnHEkPjbOrH0QnWNOPjbOP1cYrEDQPjTYrHbOP1DzPWbQPjmznHEdTEDQsjDQTEDKTiYKTEDKTHN3sWcYra3vPB3QPWnKnTDkTEDQsjDQTgVqTHDknjNYTHDknjc1PHbKuhF6uyPbPWcVuhmLriYYPAF6sycOmW9VPycdnymYuHnOmW-WTEDYTgI3nANQnvF-PADdn1nvrjEvP976phVguyPCmgEKnkD1sjDvPHE_nHN1PHmKnE78IyQ_THPhnWEOmyDOuhuhnj76PvN%22%2C%22lego_sid%22%3A%22143716796212839938880118877%22%2C%22platformCompany%22%3A%22ajk%22%2C%22lego_show_track_url_base%22%3A%22https%3A%5C%2F%5C%2Fadtrack.58.com%5C%2F1002359%5C%2F%22%2C%22lego_version%22%3A%221.0.1%22%2C%22legoHuiDu%22%3Atrue%7D%7D",
          "tid": "fbaecd62-ff79-44ba-b9b8-5b51f4e39b9c",
          "isLiving": false,
          "isVR": false,
          "distanceDesc": "",
          "isGoldOwner": false
        },
        {
          "infoid": "2030159046090762",
          "cateName": "整租",
          "cateID": "11",
          "title": "非中介房屋直租  6号线柯木塱地铁口200米 交通便利",
          "price": "999",
          "img": "https://pic1.ajkimg.com/display/anjuke/efb236fc0d480dbb40716cac52b34779/240x180c.jpg?t=1&srotate=1",
          "unit": "元/月",
          "coll": "0",
          "subwayDistance": "距6号线-柯木塱站步行234m",
          "fangwuLiangDian": [
            {
              "text": "整租",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "朝南",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "近地铁",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "精装修",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            }
          ],
          "area": "25㎡",
          "toward": "朝南",
          "shangquan": "岑村",
          "isAnxuan": "0",
          "location": false,
          "source_type": "1",
          "isauction": "6",
          "auctionShowText": "",
          "legoAuction": "%7B%22isauction%22%3A%226%22%2C%22lego%22%3A%7B%22lego_tid%22%3A%224fbbb02f-8901-4471-bdd3-0eeb6f5a9be0%22%2C%22lego_ad_click_url%22%3A%22https%3A%5C%2F%5C%2Flegoclick.58.com%5C%2Fjump%3Ftarget%3DpZwY0ZnlsztdraOWUvYKuaYYnHP6nh76uaYvnH9LsHEzP1nVrARWnBdBP1EYnAmYPWT3PH0KnHTQn1cLnWTvTEDznjnknHNOnjEvnjbkP1mzTHTKnHTkTHD_nHDKnzkQPWNYsjDdn1NvTHDvnWn3n1b3PWNLPWTKnkZ-oCM-_mg-oJsMGO4hBs4-_c4MJcf-8CtVOsBeOmBgl2ACOJB6Oer2OGa0OeiBTHDKnEDKnk7fmdnL0HKniyVxILu2EL-dwdFWwYFLPdIOELR7THDYnjEOrHbYPH0QPWD3P1bdPHcKP9DKnEDQmHFBmHPhuBdWPvu6sHw6ujDVrHn3uBdBmHNzPycznWuBrynKnHEkPjbOrHEdP1nYPWEzrjbzrTDQPjTYrHbOPjNLnWmzPHEzrjE3TEDQsjDQTEDKTiYKTEDKTHN3sWcYra3vPB3QPWnKnTDkTEDQsjDQTgVqTHDknjNYTHDknjc1PHbKPAuBmhcknhmVrjbkniYYPj0QsyFbujnVnAR-mWuhPyDOmhNkTEDYTgI3nANQnvF-PADdn1nvrjEvP976phVguyPCmgEKnkD1sjDvPHE_nHN1PHmKnE78IyQ_THNvuyuWPWcQrAczmHnLmhN%22%2C%22lego_sid%22%3A%22143716796212839938880118877%22%2C%22platformCompany%22%3A%22ajk%22%2C%22lego_show_track_url_base%22%3A%22https%3A%5C%2F%5C%2Fadtrack.58.com%5C%2F1002359%5C%2F%22%2C%22lego_version%22%3A%221.0.1%22%2C%22legoHuiDu%22%3Atrue%7D%7D",
          "tid": "4fbbb02f-8901-4471-bdd3-0eeb6f5a9be0",
          "isLiving": false,
          "isVR": false,
          "distanceDesc": "",
          "isGoldOwner": false
        },
        {
          "infoid": "1947387101961230",
          "cateName": "整租",
          "cateID": "11",
          "title": "高塘石阳光大单间近金发科技园   光线好  新房开租",
          "price": "950",
          "img": "https://pic1.ajkimg.com/display/anjuke/e9b11c805ef6ccde5a5bec02518ddaa7/240x180c.jpg?t=1&srotate=1",
          "unit": "元/月",
          "coll": "0",
          "subwayDistance": "距6号线-高塘石站步行1.0km",
          "fangwuLiangDian": [
            {
              "text": "整租",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "南北",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "近地铁",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "精装修",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            }
          ],
          "area": "25㎡",
          "toward": "南北",
          "shangquan": "岑村",
          "isAnxuan": "0",
          "location": false,
          "source_type": "1",
          "isauction": "6",
          "auctionShowText": "",
          "legoAuction": "%7B%22isauction%22%3A%226%22%2C%22lego%22%3A%7B%22lego_tid%22%3A%22b621903a-cec1-47bb-9eda-0f3f6f1f5007%22%2C%22lego_ad_click_url%22%3A%22https%3A%5C%2F%5C%2Flegoclick.58.com%5C%2Fjump%3Ftarget%3DpZwY0ZnlsztdraOWUvYKuaYYnHP6nh76uaYvnH9LsHEzP1nVrARWnBdBP1EYnAmYPWT3PH0KnWTkrjnzPWD1TEDQrHELn19LnHTQrHmQnWnkTHTKnHTkTHD_nHDKnzkQPWNYsjDdn1NvTHDvnWn3n1b3PWNLPWTKPTZ-oCM-_mg-oJsMGO4hBs4-_c4MJcf-8CtVOsBeOmBgl2ACOJB6Oer2OGa0OeiBTHDKnEDKnk7fmdnL0HKniyVxILu2EL-dwdFWwYFLPdIOELR7THDYnjELnjmdnWE3PjEknj0YnWEKP9DKnE7hnWcOPjPhPzYQnWT3sHwbnvcVmyDznzYQuHKbrHc3mWTYmhDKnHEkPj0kPWNzPHTOnHELPHEdP9DQPjTYP1TvPHcdnjDQP191PW9kTEDQsjDQTEDKTiYKTEDKTHN3sWcYra3vPB3QPWnKnTDkTEDQsjDQTgVqTHDknjNYTHDknjc1PHbKmWmznHbknvDVmvRWniYYPvFBsH--uADVnAm1uWuhnymdnjTLTEDYTgI3nANQnvF-PADdn1nvrjEvP976phVguyPCmgEKnkD1sjDvPHE_nHN1PHmKnE78IyQ_TyE3myRhrHckrAcLmyNLuH0%22%2C%22lego_sid%22%3A%22143716796212839938880118877%22%2C%22platformCompany%22%3A%22ajk%22%2C%22lego_show_track_url_base%22%3A%22https%3A%5C%2F%5C%2Fadtrack.58.com%5C%2F1002359%5C%2F%22%2C%22lego_version%22%3A%221.0.1%22%2C%22legoHuiDu%22%3Atrue%7D%7D",
          "tid": "b621903a-cec1-47bb-9eda-0f3f6f1f5007",
          "isLiving": false,
          "isVR": false,
          "distanceDesc": "",
          "isGoldOwner": false
        },
        {
          "infoid": "2036187125539851",
          "cateName": "整租",
          "cateID": "11",
          "title": "方便上班族的房子，便宜大单间出租，配置齐，安静安全，拎包入住",
          "price": "380",
          "img": "https://pic1.ajkimg.com/display/anjuke/06bc5b5c71ed98a736b93d42241138f9/240x180c.jpg?t=1&srotate=1",
          "unit": "元/月",
          "coll": "0",
          "subwayDistance": "",
          "fangwuLiangDian": [
            {
              "text": "整租",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "东南",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            }
          ],
          "area": "25㎡",
          "toward": "东南",
          "shangquan": "岑村",
          "isAnxuan": "0",
          "location": false,
          "source_type": "1",
          "isauction": "6",
          "auctionShowText": "",
          "legoAuction": "%7B%22isauction%22%3A%226%22%2C%22lego%22%3A%7B%22lego_tid%22%3A%22fb0c2a9b-1640-46e3-b01e-bc14652936c6%22%2C%22lego_ad_click_url%22%3A%22https%3A%5C%2F%5C%2Flegoclick.58.com%5C%2Fjump%3Ftarget%3DpZwY0ZnlsztdraOWUvYKuaYYnHP6nh76uaYvnH9LsHEzP1nVrARWnBdBP1EYnAmYPWT3PH0Kn1TQrjmYrTDKnWT1PWD3P1DzPHN1rH9dnEDkTHDknTDQsjDQTHn_nHmdPakQPHndP9DQPWc1rjnOrjmdP1mkTHNKOplFOUA7OplBOlXxOCB4OUa5OlBsOUJ5sXi38Syc-SB6JrpCCXX1B8pEMrXYC9DQTHDKTHnKUvPHPLDkHD-ogLIvibPOINIimYIaI1IgXNPdwEDQPjTYn1mYPWnvrjbdnjmYnjmYTHmKTHDKnycYrHcOuHbVnHIbnaYYrH9ksHb3rjmVuWnQn1FWuynvnymdTHDYnjE1PWEvn10kn10vP1TYnjTKnHEkPjnvPjm1P1Tdrjm1P19zPTDKnikQnEDKTEDVTEDKTEDdra3zPj98PWm8nHm1THTKnTDKnikQnE7exEDQnjTdPTDQnjTzn1NOTyuBnAnzmH-BsHDvPjTVPju-nzdBnj7-syFWnHEvPHcOn1uWP9DKPT7LXjK-nHPBuHw6PHn1PW9YPWmKmyGoRvRWpA7YTHnKnzkQPWNYsjDdn1NvTHDKUMR_UT7WPv76uARWnjnzPW-bPW-b%22%2C%22lego_sid%22%3A%22143716796212839938880118877%22%2C%22platformCompany%22%3A%22ajk%22%2C%22lego_show_track_url_base%22%3A%22https%3A%5C%2F%5C%2Fadtrack.58.com%5C%2F1002359%5C%2F%22%2C%22lego_version%22%3A%221.0.1%22%2C%22legoHuiDu%22%3Atrue%7D%7D",
          "tid": "fb0c2a9b-1640-46e3-b01e-bc14652936c6",
          "isLiving": false,
          "isVR": false,
          "distanceDesc": "",
          "isGoldOwner": false
        },
        {
          "infoid": "1968412523125771",
          "cateName": "整租",
          "cateID": "11",
          "title": "地铁6号线柯木塱背坪窖屋地西街小区 1室0厅1卫 790元月",
          "price": "890",
          "img": "https://pic1.ajkimg.com/display/anjuke/dbf4e0d05f4ad0a62c212977a658da09/240x180c.jpg?t=1&srotate=1",
          "unit": "元/月",
          "coll": "0",
          "subwayDistance": "距6号线-柯木塱站步行234m",
          "fangwuLiangDian": [
            {
              "text": "整租",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "南北",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "近地铁",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "有电梯",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "精装修",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            }
          ],
          "area": "26㎡",
          "toward": "南北",
          "shangquan": "岑村",
          "isAnxuan": "0",
          "location": false,
          "source_type": "1",
          "isauction": "6",
          "auctionShowText": "",
          "legoAuction": "%7B%22isauction%22%3A%226%22%2C%22lego%22%3A%7B%22lego_tid%22%3A%22de8ea6ca-4212-48fb-813a-aa649103ff2f%22%2C%22lego_ad_click_url%22%3A%22https%3A%5C%2F%5C%2Flegoclick.58.com%5C%2Fjump%3Ftarget%3DpZwY0ZnlsztdraOWUvYKuaYYnHP6nh76uaYvnH9LsHEzP1nVrARWnBdBP1EYnAmYPWT3PH0KnWTkrjnzPWD1TEDQrHm3PjDzPHc1nHcdP10QTHTKnHTkTHD_nHDKnzkQPWNYsjDdn1NvTHDvnWn3n1b3PWNLPWTKP9Z-oCM-_mg-oJsMGO4hBs4-_c4MJcf-8CtVOsBeOmBgl2ACOJB6Oer2OGa0OeiBTHDKnEDKnk7fmdnL0HKniyVxILu2EL-dwdFWwYFLPdIOELR7THDYnjEkrj9QnjEYn1DvPHmOPWTKP9DKnE7-PHuhPjT1PzdBmWcvsHwBP1cVrjuBnid-PWmYrjb3uW9QmHNKnHEkPjT3rjDkPjNLPjcvn1cOP9DQPjTYnj93nHTYPjb3P1mdrjcYTEDQsjDQTEDKTiYKTEDKTHN3sWcYra3vPB3QPWnKnTDkTEDQsjDQTgVqTHDknjNYTHDknjc1PHbKuAN3uyDvmvDVPjcQnBYYrAuBsH9QnvDVmyDvPjbQnjPhuWFhTEDYTgI3nANQnvF-PADdn1nvrjEvP976phVguyPCmgEKnkD1sjDvPHE_nHN1PHmKnE78IyQ_TycQrHIBnW6WmWPBnvcYnAN%22%2C%22lego_sid%22%3A%22143716796212839938880118877%22%2C%22platformCompany%22%3A%22ajk%22%2C%22lego_show_track_url_base%22%3A%22https%3A%5C%2F%5C%2Fadtrack.58.com%5C%2F1002359%5C%2F%22%2C%22lego_version%22%3A%221.0.1%22%2C%22legoHuiDu%22%3Atrue%7D%7D",
          "tid": "de8ea6ca-4212-48fb-813a-aa649103ff2f",
          "isLiving": false,
          "isVR": false,
          "distanceDesc": "",
          "isGoldOwner": false
        },
        {
          "infoid": "1489428273748995",
          "cateName": "整租",
          "cateID": "11",
          "title": "不走小巷，可短租，真实图片，安全安静，干净卫生，配置齐全！",
          "price": "650",
          "img": "https://pic1.ajkimg.com/display/anjuke/e6e70383000905e051893439c0cee9b2/240x180c.jpg?t=1&srotate=1",
          "unit": "元/月",
          "coll": "0",
          "subwayDistance": "",
          "fangwuLiangDian": [
            {
              "text": "安选房源",
              "textColor": "#3EC77B",
              "bgColor": "#3EC77B"
            },
            {
              "text": "整租",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "东南",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "精装修",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            }
          ],
          "area": "48㎡",
          "toward": "东南",
          "shangquan": "岑村",
          "isAnxuan": "1",
          "location": false,
          "source_type": "1",
          "isauction": "1",
          "auctionShowText": "",
          "legoAuction": "%7B%22isauction%22%3A%221%22%2C%22lego%22%3A%7B%22lego_tid%22%3A%22%22%2C%22lego_ad_click_url%22%3A%22%22%2C%22lego_sid%22%3A%22143716796212839938880118877%22%2C%22platformCompany%22%3A%22ajk%22%2C%22lego_show_track_url_base%22%3A%22https%3A%5C%2F%5C%2Fadtrack.58.com%5C%2F1001692%5C%2F%22%2C%22lego_version%22%3A%221.0.1%22%2C%22legoHuiDu%22%3Atrue%7D%7D",
          "tid": "",
          "isLiving": false,
          "isVR": false,
          "distanceDesc": "",
          "isGoldOwner": false
        },
        {
          "infoid": "2027446650346501",
          "cateName": "整租",
          "cateID": "11",
          "title": "棠下brt 天河公园 路边单间 五号线 天辉大厦 信息港 阳",
          "price": "950",
          "img": "https://pic1.ajkimg.com/display/anjuke/37519340ec80b0cf66a5ec6c045703ed/240x180c.jpg?t=1&srotate=1",
          "unit": "元/月",
          "coll": "0",
          "subwayDistance": "",
          "fangwuLiangDian": [
            {
              "text": "整租",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "朝南",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "有电梯",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "精装修",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            }
          ],
          "area": "28㎡",
          "toward": "朝南",
          "shangquan": "岑村",
          "isAnxuan": "0",
          "location": false,
          "source_type": "1",
          "isauction": "1",
          "auctionShowText": "",
          "legoAuction": "%7B%22isauction%22%3A%221%22%2C%22lego%22%3A%7B%22lego_tid%22%3A%22%22%2C%22lego_ad_click_url%22%3A%22%22%2C%22lego_sid%22%3A%22143716796212839938880118877%22%2C%22platformCompany%22%3A%22ajk%22%2C%22lego_show_track_url_base%22%3A%22https%3A%5C%2F%5C%2Fadtrack.58.com%5C%2F1001692%5C%2F%22%2C%22lego_version%22%3A%221.0.1%22%2C%22legoHuiDu%22%3Atrue%7D%7D",
          "tid": "",
          "isLiving": false,
          "isVR": false,
          "distanceDesc": "",
          "isGoldOwner": false
        }
      ]
    }
  }