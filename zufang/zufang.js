var ZUFANG_0 = {
    "code": "0",
    "message": "OK",
    "data": {
      "isToast": false,
      "activity": {
        "title": ""
      },
      "configs": {
        "banner": [],
        "houseSubject": []
      },
      "tkd": {
        "title": "广佛线沙涌整租租房网，广州广佛线沙涌整租租房信息，广佛线沙涌整租房屋出租价格-广州58安居客",
        "keywords": "广佛线沙涌整租租房，广佛线沙涌整租出租房，广佛线沙涌整租租房子",
        "description": "安居客广州租房网为您提供广佛线沙涌整租租房信息,包括广州二手房出租、新房出租，租房子,找广州出租房,就上58安居客广州租房网。"
      },
      "gongyu": {
        "isShow": true
      },
      "goldOwnerCity": false,
      "list": [
        {
          "infoid": "2058575726074891",
          "cateName": "整租",
          "cateID": "11",
          "title": "北大资源博雅1898 2室2厅1卫 5500元月 精装修",
          "price": "5500",
          "img": "https://pic1.ajkimg.com/display/anjuke/a27dc91238d00b94c213e3fea44b9b41/240x180c.jpg?t=1&srotate=1",
          "unit": "元/月",
          "coll": "0",
          "subwayDistance": "距广佛线-沙涌站步行1.8km",
          "fangwuLiangDian": [
            {
              "text": "整租",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "西南",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "近地铁",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "有电梯",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "精装修",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            }
          ],
          "area": "83㎡",
          "toward": "西南",
          "shangquan": "芳村",
          "isAnxuan": "0",
          "location": false,
          "source_type": "1",
          "isauction": "2",
          "auctionShowText": "广告",
          "legoAuction": "%7B%22isauction%22%3A%222%22%2C%22lego%22%3A%7B%22lego_tid%22%3A%22406b4791-23d9-4423-b3ea-4ee446e962fe%22%2C%22lego_ad_click_url%22%3A%22https%3A%5C%2F%5C%2Flegoclick.58.com%5C%2Fjump%3Ftarget%3DpZwY0ZnlsztdraOWUvYKuadWujDYmHb1uBYQrjDvsHE1PjNVrANdPiY1ry7-PjR6Pj93nANKPH03PHcOPWE3nHE1PH0KTHckPH9dP1NLnWmkP1E3rHDKnHn3rHbKnHn3rHbKnikQnED1THDvnWNYPWN1n10dP1EKnE7AEzdPsN72iSXMMSpcfSpEMrXYC8yc-SB6Jrh6VEDQTHDKTHnKUvPHPLDkHD-ogLIvibPOINIimYIaI1IgXNPdwEDQPjDknjbdPHcOP1D3P1D3PjmYTHmK0A7zmyd1n1NYrWcdrHm_0v6GrW7tnBQkmgF6Ugn1PHnln19KnEDvnvw-mWPBnidWuy7BsHwbPyDVmWbvmzd-P1K-PvD3PjE1uynKnHEQnjTOPHNzrHb1nWm1nWTvPTDQPjDknjbdPHcOP10LPjn3P1ckTEDQsjDQTEDKTiYKTEDKTHD3nz3vsWD3nz33n9DkTHTKTHD_nHDKXLYKnHTkn1mKnHTknHmOn9DYnjuBPj0OniYznvEOsHEYnWnVmWP-miYYuyNYPju-rHmzuhNKTHEKIL9kuHD1mhNYmHN1n1m3PjmvTy7JpdI-mv66ITDkTHn_nHmdPBkzP1TkTHDKUMR_UTD3P1DvnvFbPAw-rjmknhD1%22%2C%22lego_sid%22%3A%22130071184213052992732541465%22%2C%22platformCompany%22%3A%22ajk%22%2C%22lego_show_track_url_base%22%3A%22https%3A%5C%2F%5C%2Fadtrack.58.com%5C%2F1001692%5C%2F%22%2C%22lego_version%22%3A%221.0.1%22%2C%22legoHuiDu%22%3Atrue%7D%7D",
          "tid": "406b4791-23d9-4423-b3ea-4ee446e962fe",
          "isLiving": false,
          "isVR": false,
          "distanceDesc": "",
          "isGoldOwner": false
        },
        {
          "infoid": "2011895062389768",
          "cateName": "整租",
          "cateID": "11",
          "title": "芳华阁 2室2厅1卫 2700元月 精装修 98平",
          "price": "2700",
          "img": "https://pic1.ajkimg.com/display/anjuke/f066b72f8635068665566fe19dc9a076/240x180c.jpg?t=1&srotate=1",
          "unit": "元/月",
          "coll": "0",
          "subwayDistance": "距广佛线-沙涌站步行1.5km",
          "fangwuLiangDian": [
            {
              "text": "整租",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "朝南",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "近地铁",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "精装修",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            }
          ],
          "area": "98㎡",
          "toward": "朝南",
          "shangquan": "芳村",
          "isAnxuan": "0",
          "location": false,
          "source_type": "1",
          "isauction": "2",
          "auctionShowText": "",
          "legoAuction": "%7B%22isauction%22%3A%222%22%2C%22lego%22%3A%7B%22lego_tid%22%3A%22cf48fc2e-a9a5-45b0-8085-5289510007a1%22%2C%22lego_ad_click_url%22%3A%22https%3A%5C%2F%5C%2Flegoclick.58.com%5C%2Fjump%3Ftarget%3DpZwY0ZnlsztdraOWUvYKuadWujDYmHb1uBYQrjDvsHE1PjNVrANdPiY1ry7-PjR6Pj93nANKPH03PHcOPWE3nHE1PH0KTHcknHD3rHNkPWc1rjbLPW9KnHn3rHbKnHn3rHbKnikQnED1THDvnWNYPWN1n10dP1EKn97AEzdPsN72iSXMMSpcfSpEMrXYC8yc-SB6Jrh6VEDQTHDKTHnKUvPHPLDkHD-ogLIvibPOINIimYIaI1IgXNPdwEDQPjDknj9QPH0OPWDkPjDOnWTkTHmK0A7zmyd1n1NYrWcdrHm_0v6GrW7tnBQkmgF6Ugn1PHnln19KnEDkujnYnW76mBdbmW9YsHEzPvcVmhc1niYvuhPBuWNkuWNvrAnKnHEQnjT3nHNLrH03nWn3PHmvPTDQPjDknj9QPH0OPWcLnHbzn1ckTEDQsjDQTEDKTiYKTEDKTHD3nz3vsWD3nz33n9DkTHTKTHD_nHDKXLYKnHTkn1mKnHTknHmOn97WuWE3uhnzuid6ryDdsHEdmWTVrjT3PiYdnW9OPHDknjTLmHDKTHEKIL9kuHD1mhNYmHN1n1m3PjmvTy7JpdI-mv66ITDkTHn_nHmdPBkzP1TkTHDKUMR_UT7WnHN1myDduWNQPHIBPW-W%22%2C%22lego_sid%22%3A%22130071184213052992732541465%22%2C%22platformCompany%22%3A%22ajk%22%2C%22lego_show_track_url_base%22%3A%22https%3A%5C%2F%5C%2Fadtrack.58.com%5C%2F1001692%5C%2F%22%2C%22lego_version%22%3A%221.0.1%22%2C%22legoHuiDu%22%3Atrue%7D%7D",
          "tid": "cf48fc2e-a9a5-45b0-8085-5289510007a1",
          "isLiving": false,
          "isVR": false,
          "distanceDesc": "",
          "isGoldOwner": false
        },
        {
          "infoid": "2058750177025024",
          "cateName": "整租",
          "cateID": "11",
          "title": "芳华阁 2室2厅1卫 2700元月 51平 南北通透",
          "price": "2700",
          "img": "https://pic1.ajkimg.com/display/anjuke/51b52b498f3bb7f8451ae3a06e6a783f/240x180c.jpg?t=1&srotate=1",
          "unit": "元/月",
          "coll": "0",
          "subwayDistance": "距广佛线-沙涌站步行1.5km",
          "fangwuLiangDian": [
            {
              "text": "整租",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "朝南",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "近地铁",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "精装修",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            }
          ],
          "area": "51㎡",
          "toward": "朝南",
          "shangquan": "芳村",
          "isAnxuan": "0",
          "location": false,
          "source_type": "1",
          "isauction": "2",
          "auctionShowText": "",
          "legoAuction": "%7B%22isauction%22%3A%222%22%2C%22lego%22%3A%7B%22lego_tid%22%3A%22fa88ea13-e63e-44c1-b516-e790eee5a11a%22%2C%22lego_ad_click_url%22%3A%22https%3A%5C%2F%5C%2Flegoclick.58.com%5C%2Fjump%3Ftarget%3DpZwY0ZnlsztdraOWUvYKuadWujDYmHb1uBYQrjDvsHE1PjNVrANdPiY1ry7-PjR6Pj93nANKPH03PHcOPWE3nHE1PH0KTHckPH9LPHTQP10knWNknWEKnHn3rHbKnHn3rHbKnikQnED1THDvnWNYPWN1n10dP1EKnk7AEzdPsN72iSXMMSpcfSpEMrXYC8yc-SB6Jrh6VEDQTHDKTHnKUvPHPLDkHD-ogLIvibPOINIimYIaI1IgXNPdwEDQPjDQPHn3n1EYnHnOnjEzrjDvTHmK0A7zmyd1n1NYrWcdrHm_0v6GrW7tnBQkmgF6Ugn1PHnln19KnEDLnhnYnyEzradbnH0QsHwbmvcVmW7huBY3nANdmHwhPW9OPHEKnHEQnHN1rjnYPjcvrHTvnWDYPTDQPjDQPHn3n1EYnH0zPHb1nHNzTEDQsjDQTEDKTiYKTEDKTHD3nz3vsWD3nz33n9DkTHTKTHD_nHDKXLYKnHTkn1mKnHTknHmOn97hmH93uyDQnzd-PWP-sHEYm1DVmWNQPBd-P1bkuyR-PyDQnyDKTHEKIL9kuHD1mhNYmHN1n1m3PjmvTy7JpdI-mv66ITDkTHn_nHmdPBkzP1TkTHDKUMR_UTD1mWmYuj6Buju6n1u6PWKb%22%2C%22lego_sid%22%3A%22130071184213052992732541465%22%2C%22platformCompany%22%3A%22ajk%22%2C%22lego_show_track_url_base%22%3A%22https%3A%5C%2F%5C%2Fadtrack.58.com%5C%2F1001692%5C%2F%22%2C%22lego_version%22%3A%221.0.1%22%2C%22legoHuiDu%22%3Atrue%7D%7D",
          "tid": "fa88ea13-e63e-44c1-b516-e790eee5a11a",
          "isLiving": false,
          "isVR": false,
          "distanceDesc": "",
          "isGoldOwner": false
        },
        {
          "infoid": "1998992706806799",
          "cateName": "整租",
          "cateID": "11",
          "title": "花地湾招村北外约光线特好通风两房一厅",
          "price": "1700",
          "img": "https://pic1.ajkimg.com/display/anjuke/86f147274b588795b539cdb33ba4dc1d/240x180c.jpg?t=1&srotate=1",
          "unit": "元/月",
          "coll": "0",
          "subwayDistance": "距广佛线-沙涌站步行1.6km",
          "fangwuLiangDian": [
            {
              "text": "整租",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "东南",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "近地铁",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "精装修",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            }
          ],
          "area": "48㎡",
          "toward": "东南",
          "shangquan": "花地湾",
          "isAnxuan": "0",
          "location": false,
          "source_type": "1",
          "isauction": "2",
          "auctionShowText": "",
          "legoAuction": "%7B%22isauction%22%3A%222%22%2C%22lego%22%3A%7B%22lego_tid%22%3A%22d72ddf07-3f58-4b15-a506-1caac1f21ef9%22%2C%22lego_ad_click_url%22%3A%22https%3A%5C%2F%5C%2Flegoclick.58.com%5C%2Fjump%3Ftarget%3DpZwY0ZnlsztdraOWUvYKuadWujDYmHb1uBYQrjDvsHE1PjNVrANdPiY1ry7-PjR6Pj93nANKP1TvrjndPjcOP1mdnWNKTHDOrH9OrHcLnjm3njmLrHbKrH0knTDOP1TkTHD_nHDKnkDQPWcdPjmdn1nLPH0YTHEKwbnVHidKibfMGO4hBs4hbF1MV2s-BFxCCpWGCUNKnEDQTED1TyqWN1IQnDQFpdqLIbGjXgRZNhPZEM0LRL-jINNKnHEQnHNQnWELPWn3PHm3nHEkrTDvTgK60h7V01ndPjCzPHbvsZPCpHCQxjc_0A7zmyd1n1N1rWn3THDKryDknjN3PvNVrAn3PBYYmymQsyDOuHEVPWTzm19zrjDzmHDdTHDYnHDdnHcYP1mdPjT3P1ELPHcKnHEQnHNQnWELPWEYrjmknjTvPTDKnikQnEDKTEDVTEDKTEDQrjn8PB3Qrjn8rjcKnTDkTEDQsjDQTgVqTHDknjnvTHDknjDvrHcKuj0zuAwhnj0VnvmdraYYmWDdsyDdnjmVnyP6mynQuWcQuymOTEDYTgI3nANQnvF-PADdn1nvrjEvP976phVguyPCmgEKnTD1sjDvPHm_nHDdP1mKnE78IyQ_TyD3uhcLnAw-nWTzny76PhN%22%2C%22lego_sid%22%3A%22130071184213052992732541465%22%2C%22platformCompany%22%3A%22ajk%22%2C%22lego_show_track_url_base%22%3A%22https%3A%5C%2F%5C%2Fadtrack.58.com%5C%2F1001692%5C%2F%22%2C%22lego_version%22%3A%221.0.1%22%2C%22legoHuiDu%22%3Atrue%7D%7D",
          "tid": "d72ddf07-3f58-4b15-a506-1caac1f21ef9",
          "isLiving": false,
          "isVR": false,
          "distanceDesc": "",
          "isGoldOwner": false
        },
        {
          "infoid": "2058452082500616",
          "cateName": "整租",
          "cateID": "11",
          "title": "振业天颂 1室1厅1卫 3300元月 38平 电梯房",
          "price": "3300",
          "img": "https://pic1.ajkimg.com/display/anjuke/d09aa325417e4c6616b33a5228242bb9/240x180c.jpg?t=1&srotate=1",
          "unit": "元/月",
          "coll": "0",
          "subwayDistance": "距广佛线-沙涌站步行1.9km",
          "fangwuLiangDian": [
            {
              "text": "安选房源",
              "textColor": "#3EC77B",
              "bgColor": "#3EC77B"
            },
            {
              "text": "整租",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "朝西",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "近地铁",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "有电梯",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            }
          ],
          "area": "38㎡",
          "toward": "朝西",
          "shangquan": "芳村",
          "isAnxuan": "1",
          "location": false,
          "source_type": "1",
          "isauction": "2",
          "auctionShowText": "",
          "legoAuction": "%7B%22isauction%22%3A%222%22%2C%22lego%22%3A%7B%22lego_tid%22%3A%220e0ebfbc-8b7a-4c2c-9c73-e4dd9b656ace%22%2C%22lego_ad_click_url%22%3A%22https%3A%5C%2F%5C%2Flegoclick.58.com%5C%2Fjump%3Ftarget%3DpZwY0ZnlsztdraOWUvYKuadWujDYmHb1uBYQrjDvsHE1PjNVrANdPiY1ry7-PjR6Pj93nANKP1mkn1EdPHcOPjTdPHbKTHckPH9YPHckrjcdnjTvnHmKrH0knTDOP1TkTHD_nHDKnkDQPWcdPjmdn1nLPH0YTHNKwbnVHidKibfMGO4hBs4hbF1MV2s-BFxCCpWGCUNKnEDQTED1TyqWN1IQnDQFpdqLIbGjXgRZNhPZEM0LRL-jINNKnHEQnH9LrjbdrjDznWmLPWczPTDvTgK60h7V01ndPjCzPHbvsZPCpHCQxjc_0A7zmyd1n1N1rWn3THDKPAEzrAEzPHnVrj-6nBYYP1D1sH9vnvcVPAE3m1mYmyDduyEzTHDYnHD3P19OPH9YnjnvrHTYrHmKnHEQnH9LrjbdrjnkP1cznHNkPTDKnikQnEDKTEDVTEDKTEDQrjn8PB3Qrjn8rjcKnTDkTEDQsjDQTgVqTHDknjnvTHDknjDvrHcKnANkuyFhmhnVrAcLmiYYm1FWsH-WP1nVuHwbuj-BPWNvmyP-TEDYTgI3nANQnvF-PADdn1nvrjEvP976phVguyPCmgEKnTD1sjDvPHm_nW0knTDQTyOdUAkKP1TQuWnvnjNkuH0znHTknk%22%2C%22lego_sid%22%3A%22130071184213052992732541465%22%2C%22platformCompany%22%3A%22ajk%22%2C%22lego_show_track_url_base%22%3A%22https%3A%5C%2F%5C%2Fadtrack.58.com%5C%2F1001692%5C%2F%22%2C%22lego_version%22%3A%221.0.1%22%2C%22legoHuiDu%22%3Atrue%7D%7D",
          "tid": "0e0ebfbc-8b7a-4c2c-9c73-e4dd9b656ace",
          "isLiving": false,
          "isVR": true,
          "distanceDesc": "",
          "isGoldOwner": false
        },
        {
          "infoid": "2001939109387274",
          "cateName": "整租",
          "cateID": "11",
          "title": "尚派公寓花地湾光线特好通风一房一厅",
          "price": "1400",
          "img": "https://pic1.ajkimg.com/display/anjuke/9d32dba76eb64633f61a3ae4a47a30f0/240x180c.jpg?t=1&srotate=1",
          "unit": "元/月",
          "coll": "0",
          "subwayDistance": "距广佛线-沙涌站步行1.6km",
          "fangwuLiangDian": [
            {
              "text": "整租",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "东西",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "近地铁",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            }
          ],
          "area": "38㎡",
          "toward": "东西",
          "shangquan": "花地湾",
          "isAnxuan": "0",
          "location": false,
          "source_type": "1",
          "isauction": "2",
          "auctionShowText": "",
          "legoAuction": "%7B%22isauction%22%3A%222%22%2C%22lego%22%3A%7B%22lego_tid%22%3A%22345bb4af-a00c-4350-8374-7f0c3d87f600%22%2C%22lego_ad_click_url%22%3A%22https%3A%5C%2F%5C%2Flegoclick.58.com%5C%2Fjump%3Ftarget%3DpZwY0ZnlsztdraOWUvYKuadWujDYmHb1uBYQrjDvsHE1PjNVrANdPiY1ry7-PjR6Pj93nANKP1TvrjndPjcOP1mdnWNKTHcknjDOn1bQnjb1rj0zP1EKrH0knTDOP1TkTHD_nHDKnkDQPWcdPjmdn1nLPH0dTHmKwbnVHidKibfMGO4hBs4hbF1MV2s-BFxCCpWGCUNKnEDQTED1TyqWN1IQnDQFpdqLIbGjXgRZNhPZEM0LRL-jINNKnHEQnHNQnHmzn1T1P10drHE3rTDvTgK60h7V01ndPjCzPHbvsZPCpHCQxjc_0A7zmyd1n1N1rWn3THDKrjFWuWPbPAnVm17hriYYPj91syDdmvNVmym1m1bvuhELnv7hTHDYnHDdnHDvnWnQrj9LPHEYn1cKnHEQnHNQnHmzn1TOnWc3nHnYPTDKnikQnEDKTEDVTEDKTEDQrjn8PB3Qrjn8rjcKnTDkTEDQsjDQTgVqTHDknjnvTHDknjDvrHcKn1EdmhcYmymVmHTkmzYYn1NksH91P1EVPvmkm1PbrjIhPWTkTEDYTgI3nANQnvF-PADdn1nvrjEvP976phVguyPCmgEKnTD1sjDvPHm_nHDdP1mKnE78IyQ_TH76nH0krAPhPADQuAmYuWm%22%2C%22lego_sid%22%3A%22130071184213052992732541465%22%2C%22platformCompany%22%3A%22ajk%22%2C%22lego_show_track_url_base%22%3A%22https%3A%5C%2F%5C%2Fadtrack.58.com%5C%2F1001692%5C%2F%22%2C%22lego_version%22%3A%221.0.1%22%2C%22legoHuiDu%22%3Atrue%7D%7D",
          "tid": "345bb4af-a00c-4350-8374-7f0c3d87f600",
          "isLiving": false,
          "isVR": false,
          "distanceDesc": "",
          "isGoldOwner": false
        },
        {
          "infoid": "2055792467680257",
          "cateName": "整租",
          "cateID": "11",
          "title": "广钢新城 振业天颂 1室1厅1卫 39方 3300元 精装修",
          "price": "3300",
          "img": "https://pic1.ajkimg.com/display/anjuke/71b19d57ff5384c4a70965db7f9a738d/240x180c.jpg?t=1&srotate=1",
          "unit": "元/月",
          "coll": "0",
          "subwayDistance": "距广佛线-沙涌站步行1.9km",
          "fangwuLiangDian": [
            {
              "text": "整租",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "南北",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "近地铁",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "有电梯",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "精装修",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            }
          ],
          "area": "35.1㎡",
          "toward": "南北",
          "shangquan": "芳村",
          "isAnxuan": "0",
          "location": false,
          "source_type": "1",
          "isauction": "2",
          "auctionShowText": "",
          "legoAuction": "%7B%22isauction%22%3A%222%22%2C%22lego%22%3A%7B%22lego_tid%22%3A%221660575b-6410-4a90-a50d-9a8b6b88f899%22%2C%22lego_ad_click_url%22%3A%22https%3A%5C%2F%5C%2Flegoclick.58.com%5C%2Fjump%3Ftarget%3DpZwY0ZnlsztdraOWUvYKuadWujDYmHb1uBYQrjDvsHE1PjNVrANdPiY1ry7-PjR6Pj93nANKP1n3PW9QnHcYrjNvPWEKTHckPHNLrHcYPW0vrjTzPH0KnHn3rHbKnHn3rHbKnikQnED1THDvnWNYPWN1n10dP1NKPk7AEzdPsN72iSXMMSpcfSpEMrXYC8yc-SB6Jrh6VEDQTHDKTHnKUvPHPLDkHD-ogLIvibPOINIimYIaI1IgXNPdwEDQPjDQPH01rjbQrHnYrH9vnWEkTHmK0A7zmyd1n1NYrWcdrHm_0v6GrW7tnBQkmgF6Ugn1PHnln19KnE7bnH0vnWwhuiYvuWRBsHEdmvEVmWPbPaYOuWDzmHKbmWmvnHDKnHEQnHNLn19On1T1n19On193rEDQPjDQPH01rjbzrH0On10znjn1TEDQsjDQTEDKTiYKTEDKTHD3nz3vsWD3nz33n9DkTHTKTHD_nHDKXLYKnHTkn1mKnHTknHmOn9DQPWmkPH0dmBYvPjDksHw6rHTVmHNkuaYOmH6BPhc3rAm3rHbKTHEKIL9kuHD1mhNYmHN1n1m3PjmvTy7JpdI-mv66ITDkTHn_nHmdPBkzP1TkTHDKUMR_UT7-nyNzm19znW6buWwbnj9z%22%2C%22lego_sid%22%3A%22130071184213052992732541465%22%2C%22platformCompany%22%3A%22ajk%22%2C%22lego_show_track_url_base%22%3A%22https%3A%5C%2F%5C%2Fadtrack.58.com%5C%2F1001692%5C%2F%22%2C%22lego_version%22%3A%221.0.1%22%2C%22legoHuiDu%22%3Atrue%7D%7D",
          "tid": "1660575b-6410-4a90-a50d-9a8b6b88f899",
          "isLiving": false,
          "isVR": false,
          "distanceDesc": "",
          "isGoldOwner": false
        },
        {
          "infoid": "1666311607673866",
          "cateName": "整租",
          "cateID": "11",
          "title": "尚派公寓花地弯地铁附近光线充足一房一厅优惠出租",
          "price": "1300",
          "img": "https://pic1.ajkimg.com/display/anjuke/238434a89bdbff7ad6634b38d5ebaa64/240x180c.jpg?t=1&srotate=1",
          "unit": "元/月",
          "coll": "0",
          "subwayDistance": "距广佛线-沙涌站步行1.6km",
          "fangwuLiangDian": [
            {
              "text": "整租",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "东南",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "近地铁",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "精装修",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            }
          ],
          "area": "38㎡",
          "toward": "东南",
          "shangquan": "花地湾",
          "isAnxuan": "0",
          "location": false,
          "source_type": "1",
          "isauction": "2",
          "auctionShowText": "",
          "legoAuction": "%7B%22isauction%22%3A%222%22%2C%22lego%22%3A%7B%22lego_tid%22%3A%22c01cd426-ea1a-46cf-bb2c-89b9647e83ec%22%2C%22lego_ad_click_url%22%3A%22https%3A%5C%2F%5C%2Flegoclick.58.com%5C%2Fjump%3Ftarget%3DpZwY0ZnlsztdraOWUvYKuadWujDYmHb1uBYQrjDvsHE1PjNVrANdPiY1ry7-PjR6Pj93nANKP1TvrjndPjcOP1mdnWNKTHDvPWm1nHDvnj0vP1n3PWmKrH0knTDOP1TkTHD_nHDKnkDQPWcdPjmdn1nLPH0dTH9KwbnVHidKibfMGO4hBs4hbF1MV2s-BFxCCpWGCUNKnEDQTED1TyqWN1IQnDQFpdqLIbGjXgRZNhPZEM0LRL-jINNKnHEQnHNQnH0QnWm3nWmQPj03PTDvTgK60h7V01ndPjCzPHbvsZPCpHCQxjc_0A7zmyd1n1N1rWn3THDKn1FhuWTLrHnVnW9LuBYYuW91sH93uWTVmWubmW9QnHbknHn1THDYnHDdnHDLnHc3n103njEkn1cKnHEQnHNQnH0QnW0YPHN1n1EYnTDKnikQnEDKTEDVTEDKTEDQrjn8PB3Qrjn8rjcKnTDkTEDQsjDQTgVqTHDknjnvTHDknjDvrHcKm1TQmvEYnWmVuyDQmiYYPhPhsyFBnhnVrj-BrHmYPvN3nvRWTEDYTgI3nANQnvF-PADdn1nvrjEvP976phVguyPCmgEKnTD1sjDvPHm_nHDdP1mKnE78IyQ_THbLP1bQuhN3mWIhuHnvnhc%22%2C%22lego_sid%22%3A%22130071184213052992732541465%22%2C%22platformCompany%22%3A%22ajk%22%2C%22lego_show_track_url_base%22%3A%22https%3A%5C%2F%5C%2Fadtrack.58.com%5C%2F1001692%5C%2F%22%2C%22lego_version%22%3A%221.0.1%22%2C%22legoHuiDu%22%3Atrue%7D%7D",
          "tid": "c01cd426-ea1a-46cf-bb2c-89b9647e83ec",
          "isLiving": false,
          "isVR": false,
          "distanceDesc": "",
          "isGoldOwner": false
        },
        {
          "infoid": "1999648200499210",
          "cateName": "整租",
          "cateID": "11",
          "title": "全新公寓房 随时看房  家具齐全",
          "price": "1400",
          "img": "https://pic1.ajkimg.com/display/anjuke/e83f59cb6bea4b9cbf32d07b8f8eae78/240x180c.jpg?t=1&srotate=1",
          "unit": "元/月",
          "coll": "0",
          "subwayDistance": "距广佛线-沙涌站步行1.5km",
          "fangwuLiangDian": [
            {
              "text": "整租",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "朝南",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "近地铁",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "精装修",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            }
          ],
          "area": "30㎡",
          "toward": "朝南",
          "shangquan": "芳村",
          "isAnxuan": "0",
          "location": false,
          "source_type": "1",
          "isauction": "6",
          "auctionShowText": "",
          "legoAuction": "%7B%22isauction%22%3A%226%22%2C%22lego%22%3A%7B%22lego_tid%22%3A%22963ad58c-1a34-4b8b-988b-2f969b80b978%22%2C%22lego_ad_click_url%22%3A%22https%3A%5C%2F%5C%2Flegoclick.58.com%5C%2Fjump%3Ftarget%3DpZwY0ZnlsztdraOWUvYKuad-njbQuHEknaYLnH-BsHEYmynVrynduaYQm1nQrHTvuhnYm1bKP1nOrH0OrEDKnHbOrHmYrjcknjEOrHcQnTDkTHDknTDQsjDQTHnKnHmzPHEvPHn1P1NOP9DQTXy8BXyQ6Xy8C8XMMSpcfSykWSXCBSylWzLb8sf-BFxCCpWhJ2ZM_3ohbF1MV2cKnEDQTED1TyqWN1IQnDQFpdqLIbGjXgRZNhPZEM0LRL-jINNKnHEQnHN3PHDkPj93nWEYPWn1P9DvTgK60h7V01ndPjCzPHbvsZPCpHCQxjc_0A7zmyd1n1N1rWn3THDKP1T3rHTvn1mVPWR6riYYPhw6sH9vPHbVmvEduj9OnAwhuWN1THDYnHDdrjNQnjNQrH0knHNkPjTKnHEQnHN3PHDkPHDQP1nzn1cvPTDKnikQnEDKTEDVTEDKTEDQrjn8PB3Qrjn8rjcKnTDkTEDQsjDQTgVqTHDknjNYTHDknjc1PHbKrHm1myEdrAnVnyD1PaYYmW6BsHb3rAcVnhmOPW-BrjKBrH03TEDYTgI3nANQnvF-PADdn1nvrjEvP976phVguyPCmgEKnkD1sjDvPHm_nW0knTDQTyOdUAkKrH0duANzujcvuWn3njFBPE%22%2C%22lego_sid%22%3A%22130071184213052992732541465%22%2C%22platformCompany%22%3A%22ajk%22%2C%22lego_show_track_url_base%22%3A%22https%3A%5C%2F%5C%2Fadtrack.58.com%5C%2F1002359%5C%2F%22%2C%22lego_version%22%3A%221.0.1%22%2C%22legoHuiDu%22%3Atrue%7D%7D",
          "tid": "963ad58c-1a34-4b8b-988b-2f969b80b978",
          "isLiving": false,
          "isVR": false,
          "distanceDesc": "",
          "isGoldOwner": false
        },
        {
          "infoid": "2048811892040716",
          "cateName": "整租",
          "cateID": "11",
          "title": "沙涌 直租一房一厅两房一厅 温馨舒适安静 家私齐全近地铁",
          "price": "1100",
          "img": "https://pic1.ajkimg.com/display/anjuke/ae18978c86057753d6337f489fa2e08f/240x180c.jpg?t=1&srotate=1",
          "unit": "元/月",
          "coll": "0",
          "subwayDistance": "距广佛线-沙涌站步行370m",
          "fangwuLiangDian": [
            {
              "text": "整租",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "东南",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "近地铁",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "精装修",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            }
          ],
          "area": "36㎡",
          "toward": "东南",
          "shangquan": "芳村",
          "isAnxuan": "0",
          "location": false,
          "source_type": "1",
          "isauction": "6",
          "auctionShowText": "",
          "legoAuction": "%7B%22isauction%22%3A%226%22%2C%22lego%22%3A%7B%22lego_tid%22%3A%222b0e36eb-2f18-4a0a-b3b4-20e5f1173368%22%2C%22lego_ad_click_url%22%3A%22https%3A%5C%2F%5C%2Flegoclick.58.com%5C%2Fjump%3Ftarget%3DpZwY0ZnlsztdraOWUvYKuad-njbQuHEknaYLnH-BsHEYmynVrynduaYQm1nQrHTvuhnYm1bKnHTQnjDkn1EzTEDznjE3rjDQrjbznjEkP1DvTHTKnHTkTHD_nHDKnkDQPWcdPjmdn1nLPHbvTHcKOplFOUA7OplBOlXxOCB4OUa5OlBsOUJ5sXi38Syc-SB6JrpCCXX1B8pEMrXYC9DQTHDKTHnKUvPHPLDkHD-ogLIvibPOINIimYIaI1IgXNPdwEDQPjDQnWDzn1TOnHcYnHTknjbvTHmK0A7zmyd1n1NYrWcdrHm_0v6GrW7tnBQkmgF6Ugn1PHnln19KnE7-mynzPHubmiYkrynksHEdn1nVmyu-Pzd-PWN1ujmkrAmQuHcKnHEQnHcQnWnkrHcLPHTOnjbYPEDQPjDQnWDzn1TOnHbdPjT1nWmdTEDQsjDQTEDKTiYKTEDKTHD3nz3vsWD3nz33n9DkTHTKTHD_nHDKXLYKnHTkPHEKnHTknWndrEDzmWK-n1u-mBYzuWD3sHw6nADVmWPBPaYznANduWDQP1n1PW9KTHEKIL9kuHD1mhNYmHN1n1m3PjmvTy7JpdI-mv66ITD1THn_nHmdPBkzP1TkTHDKUMR_UTD1PjEQn1DvnWw6uWczrAFh%22%2C%22lego_sid%22%3A%22130071184213052992732541465%22%2C%22platformCompany%22%3A%22ajk%22%2C%22lego_show_track_url_base%22%3A%22https%3A%5C%2F%5C%2Fadtrack.58.com%5C%2F1002359%5C%2F%22%2C%22lego_version%22%3A%221.0.1%22%2C%22legoHuiDu%22%3Atrue%7D%7D",
          "tid": "2b0e36eb-2f18-4a0a-b3b4-20e5f1173368",
          "isLiving": false,
          "isVR": false,
          "distanceDesc": "",
          "isGoldOwner": false
        },
        {
          "infoid": "2062765630415874",
          "cateName": "整租",
          "cateID": "11",
          "title": "太古仓商圈 复式单位 2780元月 家电配置齐全 看房方便",
          "price": "2780",
          "img": "https://pic1.ajkimg.com/display/anjuke/2c1b6d6db352f1e3602762d6c09e1c01/240x180c.jpg?t=1&srotate=1",
          "unit": "元/月",
          "coll": "0",
          "subwayDistance": "距广佛线-沙涌站步行1.7km",
          "fangwuLiangDian": [
            {
              "text": "整租",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "朝南",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "近地铁",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "精装修",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            }
          ],
          "area": "30㎡",
          "toward": "朝南",
          "shangquan": "工业大道北",
          "isAnxuan": "0",
          "location": false,
          "source_type": "1",
          "isauction": "6",
          "auctionShowText": "",
          "legoAuction": "%7B%22isauction%22%3A%226%22%2C%22lego%22%3A%7B%22lego_tid%22%3A%22d5492882-4d89-40b8-a8be-e3a4fd99753d%22%2C%22lego_ad_click_url%22%3A%22https%3A%5C%2F%5C%2Flegoclick.58.com%5C%2Fjump%3Ftarget%3DpZwY0ZnlsztdraOWUvYKuad-njbQuHEknaYLnH-BsHEYmynVrynduaYQm1nQrHTvuhnYm1bKPHNknWbQP9DKnWTvnW0vPHm1njEQPH9LPTDkTHDknTDQsjDQTHnKnHmzPHEvPHn1P1NOP9D1TXy8BXyQ6Xy8C8XMMSpcfSykWSXCBSylWzLb8sf-BFxCCpWhJ2ZM_3ohbF1MV2cKnEDQTED1TyqWN1IQnDQFpdqLIbGjXgRZNhPZEM0LRL-jINNKnHEQnHDLn19zPWc3Pjc1nW0kPTDvTgK60h7V01ndPjCzPHbvsZPCpHCQxjc_0A7zmyd1n1N1rWn3THDKuju6Py7buymVrjcYmzYYnWTLsyFhn1DVuAP6nWNkuhmzrjNzTHDYnHDQP1n3nWmYPWT1rH0dPW9KnHEQnHDLn19zPWndrH01njDLP9DKnikQnEDKTEDVTEDKTEDQrjn8PB3Qrjn8rjcKnTDkTEDQsjDQTgVqTHDknjNYTHDknjc1PHbKujNYrHc3rjcVPAE3riYYnAc3syD3mhNVuHP6PAubrHbLPHPbTEDYTgI3nANQnvF-PADdn1nvrjEvP976phVguyPCmgEKnkD1sjDvPH0_nW0kPEDQTyOdUAkKnhcvn1DkPhDknvcvnjbvn9%22%2C%22lego_sid%22%3A%22130071184213052992732541465%22%2C%22platformCompany%22%3A%22ajk%22%2C%22lego_show_track_url_base%22%3A%22https%3A%5C%2F%5C%2Fadtrack.58.com%5C%2F1002359%5C%2F%22%2C%22lego_version%22%3A%221.0.1%22%2C%22legoHuiDu%22%3Atrue%7D%7D",
          "tid": "d5492882-4d89-40b8-a8be-e3a4fd99753d",
          "isLiving": false,
          "isVR": false,
          "distanceDesc": "",
          "isGoldOwner": false
        },
        {
          "infoid": "2036145780055054",
          "cateName": "整租",
          "cateID": "11",
          "title": "广钢新城 振业天颂 ，精装修1室1厅1卫 3303元",
          "price": "3303",
          "img": "https://pic1.ajkimg.com/display/anjuke/01fa20cba7e007c88da1ff6f7644e32c/240x180c.jpg?t=1&srotate=1",
          "unit": "元/月",
          "coll": "0",
          "subwayDistance": "距广佛线-沙涌站步行1.9km",
          "fangwuLiangDian": [
            {
              "text": "安选房源",
              "textColor": "#3EC77B",
              "bgColor": "#3EC77B"
            },
            {
              "text": "整租",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "南北",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "近地铁",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "有电梯",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "精装修",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            }
          ],
          "area": "35.1㎡",
          "toward": "南北",
          "shangquan": "芳村",
          "isAnxuan": "1",
          "location": false,
          "source_type": "1",
          "isauction": "1",
          "auctionShowText": "",
          "legoAuction": "%7B%22isauction%22%3A%221%22%2C%22lego%22%3A%7B%22lego_tid%22%3A%22%22%2C%22lego_ad_click_url%22%3A%22%22%2C%22lego_sid%22%3A%22130071184213052992732541465%22%2C%22platformCompany%22%3A%22ajk%22%2C%22lego_show_track_url_base%22%3A%22https%3A%5C%2F%5C%2Fadtrack.58.com%5C%2F1001692%5C%2F%22%2C%22lego_version%22%3A%221.0.1%22%2C%22legoHuiDu%22%3Atrue%7D%7D",
          "tid": "",
          "isLiving": false,
          "isVR": false,
          "distanceDesc": "",
          "isGoldOwner": false
        },
        {
          "infoid": "2064541480525839",
          "cateName": "整租",
          "cateID": "11",
          "title": "鹤苑小区，电梯2房，拎包入住",
          "price": "3300",
          "img": "https://pic1.ajkimg.com/display/anjuke/714a63366d40620624f2f5b3805cc0f3/240x180c.jpg?t=1&srotate=1",
          "unit": "元/月",
          "coll": "0",
          "subwayDistance": "距广佛线-沙涌站步行693m",
          "fangwuLiangDian": [
            {
              "text": "安选房源",
              "textColor": "#3EC77B",
              "bgColor": "#3EC77B"
            },
            {
              "text": "整租",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "南北",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "近地铁",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "新上",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "有电梯",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            }
          ],
          "area": "71㎡",
          "toward": "南北",
          "shangquan": "芳村",
          "isAnxuan": "1",
          "location": false,
          "source_type": "1",
          "isauction": "1",
          "auctionShowText": "",
          "legoAuction": "%7B%22isauction%22%3A%221%22%2C%22lego%22%3A%7B%22lego_tid%22%3A%22%22%2C%22lego_ad_click_url%22%3A%22%22%2C%22lego_sid%22%3A%22130071184213052992732541465%22%2C%22platformCompany%22%3A%22ajk%22%2C%22lego_show_track_url_base%22%3A%22https%3A%5C%2F%5C%2Fadtrack.58.com%5C%2F1001692%5C%2F%22%2C%22lego_version%22%3A%221.0.1%22%2C%22legoHuiDu%22%3Atrue%7D%7D",
          "tid": "",
          "isLiving": false,
          "isVR": false,
          "distanceDesc": "",
          "isGoldOwner": false
        },
        {
          "infoid": "2054365072745477",
          "cateName": "整租",
          "cateID": "11",
          "title": "振业天颂 家电全齐3300 花园小区 电梯中层 精装修",
          "price": "3300",
          "img": "https://pic1.ajkimg.com/display/anjuke/9ce0e128dc3fd9735548dde85ab858c2/240x180c.jpg?t=1&srotate=1",
          "unit": "元/月",
          "coll": "0",
          "subwayDistance": "距广佛线-沙涌站步行1.9km",
          "fangwuLiangDian": [
            {
              "text": "安选房源",
              "textColor": "#3EC77B",
              "bgColor": "#3EC77B"
            },
            {
              "text": "整租",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "西南",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "近地铁",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "有电梯",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "精装修",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            }
          ],
          "area": "38㎡",
          "toward": "西南",
          "shangquan": "芳村",
          "isAnxuan": "1",
          "location": false,
          "source_type": "1",
          "isauction": "1",
          "auctionShowText": "",
          "legoAuction": "%7B%22isauction%22%3A%221%22%2C%22lego%22%3A%7B%22lego_tid%22%3A%22%22%2C%22lego_ad_click_url%22%3A%22%22%2C%22lego_sid%22%3A%22130071184213052992732541465%22%2C%22platformCompany%22%3A%22ajk%22%2C%22lego_show_track_url_base%22%3A%22https%3A%5C%2F%5C%2Fadtrack.58.com%5C%2F1001692%5C%2F%22%2C%22lego_version%22%3A%221.0.1%22%2C%22legoHuiDu%22%3Atrue%7D%7D",
          "tid": "",
          "isLiving": false,
          "isVR": false,
          "distanceDesc": "",
          "isGoldOwner": false
        },
        {
          "infoid": "978495887531008",
          "cateName": "整租",
          "cateID": "11",
          "title": "毕业季特惠房 家具家电齐全 近地铁口 采光好",
          "price": "800",
          "img": "https://pic1.ajkimg.com/display/anjuke/9bae04dd076c1d8df297d16c00182d8b/240x180c.jpg?t=1&srotate=1",
          "unit": "元/月",
          "coll": "0",
          "subwayDistance": "距广佛线-沙涌站步行473m",
          "fangwuLiangDian": [
            {
              "text": "整租",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "朝南",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "近地铁",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "有电梯",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "精装修",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            }
          ],
          "area": "29㎡",
          "toward": "朝南",
          "shangquan": "芳村",
          "isAnxuan": "0",
          "location": false,
          "source_type": "1",
          "isauction": "1",
          "auctionShowText": "",
          "legoAuction": "%7B%22isauction%22%3A%221%22%2C%22lego%22%3A%7B%22lego_tid%22%3A%22%22%2C%22lego_ad_click_url%22%3A%22%22%2C%22lego_sid%22%3A%22130071184213052992732541465%22%2C%22platformCompany%22%3A%22ajk%22%2C%22lego_show_track_url_base%22%3A%22https%3A%5C%2F%5C%2Fadtrack.58.com%5C%2F1001692%5C%2F%22%2C%22lego_version%22%3A%221.0.1%22%2C%22legoHuiDu%22%3Atrue%7D%7D",
          "tid": "",
          "isLiving": false,
          "isVR": false,
          "distanceDesc": "",
          "isGoldOwner": false
        },
        {
          "infoid": "1905533260178444",
          "cateName": "整租",
          "cateID": "11",
          "title": "650一房一厅直租，采光好，免费停车位，租期灵活",
          "price": "650",
          "img": "https://pic1.ajkimg.com/display/anjuke/a602c4690950a20abdf3f9e67eeb4151/240x180c.jpg?t=1&srotate=1",
          "unit": "元/月",
          "coll": "0",
          "subwayDistance": "距广佛线-沙涌站步行1.7km",
          "fangwuLiangDian": [
            {
              "text": "整租",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "朝南",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "近地铁",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "精装修",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            }
          ],
          "area": "35㎡",
          "toward": "朝南",
          "shangquan": "芳村",
          "isAnxuan": "0",
          "location": false,
          "source_type": "1",
          "isauction": "1",
          "auctionShowText": "",
          "legoAuction": "%7B%22isauction%22%3A%221%22%2C%22lego%22%3A%7B%22lego_tid%22%3A%22%22%2C%22lego_ad_click_url%22%3A%22%22%2C%22lego_sid%22%3A%22130071184213052992732541465%22%2C%22platformCompany%22%3A%22ajk%22%2C%22lego_show_track_url_base%22%3A%22https%3A%5C%2F%5C%2Fadtrack.58.com%5C%2F1001692%5C%2F%22%2C%22lego_version%22%3A%221.0.1%22%2C%22legoHuiDu%22%3Atrue%7D%7D",
          "tid": "",
          "isLiving": false,
          "isVR": false,
          "distanceDesc": "",
          "isGoldOwner": false
        },
        {
          "infoid": "2042935351909385",
          "cateName": "整租",
          "cateID": "11",
          "title": "怡景苑(荔湾) 找房子来找我，整租单间，随时看房，押一付一",
          "price": "620",
          "img": "https://pic1.ajkimg.com/display/anjuke/c7e9802e00cfa4ddc5e60c6dd7aa9151/240x180c.jpg?t=1&srotate=1",
          "unit": "元/月",
          "coll": "0",
          "subwayDistance": "距广佛线-沙涌站步行963m",
          "fangwuLiangDian": [
            {
              "text": "整租",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "东南",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "近地铁",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            }
          ],
          "area": "20㎡",
          "toward": "东南",
          "shangquan": "芳村",
          "isAnxuan": "0",
          "location": false,
          "source_type": "1",
          "isauction": "1",
          "auctionShowText": "",
          "legoAuction": "%7B%22isauction%22%3A%221%22%2C%22lego%22%3A%7B%22lego_tid%22%3A%22%22%2C%22lego_ad_click_url%22%3A%22%22%2C%22lego_sid%22%3A%22130071184213052992732541465%22%2C%22platformCompany%22%3A%22ajk%22%2C%22lego_show_track_url_base%22%3A%22https%3A%5C%2F%5C%2Fadtrack.58.com%5C%2F1001692%5C%2F%22%2C%22lego_version%22%3A%221.0.1%22%2C%22legoHuiDu%22%3Atrue%7D%7D",
          "tid": "",
          "isLiving": false,
          "isVR": false,
          "distanceDesc": "",
          "isGoldOwner": false
        },
        {
          "infoid": "2048553885410306",
          "cateName": "整租",
          "cateID": "11",
          "title": "坑口村 找房子来找我，整租单间，随时看房，押一付一",
          "price": "600",
          "img": "https://pic1.ajkimg.com/display/anjuke/1dcb9c04c7a5cd25e2eac8dcf1693f41/240x180c.jpg?t=1&srotate=1",
          "unit": "元/月",
          "coll": "0",
          "subwayDistance": "距广佛线-沙涌站步行1.7km",
          "fangwuLiangDian": [
            {
              "text": "整租",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "东南",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "近地铁",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            }
          ],
          "area": "20㎡",
          "toward": "东南",
          "shangquan": "芳村",
          "isAnxuan": "0",
          "location": false,
          "source_type": "1",
          "isauction": "1",
          "auctionShowText": "",
          "legoAuction": "%7B%22isauction%22%3A%221%22%2C%22lego%22%3A%7B%22lego_tid%22%3A%22%22%2C%22lego_ad_click_url%22%3A%22%22%2C%22lego_sid%22%3A%22130071184213052992732541465%22%2C%22platformCompany%22%3A%22ajk%22%2C%22lego_show_track_url_base%22%3A%22https%3A%5C%2F%5C%2Fadtrack.58.com%5C%2F1001692%5C%2F%22%2C%22lego_version%22%3A%221.0.1%22%2C%22legoHuiDu%22%3Atrue%7D%7D",
          "tid": "",
          "isLiving": false,
          "isVR": false,
          "distanceDesc": "",
          "isGoldOwner": false
        },
        {
          "infoid": "1914245505965071",
          "cateName": "整租",
          "cateID": "11",
          "title": "精装修公寓 一室一厅650 家具齐全拎包入住 通风透气 整租",
          "price": "650",
          "img": "https://pic1.ajkimg.com/display/anjuke/bdc70f5ddcff12459ab53592c7677a4c/240x180c.jpg?t=1&srotate=1",
          "unit": "元/月",
          "coll": "0",
          "subwayDistance": "距广佛线-沙涌站步行1.8km",
          "fangwuLiangDian": [
            {
              "text": "整租",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "朝南",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "近地铁",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "精装修",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            }
          ],
          "area": "35㎡",
          "toward": "朝南",
          "shangquan": "广钢新城",
          "isAnxuan": "0",
          "location": false,
          "source_type": "1",
          "isauction": "1",
          "auctionShowText": "",
          "legoAuction": "%7B%22isauction%22%3A%221%22%2C%22lego%22%3A%7B%22lego_tid%22%3A%22%22%2C%22lego_ad_click_url%22%3A%22%22%2C%22lego_sid%22%3A%22130071184213052992732541465%22%2C%22platformCompany%22%3A%22ajk%22%2C%22lego_show_track_url_base%22%3A%22https%3A%5C%2F%5C%2Fadtrack.58.com%5C%2F1001692%5C%2F%22%2C%22lego_version%22%3A%221.0.1%22%2C%22legoHuiDu%22%3Atrue%7D%7D",
          "tid": "",
          "isLiving": false,
          "isVR": false,
          "distanceDesc": "",
          "isGoldOwner": false
        },
        {
          "infoid": "1999482961667082",
          "cateName": "整租",
          "cateID": "11",
          "title": "特价2房一厅，家电齐全，地铁1号线，可做饭，免费停车位，",
          "price": "750",
          "img": "https://pic1.ajkimg.com/display/anjuke/e776403ba2a424977c5b56817205a05e/240x180c.jpg?t=1&srotate=1",
          "unit": "元/月",
          "coll": "0",
          "subwayDistance": "距广佛线-沙涌站步行329m",
          "fangwuLiangDian": [
            {
              "text": "整租",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "朝南",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "近地铁",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "精装修",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            }
          ],
          "area": "60㎡",
          "toward": "朝南",
          "shangquan": "芳村",
          "isAnxuan": "0",
          "location": false,
          "source_type": "1",
          "isauction": "1",
          "auctionShowText": "",
          "legoAuction": "%7B%22isauction%22%3A%221%22%2C%22lego%22%3A%7B%22lego_tid%22%3A%22%22%2C%22lego_ad_click_url%22%3A%22%22%2C%22lego_sid%22%3A%22130071184213052992732541465%22%2C%22platformCompany%22%3A%22ajk%22%2C%22lego_show_track_url_base%22%3A%22https%3A%5C%2F%5C%2Fadtrack.58.com%5C%2F1001692%5C%2F%22%2C%22lego_version%22%3A%221.0.1%22%2C%22legoHuiDu%22%3Atrue%7D%7D",
          "tid": "",
          "isLiving": false,
          "isVR": false,
          "distanceDesc": "",
          "isGoldOwner": false
        },
        {
          "infoid": "2048770777490442",
          "cateName": "整租",
          "cateID": "11",
          "title": "沙涌 两房一厅 全家私家电 舒适安静 宜居住 随时入住",
          "price": "1600",
          "img": "https://pic1.ajkimg.com/display/anjuke/61d036889e758259ca27630212bcd53c/240x180c.jpg?t=1&srotate=1",
          "unit": "元/月",
          "coll": "0",
          "subwayDistance": "距广佛线-沙涌站步行1.7km",
          "fangwuLiangDian": [
            {
              "text": "整租",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "东南",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "近地铁",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "精装修",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            }
          ],
          "area": "50㎡",
          "toward": "东南",
          "shangquan": "坑口",
          "isAnxuan": "0",
          "location": false,
          "source_type": "1",
          "isauction": "1",
          "auctionShowText": "",
          "legoAuction": "%7B%22isauction%22%3A%221%22%2C%22lego%22%3A%7B%22lego_tid%22%3A%22%22%2C%22lego_ad_click_url%22%3A%22%22%2C%22lego_sid%22%3A%22130071184213052992732541465%22%2C%22platformCompany%22%3A%22ajk%22%2C%22lego_show_track_url_base%22%3A%22https%3A%5C%2F%5C%2Fadtrack.58.com%5C%2F1001692%5C%2F%22%2C%22lego_version%22%3A%221.0.1%22%2C%22legoHuiDu%22%3Atrue%7D%7D",
          "tid": "",
          "isLiving": false,
          "isVR": false,
          "distanceDesc": "",
          "isGoldOwner": false
        },
        {
          "infoid": "2063994226809861",
          "cateName": "整租",
          "cateID": "11",
          "title": "地铁口两房，业主急租！！！",
          "price": "4100",
          "img": "https://pic1.ajkimg.com/display/anjuke/1bee933b4458877d050cf899525c8339/240x180c.jpg?t=1&srotate=1",
          "unit": "元/月",
          "coll": "0",
          "subwayDistance": "距广佛线-沙涌站步行2.0km",
          "fangwuLiangDian": [
            {
              "text": "整租",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "西南",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "近地铁",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "有电梯",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "精装修",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            }
          ],
          "area": "82㎡",
          "toward": "西南",
          "shangquan": "坑口",
          "isAnxuan": "0",
          "location": false,
          "source_type": "1",
          "isauction": "1",
          "auctionShowText": "",
          "legoAuction": "%7B%22isauction%22%3A%221%22%2C%22lego%22%3A%7B%22lego_tid%22%3A%22%22%2C%22lego_ad_click_url%22%3A%22%22%2C%22lego_sid%22%3A%22130071184213052992732541465%22%2C%22platformCompany%22%3A%22ajk%22%2C%22lego_show_track_url_base%22%3A%22https%3A%5C%2F%5C%2Fadtrack.58.com%5C%2F1001692%5C%2F%22%2C%22lego_version%22%3A%221.0.1%22%2C%22legoHuiDu%22%3Atrue%7D%7D",
          "tid": "",
          "isLiving": false,
          "isVR": false,
          "distanceDesc": "",
          "isGoldOwner": false
        },
        {
          "infoid": "2011689563431942",
          "cateName": "整租",
          "cateID": "11",
          "title": "地铁旁整租单间，学生租房有优惠，无中介，免费看房，家私家电齐",
          "price": "688",
          "img": "https://pic1.ajkimg.com/display/anjuke/5d487ca7ad84d9bcaa123cc39a8029a3/240x180c.jpg?t=1&srotate=1",
          "unit": "元/月",
          "coll": "0",
          "subwayDistance": "距广佛线-沙涌站步行1.9km",
          "fangwuLiangDian": [
            {
              "text": "整租",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "朝南",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "近地铁",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "精装修",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            }
          ],
          "area": "20㎡",
          "toward": "朝南",
          "shangquan": "芳村",
          "isAnxuan": "0",
          "location": false,
          "source_type": "1",
          "isauction": "1",
          "auctionShowText": "",
          "legoAuction": "%7B%22isauction%22%3A%221%22%2C%22lego%22%3A%7B%22lego_tid%22%3A%22%22%2C%22lego_ad_click_url%22%3A%22%22%2C%22lego_sid%22%3A%22130071184213052992732541465%22%2C%22platformCompany%22%3A%22ajk%22%2C%22lego_show_track_url_base%22%3A%22https%3A%5C%2F%5C%2Fadtrack.58.com%5C%2F1001692%5C%2F%22%2C%22lego_version%22%3A%221.0.1%22%2C%22legoHuiDu%22%3Atrue%7D%7D",
          "tid": "",
          "isLiving": false,
          "isVR": false,
          "distanceDesc": "",
          "isGoldOwner": false
        },
        {
          "infoid": "2057339375165443",
          "cateName": "整租",
          "cateID": "11",
          "title": "地铁口一房  家私全新  看房有钥匙",
          "price": "1600",
          "img": "https://pic1.ajkimg.com/display/anjuke/f1dbbcabd5457946e78d03aea3c57c11/240x180c.jpg?t=1&srotate=1",
          "unit": "元/月",
          "coll": "0",
          "subwayDistance": "距广佛线-沙涌站步行1.6km",
          "fangwuLiangDian": [
            {
              "text": "整租",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "东南",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "近地铁",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "精装修",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            }
          ],
          "area": "28㎡",
          "toward": "东南",
          "shangquan": "芳村",
          "isAnxuan": "0",
          "location": false,
          "source_type": "1",
          "isauction": "1",
          "auctionShowText": "",
          "legoAuction": "%7B%22isauction%22%3A%221%22%2C%22lego%22%3A%7B%22lego_tid%22%3A%22%22%2C%22lego_ad_click_url%22%3A%22%22%2C%22lego_sid%22%3A%22130071184213052992732541465%22%2C%22platformCompany%22%3A%22ajk%22%2C%22lego_show_track_url_base%22%3A%22https%3A%5C%2F%5C%2Fadtrack.58.com%5C%2F1001692%5C%2F%22%2C%22lego_version%22%3A%221.0.1%22%2C%22legoHuiDu%22%3Atrue%7D%7D",
          "tid": "",
          "isLiving": false,
          "isVR": false,
          "distanceDesc": "",
          "isGoldOwner": false
        },
        {
          "infoid": "2055697971586048",
          "cateName": "整租",
          "cateID": "11",
          "title": "单身公寓出租，家私家电齐全，近地铁口",
          "price": "1700",
          "img": "https://pic1.ajkimg.com/display/anjuke/9001f16457d1a7c2c721e079600b1d99/240x180c.jpg?t=1&srotate=1",
          "unit": "元/月",
          "coll": "0",
          "subwayDistance": "距广佛线-沙涌站步行1.6km",
          "fangwuLiangDian": [
            {
              "text": "整租",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "朝北",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "近地铁",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            }
          ],
          "area": "18㎡",
          "toward": "朝北",
          "shangquan": "芳村",
          "isAnxuan": "0",
          "location": false,
          "source_type": "1",
          "isauction": "1",
          "auctionShowText": "",
          "legoAuction": "%7B%22isauction%22%3A%221%22%2C%22lego%22%3A%7B%22lego_tid%22%3A%22%22%2C%22lego_ad_click_url%22%3A%22%22%2C%22lego_sid%22%3A%22130071184213052992732541465%22%2C%22platformCompany%22%3A%22ajk%22%2C%22lego_show_track_url_base%22%3A%22https%3A%5C%2F%5C%2Fadtrack.58.com%5C%2F1001692%5C%2F%22%2C%22lego_version%22%3A%221.0.1%22%2C%22legoHuiDu%22%3Atrue%7D%7D",
          "tid": "",
          "isLiving": false,
          "isVR": false,
          "distanceDesc": "",
          "isGoldOwner": false
        },
        {
          "infoid": "2061576821547016",
          "cateName": "整租",
          "cateID": "11",
          "title": "广佛线沙涌A出口 一房 两房（房东）直租 家私齐全 舒适安静",
          "price": "1100",
          "img": "https://pic1.ajkimg.com/display/anjuke/2b8d0225a30ca3e983b57bfc1785f61e/240x180c.jpg?t=1&srotate=1",
          "unit": "元/月",
          "coll": "0",
          "subwayDistance": "距广佛线-沙涌站步行1.0km",
          "fangwuLiangDian": [
            {
              "text": "整租",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "东南",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "近地铁",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "精装修",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            }
          ],
          "area": "36㎡",
          "toward": "东南",
          "shangquan": "芳村",
          "isAnxuan": "0",
          "location": false,
          "source_type": "1",
          "isauction": "1",
          "auctionShowText": "",
          "legoAuction": "%7B%22isauction%22%3A%221%22%2C%22lego%22%3A%7B%22lego_tid%22%3A%22%22%2C%22lego_ad_click_url%22%3A%22%22%2C%22lego_sid%22%3A%22130071184213052992732541465%22%2C%22platformCompany%22%3A%22ajk%22%2C%22lego_show_track_url_base%22%3A%22https%3A%5C%2F%5C%2Fadtrack.58.com%5C%2F1001692%5C%2F%22%2C%22lego_version%22%3A%221.0.1%22%2C%22legoHuiDu%22%3Atrue%7D%7D",
          "tid": "",
          "isLiving": false,
          "isVR": false,
          "distanceDesc": "",
          "isGoldOwner": false
        },
        {
          "infoid": "2000348167857158",
          "cateName": "整租",
          "cateID": "11",
          "title": "无中介 双地铁站 LOFT复式 实拍图 家电齐全 拎包入住",
          "price": "800",
          "img": "https://pic1.ajkimg.com/display/anjuke/b01cb928750fd508032537e9b9ce6b2c/240x180c.jpg?t=1&srotate=1",
          "unit": "元/月",
          "coll": "0",
          "subwayDistance": "距广佛线-沙涌站步行1.7km",
          "fangwuLiangDian": [
            {
              "text": "整租",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "朝东",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "近地铁",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "精装修",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            }
          ],
          "area": "27㎡",
          "toward": "朝东",
          "shangquan": "芳村",
          "isAnxuan": "0",
          "location": false,
          "source_type": "1",
          "isauction": "1",
          "auctionShowText": "",
          "legoAuction": "%7B%22isauction%22%3A%221%22%2C%22lego%22%3A%7B%22lego_tid%22%3A%22%22%2C%22lego_ad_click_url%22%3A%22%22%2C%22lego_sid%22%3A%22130071184213052992732541465%22%2C%22platformCompany%22%3A%22ajk%22%2C%22lego_show_track_url_base%22%3A%22https%3A%5C%2F%5C%2Fadtrack.58.com%5C%2F1001692%5C%2F%22%2C%22lego_version%22%3A%221.0.1%22%2C%22legoHuiDu%22%3Atrue%7D%7D",
          "tid": "",
          "isLiving": false,
          "isVR": false,
          "distanceDesc": "",
          "isGoldOwner": false
        },
        {
          "infoid": "2051327810020360",
          "cateName": "整租",
          "cateID": "11",
          "title": "双地铁，广佛线，1号线特价2房一厅，采光好，大阳台，可做饭",
          "price": "780",
          "img": "https://pic1.ajkimg.com/display/anjuke/740a49b52c2cb1c2649ab7d547c4e72d/240x180c.jpg?t=1&srotate=1",
          "unit": "元/月",
          "coll": "0",
          "subwayDistance": "距广佛线-沙涌站步行1.7km",
          "fangwuLiangDian": [
            {
              "text": "整租",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "朝南",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "近地铁",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "精装修",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            }
          ],
          "area": "68㎡",
          "toward": "朝南",
          "shangquan": "芳村",
          "isAnxuan": "0",
          "location": false,
          "source_type": "1",
          "isauction": "1",
          "auctionShowText": "",
          "legoAuction": "%7B%22isauction%22%3A%221%22%2C%22lego%22%3A%7B%22lego_tid%22%3A%22%22%2C%22lego_ad_click_url%22%3A%22%22%2C%22lego_sid%22%3A%22130071184213052992732541465%22%2C%22platformCompany%22%3A%22ajk%22%2C%22lego_show_track_url_base%22%3A%22https%3A%5C%2F%5C%2Fadtrack.58.com%5C%2F1001692%5C%2F%22%2C%22lego_version%22%3A%221.0.1%22%2C%22legoHuiDu%22%3Atrue%7D%7D",
          "tid": "",
          "isLiving": false,
          "isVR": false,
          "distanceDesc": "",
          "isGoldOwner": false
        },
        {
          "infoid": "2055768169421834",
          "cateName": "整租",
          "cateID": "11",
          "title": "1号线，广佛线，精装修一房一厅，家具家电齐全，租期灵活，",
          "price": "500",
          "img": "https://pic1.ajkimg.com/display/anjuke/efac125d9797b9d56dfc1b349e079da9/240x180c.jpg?t=1&srotate=1",
          "unit": "元/月",
          "coll": "0",
          "subwayDistance": "距广佛线-沙涌站步行1.7km",
          "fangwuLiangDian": [
            {
              "text": "整租",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "朝南",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "近地铁",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "精装修",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            }
          ],
          "area": "36㎡",
          "toward": "朝南",
          "shangquan": "芳村",
          "isAnxuan": "0",
          "location": false,
          "source_type": "1",
          "isauction": "1",
          "auctionShowText": "",
          "legoAuction": "%7B%22isauction%22%3A%221%22%2C%22lego%22%3A%7B%22lego_tid%22%3A%22%22%2C%22lego_ad_click_url%22%3A%22%22%2C%22lego_sid%22%3A%22130071184213052992732541465%22%2C%22platformCompany%22%3A%22ajk%22%2C%22lego_show_track_url_base%22%3A%22https%3A%5C%2F%5C%2Fadtrack.58.com%5C%2F1001692%5C%2F%22%2C%22lego_version%22%3A%221.0.1%22%2C%22legoHuiDu%22%3Atrue%7D%7D",
          "tid": "",
          "isLiving": false,
          "isVR": false,
          "distanceDesc": "",
          "isGoldOwner": false
        },
        {
          "infoid": "2052870345531399",
          "cateName": "整租",
          "cateID": "11",
          "title": "1号线大单间，交通便利，采光好，就等你来",
          "price": "650",
          "img": "https://pic1.ajkimg.com/display/anjuke/e040c2695df4173ba032abe2bfba1384/240x180c.jpg?t=1&srotate=1",
          "unit": "元/月",
          "coll": "0",
          "subwayDistance": "距广佛线-沙涌站步行1.7km",
          "fangwuLiangDian": [
            {
              "text": "整租",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "东西",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "近地铁",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "精装修",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            }
          ],
          "area": "23㎡",
          "toward": "东西",
          "shangquan": "芳村",
          "isAnxuan": "0",
          "location": false,
          "source_type": "1",
          "isauction": "1",
          "auctionShowText": "",
          "legoAuction": "%7B%22isauction%22%3A%221%22%2C%22lego%22%3A%7B%22lego_tid%22%3A%22%22%2C%22lego_ad_click_url%22%3A%22%22%2C%22lego_sid%22%3A%22130071184213052992732541465%22%2C%22platformCompany%22%3A%22ajk%22%2C%22lego_show_track_url_base%22%3A%22https%3A%5C%2F%5C%2Fadtrack.58.com%5C%2F1001692%5C%2F%22%2C%22lego_version%22%3A%221.0.1%22%2C%22legoHuiDu%22%3Atrue%7D%7D",
          "tid": "",
          "isLiving": false,
          "isVR": false,
          "distanceDesc": "",
          "isGoldOwner": false
        },
        {
          "infoid": "1959366007054340",
          "cateName": "整租",
          "cateID": "11",
          "title": "非中介，精装全配，空间够大，采光布局好，随时看房",
          "price": "700",
          "img": "https://pic1.ajkimg.com/display/anjuke/670636d582e75dd7a518830fe890f4de/240x180c.jpg?t=1&srotate=1",
          "unit": "元/月",
          "coll": "0",
          "subwayDistance": "距广佛线-沙涌站步行473m",
          "fangwuLiangDian": [
            {
              "text": "整租",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "南北",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "近地铁",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "精装修",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            }
          ],
          "area": "23㎡",
          "toward": "南北",
          "shangquan": "芳村",
          "isAnxuan": "0",
          "location": false,
          "source_type": "1",
          "isauction": "1",
          "auctionShowText": "",
          "legoAuction": "%7B%22isauction%22%3A%221%22%2C%22lego%22%3A%7B%22lego_tid%22%3A%22%22%2C%22lego_ad_click_url%22%3A%22%22%2C%22lego_sid%22%3A%22130071184213052992732541465%22%2C%22platformCompany%22%3A%22ajk%22%2C%22lego_show_track_url_base%22%3A%22https%3A%5C%2F%5C%2Fadtrack.58.com%5C%2F1001692%5C%2F%22%2C%22lego_version%22%3A%221.0.1%22%2C%22legoHuiDu%22%3Atrue%7D%7D",
          "tid": "",
          "isLiving": false,
          "isVR": false,
          "distanceDesc": "",
          "isGoldOwner": false
        },
        {
          "infoid": "2052806485730309",
          "cateName": "整租",
          "cateID": "11",
          "title": "坑口一房一厅 房东租 价格优惠 家具电齐全 有阳台厨卫",
          "price": "700",
          "img": "https://pic1.ajkimg.com/display/anjuke/bd9cd179097711cab99cb017575eb45c/240x180c.jpg?t=1&srotate=1",
          "unit": "元/月",
          "coll": "0",
          "subwayDistance": "距广佛线-沙涌站步行1.7km",
          "fangwuLiangDian": [
            {
              "text": "整租",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "朝南",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "近地铁",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "精装修",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            }
          ],
          "area": "38㎡",
          "toward": "朝南",
          "shangquan": "芳村",
          "isAnxuan": "0",
          "location": false,
          "source_type": "1",
          "isauction": "1",
          "auctionShowText": "",
          "legoAuction": "%7B%22isauction%22%3A%221%22%2C%22lego%22%3A%7B%22lego_tid%22%3A%22%22%2C%22lego_ad_click_url%22%3A%22%22%2C%22lego_sid%22%3A%22130071184213052992732541465%22%2C%22platformCompany%22%3A%22ajk%22%2C%22lego_show_track_url_base%22%3A%22https%3A%5C%2F%5C%2Fadtrack.58.com%5C%2F1001692%5C%2F%22%2C%22lego_version%22%3A%221.0.1%22%2C%22legoHuiDu%22%3Atrue%7D%7D",
          "tid": "",
          "isLiving": false,
          "isVR": false,
          "distanceDesc": "",
          "isGoldOwner": false
        },
        {
          "infoid": "1875801667117056",
          "cateName": "整租",
          "cateID": "11",
          "title": "西塱海南连生坊 公交直达 一室一厅650送网包管理费 可少押",
          "price": "600",
          "img": "https://pic1.ajkimg.com/display/anjuke/35561993804bdcaacb0545f98733d614/240x180c.jpg?t=1&srotate=1",
          "unit": "元/月",
          "coll": "0",
          "subwayDistance": "距广佛线-沙涌站步行329m",
          "fangwuLiangDian": [
            {
              "text": "整租",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "朝南",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "近地铁",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "精装修",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            }
          ],
          "area": "35㎡",
          "toward": "朝南",
          "shangquan": "芳村",
          "isAnxuan": "0",
          "location": false,
          "source_type": "1",
          "isauction": "1",
          "auctionShowText": "",
          "legoAuction": "%7B%22isauction%22%3A%221%22%2C%22lego%22%3A%7B%22lego_tid%22%3A%22%22%2C%22lego_ad_click_url%22%3A%22%22%2C%22lego_sid%22%3A%22130071184213052992732541465%22%2C%22platformCompany%22%3A%22ajk%22%2C%22lego_show_track_url_base%22%3A%22https%3A%5C%2F%5C%2Fadtrack.58.com%5C%2F1001692%5C%2F%22%2C%22lego_version%22%3A%221.0.1%22%2C%22legoHuiDu%22%3Atrue%7D%7D",
          "tid": "",
          "isLiving": false,
          "isVR": false,
          "distanceDesc": "",
          "isGoldOwner": false
        },
        {
          "infoid": "2058720938058757",
          "cateName": "整租",
          "cateID": "11",
          "title": "双地铁，1号线西塱，广佛地铁一房一厅，精装修，家电齐全。",
          "price": "780",
          "img": "https://pic1.ajkimg.com/display/anjuke/30402e8c059202ee49fd3dfd91deb109/240x180c.jpg?t=1&srotate=1",
          "unit": "元/月",
          "coll": "0",
          "subwayDistance": "距广佛线-沙涌站步行1.7km",
          "fangwuLiangDian": [
            {
              "text": "整租",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "朝南",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "近地铁",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "精装修",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            }
          ],
          "area": "120㎡",
          "toward": "朝南",
          "shangquan": "芳村",
          "isAnxuan": "0",
          "location": false,
          "source_type": "1",
          "isauction": "1",
          "auctionShowText": "",
          "legoAuction": "%7B%22isauction%22%3A%221%22%2C%22lego%22%3A%7B%22lego_tid%22%3A%22%22%2C%22lego_ad_click_url%22%3A%22%22%2C%22lego_sid%22%3A%22130071184213052992732541465%22%2C%22platformCompany%22%3A%22ajk%22%2C%22lego_show_track_url_base%22%3A%22https%3A%5C%2F%5C%2Fadtrack.58.com%5C%2F1001692%5C%2F%22%2C%22lego_version%22%3A%221.0.1%22%2C%22legoHuiDu%22%3Atrue%7D%7D",
          "tid": "",
          "isLiving": false,
          "isVR": false,
          "distanceDesc": "",
          "isGoldOwner": false
        },
        {
          "infoid": "2065404856212494",
          "cateName": "整租",
          "cateID": "11",
          "title": "鹤洞地铁站旁  全新家私电  一房 钥匙在口袋随时可看",
          "price": "17000",
          "img": "https://pic1.ajkimg.com/display/anjuke/c80cc27003f978e1d05bcc2666a9030e/240x180c.jpg?t=1&srotate=1",
          "unit": "元/月",
          "coll": "0",
          "subwayDistance": "距广佛线-沙涌站步行1.6km",
          "fangwuLiangDian": [
            {
              "text": "整租",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "朝东",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "近地铁",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "新上",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "精装修",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            }
          ],
          "area": "28㎡",
          "toward": "朝东",
          "shangquan": "芳村",
          "isAnxuan": "0",
          "location": false,
          "source_type": "1",
          "isauction": "1",
          "auctionShowText": "",
          "legoAuction": "%7B%22isauction%22%3A%221%22%2C%22lego%22%3A%7B%22lego_tid%22%3A%22%22%2C%22lego_ad_click_url%22%3A%22%22%2C%22lego_sid%22%3A%22130071184213052992732541465%22%2C%22platformCompany%22%3A%22ajk%22%2C%22lego_show_track_url_base%22%3A%22https%3A%5C%2F%5C%2Fadtrack.58.com%5C%2F1001692%5C%2F%22%2C%22lego_version%22%3A%221.0.1%22%2C%22legoHuiDu%22%3Atrue%7D%7D",
          "tid": "",
          "isLiving": false,
          "isVR": false,
          "distanceDesc": "",
          "isGoldOwner": false
        },
        {
          "infoid": "2054149651883022",
          "cateName": "整租",
          "cateID": "11",
          "title": "泊寓菊树店应届生押一付一 小单间独立阳台卫浴 地铁旁停车免费",
          "price": "1467",
          "img": "https://pic1.ajkimg.com/display/anjuke/4257213b4004e1f154721edb5d33201f/240x180c.jpg?t=1&srotate=1",
          "unit": "元/月",
          "coll": "0",
          "subwayDistance": "距广佛线-沙涌站步行1.7km",
          "fangwuLiangDian": [
            {
              "text": "整租",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "朝南",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "近地铁",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "精装修",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            }
          ],
          "area": "26㎡",
          "toward": "朝南",
          "shangquan": "芳村",
          "isAnxuan": "0",
          "location": false,
          "source_type": "1",
          "isauction": "1",
          "auctionShowText": "",
          "legoAuction": "%7B%22isauction%22%3A%221%22%2C%22lego%22%3A%7B%22lego_tid%22%3A%22%22%2C%22lego_ad_click_url%22%3A%22%22%2C%22lego_sid%22%3A%22130071184213052992732541465%22%2C%22platformCompany%22%3A%22ajk%22%2C%22lego_show_track_url_base%22%3A%22https%3A%5C%2F%5C%2Fadtrack.58.com%5C%2F1001692%5C%2F%22%2C%22lego_version%22%3A%221.0.1%22%2C%22legoHuiDu%22%3Atrue%7D%7D",
          "tid": "",
          "isLiving": false,
          "isVR": false,
          "distanceDesc": "",
          "isGoldOwner": false
        },
        {
          "infoid": "1921135873892357",
          "cateName": "整租",
          "cateID": "11",
          "title": "地铁芳村站 近地铁单间一房两房 大阳台光线佳 家私电齐全",
          "price": "900",
          "img": "https://pic1.ajkimg.com/display/anjuke/0b3515e44386a907c70dc89286e15205/240x180c.jpg?t=1&srotate=1",
          "unit": "元/月",
          "coll": "0",
          "subwayDistance": "距广佛线-沙涌站步行514m",
          "fangwuLiangDian": [
            {
              "text": "整租",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "朝南",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "近地铁",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "精装修",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            }
          ],
          "area": "38㎡",
          "toward": "朝南",
          "shangquan": "芳村",
          "isAnxuan": "0",
          "location": false,
          "source_type": "1",
          "isauction": "1",
          "auctionShowText": "",
          "legoAuction": "%7B%22isauction%22%3A%221%22%2C%22lego%22%3A%7B%22lego_tid%22%3A%22%22%2C%22lego_ad_click_url%22%3A%22%22%2C%22lego_sid%22%3A%22130071184213052992732541465%22%2C%22platformCompany%22%3A%22ajk%22%2C%22lego_show_track_url_base%22%3A%22https%3A%5C%2F%5C%2Fadtrack.58.com%5C%2F1001692%5C%2F%22%2C%22lego_version%22%3A%221.0.1%22%2C%22legoHuiDu%22%3Atrue%7D%7D",
          "tid": "",
          "isLiving": false,
          "isVR": false,
          "distanceDesc": "",
          "isGoldOwner": false
        },
        {
          "infoid": "2044359803051020",
          "cateName": "整租",
          "cateID": "11",
          "title": "如果你没有安全感，我给你一个安稳的家",
          "price": "700",
          "img": "https://pic1.ajkimg.com/display/anjuke/de2258bf5347f1c951115d72670285e8/240x180c.jpg?t=1&srotate=1",
          "unit": "元/月",
          "coll": "0",
          "subwayDistance": "距广佛线-沙涌站步行370m",
          "fangwuLiangDian": [
            {
              "text": "整租",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "东西",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "近地铁",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "精装修",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            }
          ],
          "area": "23㎡",
          "toward": "东西",
          "shangquan": "芳村",
          "isAnxuan": "0",
          "location": false,
          "source_type": "1",
          "isauction": "1",
          "auctionShowText": "",
          "legoAuction": "%7B%22isauction%22%3A%221%22%2C%22lego%22%3A%7B%22lego_tid%22%3A%22%22%2C%22lego_ad_click_url%22%3A%22%22%2C%22lego_sid%22%3A%22130071184213052992732541465%22%2C%22platformCompany%22%3A%22ajk%22%2C%22lego_show_track_url_base%22%3A%22https%3A%5C%2F%5C%2Fadtrack.58.com%5C%2F1001692%5C%2F%22%2C%22lego_version%22%3A%221.0.1%22%2C%22legoHuiDu%22%3Atrue%7D%7D",
          "tid": "",
          "isLiving": false,
          "isVR": false,
          "distanceDesc": "",
          "isGoldOwner": false
        },
        {
          "infoid": "2060086715507725",
          "cateName": "整租",
          "cateID": "11",
          "title": "一房一厅 全家私家电 采光好温馨舒适 随时入住",
          "price": "850",
          "img": "https://pic1.ajkimg.com/display/anjuke/39705acb2fc6bac5868d0d6550849b54/240x180c.jpg?t=1&srotate=1",
          "unit": "元/月",
          "coll": "0",
          "subwayDistance": "距广佛线-沙涌站步行1.7km",
          "fangwuLiangDian": [
            {
              "text": "整租",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "东南",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "近地铁",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "精装修",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            }
          ],
          "area": "38㎡",
          "toward": "东南",
          "shangquan": "芳村",
          "isAnxuan": "0",
          "location": false,
          "source_type": "1",
          "isauction": "1",
          "auctionShowText": "",
          "legoAuction": "%7B%22isauction%22%3A%221%22%2C%22lego%22%3A%7B%22lego_tid%22%3A%22%22%2C%22lego_ad_click_url%22%3A%22%22%2C%22lego_sid%22%3A%22130071184213052992732541465%22%2C%22platformCompany%22%3A%22ajk%22%2C%22lego_show_track_url_base%22%3A%22https%3A%5C%2F%5C%2Fadtrack.58.com%5C%2F1001692%5C%2F%22%2C%22lego_version%22%3A%221.0.1%22%2C%22legoHuiDu%22%3Atrue%7D%7D",
          "tid": "",
          "isLiving": false,
          "isVR": false,
          "distanceDesc": "",
          "isGoldOwner": false
        },
        {
          "infoid": "2063050314505223",
          "cateName": "整租",
          "cateID": "11",
          "title": "地铁口小区 高层精装修2房 南向 家私家电齐全 拎包入住",
          "price": "4300",
          "img": "https://pic1.ajkimg.com/display/anjuke/c5dc339e2305b5dfdc09e1c054c99838/240x180c.jpg?t=1&srotate=1",
          "unit": "元/月",
          "coll": "0",
          "subwayDistance": "距广佛线-沙涌站步行1.8km",
          "fangwuLiangDian": [
            {
              "text": "整租",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "东南",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "近地铁",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "有电梯",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "精装修",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            }
          ],
          "area": "83㎡",
          "toward": "东南",
          "shangquan": "芳村",
          "isAnxuan": "0",
          "location": false,
          "source_type": "1",
          "isauction": "1",
          "auctionShowText": "",
          "legoAuction": "%7B%22isauction%22%3A%221%22%2C%22lego%22%3A%7B%22lego_tid%22%3A%22%22%2C%22lego_ad_click_url%22%3A%22%22%2C%22lego_sid%22%3A%22130071184213052992732541465%22%2C%22platformCompany%22%3A%22ajk%22%2C%22lego_show_track_url_base%22%3A%22https%3A%5C%2F%5C%2Fadtrack.58.com%5C%2F1001692%5C%2F%22%2C%22lego_version%22%3A%221.0.1%22%2C%22legoHuiDu%22%3Atrue%7D%7D",
          "tid": "",
          "isLiving": false,
          "isVR": false,
          "distanceDesc": "",
          "isGoldOwner": false
        },
        {
          "infoid": "1230476851387393",
          "cateName": "整租",
          "cateID": "11",
          "title": "芳村，花地湾，坑口 地铁口精装大二房一厅 家电齐全拎包入住",
          "price": "1099",
          "img": "https://pic1.ajkimg.com/display/anjuke/81c25b4a4ada76976cf572be123cb80e/240x180c.jpg?t=1&srotate=1",
          "unit": "元/月",
          "coll": "0",
          "subwayDistance": "距广佛线-沙涌站步行1.4km",
          "fangwuLiangDian": [
            {
              "text": "整租",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "南北",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "近地铁",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "精装修",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            }
          ],
          "area": "60㎡",
          "toward": "南北",
          "shangquan": "芳村",
          "isAnxuan": "0",
          "location": false,
          "source_type": "1",
          "isauction": "1",
          "auctionShowText": "",
          "legoAuction": "%7B%22isauction%22%3A%221%22%2C%22lego%22%3A%7B%22lego_tid%22%3A%22%22%2C%22lego_ad_click_url%22%3A%22%22%2C%22lego_sid%22%3A%22130071184213052992732541465%22%2C%22platformCompany%22%3A%22ajk%22%2C%22lego_show_track_url_base%22%3A%22https%3A%5C%2F%5C%2Fadtrack.58.com%5C%2F1001692%5C%2F%22%2C%22lego_version%22%3A%221.0.1%22%2C%22legoHuiDu%22%3Atrue%7D%7D",
          "tid": "",
          "isLiving": false,
          "isVR": false,
          "distanceDesc": "",
          "isGoldOwner": false
        },
        {
          "infoid": "2064827724046338",
          "cateName": "整租",
          "cateID": "11",
          "title": "接龙里小区精装二房一厅租2300元",
          "price": "2300",
          "img": "https://pic1.ajkimg.com/display/anjuke/72b9a1d3d2b305cc8e94b585e3a53fe4/240x180c.jpg?t=1&srotate=1",
          "unit": "元/月",
          "coll": "0",
          "subwayDistance": "距广佛线-沙涌站步行1.2km",
          "fangwuLiangDian": [
            {
              "text": "整租",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "东南",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "近地铁",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "新上",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "精装修",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            }
          ],
          "area": "75㎡",
          "toward": "东南",
          "shangquan": "芳村",
          "isAnxuan": "0",
          "location": false,
          "source_type": "1",
          "isauction": "1",
          "auctionShowText": "",
          "legoAuction": "%7B%22isauction%22%3A%221%22%2C%22lego%22%3A%7B%22lego_tid%22%3A%22%22%2C%22lego_ad_click_url%22%3A%22%22%2C%22lego_sid%22%3A%22130071184213052992732541465%22%2C%22platformCompany%22%3A%22ajk%22%2C%22lego_show_track_url_base%22%3A%22https%3A%5C%2F%5C%2Fadtrack.58.com%5C%2F1001692%5C%2F%22%2C%22lego_version%22%3A%221.0.1%22%2C%22legoHuiDu%22%3Atrue%7D%7D",
          "tid": "",
          "isLiving": false,
          "isVR": false,
          "distanceDesc": "",
          "isGoldOwner": false
        },
        {
          "infoid": "2002135285621770",
          "cateName": "整租",
          "cateID": "11",
          "title": "靠近地铁站  附近商圈多 管家管理 安全卫生  毕业季特惠",
          "price": "1500",
          "img": "https://pic1.ajkimg.com/display/anjuke/83ec191745a79045bc04710758d15ba0/240x180c.jpg?t=1&srotate=1",
          "unit": "元/月",
          "coll": "0",
          "subwayDistance": "距广佛线-沙涌站步行1.9km",
          "fangwuLiangDian": [
            {
              "text": "整租",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "朝南",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "近地铁",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "有电梯",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "精装修",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            }
          ],
          "area": "33㎡",
          "toward": "朝南",
          "shangquan": "坑口",
          "isAnxuan": "0",
          "location": false,
          "source_type": "1",
          "isauction": "1",
          "auctionShowText": "",
          "legoAuction": "%7B%22isauction%22%3A%221%22%2C%22lego%22%3A%7B%22lego_tid%22%3A%22%22%2C%22lego_ad_click_url%22%3A%22%22%2C%22lego_sid%22%3A%22130071184213052992732541465%22%2C%22platformCompany%22%3A%22ajk%22%2C%22lego_show_track_url_base%22%3A%22https%3A%5C%2F%5C%2Fadtrack.58.com%5C%2F1001692%5C%2F%22%2C%22lego_version%22%3A%221.0.1%22%2C%22legoHuiDu%22%3Atrue%7D%7D",
          "tid": "",
          "isLiving": false,
          "isVR": false,
          "distanceDesc": "",
          "isGoldOwner": false
        },
        {
          "infoid": "2064134822322190",
          "cateName": "整租",
          "cateID": "11",
          "title": "逸合中心 双钥匙户型 地铁400米 家电齐全 看房方便",
          "price": "1800",
          "img": "https://pic1.ajkimg.com/display/anjuke/d14d8a94afc58c3e243f4516ae3a57a1/240x180c.jpg?t=1&srotate=1",
          "unit": "元/月",
          "coll": "0",
          "subwayDistance": "距广佛线-沙涌站步行596m",
          "fangwuLiangDian": [
            {
              "text": "整租",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "朝南",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "近地铁",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "有电梯",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "豪华装修",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            }
          ],
          "area": "26㎡",
          "toward": "朝南",
          "shangquan": "芳村",
          "isAnxuan": "0",
          "location": false,
          "source_type": "1",
          "isauction": "1",
          "auctionShowText": "",
          "legoAuction": "%7B%22isauction%22%3A%221%22%2C%22lego%22%3A%7B%22lego_tid%22%3A%22%22%2C%22lego_ad_click_url%22%3A%22%22%2C%22lego_sid%22%3A%22130071184213052992732541465%22%2C%22platformCompany%22%3A%22ajk%22%2C%22lego_show_track_url_base%22%3A%22https%3A%5C%2F%5C%2Fadtrack.58.com%5C%2F1001692%5C%2F%22%2C%22lego_version%22%3A%221.0.1%22%2C%22legoHuiDu%22%3Atrue%7D%7D",
          "tid": "",
          "isLiving": false,
          "isVR": false,
          "distanceDesc": "",
          "isGoldOwner": false
        },
        {
          "infoid": "2062745949199372",
          "cateName": "整租",
          "cateID": "11",
          "title": "1号线芳村地铁口全福里 1室1厅1卫 580元月 精装修",
          "price": "580",
          "img": "https://pic1.ajkimg.com/display/anjuke/3db71954fbcb6b12ff80cb037aac6032/240x180c.jpg?t=1&srotate=1",
          "unit": "元/月",
          "coll": "0",
          "subwayDistance": "距广佛线-沙涌站步行2.0km",
          "fangwuLiangDian": [
            {
              "text": "整租",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "朝南",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "近地铁",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "精装修",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            }
          ],
          "area": "42㎡",
          "toward": "朝南",
          "shangquan": "沙面",
          "isAnxuan": "0",
          "location": false,
          "source_type": "1",
          "isauction": "1",
          "auctionShowText": "",
          "legoAuction": "%7B%22isauction%22%3A%221%22%2C%22lego%22%3A%7B%22lego_tid%22%3A%22%22%2C%22lego_ad_click_url%22%3A%22%22%2C%22lego_sid%22%3A%22130071184213052992732541465%22%2C%22platformCompany%22%3A%22ajk%22%2C%22lego_show_track_url_base%22%3A%22https%3A%5C%2F%5C%2Fadtrack.58.com%5C%2F1001692%5C%2F%22%2C%22lego_version%22%3A%221.0.1%22%2C%22legoHuiDu%22%3Atrue%7D%7D",
          "tid": "",
          "isLiving": false,
          "isVR": false,
          "distanceDesc": "",
          "isGoldOwner": false
        },
        {
          "infoid": "2065680853818380",
          "cateName": "整租",
          "cateID": "11",
          "title": "沙涌新村 2室1厅1卫 999元月 电梯房 精装修",
          "price": "999",
          "img": "https://pic1.ajkimg.com/display/anjuke/c538f9eae8ae8b7ebe6ea59d04fdc5c4/240x180c.jpg?t=1&srotate=1",
          "unit": "元/月",
          "coll": "0",
          "subwayDistance": "距广佛线-沙涌站步行370m",
          "fangwuLiangDian": [
            {
              "text": "整租",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "南北",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "近地铁",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "新上",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "有电梯",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "精装修",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            }
          ],
          "area": "44㎡",
          "toward": "南北",
          "shangquan": "芳村",
          "isAnxuan": "0",
          "location": false,
          "source_type": "1",
          "isauction": "1",
          "auctionShowText": "",
          "legoAuction": "%7B%22isauction%22%3A%221%22%2C%22lego%22%3A%7B%22lego_tid%22%3A%22%22%2C%22lego_ad_click_url%22%3A%22%22%2C%22lego_sid%22%3A%22130071184213052992732541465%22%2C%22platformCompany%22%3A%22ajk%22%2C%22lego_show_track_url_base%22%3A%22https%3A%5C%2F%5C%2Fadtrack.58.com%5C%2F1001692%5C%2F%22%2C%22lego_version%22%3A%221.0.1%22%2C%22legoHuiDu%22%3Atrue%7D%7D",
          "tid": "",
          "isLiving": false,
          "isVR": false,
          "distanceDesc": "",
          "isGoldOwner": false
        },
        {
          "infoid": "1877217387584515",
          "cateName": "整租",
          "cateID": "11",
          "title": "新年特惠，一房一厅780元，拎包入住，交通便利，随时看房",
          "price": "780",
          "img": "https://pic1.ajkimg.com/display/anjuke/028da37207784ee1b1404c3bfa75d2aa/240x180c.jpg?t=1&srotate=1",
          "unit": "元/月",
          "coll": "0",
          "subwayDistance": "距广佛线-沙涌站步行370m",
          "fangwuLiangDian": [
            {
              "text": "整租",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "南北",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "近地铁",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "有电梯",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "精装修",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            }
          ],
          "area": "28㎡",
          "toward": "南北",
          "shangquan": "芳村",
          "isAnxuan": "0",
          "location": false,
          "source_type": "1",
          "isauction": "1",
          "auctionShowText": "",
          "legoAuction": "%7B%22isauction%22%3A%221%22%2C%22lego%22%3A%7B%22lego_tid%22%3A%22%22%2C%22lego_ad_click_url%22%3A%22%22%2C%22lego_sid%22%3A%22130071184213052992732541465%22%2C%22platformCompany%22%3A%22ajk%22%2C%22lego_show_track_url_base%22%3A%22https%3A%5C%2F%5C%2Fadtrack.58.com%5C%2F1001692%5C%2F%22%2C%22lego_version%22%3A%221.0.1%22%2C%22legoHuiDu%22%3Atrue%7D%7D",
          "tid": "",
          "isLiving": false,
          "isVR": false,
          "distanceDesc": "",
          "isGoldOwner": false
        },
        {
          "infoid": "2055632930627589",
          "cateName": "整租",
          "cateID": "11",
          "title": "翠竹苑 实拍放租 豪华两房 交通方便",
          "price": "3600",
          "img": "https://pic1.ajkimg.com/display/anjuke/3832b996b0bd540065abcfe283caae56/240x180c.jpg?t=1&srotate=1",
          "unit": "元/月",
          "coll": "0",
          "subwayDistance": "距广佛线-沙涌站步行956m",
          "fangwuLiangDian": [
            {
              "text": "整租",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "西南",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "近地铁",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "有电梯",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "精装修",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            }
          ],
          "area": "85㎡",
          "toward": "西南",
          "shangquan": "花地湾",
          "isAnxuan": "0",
          "location": false,
          "source_type": "1",
          "isauction": "1",
          "auctionShowText": "",
          "legoAuction": "%7B%22isauction%22%3A%221%22%2C%22lego%22%3A%7B%22lego_tid%22%3A%22%22%2C%22lego_ad_click_url%22%3A%22%22%2C%22lego_sid%22%3A%22130071184213052992732541465%22%2C%22platformCompany%22%3A%22ajk%22%2C%22lego_show_track_url_base%22%3A%22https%3A%5C%2F%5C%2Fadtrack.58.com%5C%2F1001692%5C%2F%22%2C%22lego_version%22%3A%221.0.1%22%2C%22legoHuiDu%22%3Atrue%7D%7D",
          "tid": "",
          "isLiving": false,
          "isVR": false,
          "distanceDesc": "",
          "isGoldOwner": false
        },
        {
          "infoid": "2065580920871939",
          "cateName": "整租",
          "cateID": "11",
          "title": "沙涌大街17号小区 1室0厅1卫 家电齐全 南北通透",
          "price": "580",
          "img": "https://pic1.ajkimg.com/display/anjuke/b1d986e73b7fc2c667ed0c71c9c6cae1/240x180c.jpg?t=1&srotate=1",
          "unit": "元/月",
          "coll": "0",
          "subwayDistance": "距广佛线-沙涌站步行473m",
          "fangwuLiangDian": [
            {
              "text": "整租",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "朝南",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "近地铁",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "新上",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            }
          ],
          "area": "23㎡",
          "toward": "朝南",
          "shangquan": "芳村",
          "isAnxuan": "0",
          "location": false,
          "source_type": "1",
          "isauction": "1",
          "auctionShowText": "",
          "legoAuction": "%7B%22isauction%22%3A%221%22%2C%22lego%22%3A%7B%22lego_tid%22%3A%22%22%2C%22lego_ad_click_url%22%3A%22%22%2C%22lego_sid%22%3A%22130071184213052992732541465%22%2C%22platformCompany%22%3A%22ajk%22%2C%22lego_show_track_url_base%22%3A%22https%3A%5C%2F%5C%2Fadtrack.58.com%5C%2F1001692%5C%2F%22%2C%22lego_version%22%3A%221.0.1%22%2C%22legoHuiDu%22%3Atrue%7D%7D",
          "tid": "",
          "isLiving": false,
          "isVR": false,
          "distanceDesc": "",
          "isGoldOwner": false
        },
        {
          "infoid": "2058549323211789",
          "cateName": "整租",
          "cateID": "11",
          "title": "鹤洞地铁站，直接出租，拎包入住，有单间，一房，二房一厅。",
          "price": "900",
          "img": "https://pic1.ajkimg.com/display/anjuke/acf86528119aae15bd62ff813ffd4286/240x180c.jpg?t=1&srotate=1",
          "unit": "元/月",
          "coll": "0",
          "subwayDistance": "距广佛线-沙涌站步行1.8km",
          "fangwuLiangDian": [
            {
              "text": "整租",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "东南",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "近地铁",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "精装修",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            }
          ],
          "area": "60㎡",
          "toward": "东南",
          "shangquan": "广钢新城",
          "isAnxuan": "0",
          "location": false,
          "source_type": "1",
          "isauction": "1",
          "auctionShowText": "",
          "legoAuction": "%7B%22isauction%22%3A%221%22%2C%22lego%22%3A%7B%22lego_tid%22%3A%22%22%2C%22lego_ad_click_url%22%3A%22%22%2C%22lego_sid%22%3A%22130071184213052992732541465%22%2C%22platformCompany%22%3A%22ajk%22%2C%22lego_show_track_url_base%22%3A%22https%3A%5C%2F%5C%2Fadtrack.58.com%5C%2F1001692%5C%2F%22%2C%22lego_version%22%3A%221.0.1%22%2C%22legoHuiDu%22%3Atrue%7D%7D",
          "tid": "",
          "isLiving": false,
          "isVR": false,
          "distanceDesc": "",
          "isGoldOwner": false
        },
        {
          "infoid": "2064108374516736",
          "cateName": "整租",
          "cateID": "11",
          "title": "广州壹城广场 2室2厅全新交楼 住得舒适",
          "price": "2300",
          "img": "https://pic1.ajkimg.com/display/anjuke/77eae870b026063d79475f7eb92a596f/240x180c.jpg?t=1&srotate=1",
          "unit": "元/月",
          "coll": "0",
          "subwayDistance": "距广佛线-沙涌站步行1.9km",
          "fangwuLiangDian": [
            {
              "text": "整租",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "朝西",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "近地铁",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "有电梯",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "精装修",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            }
          ],
          "area": "28㎡",
          "toward": "朝西",
          "shangquan": "芳村",
          "isAnxuan": "0",
          "location": false,
          "source_type": "1",
          "isauction": "1",
          "auctionShowText": "",
          "legoAuction": "%7B%22isauction%22%3A%221%22%2C%22lego%22%3A%7B%22lego_tid%22%3A%22%22%2C%22lego_ad_click_url%22%3A%22%22%2C%22lego_sid%22%3A%22130071184213052992732541465%22%2C%22platformCompany%22%3A%22ajk%22%2C%22lego_show_track_url_base%22%3A%22https%3A%5C%2F%5C%2Fadtrack.58.com%5C%2F1001692%5C%2F%22%2C%22lego_version%22%3A%221.0.1%22%2C%22legoHuiDu%22%3Atrue%7D%7D",
          "tid": "",
          "isLiving": false,
          "isVR": false,
          "distanceDesc": "",
          "isGoldOwner": false
        },
        {
          "infoid": "2065523529524224",
          "cateName": "整租",
          "cateID": "11",
          "title": "盛大蓝庭 园林小区  精装新净2房 家电齐全 随时入住",
          "price": "3000",
          "img": "https://pic1.ajkimg.com/display/anjuke/21f65a442628e07c19c53c60197e3839/240x180c.jpg?t=1&srotate=1",
          "unit": "元/月",
          "coll": "0",
          "subwayDistance": "距广佛线-沙涌站步行2.0km",
          "fangwuLiangDian": [
            {
              "text": "整租",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "朝南",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "近地铁",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "新上",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "有电梯",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "精装修",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            }
          ],
          "area": "81㎡",
          "toward": "朝南",
          "shangquan": "坑口",
          "isAnxuan": "0",
          "location": false,
          "source_type": "1",
          "isauction": "1",
          "auctionShowText": "",
          "legoAuction": "%7B%22isauction%22%3A%221%22%2C%22lego%22%3A%7B%22lego_tid%22%3A%22%22%2C%22lego_ad_click_url%22%3A%22%22%2C%22lego_sid%22%3A%22130071184213052992732541465%22%2C%22platformCompany%22%3A%22ajk%22%2C%22lego_show_track_url_base%22%3A%22https%3A%5C%2F%5C%2Fadtrack.58.com%5C%2F1001692%5C%2F%22%2C%22lego_version%22%3A%221.0.1%22%2C%22legoHuiDu%22%3Atrue%7D%7D",
          "tid": "",
          "isLiving": false,
          "isVR": false,
          "distanceDesc": "",
          "isGoldOwner": false
        },
        {
          "infoid": "2065497819931660",
          "cateName": "整租",
          "cateID": "11",
          "title": "康乃磬苑 两房精装修 家电齐全 配齐全 南北通透",
          "price": "2600",
          "img": "https://pic1.ajkimg.com/display/anjuke/1b75c07595dd1b85795e4c809a64cdcf/240x180c.jpg?t=1&srotate=1",
          "unit": "元/月",
          "coll": "0",
          "subwayDistance": "距广佛线-沙涌站步行1.7km",
          "fangwuLiangDian": [
            {
              "text": "整租",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "东南",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "近地铁",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "新上",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "精装修",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            }
          ],
          "area": "62.1㎡",
          "toward": "东南",
          "shangquan": "坑口",
          "isAnxuan": "0",
          "location": false,
          "source_type": "1",
          "isauction": "1",
          "auctionShowText": "",
          "legoAuction": "%7B%22isauction%22%3A%221%22%2C%22lego%22%3A%7B%22lego_tid%22%3A%22%22%2C%22lego_ad_click_url%22%3A%22%22%2C%22lego_sid%22%3A%22130071184213052992732541465%22%2C%22platformCompany%22%3A%22ajk%22%2C%22lego_show_track_url_base%22%3A%22https%3A%5C%2F%5C%2Fadtrack.58.com%5C%2F1001692%5C%2F%22%2C%22lego_version%22%3A%221.0.1%22%2C%22legoHuiDu%22%3Atrue%7D%7D",
          "tid": "",
          "isLiving": false,
          "isVR": false,
          "distanceDesc": "",
          "isGoldOwner": false
        },
        {
          "infoid": "2065465672736780",
          "cateName": "整租",
          "cateID": "11",
          "title": "一号线地铁花地湾站 乐怡居小区 精装两房 拎包入住 方便看房",
          "price": "2900",
          "img": "https://pic1.ajkimg.com/display/anjuke/daba322b410052d3d825ae0ba672abdc/240x180c.jpg?t=1&srotate=1",
          "unit": "元/月",
          "coll": "0",
          "subwayDistance": "距广佛线-沙涌站步行1.9km",
          "fangwuLiangDian": [
            {
              "text": "整租",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "南北",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "近地铁",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "新上",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "有电梯",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "精装修",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            }
          ],
          "area": "75㎡",
          "toward": "南北",
          "shangquan": "芳村",
          "isAnxuan": "0",
          "location": false,
          "source_type": "1",
          "isauction": "1",
          "auctionShowText": "",
          "legoAuction": "%7B%22isauction%22%3A%221%22%2C%22lego%22%3A%7B%22lego_tid%22%3A%22%22%2C%22lego_ad_click_url%22%3A%22%22%2C%22lego_sid%22%3A%22130071184213052992732541465%22%2C%22platformCompany%22%3A%22ajk%22%2C%22lego_show_track_url_base%22%3A%22https%3A%5C%2F%5C%2Fadtrack.58.com%5C%2F1001692%5C%2F%22%2C%22lego_version%22%3A%221.0.1%22%2C%22legoHuiDu%22%3Atrue%7D%7D",
          "tid": "",
          "isLiving": false,
          "isVR": false,
          "distanceDesc": "",
          "isGoldOwner": false
        },
        {
          "infoid": "2059962707517455",
          "cateName": "整租",
          "cateID": "11",
          "title": "坑口地铁1号线 盛大蓝庭 精装二房一厅 采光好家电齐全",
          "price": "3000",
          "img": "https://pic1.ajkimg.com/display/anjuke/5dd4256e0f299bfc9b5105018b82f2a7/240x180c.jpg?t=1&srotate=1",
          "unit": "元/月",
          "coll": "0",
          "subwayDistance": "距广佛线-沙涌站步行2.0km",
          "fangwuLiangDian": [
            {
              "text": "整租",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "朝南",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "近地铁",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "有电梯",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "精装修",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            }
          ],
          "area": "81㎡",
          "toward": "朝南",
          "shangquan": "坑口",
          "isAnxuan": "0",
          "location": false,
          "source_type": "1",
          "isauction": "1",
          "auctionShowText": "",
          "legoAuction": "%7B%22isauction%22%3A%221%22%2C%22lego%22%3A%7B%22lego_tid%22%3A%22%22%2C%22lego_ad_click_url%22%3A%22%22%2C%22lego_sid%22%3A%22130071184213052992732541465%22%2C%22platformCompany%22%3A%22ajk%22%2C%22lego_show_track_url_base%22%3A%22https%3A%5C%2F%5C%2Fadtrack.58.com%5C%2F1001692%5C%2F%22%2C%22lego_version%22%3A%221.0.1%22%2C%22legoHuiDu%22%3Atrue%7D%7D",
          "tid": "",
          "isLiving": false,
          "isVR": false,
          "distanceDesc": "",
          "isGoldOwner": false
        },
        {
          "infoid": "1884892729598979",
          "cateName": "整租",
          "cateID": "11",
          "title": "花地湾尚派公寓光线好家电家具齐全优惠出租",
          "price": "1300",
          "img": "https://pic1.ajkimg.com/display/anjuke/e18a45d62ace5093a7ee3bbbb2ec90fe/240x180c.jpg?t=1&srotate=1",
          "unit": "元/月",
          "coll": "0",
          "subwayDistance": "距广佛线-沙涌站步行1.6km",
          "fangwuLiangDian": [
            {
              "text": "整租",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "东南",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "近地铁",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "精装修",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            }
          ],
          "area": "30㎡",
          "toward": "东南",
          "shangquan": "花地湾",
          "isAnxuan": "0",
          "location": false,
          "source_type": "1",
          "isauction": "1",
          "auctionShowText": "",
          "legoAuction": "%7B%22isauction%22%3A%221%22%2C%22lego%22%3A%7B%22lego_tid%22%3A%22%22%2C%22lego_ad_click_url%22%3A%22%22%2C%22lego_sid%22%3A%22130071184213052992732541465%22%2C%22platformCompany%22%3A%22ajk%22%2C%22lego_show_track_url_base%22%3A%22https%3A%5C%2F%5C%2Fadtrack.58.com%5C%2F1001692%5C%2F%22%2C%22lego_version%22%3A%221.0.1%22%2C%22legoHuiDu%22%3Atrue%7D%7D",
          "tid": "",
          "isLiving": false,
          "isVR": false,
          "distanceDesc": "",
          "isGoldOwner": false
        },
        {
          "infoid": "2058519860896782",
          "cateName": "整租",
          "cateID": "11",
          "title": "35平方接龙里一房一厅出租",
          "price": "1300",
          "img": "https://pic1.ajkimg.com/display/anjuke/02f107cd0e7409e07bc82fe9f6c69efb/240x180c.jpg?t=1&srotate=1",
          "unit": "元/月",
          "coll": "0",
          "subwayDistance": "距广佛线-沙涌站步行1.2km",
          "fangwuLiangDian": [
            {
              "text": "整租",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "朝南",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "近地铁",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            }
          ],
          "area": "38㎡",
          "toward": "朝南",
          "shangquan": "芳村",
          "isAnxuan": "0",
          "location": false,
          "source_type": "1",
          "isauction": "1",
          "auctionShowText": "",
          "legoAuction": "%7B%22isauction%22%3A%221%22%2C%22lego%22%3A%7B%22lego_tid%22%3A%22%22%2C%22lego_ad_click_url%22%3A%22%22%2C%22lego_sid%22%3A%22130071184213052992732541465%22%2C%22platformCompany%22%3A%22ajk%22%2C%22lego_show_track_url_base%22%3A%22https%3A%5C%2F%5C%2Fadtrack.58.com%5C%2F1001692%5C%2F%22%2C%22lego_version%22%3A%221.0.1%22%2C%22legoHuiDu%22%3Atrue%7D%7D",
          "tid": "",
          "isLiving": false,
          "isVR": false,
          "distanceDesc": "",
          "isGoldOwner": false
        },
        {
          "infoid": "2048735144174601",
          "cateName": "整租",
          "cateID": "11",
          "title": "花地湾B口旁，毕业季优惠大，自己的房子价格随便租，光线非常好",
          "price": "1300",
          "img": "https://pic1.ajkimg.com/display/anjuke/bb230bba9e24abf4c805f2771ca49d8c/240x180c.jpg?t=1&srotate=1",
          "unit": "元/月",
          "coll": "0",
          "subwayDistance": "距广佛线-沙涌站步行1.9km",
          "fangwuLiangDian": [
            {
              "text": "整租",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "朝南",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "近地铁",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "精装修",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            }
          ],
          "area": "40㎡",
          "toward": "朝南",
          "shangquan": "芳村",
          "isAnxuan": "0",
          "location": false,
          "source_type": "1",
          "isauction": "1",
          "auctionShowText": "",
          "legoAuction": "%7B%22isauction%22%3A%221%22%2C%22lego%22%3A%7B%22lego_tid%22%3A%22%22%2C%22lego_ad_click_url%22%3A%22%22%2C%22lego_sid%22%3A%22130071184213052992732541465%22%2C%22platformCompany%22%3A%22ajk%22%2C%22lego_show_track_url_base%22%3A%22https%3A%5C%2F%5C%2Fadtrack.58.com%5C%2F1001692%5C%2F%22%2C%22lego_version%22%3A%221.0.1%22%2C%22legoHuiDu%22%3Atrue%7D%7D",
          "tid": "",
          "isLiving": false,
          "isVR": false,
          "distanceDesc": "",
          "isGoldOwner": false
        },
        {
          "infoid": "2010307766951936",
          "cateName": "整租",
          "cateID": "11",
          "title": "荔湾区鹤洞观鹤小区 1室0厅1卫 1500元月 精装修",
          "price": "1500",
          "img": "https://pic1.ajkimg.com/display/anjuke/bd47b5ee12316784ed55b81f52070b1c/240x180c.jpg?t=1&srotate=1",
          "unit": "元/月",
          "coll": "0",
          "subwayDistance": "距广佛线-沙涌站步行1.6km",
          "fangwuLiangDian": [
            {
              "text": "整租",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "南北",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "近地铁",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "精装修",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            }
          ],
          "area": "25㎡",
          "toward": "南北",
          "shangquan": "芳村",
          "isAnxuan": "0",
          "location": false,
          "source_type": "1",
          "isauction": "1",
          "auctionShowText": "",
          "legoAuction": "%7B%22isauction%22%3A%221%22%2C%22lego%22%3A%7B%22lego_tid%22%3A%22%22%2C%22lego_ad_click_url%22%3A%22%22%2C%22lego_sid%22%3A%22130071184213052992732541465%22%2C%22platformCompany%22%3A%22ajk%22%2C%22lego_show_track_url_base%22%3A%22https%3A%5C%2F%5C%2Fadtrack.58.com%5C%2F1001692%5C%2F%22%2C%22lego_version%22%3A%221.0.1%22%2C%22legoHuiDu%22%3Atrue%7D%7D",
          "tid": "",
          "isLiving": false,
          "isVR": false,
          "distanceDesc": "",
          "isGoldOwner": false
        },
        {
          "infoid": "1955362651316227",
          "cateName": "整租",
          "cateID": "11",
          "title": "幸福 东漖新村 2室2厅1卫3500元月 精装修南北通透",
          "price": "3500",
          "img": "https://pic1.ajkimg.com/display/anjuke/360fef6c601c6e62c5e2c223e54b88c7/240x180c.jpg?t=1&srotate=1",
          "unit": "元/月",
          "coll": "0",
          "subwayDistance": "距广佛线-沙涌站步行1.9km",
          "fangwuLiangDian": [
            {
              "text": "整租",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "南北",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "近地铁",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "有电梯",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "精装修",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            }
          ],
          "area": "71㎡",
          "toward": "南北",
          "shangquan": "坑口",
          "isAnxuan": "0",
          "location": false,
          "source_type": "1",
          "isauction": "1",
          "auctionShowText": "",
          "legoAuction": "%7B%22isauction%22%3A%221%22%2C%22lego%22%3A%7B%22lego_tid%22%3A%22%22%2C%22lego_ad_click_url%22%3A%22%22%2C%22lego_sid%22%3A%22130071184213052992732541465%22%2C%22platformCompany%22%3A%22ajk%22%2C%22lego_show_track_url_base%22%3A%22https%3A%5C%2F%5C%2Fadtrack.58.com%5C%2F1001692%5C%2F%22%2C%22lego_version%22%3A%221.0.1%22%2C%22legoHuiDu%22%3Atrue%7D%7D",
          "tid": "",
          "isLiving": false,
          "isVR": false,
          "distanceDesc": "",
          "isGoldOwner": false
        },
        {
          "infoid": "2054801338769417",
          "cateName": "整租",
          "cateID": "11",
          "title": "广钢新城 振业天颂花园 全新未住 家电齐全 图片真实",
          "price": "3000",
          "img": "https://pic1.ajkimg.com/display/anjuke/b49a33a1c63d2a62876a0bc7d1fd64f6/240x180c.jpg?t=1&srotate=1",
          "unit": "元/月",
          "coll": "0",
          "subwayDistance": "距广佛线-沙涌站步行1.9km",
          "fangwuLiangDian": [
            {
              "text": "整租",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "朝东",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "近地铁",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "有电梯",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "精装修",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            }
          ],
          "area": "40㎡",
          "toward": "朝东",
          "shangquan": "芳村",
          "isAnxuan": "0",
          "location": false,
          "source_type": "1",
          "isauction": "1",
          "auctionShowText": "",
          "legoAuction": "%7B%22isauction%22%3A%221%22%2C%22lego%22%3A%7B%22lego_tid%22%3A%22%22%2C%22lego_ad_click_url%22%3A%22%22%2C%22lego_sid%22%3A%22130071184213052992732541465%22%2C%22platformCompany%22%3A%22ajk%22%2C%22lego_show_track_url_base%22%3A%22https%3A%5C%2F%5C%2Fadtrack.58.com%5C%2F1001692%5C%2F%22%2C%22lego_version%22%3A%221.0.1%22%2C%22legoHuiDu%22%3Atrue%7D%7D",
          "tid": "",
          "isLiving": false,
          "isVR": false,
          "distanceDesc": "",
          "isGoldOwner": false
        },
        {
          "infoid": "1976308284349450",
          "cateName": "整租",
          "cateID": "11",
          "title": "幸福 康乃磬苑 2室2厅1卫2800元月 南北通透电梯房",
          "price": "2800",
          "img": "https://pic1.ajkimg.com/display/anjuke/42e263398bc75a9110dffa8dbfcfcdbf/240x180c.jpg?t=1&srotate=1",
          "unit": "元/月",
          "coll": "0",
          "subwayDistance": "距广佛线-沙涌站步行1.7km",
          "fangwuLiangDian": [
            {
              "text": "整租",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "西南",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "近地铁",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "有电梯",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "精装修",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            }
          ],
          "area": "65㎡",
          "toward": "西南",
          "shangquan": "坑口",
          "isAnxuan": "0",
          "location": false,
          "source_type": "1",
          "isauction": "1",
          "auctionShowText": "",
          "legoAuction": "%7B%22isauction%22%3A%221%22%2C%22lego%22%3A%7B%22lego_tid%22%3A%22%22%2C%22lego_ad_click_url%22%3A%22%22%2C%22lego_sid%22%3A%22130071184213052992732541465%22%2C%22platformCompany%22%3A%22ajk%22%2C%22lego_show_track_url_base%22%3A%22https%3A%5C%2F%5C%2Fadtrack.58.com%5C%2F1001692%5C%2F%22%2C%22lego_version%22%3A%221.0.1%22%2C%22legoHuiDu%22%3Atrue%7D%7D",
          "tid": "",
          "isLiving": false,
          "isVR": false,
          "distanceDesc": "",
          "isGoldOwner": false
        },
        {
          "infoid": "1958099753870342",
          "cateName": "整租",
          "cateID": "11",
          "title": "地铁口小区 高层精装修2房 南向 家私家电齐全 拎包入住",
          "price": "3600",
          "img": "https://pic1.ajkimg.com/display/anjuke/6493c77ad13643b5006699c0385d59f4/240x180c.jpg?t=1&srotate=1",
          "unit": "元/月",
          "coll": "0",
          "subwayDistance": "距广佛线-沙涌站步行1.8km",
          "fangwuLiangDian": [
            {
              "text": "整租",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "朝南",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "近地铁",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "有电梯",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "精装修",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            }
          ],
          "area": "78㎡",
          "toward": "朝南",
          "shangquan": "芳村",
          "isAnxuan": "0",
          "location": false,
          "source_type": "1",
          "isauction": "1",
          "auctionShowText": "",
          "legoAuction": "%7B%22isauction%22%3A%221%22%2C%22lego%22%3A%7B%22lego_tid%22%3A%22%22%2C%22lego_ad_click_url%22%3A%22%22%2C%22lego_sid%22%3A%22130071184213052992732541465%22%2C%22platformCompany%22%3A%22ajk%22%2C%22lego_show_track_url_base%22%3A%22https%3A%5C%2F%5C%2Fadtrack.58.com%5C%2F1001692%5C%2F%22%2C%22lego_version%22%3A%221.0.1%22%2C%22legoHuiDu%22%3Atrue%7D%7D",
          "tid": "",
          "isLiving": false,
          "isVR": false,
          "distanceDesc": "",
          "isGoldOwner": false
        },
        {
          "infoid": "2063035196678150",
          "cateName": "整租",
          "cateID": "11",
          "title": "盛大蓝庭 1室1厅1卫 888元月 南北通透 精装修",
          "price": "888",
          "img": "https://pic1.ajkimg.com/display/anjuke/4f2121a5e2441b441e88c21f01689c3c/240x180c.jpg?t=1&srotate=1",
          "unit": "元/月",
          "coll": "0",
          "subwayDistance": "距广佛线-沙涌站步行2.0km",
          "fangwuLiangDian": [
            {
              "text": "整租",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "南北",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "近地铁",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "有电梯",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "精装修",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            }
          ],
          "area": "14㎡",
          "toward": "南北",
          "shangquan": "坑口",
          "isAnxuan": "0",
          "location": false,
          "source_type": "1",
          "isauction": "1",
          "auctionShowText": "",
          "legoAuction": "%7B%22isauction%22%3A%221%22%2C%22lego%22%3A%7B%22lego_tid%22%3A%22%22%2C%22lego_ad_click_url%22%3A%22%22%2C%22lego_sid%22%3A%22130071184213052992732541465%22%2C%22platformCompany%22%3A%22ajk%22%2C%22lego_show_track_url_base%22%3A%22https%3A%5C%2F%5C%2Fadtrack.58.com%5C%2F1001692%5C%2F%22%2C%22lego_version%22%3A%221.0.1%22%2C%22legoHuiDu%22%3Atrue%7D%7D",
          "tid": "",
          "isLiving": false,
          "isVR": false,
          "distanceDesc": "",
          "isGoldOwner": false
        },
        {
          "infoid": "2064398818222095",
          "cateName": "整租",
          "cateID": "11",
          "title": "乐怡居 2室1厅1卫 999元月 精装修 电梯房",
          "price": "999",
          "img": "https://pic1.ajkimg.com/display/anjuke/7dbdddcdae7f3b267d534c51631a0215/240x180c.jpg?t=1&srotate=1",
          "unit": "元/月",
          "coll": "0",
          "subwayDistance": "距广佛线-沙涌站步行1.9km",
          "fangwuLiangDian": [
            {
              "text": "整租",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "南北",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "近地铁",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "新上",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "有电梯",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "精装修",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            }
          ],
          "area": "44㎡",
          "toward": "南北",
          "shangquan": "芳村",
          "isAnxuan": "0",
          "location": false,
          "source_type": "1",
          "isauction": "1",
          "auctionShowText": "",
          "legoAuction": "%7B%22isauction%22%3A%221%22%2C%22lego%22%3A%7B%22lego_tid%22%3A%22%22%2C%22lego_ad_click_url%22%3A%22%22%2C%22lego_sid%22%3A%22130071184213052992732541465%22%2C%22platformCompany%22%3A%22ajk%22%2C%22lego_show_track_url_base%22%3A%22https%3A%5C%2F%5C%2Fadtrack.58.com%5C%2F1001692%5C%2F%22%2C%22lego_version%22%3A%221.0.1%22%2C%22legoHuiDu%22%3Atrue%7D%7D",
          "tid": "",
          "isLiving": false,
          "isVR": false,
          "distanceDesc": "",
          "isGoldOwner": false
        },
        {
          "infoid": "2047174765077508",
          "cateName": "整租",
          "cateID": "11",
          "title": "1室1厅1卫 650元月  南北通透 拎包入住 随时看房",
          "price": "650",
          "img": "https://pic1.ajkimg.com/display/anjuke/a9834c9278b8879eb91638412eb8175a/240x180c.jpg?t=1&srotate=1",
          "unit": "元/月",
          "coll": "0",
          "subwayDistance": "距广佛线-沙涌站步行1.8km",
          "fangwuLiangDian": [
            {
              "text": "整租",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "朝南",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "近地铁",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "有电梯",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "精装修",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            }
          ],
          "area": "36㎡",
          "toward": "朝南",
          "shangquan": "坑口",
          "isAnxuan": "0",
          "location": false,
          "source_type": "1",
          "isauction": "1",
          "auctionShowText": "",
          "legoAuction": "%7B%22isauction%22%3A%221%22%2C%22lego%22%3A%7B%22lego_tid%22%3A%22%22%2C%22lego_ad_click_url%22%3A%22%22%2C%22lego_sid%22%3A%22130071184213052992732541465%22%2C%22platformCompany%22%3A%22ajk%22%2C%22lego_show_track_url_base%22%3A%22https%3A%5C%2F%5C%2Fadtrack.58.com%5C%2F1001692%5C%2F%22%2C%22lego_version%22%3A%221.0.1%22%2C%22legoHuiDu%22%3Atrue%7D%7D",
          "tid": "",
          "isLiving": false,
          "isVR": false,
          "distanceDesc": "",
          "isGoldOwner": false
        },
        {
          "infoid": "2057116944901120",
          "cateName": "整租",
          "cateID": "11",
          "title": "坑口地铁口(月付)(可短租)拎包住(家电全)",
          "price": "500",
          "img": "https://pic1.ajkimg.com/display/anjuke/b982b7ed0246a1be16e22e3cd59bbd36/240x180c.jpg?t=1&srotate=1",
          "unit": "元/月",
          "coll": "0",
          "subwayDistance": "距广佛线-沙涌站步行2.0km",
          "fangwuLiangDian": [
            {
              "text": "整租",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "朝南",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "近地铁",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            }
          ],
          "area": "12㎡",
          "toward": "朝南",
          "shangquan": "坑口",
          "isAnxuan": "0",
          "location": false,
          "source_type": "1",
          "isauction": "1",
          "auctionShowText": "",
          "legoAuction": "%7B%22isauction%22%3A%221%22%2C%22lego%22%3A%7B%22lego_tid%22%3A%22%22%2C%22lego_ad_click_url%22%3A%22%22%2C%22lego_sid%22%3A%22130071184213052992732541465%22%2C%22platformCompany%22%3A%22ajk%22%2C%22lego_show_track_url_base%22%3A%22https%3A%5C%2F%5C%2Fadtrack.58.com%5C%2F1001692%5C%2F%22%2C%22lego_version%22%3A%221.0.1%22%2C%22legoHuiDu%22%3Atrue%7D%7D",
          "tid": "",
          "isLiving": false,
          "isVR": false,
          "distanceDesc": "",
          "isGoldOwner": false
        },
        {
          "infoid": "2044344945761291",
          "cateName": "整租",
          "cateID": "11",
          "title": "坑口南围村，精装修一房一厅，采光好，环境美，配置齐全拎包入住",
          "price": "900",
          "img": "https://pic1.ajkimg.com/display/anjuke/005cf11c0e893c11c336beb3c2d5735d/240x180c.jpg?t=1&srotate=1",
          "unit": "元/月",
          "coll": "0",
          "subwayDistance": "距广佛线-沙涌站步行1.7km",
          "fangwuLiangDian": [
            {
              "text": "整租",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "南北",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "近地铁",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "有电梯",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "精装修",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            }
          ],
          "area": "40㎡",
          "toward": "南北",
          "shangquan": "芳村",
          "isAnxuan": "0",
          "location": false,
          "source_type": "1",
          "isauction": "1",
          "auctionShowText": "",
          "legoAuction": "%7B%22isauction%22%3A%221%22%2C%22lego%22%3A%7B%22lego_tid%22%3A%22%22%2C%22lego_ad_click_url%22%3A%22%22%2C%22lego_sid%22%3A%22130071184213052992732541465%22%2C%22platformCompany%22%3A%22ajk%22%2C%22lego_show_track_url_base%22%3A%22https%3A%5C%2F%5C%2Fadtrack.58.com%5C%2F1001692%5C%2F%22%2C%22lego_version%22%3A%221.0.1%22%2C%22legoHuiDu%22%3Atrue%7D%7D",
          "tid": "",
          "isLiving": false,
          "isVR": false,
          "distanceDesc": "",
          "isGoldOwner": false
        },
        {
          "infoid": "2059830833456142",
          "cateName": "整租",
          "cateID": "11",
          "title": "盛大蓝庭 2室1厅1卫 3300元月 电梯房 配套齐全",
          "price": "3300",
          "img": "https://pic1.ajkimg.com/display/anjuke/822ddff5c73285adb5b84442937983aa/240x180c.jpg?t=1&srotate=1",
          "unit": "元/月",
          "coll": "0",
          "subwayDistance": "距广佛线-沙涌站步行2.0km",
          "fangwuLiangDian": [
            {
              "text": "整租",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "朝南",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "近地铁",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "有电梯",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "精装修",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            }
          ],
          "area": "59㎡",
          "toward": "朝南",
          "shangquan": "坑口",
          "isAnxuan": "0",
          "location": false,
          "source_type": "1",
          "isauction": "1",
          "auctionShowText": "",
          "legoAuction": "%7B%22isauction%22%3A%221%22%2C%22lego%22%3A%7B%22lego_tid%22%3A%22%22%2C%22lego_ad_click_url%22%3A%22%22%2C%22lego_sid%22%3A%22130071184213052992732541465%22%2C%22platformCompany%22%3A%22ajk%22%2C%22lego_show_track_url_base%22%3A%22https%3A%5C%2F%5C%2Fadtrack.58.com%5C%2F1001692%5C%2F%22%2C%22lego_version%22%3A%221.0.1%22%2C%22legoHuiDu%22%3Atrue%7D%7D",
          "tid": "",
          "isLiving": false,
          "isVR": false,
          "distanceDesc": "",
          "isGoldOwner": false
        },
        {
          "infoid": "2047238130130947",
          "cateName": "整租",
          "cateID": "11",
          "title": "1室1厅1卫 680元月 精装修 拎包入住 生活便利",
          "price": "680",
          "img": "https://pic1.ajkimg.com/display/anjuke/37cf0ea5c65a263f0ce41ce623658670/240x180c.jpg?t=1&srotate=1",
          "unit": "元/月",
          "coll": "0",
          "subwayDistance": "距广佛线-沙涌站步行1.4km",
          "fangwuLiangDian": [
            {
              "text": "整租",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "朝南",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "近地铁",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "精装修",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            }
          ],
          "area": "38㎡",
          "toward": "朝南",
          "shangquan": "芳村",
          "isAnxuan": "0",
          "location": false,
          "source_type": "1",
          "isauction": "1",
          "auctionShowText": "",
          "legoAuction": "%7B%22isauction%22%3A%221%22%2C%22lego%22%3A%7B%22lego_tid%22%3A%22%22%2C%22lego_ad_click_url%22%3A%22%22%2C%22lego_sid%22%3A%22130071184213052992732541465%22%2C%22platformCompany%22%3A%22ajk%22%2C%22lego_show_track_url_base%22%3A%22https%3A%5C%2F%5C%2Fadtrack.58.com%5C%2F1001692%5C%2F%22%2C%22lego_version%22%3A%221.0.1%22%2C%22legoHuiDu%22%3Atrue%7D%7D",
          "tid": "",
          "isLiving": false,
          "isVR": false,
          "distanceDesc": "",
          "isGoldOwner": false
        },
        {
          "infoid": "1971606211044358",
          "cateName": "整租",
          "cateID": "11",
          "title": "1室1厅780   精装修  南北通透  电梯房  拎包入住",
          "price": "780",
          "img": "https://pic1.ajkimg.com/display/anjuke/755790d8e0225be0625afdae23b714a8/240x180c.jpg?t=1&srotate=1",
          "unit": "元/月",
          "coll": "0",
          "subwayDistance": "距广佛线-沙涌站步行473m",
          "fangwuLiangDian": [
            {
              "text": "整租",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "朝南",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "近地铁",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "有电梯",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "精装修",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            }
          ],
          "area": "38.5㎡",
          "toward": "朝南",
          "shangquan": "芳村",
          "isAnxuan": "0",
          "location": false,
          "source_type": "1",
          "isauction": "1",
          "auctionShowText": "",
          "legoAuction": "%7B%22isauction%22%3A%221%22%2C%22lego%22%3A%7B%22lego_tid%22%3A%22%22%2C%22lego_ad_click_url%22%3A%22%22%2C%22lego_sid%22%3A%22130071184213052992732541465%22%2C%22platformCompany%22%3A%22ajk%22%2C%22lego_show_track_url_base%22%3A%22https%3A%5C%2F%5C%2Fadtrack.58.com%5C%2F1001692%5C%2F%22%2C%22lego_version%22%3A%221.0.1%22%2C%22legoHuiDu%22%3Atrue%7D%7D",
          "tid": "",
          "isLiving": false,
          "isVR": false,
          "distanceDesc": "",
          "isGoldOwner": false
        },
        {
          "infoid": "2061313909530638",
          "cateName": "整租",
          "cateID": "11",
          "title": "荔湾区下芳村信联路小区精装二房一厅租2000元",
          "price": "2000",
          "img": "https://pic1.ajkimg.com/display/anjuke/9a32db6f5910a5b595d2f5b981f080d1/240x180c.jpg?t=1&srotate=1",
          "unit": "元/月",
          "coll": "0",
          "subwayDistance": "距广佛线-沙涌站步行1.4km",
          "fangwuLiangDian": [
            {
              "text": "整租",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "南北",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "近地铁",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "精装修",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            }
          ],
          "area": "68㎡",
          "toward": "南北",
          "shangquan": "芳村",
          "isAnxuan": "0",
          "location": false,
          "source_type": "1",
          "isauction": "1",
          "auctionShowText": "",
          "legoAuction": "%7B%22isauction%22%3A%221%22%2C%22lego%22%3A%7B%22lego_tid%22%3A%22%22%2C%22lego_ad_click_url%22%3A%22%22%2C%22lego_sid%22%3A%22130071184213052992732541465%22%2C%22platformCompany%22%3A%22ajk%22%2C%22lego_show_track_url_base%22%3A%22https%3A%5C%2F%5C%2Fadtrack.58.com%5C%2F1001692%5C%2F%22%2C%22lego_version%22%3A%221.0.1%22%2C%22legoHuiDu%22%3Atrue%7D%7D",
          "tid": "",
          "isLiving": false,
          "isVR": false,
          "distanceDesc": "",
          "isGoldOwner": false
        },
        {
          "infoid": "2061227068497923",
          "cateName": "整租",
          "cateID": "11",
          "title": "泊寓菊树店应届生押一付一 大平层独立阳台厨房 停车免费地铁旁",
          "price": "1935",
          "img": "https://pic1.ajkimg.com/display/anjuke/13759a23cc8666b8a205914a14ea5b84/240x180c.jpg?t=1&srotate=1",
          "unit": "元/月",
          "coll": "0",
          "subwayDistance": "距广佛线-沙涌站步行1.7km",
          "fangwuLiangDian": [
            {
              "text": "整租",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "朝北",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "近地铁",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "精装修",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            }
          ],
          "area": "40㎡",
          "toward": "朝北",
          "shangquan": "芳村",
          "isAnxuan": "0",
          "location": false,
          "source_type": "1",
          "isauction": "1",
          "auctionShowText": "",
          "legoAuction": "%7B%22isauction%22%3A%221%22%2C%22lego%22%3A%7B%22lego_tid%22%3A%22%22%2C%22lego_ad_click_url%22%3A%22%22%2C%22lego_sid%22%3A%22130071184213052992732541465%22%2C%22platformCompany%22%3A%22ajk%22%2C%22lego_show_track_url_base%22%3A%22https%3A%5C%2F%5C%2Fadtrack.58.com%5C%2F1001692%5C%2F%22%2C%22lego_version%22%3A%221.0.1%22%2C%22legoHuiDu%22%3Atrue%7D%7D",
          "tid": "",
          "isLiving": false,
          "isVR": false,
          "distanceDesc": "",
          "isGoldOwner": false
        },
        {
          "infoid": "1988385543363598",
          "cateName": "整租",
          "cateID": "11",
          "title": "沙涌地铁站 沙涌新村 大单间 750元 电梯房",
          "price": "850",
          "img": "https://pic1.ajkimg.com/display/anjuke/783a607a3a9e255b9de156917b43aa65/240x180c.jpg?t=1&srotate=1",
          "unit": "元/月",
          "coll": "0",
          "subwayDistance": "距广佛线-沙涌站步行370m",
          "fangwuLiangDian": [
            {
              "text": "整租",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "朝南",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "近地铁",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "有电梯",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "精装修",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            }
          ],
          "area": "35㎡",
          "toward": "朝南",
          "shangquan": "芳村",
          "isAnxuan": "0",
          "location": false,
          "source_type": "1",
          "isauction": "1",
          "auctionShowText": "",
          "legoAuction": "%7B%22isauction%22%3A%221%22%2C%22lego%22%3A%7B%22lego_tid%22%3A%22%22%2C%22lego_ad_click_url%22%3A%22%22%2C%22lego_sid%22%3A%22130071184213052992732541465%22%2C%22platformCompany%22%3A%22ajk%22%2C%22lego_show_track_url_base%22%3A%22https%3A%5C%2F%5C%2Fadtrack.58.com%5C%2F1001692%5C%2F%22%2C%22lego_version%22%3A%221.0.1%22%2C%22legoHuiDu%22%3Atrue%7D%7D",
          "tid": "",
          "isLiving": false,
          "isVR": false,
          "distanceDesc": "",
          "isGoldOwner": false
        },
        {
          "infoid": "2008108818014223",
          "cateName": "整租",
          "cateID": "11",
          "title": "推荐芳村整租78平东漖新村享受生活的快感！！！",
          "price": "3500",
          "img": "https://pic1.ajkimg.com/display/anjuke/7988a9eef793b34ccbb50e5b4079de1c/240x180c.jpg?t=1&srotate=1",
          "unit": "元/月",
          "coll": "0",
          "subwayDistance": "距广佛线-沙涌站步行1.9km",
          "fangwuLiangDian": [
            {
              "text": "整租",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "朝南",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "近地铁",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "有电梯",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "精装修",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            }
          ],
          "area": "69.4㎡",
          "toward": "朝南",
          "shangquan": "坑口",
          "isAnxuan": "0",
          "location": false,
          "source_type": "1",
          "isauction": "1",
          "auctionShowText": "",
          "legoAuction": "%7B%22isauction%22%3A%221%22%2C%22lego%22%3A%7B%22lego_tid%22%3A%22%22%2C%22lego_ad_click_url%22%3A%22%22%2C%22lego_sid%22%3A%22130071184213052992732541465%22%2C%22platformCompany%22%3A%22ajk%22%2C%22lego_show_track_url_base%22%3A%22https%3A%5C%2F%5C%2Fadtrack.58.com%5C%2F1001692%5C%2F%22%2C%22lego_version%22%3A%221.0.1%22%2C%22legoHuiDu%22%3Atrue%7D%7D",
          "tid": "",
          "isLiving": false,
          "isVR": false,
          "distanceDesc": "",
          "isGoldOwner": false
        },
        {
          "infoid": "2053235900194820",
          "cateName": "整租",
          "cateID": "11",
          "title": "一号线坑口站 低层两房 家私家电齐全 拎包入住",
          "price": "2500",
          "img": "https://pic1.ajkimg.com/display/anjuke/5ab0327ea2974c7eafb168c6c3620b75/240x180c.jpg?t=1&srotate=1",
          "unit": "元/月",
          "coll": "0",
          "subwayDistance": "距广佛线-沙涌站步行1.7km",
          "fangwuLiangDian": [
            {
              "text": "整租",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "朝南",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "近地铁",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "精装修",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            }
          ],
          "area": "56㎡",
          "toward": "朝南",
          "shangquan": "坑口",
          "isAnxuan": "0",
          "location": false,
          "source_type": "1",
          "isauction": "1",
          "auctionShowText": "",
          "legoAuction": "%7B%22isauction%22%3A%221%22%2C%22lego%22%3A%7B%22lego_tid%22%3A%22%22%2C%22lego_ad_click_url%22%3A%22%22%2C%22lego_sid%22%3A%22130071184213052992732541465%22%2C%22platformCompany%22%3A%22ajk%22%2C%22lego_show_track_url_base%22%3A%22https%3A%5C%2F%5C%2Fadtrack.58.com%5C%2F1001692%5C%2F%22%2C%22lego_version%22%3A%221.0.1%22%2C%22legoHuiDu%22%3Atrue%7D%7D",
          "tid": "",
          "isLiving": false,
          "isVR": false,
          "distanceDesc": "",
          "isGoldOwner": false
        },
        {
          "infoid": "2056040232952846",
          "cateName": "整租",
          "cateID": "11",
          "title": "推荐芳村整租78平东漖新村享受生活的快感！！！",
          "price": "3500",
          "img": "https://pic1.ajkimg.com/display/anjuke/a347aa21012732d052f87d195ac88922/240x180c.jpg?t=1&srotate=1",
          "unit": "元/月",
          "coll": "0",
          "subwayDistance": "距广佛线-沙涌站步行1.9km",
          "fangwuLiangDian": [
            {
              "text": "整租",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "朝南",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "近地铁",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "有电梯",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "精装修",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            }
          ],
          "area": "78㎡",
          "toward": "朝南",
          "shangquan": "坑口",
          "isAnxuan": "0",
          "location": false,
          "source_type": "1",
          "isauction": "1",
          "auctionShowText": "",
          "legoAuction": "%7B%22isauction%22%3A%221%22%2C%22lego%22%3A%7B%22lego_tid%22%3A%22%22%2C%22lego_ad_click_url%22%3A%22%22%2C%22lego_sid%22%3A%22130071184213052992732541465%22%2C%22platformCompany%22%3A%22ajk%22%2C%22lego_show_track_url_base%22%3A%22https%3A%5C%2F%5C%2Fadtrack.58.com%5C%2F1001692%5C%2F%22%2C%22lego_version%22%3A%221.0.1%22%2C%22legoHuiDu%22%3Atrue%7D%7D",
          "tid": "",
          "isLiving": false,
          "isVR": false,
          "distanceDesc": "",
          "isGoldOwner": false
        },
        {
          "infoid": "1966406618669069",
          "cateName": "整租",
          "cateID": "11",
          "title": "全新豪华特价电梯一房一厅780元，拎包入住，交通便利随时看房",
          "price": "780",
          "img": "https://pic1.ajkimg.com/display/anjuke/b61503212b96a68e003996a230ae0a1c/240x180c.jpg?t=1&srotate=1",
          "unit": "元/月",
          "coll": "0",
          "subwayDistance": "距广佛线-沙涌站步行206m",
          "fangwuLiangDian": [
            {
              "text": "整租",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "南北",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "近地铁",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "有电梯",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "精装修",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            }
          ],
          "area": "25㎡",
          "toward": "南北",
          "shangquan": "芳村",
          "isAnxuan": "0",
          "location": false,
          "source_type": "1",
          "isauction": "1",
          "auctionShowText": "",
          "legoAuction": "%7B%22isauction%22%3A%221%22%2C%22lego%22%3A%7B%22lego_tid%22%3A%22%22%2C%22lego_ad_click_url%22%3A%22%22%2C%22lego_sid%22%3A%22130071184213052992732541465%22%2C%22platformCompany%22%3A%22ajk%22%2C%22lego_show_track_url_base%22%3A%22https%3A%5C%2F%5C%2Fadtrack.58.com%5C%2F1001692%5C%2F%22%2C%22lego_version%22%3A%221.0.1%22%2C%22legoHuiDu%22%3Atrue%7D%7D",
          "tid": "",
          "isLiving": false,
          "isVR": false,
          "distanceDesc": "",
          "isGoldOwner": false
        },
        {
          "infoid": "1968090174332937",
          "cateName": "整租",
          "cateID": "11",
          "title": "广佛线 鹤洞地铁附近 精装一房一厅 家私家电齐全 699起",
          "price": "699",
          "img": "https://pic1.ajkimg.com/display/anjuke/34dc09f5e9b420abd913b0c0d20105e6/240x180c.jpg?t=1&srotate=1",
          "unit": "元/月",
          "coll": "0",
          "subwayDistance": "距广佛线-沙涌站步行329m",
          "fangwuLiangDian": [
            {
              "text": "整租",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "东南",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "近地铁",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "精装修",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            }
          ],
          "area": "35㎡",
          "toward": "东南",
          "shangquan": "芳村",
          "isAnxuan": "0",
          "location": false,
          "source_type": "1",
          "isauction": "1",
          "auctionShowText": "",
          "legoAuction": "%7B%22isauction%22%3A%221%22%2C%22lego%22%3A%7B%22lego_tid%22%3A%22%22%2C%22lego_ad_click_url%22%3A%22%22%2C%22lego_sid%22%3A%22130071184213052992732541465%22%2C%22platformCompany%22%3A%22ajk%22%2C%22lego_show_track_url_base%22%3A%22https%3A%5C%2F%5C%2Fadtrack.58.com%5C%2F1001692%5C%2F%22%2C%22lego_version%22%3A%221.0.1%22%2C%22legoHuiDu%22%3Atrue%7D%7D",
          "tid": "",
          "isLiving": false,
          "isVR": false,
          "distanceDesc": "",
          "isGoldOwner": false
        },
        {
          "infoid": "2055702248661002",
          "cateName": "整租",
          "cateID": "11",
          "title": "盛大蓝庭，带装修2房2厅眼见为实实地拍摄的现场图片",
          "price": "3900",
          "img": "https://pic1.ajkimg.com/display/anjuke/f9a53b10c79e09a3512a4ad251475884/240x180c.jpg?t=1&srotate=1",
          "unit": "元/月",
          "coll": "0",
          "subwayDistance": "距广佛线-沙涌站步行2.0km",
          "fangwuLiangDian": [
            {
              "text": "安选房源",
              "textColor": "#3EC77B",
              "bgColor": "#3EC77B"
            },
            {
              "text": "整租",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "西南",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "近地铁",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "有电梯",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "精装修",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            }
          ],
          "area": "82㎡",
          "toward": "西南",
          "shangquan": "坑口",
          "isAnxuan": "1",
          "location": false,
          "source_type": "1",
          "isauction": "1",
          "auctionShowText": "",
          "legoAuction": "%7B%22isauction%22%3A%221%22%2C%22lego%22%3A%7B%22lego_tid%22%3A%22%22%2C%22lego_ad_click_url%22%3A%22%22%2C%22lego_sid%22%3A%22130071184213052992732541465%22%2C%22platformCompany%22%3A%22ajk%22%2C%22lego_show_track_url_base%22%3A%22https%3A%5C%2F%5C%2Fadtrack.58.com%5C%2F1001692%5C%2F%22%2C%22lego_version%22%3A%221.0.1%22%2C%22legoHuiDu%22%3Atrue%7D%7D",
          "tid": "",
          "isLiving": false,
          "isVR": false,
          "distanceDesc": "",
          "isGoldOwner": false
        },
        {
          "infoid": "2023040943561741",
          "cateName": "整租",
          "cateID": "11",
          "title": "力荐租房！南北通透！一线江景yyds！沙园地铁口，出行便利！",
          "price": "4500",
          "img": "https://pic1.ajkimg.com/display/anjuke/3e9082455abf36a274bde10320a29a7e/240x180c.jpg?t=1&srotate=1",
          "unit": "元/月",
          "coll": "0",
          "subwayDistance": "距广佛线-沙涌站步行1.7km",
          "fangwuLiangDian": [
            {
              "text": "整租",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "西北",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "近地铁",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "有电梯",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "精装修",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            }
          ],
          "area": "78㎡",
          "toward": "西北",
          "shangquan": "工业大道北",
          "isAnxuan": "0",
          "location": false,
          "source_type": "1",
          "isauction": "1",
          "auctionShowText": "",
          "legoAuction": "%7B%22isauction%22%3A%221%22%2C%22lego%22%3A%7B%22lego_tid%22%3A%22%22%2C%22lego_ad_click_url%22%3A%22%22%2C%22lego_sid%22%3A%22130071184213052992732541465%22%2C%22platformCompany%22%3A%22ajk%22%2C%22lego_show_track_url_base%22%3A%22https%3A%5C%2F%5C%2Fadtrack.58.com%5C%2F1001692%5C%2F%22%2C%22lego_version%22%3A%221.0.1%22%2C%22legoHuiDu%22%3Atrue%7D%7D",
          "tid": "",
          "isLiving": false,
          "isVR": false,
          "distanceDesc": "",
          "isGoldOwner": false
        },
        {
          "infoid": "2054237511721985",
          "cateName": "整租",
          "cateID": "11",
          "title": "白鹤洞路小区 紧邻地铁，清新装修，宜家配置，居住舒适交通便利",
          "price": "600",
          "img": "https://pic1.ajkimg.com/display/anjuke/b7876dd1dd3bcc88ed3d2805b3110e4e/240x180c.jpg?t=1&srotate=1",
          "unit": "元/月",
          "coll": "0",
          "subwayDistance": "距广佛线-沙涌站步行1.8km",
          "fangwuLiangDian": [
            {
              "text": "整租",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "朝南",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "近地铁",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "精装修",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            }
          ],
          "area": "23㎡",
          "toward": "朝南",
          "shangquan": "广钢新城",
          "isAnxuan": "0",
          "location": false,
          "source_type": "1",
          "isauction": "1",
          "auctionShowText": "",
          "legoAuction": "%7B%22isauction%22%3A%221%22%2C%22lego%22%3A%7B%22lego_tid%22%3A%22%22%2C%22lego_ad_click_url%22%3A%22%22%2C%22lego_sid%22%3A%22130071184213052992732541465%22%2C%22platformCompany%22%3A%22ajk%22%2C%22lego_show_track_url_base%22%3A%22https%3A%5C%2F%5C%2Fadtrack.58.com%5C%2F1001692%5C%2F%22%2C%22lego_version%22%3A%221.0.1%22%2C%22legoHuiDu%22%3Atrue%7D%7D",
          "tid": "",
          "isLiving": false,
          "isVR": false,
          "distanceDesc": "",
          "isGoldOwner": false
        },
        {
          "infoid": "2007622017556488",
          "cateName": "整租",
          "cateID": "11",
          "title": "沙涌芳村，舒适一房一厅1099，随时入住，不走巷子",
          "price": "1099",
          "img": "https://pic1.ajkimg.com/display/anjuke/573cdc21b851ba42c572bc04e5a13fc8/240x180c.jpg?t=1&srotate=1",
          "unit": "元/月",
          "coll": "0",
          "subwayDistance": "距广佛线-沙涌站步行679m",
          "fangwuLiangDian": [
            {
              "text": "整租",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "东南",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "近地铁",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "精装修",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            }
          ],
          "area": "30㎡",
          "toward": "东南",
          "shangquan": "芳村",
          "isAnxuan": "0",
          "location": false,
          "source_type": "1",
          "isauction": "1",
          "auctionShowText": "",
          "legoAuction": "%7B%22isauction%22%3A%221%22%2C%22lego%22%3A%7B%22lego_tid%22%3A%22%22%2C%22lego_ad_click_url%22%3A%22%22%2C%22lego_sid%22%3A%22130071184213052992732541465%22%2C%22platformCompany%22%3A%22ajk%22%2C%22lego_show_track_url_base%22%3A%22https%3A%5C%2F%5C%2Fadtrack.58.com%5C%2F1001692%5C%2F%22%2C%22lego_version%22%3A%221.0.1%22%2C%22legoHuiDu%22%3Atrue%7D%7D",
          "tid": "",
          "isLiving": false,
          "isVR": false,
          "distanceDesc": "",
          "isGoldOwner": false
        },
        {
          "infoid": "1974945682317313",
          "cateName": "整租",
          "cateID": "11",
          "title": "鹤洞地铁站旁，小区管理，环境优美，家私电器齐全，拎包入住",
          "price": "1300",
          "img": "https://pic1.ajkimg.com/display/anjuke/a7a221e9f37218f7a8deb310cb0e6e42/240x180c.jpg?t=1&srotate=1",
          "unit": "元/月",
          "coll": "0",
          "subwayDistance": "距广佛线-沙涌站步行1.6km",
          "fangwuLiangDian": [
            {
              "text": "整租",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "朝南",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "近地铁",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "精装修",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            }
          ],
          "area": "30㎡",
          "toward": "朝南",
          "shangquan": "芳村",
          "isAnxuan": "0",
          "location": false,
          "source_type": "1",
          "isauction": "1",
          "auctionShowText": "",
          "legoAuction": "%7B%22isauction%22%3A%221%22%2C%22lego%22%3A%7B%22lego_tid%22%3A%22%22%2C%22lego_ad_click_url%22%3A%22%22%2C%22lego_sid%22%3A%22130071184213052992732541465%22%2C%22platformCompany%22%3A%22ajk%22%2C%22lego_show_track_url_base%22%3A%22https%3A%5C%2F%5C%2Fadtrack.58.com%5C%2F1001692%5C%2F%22%2C%22lego_version%22%3A%221.0.1%22%2C%22legoHuiDu%22%3Atrue%7D%7D",
          "tid": "",
          "isLiving": false,
          "isVR": false,
          "distanceDesc": "",
          "isGoldOwner": false
        },
        {
          "infoid": "2059834469377027",
          "cateName": "整租",
          "cateID": "11",
          "title": "乐怡居 精装2房 家电齐全 房子干净整洁 生活区交通一步到位",
          "price": "2900",
          "img": "https://pic1.ajkimg.com/display/anjuke/4b0ed0d730eb3a0c51e2d6215d4f99b6/240x180c.jpg?t=1&srotate=1",
          "unit": "元/月",
          "coll": "0",
          "subwayDistance": "距广佛线-沙涌站步行1.9km",
          "fangwuLiangDian": [
            {
              "text": "整租",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "南北",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "近地铁",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "精装修",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            }
          ],
          "area": "71㎡",
          "toward": "南北",
          "shangquan": "芳村",
          "isAnxuan": "0",
          "location": false,
          "source_type": "1",
          "isauction": "1",
          "auctionShowText": "",
          "legoAuction": "%7B%22isauction%22%3A%221%22%2C%22lego%22%3A%7B%22lego_tid%22%3A%22%22%2C%22lego_ad_click_url%22%3A%22%22%2C%22lego_sid%22%3A%22130071184213052992732541465%22%2C%22platformCompany%22%3A%22ajk%22%2C%22lego_show_track_url_base%22%3A%22https%3A%5C%2F%5C%2Fadtrack.58.com%5C%2F1001692%5C%2F%22%2C%22lego_version%22%3A%221.0.1%22%2C%22legoHuiDu%22%3Atrue%7D%7D",
          "tid": "",
          "isLiving": false,
          "isVR": false,
          "distanceDesc": "",
          "isGoldOwner": false
        },
        {
          "infoid": "2014508255667211",
          "cateName": "整租",
          "cateID": "11",
          "title": "沙涌新村 1室0厅1卫 331元月 30平 南北通透",
          "price": "331",
          "img": "https://pic1.ajkimg.com/display/anjuke/3dd0486452e75901309db02647d5367a/240x180c.jpg?t=1&srotate=1",
          "unit": "元/月",
          "coll": "0",
          "subwayDistance": "距广佛线-沙涌站步行370m",
          "fangwuLiangDian": [
            {
              "text": "整租",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "朝南",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "近地铁",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "有电梯",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "精装修",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            }
          ],
          "area": "30㎡",
          "toward": "朝南",
          "shangquan": "芳村",
          "isAnxuan": "0",
          "location": false,
          "source_type": "1",
          "isauction": "1",
          "auctionShowText": "",
          "legoAuction": "%7B%22isauction%22%3A%221%22%2C%22lego%22%3A%7B%22lego_tid%22%3A%22%22%2C%22lego_ad_click_url%22%3A%22%22%2C%22lego_sid%22%3A%22130071184213052992732541465%22%2C%22platformCompany%22%3A%22ajk%22%2C%22lego_show_track_url_base%22%3A%22https%3A%5C%2F%5C%2Fadtrack.58.com%5C%2F1001692%5C%2F%22%2C%22lego_version%22%3A%221.0.1%22%2C%22legoHuiDu%22%3Atrue%7D%7D",
          "tid": "",
          "isLiving": false,
          "isVR": false,
          "distanceDesc": "",
          "isGoldOwner": false
        },
        {
          "infoid": "1751479484259336",
          "cateName": "整租",
          "cateID": "11",
          "title": "首月免租 两房一厅 近地铁市场 独立厨卫 带阳台",
          "price": "1600",
          "img": "https://pic1.ajkimg.com/display/anjuke/bb219814fae36b3118b0e87e2aace1c0/240x180c.jpg?t=1&srotate=1",
          "unit": "元/月",
          "coll": "0",
          "subwayDistance": "距广佛线-沙涌站步行2.0km",
          "fangwuLiangDian": [
            {
              "text": "整租",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "南北",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "近地铁",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "有电梯",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "精装修",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            }
          ],
          "area": "59㎡",
          "toward": "南北",
          "shangquan": "沙面",
          "isAnxuan": "0",
          "location": false,
          "source_type": "1",
          "isauction": "1",
          "auctionShowText": "",
          "legoAuction": "%7B%22isauction%22%3A%221%22%2C%22lego%22%3A%7B%22lego_tid%22%3A%22%22%2C%22lego_ad_click_url%22%3A%22%22%2C%22lego_sid%22%3A%22130071184213052992732541465%22%2C%22platformCompany%22%3A%22ajk%22%2C%22lego_show_track_url_base%22%3A%22https%3A%5C%2F%5C%2Fadtrack.58.com%5C%2F1001692%5C%2F%22%2C%22lego_version%22%3A%221.0.1%22%2C%22legoHuiDu%22%3Atrue%7D%7D",
          "tid": "",
          "isLiving": false,
          "isVR": false,
          "distanceDesc": "",
          "isGoldOwner": false
        },
        {
          "infoid": "2051743133294603",
          "cateName": "整租",
          "cateID": "11",
          "title": "地铁附近 交通方便 房间舒适 购物便利 网络覆盖",
          "price": "551",
          "img": "https://pic1.ajkimg.com/display/anjuke/5ed93ef138b07b40bf147db62bb5430a/240x180c.jpg?t=1&srotate=1",
          "unit": "元/月",
          "coll": "0",
          "subwayDistance": "距广佛线-沙涌站步行1.9km",
          "fangwuLiangDian": [
            {
              "text": "整租",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "东南",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "近地铁",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "精装修",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            }
          ],
          "area": "30㎡",
          "toward": "东南",
          "shangquan": "芳村",
          "isAnxuan": "0",
          "location": false,
          "source_type": "1",
          "isauction": "1",
          "auctionShowText": "",
          "legoAuction": "%7B%22isauction%22%3A%221%22%2C%22lego%22%3A%7B%22lego_tid%22%3A%22%22%2C%22lego_ad_click_url%22%3A%22%22%2C%22lego_sid%22%3A%22130071184213052992732541465%22%2C%22platformCompany%22%3A%22ajk%22%2C%22lego_show_track_url_base%22%3A%22https%3A%5C%2F%5C%2Fadtrack.58.com%5C%2F1001692%5C%2F%22%2C%22lego_version%22%3A%221.0.1%22%2C%22legoHuiDu%22%3Atrue%7D%7D",
          "tid": "",
          "isLiving": false,
          "isVR": false,
          "distanceDesc": "",
          "isGoldOwner": false
        },
        {
          "infoid": "2060583712660491",
          "cateName": "整租",
          "cateID": "11",
          "title": "康乃馨小区 精装两房 出租 拎包入住 周围配套成熟采光好",
          "price": "2600",
          "img": "https://pic1.ajkimg.com/display/anjuke/2073db6f9149a32883f822b2c8ae403a/240x180c.jpg?t=1&srotate=1",
          "unit": "元/月",
          "coll": "0",
          "subwayDistance": "距广佛线-沙涌站步行1.7km",
          "fangwuLiangDian": [
            {
              "text": "整租",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "南北",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "近地铁",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "有电梯",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "豪华装修",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            }
          ],
          "area": "70㎡",
          "toward": "南北",
          "shangquan": "坑口",
          "isAnxuan": "0",
          "location": false,
          "source_type": "1",
          "isauction": "1",
          "auctionShowText": "",
          "legoAuction": "%7B%22isauction%22%3A%221%22%2C%22lego%22%3A%7B%22lego_tid%22%3A%22%22%2C%22lego_ad_click_url%22%3A%22%22%2C%22lego_sid%22%3A%22130071184213052992732541465%22%2C%22platformCompany%22%3A%22ajk%22%2C%22lego_show_track_url_base%22%3A%22https%3A%5C%2F%5C%2Fadtrack.58.com%5C%2F1001692%5C%2F%22%2C%22lego_version%22%3A%221.0.1%22%2C%22legoHuiDu%22%3Atrue%7D%7D",
          "tid": "",
          "isLiving": false,
          "isVR": false,
          "distanceDesc": "",
          "isGoldOwner": false
        },
        {
          "infoid": "2064336787332099",
          "cateName": "整租",
          "cateID": "11",
          "title": "沙涌地铁站 电梯 小区水电 图片真实",
          "price": "1800",
          "img": "https://pic1.ajkimg.com/display/anjuke/380e35c6cb33a69757b1eb39a92907cd/240x180c.jpg?t=1&srotate=1",
          "unit": "元/月",
          "coll": "0",
          "subwayDistance": "距广佛线-沙涌站步行370m",
          "fangwuLiangDian": [
            {
              "text": "整租",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "朝南",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "近地铁",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "新上",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "有电梯",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "精装修",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            }
          ],
          "area": "35㎡",
          "toward": "朝南",
          "shangquan": "芳村",
          "isAnxuan": "0",
          "location": false,
          "source_type": "1",
          "isauction": "1",
          "auctionShowText": "",
          "legoAuction": "%7B%22isauction%22%3A%221%22%2C%22lego%22%3A%7B%22lego_tid%22%3A%22%22%2C%22lego_ad_click_url%22%3A%22%22%2C%22lego_sid%22%3A%22130071184213052992732541465%22%2C%22platformCompany%22%3A%22ajk%22%2C%22lego_show_track_url_base%22%3A%22https%3A%5C%2F%5C%2Fadtrack.58.com%5C%2F1001692%5C%2F%22%2C%22lego_version%22%3A%221.0.1%22%2C%22legoHuiDu%22%3Atrue%7D%7D",
          "tid": "",
          "isLiving": false,
          "isVR": false,
          "distanceDesc": "",
          "isGoldOwner": false
        },
        {
          "infoid": "1877226680197126",
          "cateName": "整租",
          "cateID": "11",
          "title": "新年特惠，1室0厅1卫680元，手快有手慢无，随时看房",
          "price": "680",
          "img": "https://pic1.ajkimg.com/display/anjuke/20dd5481a5a4d80e5308a8d48d37c899/240x180c.jpg?t=1&srotate=1",
          "unit": "元/月",
          "coll": "0",
          "subwayDistance": "距广佛线-沙涌站步行2.0km",
          "fangwuLiangDian": [
            {
              "text": "整租",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "南北",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "近地铁",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "有电梯",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "精装修",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            }
          ],
          "area": "16㎡",
          "toward": "南北",
          "shangquan": "沙面",
          "isAnxuan": "0",
          "location": false,
          "source_type": "1",
          "isauction": "1",
          "auctionShowText": "",
          "legoAuction": "%7B%22isauction%22%3A%221%22%2C%22lego%22%3A%7B%22lego_tid%22%3A%22%22%2C%22lego_ad_click_url%22%3A%22%22%2C%22lego_sid%22%3A%22130071184213052992732541465%22%2C%22platformCompany%22%3A%22ajk%22%2C%22lego_show_track_url_base%22%3A%22https%3A%5C%2F%5C%2Fadtrack.58.com%5C%2F1001692%5C%2F%22%2C%22lego_version%22%3A%221.0.1%22%2C%22legoHuiDu%22%3Atrue%7D%7D",
          "tid": "",
          "isLiving": false,
          "isVR": false,
          "distanceDesc": "",
          "isGoldOwner": false
        },
        {
          "infoid": "2007526934111233",
          "cateName": "整租",
          "cateID": "11",
          "title": "坑口地铁口(月付可短租)无中介(家电全)",
          "price": "500",
          "img": "https://pic1.ajkimg.com/display/anjuke/d449ac8dd02efb0aead716c25a3f9302/240x180c.jpg?t=1&srotate=1",
          "unit": "元/月",
          "coll": "0",
          "subwayDistance": "距广佛线-沙涌站步行2.0km",
          "fangwuLiangDian": [
            {
              "text": "整租",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "朝南",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "近地铁",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "有电梯",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            }
          ],
          "area": "20㎡",
          "toward": "朝南",
          "shangquan": "坑口",
          "isAnxuan": "0",
          "location": false,
          "source_type": "1",
          "isauction": "1",
          "auctionShowText": "",
          "legoAuction": "%7B%22isauction%22%3A%221%22%2C%22lego%22%3A%7B%22lego_tid%22%3A%22%22%2C%22lego_ad_click_url%22%3A%22%22%2C%22lego_sid%22%3A%22130071184213052992732541465%22%2C%22platformCompany%22%3A%22ajk%22%2C%22lego_show_track_url_base%22%3A%22https%3A%5C%2F%5C%2Fadtrack.58.com%5C%2F1001692%5C%2F%22%2C%22lego_version%22%3A%221.0.1%22%2C%22legoHuiDu%22%3Atrue%7D%7D",
          "tid": "",
          "isLiving": false,
          "isVR": false,
          "distanceDesc": "",
          "isGoldOwner": false
        },
        {
          "infoid": "2065492633428998",
          "cateName": "整租",
          "cateID": "11",
          "title": "盛大蓝庭 精装一房一厅 中层电梯房 采光好 家私家电齐全",
          "price": "780",
          "img": "https://pic1.ajkimg.com/display/anjuke/0330304fcb8640a2f8e10ba82ac9537f/240x180c.jpg?t=1&srotate=1",
          "unit": "元/月",
          "coll": "0",
          "subwayDistance": "距广佛线-沙涌站步行2.0km",
          "fangwuLiangDian": [
            {
              "text": "整租",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "南北",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "近地铁",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "新上",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "有电梯",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "精装修",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            }
          ],
          "area": "40㎡",
          "toward": "南北",
          "shangquan": "坑口",
          "isAnxuan": "0",
          "location": false,
          "source_type": "1",
          "isauction": "1",
          "auctionShowText": "",
          "legoAuction": "%7B%22isauction%22%3A%221%22%2C%22lego%22%3A%7B%22lego_tid%22%3A%22%22%2C%22lego_ad_click_url%22%3A%22%22%2C%22lego_sid%22%3A%22130071184213052992732541465%22%2C%22platformCompany%22%3A%22ajk%22%2C%22lego_show_track_url_base%22%3A%22https%3A%5C%2F%5C%2Fadtrack.58.com%5C%2F1001692%5C%2F%22%2C%22lego_version%22%3A%221.0.1%22%2C%22legoHuiDu%22%3Atrue%7D%7D",
          "tid": "",
          "isLiving": false,
          "isVR": false,
          "distanceDesc": "",
          "isGoldOwner": false
        },
        {
          "infoid": "2059847086251013",
          "cateName": "整租",
          "cateID": "11",
          "title": "40平方一房一厅租1000元",
          "price": "1000",
          "img": "https://pic1.ajkimg.com/display/anjuke/34920fed86ec936c6150fb99fc0bb22b/240x180c.jpg?t=1&srotate=1",
          "unit": "元/月",
          "coll": "0",
          "subwayDistance": "距广佛线-沙涌站步行514m",
          "fangwuLiangDian": [
            {
              "text": "整租",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "朝东",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "近地铁",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            }
          ],
          "area": "40㎡",
          "toward": "朝东",
          "shangquan": "芳村",
          "isAnxuan": "0",
          "location": false,
          "source_type": "1",
          "isauction": "1",
          "auctionShowText": "",
          "legoAuction": "%7B%22isauction%22%3A%221%22%2C%22lego%22%3A%7B%22lego_tid%22%3A%22%22%2C%22lego_ad_click_url%22%3A%22%22%2C%22lego_sid%22%3A%22130071184213052992732541465%22%2C%22platformCompany%22%3A%22ajk%22%2C%22lego_show_track_url_base%22%3A%22https%3A%5C%2F%5C%2Fadtrack.58.com%5C%2F1001692%5C%2F%22%2C%22lego_version%22%3A%221.0.1%22%2C%22legoHuiDu%22%3Atrue%7D%7D",
          "tid": "",
          "isLiving": false,
          "isVR": false,
          "distanceDesc": "",
          "isGoldOwner": false
        },
        {
          "infoid": "2061578203227143",
          "cateName": "整租",
          "cateID": "11",
          "title": "光大花园 电梯高层 望小区里 家电全新 此房抢手",
          "price": "4600",
          "img": "https://pic1.ajkimg.com/display/anjuke/1d951b0f9c2e58467908a7bc527cc961/240x180c.jpg?t=1&srotate=1",
          "unit": "元/月",
          "coll": "0",
          "subwayDistance": "距广佛线-沙涌站步行1.7km",
          "fangwuLiangDian": [
            {
              "text": "整租",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "东南",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "近地铁",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "有电梯",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "精装修",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            }
          ],
          "area": "78㎡",
          "toward": "东南",
          "shangquan": "工业大道北",
          "isAnxuan": "0",
          "location": false,
          "source_type": "1",
          "isauction": "1",
          "auctionShowText": "",
          "legoAuction": "%7B%22isauction%22%3A%221%22%2C%22lego%22%3A%7B%22lego_tid%22%3A%22%22%2C%22lego_ad_click_url%22%3A%22%22%2C%22lego_sid%22%3A%22130071184213052992732541465%22%2C%22platformCompany%22%3A%22ajk%22%2C%22lego_show_track_url_base%22%3A%22https%3A%5C%2F%5C%2Fadtrack.58.com%5C%2F1001692%5C%2F%22%2C%22lego_version%22%3A%221.0.1%22%2C%22legoHuiDu%22%3Atrue%7D%7D",
          "tid": "",
          "isLiving": false,
          "isVR": false,
          "distanceDesc": "",
          "isGoldOwner": false
        },
        {
          "infoid": "2017948285280263",
          "cateName": "整租",
          "cateID": "11",
          "title": "乐怡居 电梯22楼 盗版视频 花地湾站  业主好说话",
          "price": "3400",
          "img": "https://pic1.ajkimg.com/display/anjuke/35666f1d66c513600ab5705221937b5e/240x180c.jpg?t=1&srotate=1",
          "unit": "元/月",
          "coll": "0",
          "subwayDistance": "距广佛线-沙涌站步行1.9km",
          "fangwuLiangDian": [
            {
              "text": "整租",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "朝南",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "近地铁",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "有电梯",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "精装修",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            }
          ],
          "area": "81㎡",
          "toward": "朝南",
          "shangquan": "芳村",
          "isAnxuan": "0",
          "location": false,
          "source_type": "1",
          "isauction": "1",
          "auctionShowText": "",
          "legoAuction": "%7B%22isauction%22%3A%221%22%2C%22lego%22%3A%7B%22lego_tid%22%3A%22%22%2C%22lego_ad_click_url%22%3A%22%22%2C%22lego_sid%22%3A%22130071184213052992732541465%22%2C%22platformCompany%22%3A%22ajk%22%2C%22lego_show_track_url_base%22%3A%22https%3A%5C%2F%5C%2Fadtrack.58.com%5C%2F1001692%5C%2F%22%2C%22lego_version%22%3A%221.0.1%22%2C%22legoHuiDu%22%3Atrue%7D%7D",
          "tid": "",
          "isLiving": false,
          "isVR": false,
          "distanceDesc": "",
          "isGoldOwner": false
        },
        {
          "infoid": "2065542976791558",
          "cateName": "整租",
          "cateID": "11",
          "title": "特价一房一厅，独立厨房卫生间，大阳台，购物方便",
          "price": "750",
          "img": "https://pic1.ajkimg.com/display/anjuke/2ce76c6cb24615e0fb081e47cec69a27/240x180c.jpg?t=1&srotate=1",
          "unit": "元/月",
          "coll": "0",
          "subwayDistance": "距广佛线-沙涌站步行2.0km",
          "fangwuLiangDian": [
            {
              "text": "整租",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "朝南",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "近地铁",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "新上",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            }
          ],
          "area": "31㎡",
          "toward": "朝南",
          "shangquan": "花地湾",
          "isAnxuan": "0",
          "location": false,
          "source_type": "1",
          "isauction": "1",
          "auctionShowText": "",
          "legoAuction": "%7B%22isauction%22%3A%221%22%2C%22lego%22%3A%7B%22lego_tid%22%3A%22%22%2C%22lego_ad_click_url%22%3A%22%22%2C%22lego_sid%22%3A%22130071184213052992732541465%22%2C%22platformCompany%22%3A%22ajk%22%2C%22lego_show_track_url_base%22%3A%22https%3A%5C%2F%5C%2Fadtrack.58.com%5C%2F1001692%5C%2F%22%2C%22lego_version%22%3A%221.0.1%22%2C%22legoHuiDu%22%3Atrue%7D%7D",
          "tid": "",
          "isLiving": false,
          "isVR": false,
          "distanceDesc": "",
          "isGoldOwner": false
        },
        {
          "infoid": "2065523285058571",
          "cateName": "整租",
          "cateID": "11",
          "title": "盛大蓝庭 精装两房 中层电梯房 采光好 家私家电齐全",
          "price": "3600",
          "img": "https://pic1.ajkimg.com/display/anjuke/7c225a7a4ab56a2df98d427ef50935f9/240x180c.jpg?t=1&srotate=1",
          "unit": "元/月",
          "coll": "0",
          "subwayDistance": "距广佛线-沙涌站步行2.0km",
          "fangwuLiangDian": [
            {
              "text": "整租",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "朝南",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "近地铁",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "新上",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "有电梯",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "豪华装修",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            }
          ],
          "area": "84㎡",
          "toward": "朝南",
          "shangquan": "坑口",
          "isAnxuan": "0",
          "location": false,
          "source_type": "1",
          "isauction": "1",
          "auctionShowText": "",
          "legoAuction": "%7B%22isauction%22%3A%221%22%2C%22lego%22%3A%7B%22lego_tid%22%3A%22%22%2C%22lego_ad_click_url%22%3A%22%22%2C%22lego_sid%22%3A%22130071184213052992732541465%22%2C%22platformCompany%22%3A%22ajk%22%2C%22lego_show_track_url_base%22%3A%22https%3A%5C%2F%5C%2Fadtrack.58.com%5C%2F1001692%5C%2F%22%2C%22lego_version%22%3A%221.0.1%22%2C%22legoHuiDu%22%3Atrue%7D%7D",
          "tid": "",
          "isLiving": false,
          "isVR": false,
          "distanceDesc": "",
          "isGoldOwner": false
        },
        {
          "infoid": "1733708325330946",
          "cateName": "整租",
          "cateID": "11",
          "title": "1号线精装两房一厅 带阳台 交通便利 1299起 中介勿扰",
          "price": "1980",
          "img": "https://pic1.ajkimg.com/display/anjuke/6682ae87c302c642258cdd422f792927/240x180c.jpg?t=1&srotate=1",
          "unit": "元/月",
          "coll": "0",
          "subwayDistance": "距广佛线-沙涌站步行1.8km",
          "fangwuLiangDian": [
            {
              "text": "整租",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "东南",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "近地铁",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            },
            {
              "text": "精装修",
              "textColor": "#7397C1",
              "bgColor": "#7397C1"
            }
          ],
          "area": "50㎡",
          "toward": "东南",
          "shangquan": "广钢新城",
          "isAnxuan": "0",
          "location": false,
          "source_type": "1",
          "isauction": "1",
          "auctionShowText": "",
          "legoAuction": "%7B%22isauction%22%3A%221%22%2C%22lego%22%3A%7B%22lego_tid%22%3A%22%22%2C%22lego_ad_click_url%22%3A%22%22%2C%22lego_sid%22%3A%22130071184213052992732541465%22%2C%22platformCompany%22%3A%22ajk%22%2C%22lego_show_track_url_base%22%3A%22https%3A%5C%2F%5C%2Fadtrack.58.com%5C%2F1001692%5C%2F%22%2C%22lego_version%22%3A%221.0.1%22%2C%22legoHuiDu%22%3Atrue%7D%7D",
          "tid": "",
          "isLiving": false,
          "isVR": false,
          "distanceDesc": "",
          "isGoldOwner": false
        }
      ]
    }
  }