var PAGE_DATA = {
  subtitle: '',
  types: [
    {
      title: '响应性基础 API',
      list: [
        {
          key: '* reactive()',
          caption: `返回对象(引用类型) 的响应式副本 <br/> 
          当将 ref 分配给 reactive property 时，ref 将被自动解包。`,
        },
        {
          key: '* readonly()',
          caption: '接受一个对象 (响应式或纯对象) 或 ref 并返回原始对象的只读代理。只读代理是深层的：任何被访问的嵌套 property 也是只读的。<br> 返回一个只读变量，且是深层只读',
        },
        {
          key: 'isProxy 、 isReactive',
          caption: '检查对象是否是由 reactive 或 readonly 创建的 proxy。<br> 检查对象是否是由 reactive 创建的响应式代理。',
        },
        {
          key: 'isReadonly()',
          caption: '检查对象是否是由 readonly 创建的只读代理。',
        },
        {
          key: 'toRaw',
          caption:
            '返回 reactive 或 readonly 代理的原始对象。<br> 这是一个“逃生舱”，可用于临时读取数据而无需承担代理访问/跟踪的开销，也可用于写入数据而避免触发更改。不建议保留对原始对象的持久引用。请谨慎使用。',
        },
        {
          key: 'markRaw',
          caption: `标记一个对象，使其永远不会转换为 proxy。返回对象本身。 <br> 
          有些值不应该是响应式的，例如复杂的第三方类实例或 Vue 组件对象。 <br> 
          当渲染具有不可变数据源的大列表时，跳过 proxy 转换可以提高性能。
          const foo = markRaw({})`,
        },
        {
          key: 'shallowReactive',
          caption: `创建一个响应式代理，它跟踪其自身 property 的响应性，但不执行嵌套对象的深层响应式转换 (暴露原始值)。 <br> 
          里面的引用类型不是响应式的
          const state = shallowReactive({ a: 123, cc: { name: '你好' } })`,
        },
        {
          key: 'shallowReadonly',
          caption: `创建一个 proxy，使其自身的 property 为只读，但不执行嵌套对象的深度只读转换 (暴露原始值)。 <br> const state = shallowReadonly({ a: 123, cc: { name: '你好' } })`,
        },
      ],
    },
    {
      title: 'Refs',
      list: [
        {
          key: '',
          caption: ``,
        },
        {
          key: 'ref',
          caption: `接受一个内部值并返回一个响应式且可变的 ref 对象。ref 对象具有指向内部值的单个 property .value。`,
        },
        {
          key: 'unref',
          caption: `如果参数是一个 ref，则返回内部值，否则返回参数本身。 <br> 这是 val = isRef(val) ? val.value : val 的语法糖函数。`,
        },
        {
          key: 'toRef',
          caption: `可以用来为源响应式对象上的某个 property 新创建一个 ref。 <br> 然后，ref 可以被传递，它会保持对其源 property 的响应式连接。
          const fooRef = toRef(obj, 'obj_key_str')`,
        },
        {
          key: 'toRefs',
          caption: `将响应式对象转换为普通对象，其中结果对象的每个 property 都是指向原始对象相应 property 的 ref。`,
        },
        {
          key: 'isRef',
          caption: `检查值是否为一个 ref 对象。`,
        },
        {
          key: 'shallowRef()',
          caption: `创建一个跟踪自身 .value 变化的 ref，但不会使其值也变成响应式的。`,
        },
        {
          key: 'triggerRef',
          caption: `手动执行与 shallowRef 关联的任何作用 (effect)。`,
        },
        {
          key: 'customRef((track, trigger) => { return { get(), set() } })',
          caption: `创建一个自定义的 ref，并对其依赖项跟踪和更新触发进行显式控制。它需要一个工厂函数，该函数接收 track 和 trigger 函数作为参数，并且应该返回一个带有 get 和 set 的对象。`,
        },
      ],
    },
    {
      title: 'computed 与 watch',
      list: [
        {
          key: '* computed',
          caption: `返回一个不可变的响应式 ref 对象。          计算属性，setup()里面的用法 <br>
          const plusOne = computed(() => count.value + 1) <br>

          const plusOne = computed({
            get: () => count.value + 1,
            set: val => {
              count.value = val - 1
            }
          })
          `,
        },
        {
          key: 'watchEffect, watchPostEffect, watchSyncEffect',
          caption: `立即执行传入的一个函数，同时响应式追踪其依赖，并在其依赖变更时重新运行该函数。<br>
          watchEffect(() => console.log(count.value))
          `,
        },
        {
          key: '* watch',
          caption: `watch 需要侦听特定的数据源，并在单独的回调函数中执行副作用。<br>
          它也是惰性的——即回调仅在侦听源发生变化时被调用。 <br>
          
          侦听单一源 <br>
          // 侦听一个 getter <br>
          const state = reactive({ count: 0 }) <br>
          watch( () => state.count, (count, prevCount) => { /* ... */ }) <br>
          // 直接侦听一个 ref <br>
          const count = ref(0) <br>
          watch(count, (count, prevCount) => { /* ... */ }) <br>

          侦听多个源 <br>
          watch([fooRef, barRef], ([foo, bar], [prevFoo, prevBar]) => { <br>
            /* ... */ <br>
          }) <br>
          `,
        },
        {
          key: '',
          caption: ``,
        },
      ],
    },
    {
      title: 'Effect 作用域 API   (3.2+)',
      list: [
        {
          key: '',
          caption: ``,
        },
        {
          key: '',
          caption: ``,
        },
        {
          key: '',
          caption: ``,
        },
        {
          key: '',
          caption: ``,
        },
      ],
    },
  ],
}
