const API_LIST = [
  {
    name: 'JavaScript',
    showDetail: true,
    ico: 'images/javascript.ico',
    child: ['Array', 'String', 'Object', 'Set', 'Map', 'Number', 'Math', 'promise'],
    urlFn: (key) => {
      return `page/apiDetail.html?type=${key}&pageType=JavaScript`
    }
  },
  {
    name: 'CSS',
    showDetail: false,
    ico: 'images/http.png',
    child: ['animation'],
    urlFn: 'page/css/css-animation.html',
  },
  {
    name: 'VUE',
    showDetail: false,
    ico: 'images/vue.png',
    child: ['响应性API'],
    urlFn: 'page/vue/vue3.html',
  },
  {
    name: 'HTTP',
    showDetail: false,
    ico: 'images/http.png',
    child: ['HTTP状态码'],
    urlFn: 'page/http/http-code.html',
  },
  {
    name: '运算符',
    showDetail: false,
    ico: 'images/运算符.png',
    child: ['运算符'],
    urlFn: 'page/运算符/index.html',
  },
  {
    name: 'Editor keymap',
    showDetail: false,
    ico: 'images/vscode.ico',
    child: ['vscode'],
    urlFn: 'page/编辑器/vscode.html',
  },
  {
    name: '语言对比',
    showDetail: false,
    ico: 'images/code_favicon.ico',
    child: ['java javascript'],
    urlFn: 'page/base/datatype.html',
  },
]

const docTypeArr = ['Array', 'String', 'Object', 'Set', 'Map', 'Number', 'Math', 'promise']

// export { docTypeArr }
